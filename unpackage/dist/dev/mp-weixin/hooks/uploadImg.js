"use strict";
const common_vendor = require("../common/vendor.js");
const service_request_config = require("../service/request/config.js");
require("../stores/user.js");
const uploadImgHook = (emitName, url, filePath = "") => {
  if (filePath) {
    common_vendor.index.showLoading({
      title: "上传中..."
    });
    common_vendor.index.uploadFile({
      url: service_request_config.config.base_url + url,
      filePath,
      name: "file",
      //类型为文件
      header: {
        "authorization": service_request_config.TOKEN_PRE + common_vendor.index.getStorageSync("token")
      },
      success: async function(uploadFileRes) {
        const { data, success } = JSON.parse(uploadFileRes.data);
        if (success) {
          common_vendor.index.hideLoading();
          common_vendor.index.showToast({
            title: "上传成功"
          });
          common_vendor.index.$emit(emitName, data);
        } else {
          common_vendor.index.hideLoading();
          common_vendor.index.showToast({
            title: data.msg || "上传失败",
            icon: "error"
          });
          common_vendor.index.$emit(emitName, false);
        }
      }
    });
  } else {
    common_vendor.index.chooseImage({
      count: 1,
      sizeType: ["original", "compressed"],
      success: (res) => {
        res.tempFiles[0];
        let filePath2 = res.tempFilePaths[0];
        common_vendor.index.showLoading({
          title: "上传中..."
        });
        common_vendor.index.uploadFile({
          url: service_request_config.config.base_url + url,
          filePath: filePath2,
          name: "file",
          //类型为文件
          header: {
            // "content-type" : 'multipart/form-data',
            "authorization": service_request_config.TOKEN_PRE + common_vendor.index.getStorageSync("token")
          },
          success: async function(uploadFileRes) {
            const { data, success } = JSON.parse(uploadFileRes.data);
            if (success) {
              common_vendor.index.hideLoading();
              common_vendor.index.showToast({
                title: "上传成功"
              });
              common_vendor.index.$emit(emitName, data);
            } else {
              common_vendor.index.hideLoading();
              common_vendor.index.showToast({
                title: data.msg || "上传失败",
                icon: "error"
              });
              common_vendor.index.$emit(emitName, false);
            }
          }
        });
      }
    });
  }
};
exports.uploadImgHook = uploadImgHook;
