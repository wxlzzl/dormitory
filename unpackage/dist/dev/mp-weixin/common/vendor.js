"use strict";
const _export_sfc = (sfc, props) => {
  const target = sfc.__vccOpts || sfc;
  for (const [key, val] of props) {
    target[key] = val;
  }
  return target;
};
function makeMap(str, expectsLowerCase) {
  const map2 = /* @__PURE__ */ Object.create(null);
  const list = str.split(",");
  for (let i = 0; i < list.length; i++) {
    map2[list[i]] = true;
  }
  return expectsLowerCase ? (val) => !!map2[val.toLowerCase()] : (val) => !!map2[val];
}
function normalizeStyle(value) {
  if (isArray$2(value)) {
    const res = {};
    for (let i = 0; i < value.length; i++) {
      const item = value[i];
      const normalized = isString$1(item) ? parseStringStyle(item) : normalizeStyle(item);
      if (normalized) {
        for (const key in normalized) {
          res[key] = normalized[key];
        }
      }
    }
    return res;
  } else if (isString$1(value)) {
    return value;
  } else if (isObject$2(value)) {
    return value;
  }
}
const listDelimiterRE = /;(?![^(]*\))/g;
const propertyDelimiterRE = /:([^]+)/;
const styleCommentRE = /\/\*.*?\*\//gs;
function parseStringStyle(cssText) {
  const ret = {};
  cssText.replace(styleCommentRE, "").split(listDelimiterRE).forEach((item) => {
    if (item) {
      const tmp = item.split(propertyDelimiterRE);
      tmp.length > 1 && (ret[tmp[0].trim()] = tmp[1].trim());
    }
  });
  return ret;
}
function normalizeClass(value) {
  let res = "";
  if (isString$1(value)) {
    res = value;
  } else if (isArray$2(value)) {
    for (let i = 0; i < value.length; i++) {
      const normalized = normalizeClass(value[i]);
      if (normalized) {
        res += normalized + " ";
      }
    }
  } else if (isObject$2(value)) {
    for (const name in value) {
      if (value[name]) {
        res += name + " ";
      }
    }
  }
  return res.trim();
}
const toDisplayString = (val) => {
  return isString$1(val) ? val : val == null ? "" : isArray$2(val) || isObject$2(val) && (val.toString === objectToString || !isFunction$1(val.toString)) ? JSON.stringify(val, replacer, 2) : String(val);
};
const replacer = (_key, val) => {
  if (val && val.__v_isRef) {
    return replacer(_key, val.value);
  } else if (isMap(val)) {
    return {
      [`Map(${val.size})`]: [...val.entries()].reduce((entries, [key, val2]) => {
        entries[`${key} =>`] = val2;
        return entries;
      }, {})
    };
  } else if (isSet(val)) {
    return {
      [`Set(${val.size})`]: [...val.values()]
    };
  } else if (isObject$2(val) && !isArray$2(val) && !isPlainObject$1(val)) {
    return String(val);
  }
  return val;
};
const EMPTY_OBJ = Object.freeze({});
const EMPTY_ARR = Object.freeze([]);
const NOOP = () => {
};
const NO = () => false;
const onRE = /^on[^a-z]/;
const isOn = (key) => onRE.test(key);
const isModelListener = (key) => key.startsWith("onUpdate:");
const extend$1 = Object.assign;
const remove = (arr, el) => {
  const i = arr.indexOf(el);
  if (i > -1) {
    arr.splice(i, 1);
  }
};
const hasOwnProperty$2 = Object.prototype.hasOwnProperty;
const hasOwn$1 = (val, key) => hasOwnProperty$2.call(val, key);
const isArray$2 = Array.isArray;
const isMap = (val) => toTypeString(val) === "[object Map]";
const isSet = (val) => toTypeString(val) === "[object Set]";
const isFunction$1 = (val) => typeof val === "function";
const isString$1 = (val) => typeof val === "string";
const isSymbol = (val) => typeof val === "symbol";
const isObject$2 = (val) => val !== null && typeof val === "object";
const isPromise = (val) => {
  return isObject$2(val) && isFunction$1(val.then) && isFunction$1(val.catch);
};
const objectToString = Object.prototype.toString;
const toTypeString = (value) => objectToString.call(value);
const toRawType = (value) => {
  return toTypeString(value).slice(8, -1);
};
const isPlainObject$1 = (val) => toTypeString(val) === "[object Object]";
const isIntegerKey = (key) => isString$1(key) && key !== "NaN" && key[0] !== "-" && "" + parseInt(key, 10) === key;
const isReservedProp = /* @__PURE__ */ makeMap(
  // the leading comma is intentional so empty string "" is also included
  ",key,ref,ref_for,ref_key,onVnodeBeforeMount,onVnodeMounted,onVnodeBeforeUpdate,onVnodeUpdated,onVnodeBeforeUnmount,onVnodeUnmounted"
);
const isBuiltInDirective = /* @__PURE__ */ makeMap("bind,cloak,else-if,else,for,html,if,model,on,once,pre,show,slot,text,memo");
const cacheStringFunction = (fn) => {
  const cache = /* @__PURE__ */ Object.create(null);
  return (str) => {
    const hit = cache[str];
    return hit || (cache[str] = fn(str));
  };
};
const camelizeRE = /-(\w)/g;
const camelize = cacheStringFunction((str) => {
  return str.replace(camelizeRE, (_, c) => c ? c.toUpperCase() : "");
});
const hyphenateRE = /\B([A-Z])/g;
const hyphenate = cacheStringFunction((str) => str.replace(hyphenateRE, "-$1").toLowerCase());
const capitalize = cacheStringFunction((str) => str.charAt(0).toUpperCase() + str.slice(1));
const toHandlerKey = cacheStringFunction((str) => str ? `on${capitalize(str)}` : ``);
const hasChanged = (value, oldValue) => !Object.is(value, oldValue);
const invokeArrayFns$1 = (fns, arg) => {
  for (let i = 0; i < fns.length; i++) {
    fns[i](arg);
  }
};
const def = (obj, key, value) => {
  Object.defineProperty(obj, key, {
    configurable: true,
    enumerable: false,
    value
  });
};
const looseToNumber = (val) => {
  const n2 = parseFloat(val);
  return isNaN(n2) ? val : n2;
};
const LINEFEED = "\n";
const SLOT_DEFAULT_NAME = "d";
const ON_SHOW = "onShow";
const ON_HIDE = "onHide";
const ON_LAUNCH = "onLaunch";
const ON_ERROR = "onError";
const ON_THEME_CHANGE = "onThemeChange";
const ON_PAGE_NOT_FOUND = "onPageNotFound";
const ON_UNHANDLE_REJECTION = "onUnhandledRejection";
const ON_LOAD = "onLoad";
const ON_READY = "onReady";
const ON_UNLOAD = "onUnload";
const ON_INIT = "onInit";
const ON_SAVE_EXIT_STATE = "onSaveExitState";
const ON_RESIZE = "onResize";
const ON_BACK_PRESS = "onBackPress";
const ON_PAGE_SCROLL = "onPageScroll";
const ON_TAB_ITEM_TAP = "onTabItemTap";
const ON_REACH_BOTTOM = "onReachBottom";
const ON_PULL_DOWN_REFRESH = "onPullDownRefresh";
const ON_SHARE_TIMELINE = "onShareTimeline";
const ON_ADD_TO_FAVORITES = "onAddToFavorites";
const ON_SHARE_APP_MESSAGE = "onShareAppMessage";
const ON_NAVIGATION_BAR_BUTTON_TAP = "onNavigationBarButtonTap";
const ON_NAVIGATION_BAR_SEARCH_INPUT_CLICKED = "onNavigationBarSearchInputClicked";
const ON_NAVIGATION_BAR_SEARCH_INPUT_CHANGED = "onNavigationBarSearchInputChanged";
const ON_NAVIGATION_BAR_SEARCH_INPUT_CONFIRMED = "onNavigationBarSearchInputConfirmed";
const ON_NAVIGATION_BAR_SEARCH_INPUT_FOCUS_CHANGED = "onNavigationBarSearchInputFocusChanged";
function dynamicSlotName(name) {
  return name === "default" ? SLOT_DEFAULT_NAME : name;
}
const customizeRE = /:/g;
function customizeEvent(str) {
  return camelize(str.replace(customizeRE, "-"));
}
function hasLeadingSlash(str) {
  return str.indexOf("/") === 0;
}
function addLeadingSlash(str) {
  return hasLeadingSlash(str) ? str : "/" + str;
}
const invokeArrayFns = (fns, arg) => {
  let ret;
  for (let i = 0; i < fns.length; i++) {
    ret = fns[i](arg);
  }
  return ret;
};
function once(fn, ctx = null) {
  let res;
  return (...args) => {
    if (fn) {
      res = fn.apply(ctx, args);
      fn = null;
    }
    return res;
  };
}
function getValueByDataPath(obj, path) {
  if (!isString$1(path)) {
    return;
  }
  path = path.replace(/\[(\d+)\]/g, ".$1");
  const parts = path.split(".");
  let key = parts[0];
  if (!obj) {
    obj = {};
  }
  if (parts.length === 1) {
    return obj[key];
  }
  return getValueByDataPath(obj[key], parts.slice(1).join("."));
}
function sortObject(obj) {
  let sortObj = {};
  if (isPlainObject$1(obj)) {
    Object.keys(obj).sort().forEach((key) => {
      const _key = key;
      sortObj[_key] = obj[_key];
    });
  }
  return !Object.keys(sortObj) ? obj : sortObj;
}
const encode = encodeURIComponent;
function stringifyQuery(obj, encodeStr = encode) {
  const res = obj ? Object.keys(obj).map((key) => {
    let val = obj[key];
    if (typeof val === void 0 || val === null) {
      val = "";
    } else if (isPlainObject$1(val)) {
      val = JSON.stringify(val);
    }
    return encodeStr(key) + "=" + encodeStr(val);
  }).filter((x) => x.length > 0).join("&") : null;
  return res ? `?${res}` : "";
}
const PAGE_HOOKS = [
  ON_INIT,
  ON_LOAD,
  ON_SHOW,
  ON_HIDE,
  ON_UNLOAD,
  ON_BACK_PRESS,
  ON_PAGE_SCROLL,
  ON_TAB_ITEM_TAP,
  ON_REACH_BOTTOM,
  ON_PULL_DOWN_REFRESH,
  ON_SHARE_TIMELINE,
  ON_SHARE_APP_MESSAGE,
  ON_ADD_TO_FAVORITES,
  ON_SAVE_EXIT_STATE,
  ON_NAVIGATION_BAR_BUTTON_TAP,
  ON_NAVIGATION_BAR_SEARCH_INPUT_CLICKED,
  ON_NAVIGATION_BAR_SEARCH_INPUT_CHANGED,
  ON_NAVIGATION_BAR_SEARCH_INPUT_CONFIRMED,
  ON_NAVIGATION_BAR_SEARCH_INPUT_FOCUS_CHANGED
];
function isRootHook(name) {
  return PAGE_HOOKS.indexOf(name) > -1;
}
const UniLifecycleHooks = [
  ON_SHOW,
  ON_HIDE,
  ON_LAUNCH,
  ON_ERROR,
  ON_THEME_CHANGE,
  ON_PAGE_NOT_FOUND,
  ON_UNHANDLE_REJECTION,
  ON_INIT,
  ON_LOAD,
  ON_READY,
  ON_UNLOAD,
  ON_RESIZE,
  ON_BACK_PRESS,
  ON_PAGE_SCROLL,
  ON_TAB_ITEM_TAP,
  ON_REACH_BOTTOM,
  ON_PULL_DOWN_REFRESH,
  ON_SHARE_TIMELINE,
  ON_ADD_TO_FAVORITES,
  ON_SHARE_APP_MESSAGE,
  ON_SAVE_EXIT_STATE,
  ON_NAVIGATION_BAR_BUTTON_TAP,
  ON_NAVIGATION_BAR_SEARCH_INPUT_CLICKED,
  ON_NAVIGATION_BAR_SEARCH_INPUT_CHANGED,
  ON_NAVIGATION_BAR_SEARCH_INPUT_CONFIRMED,
  ON_NAVIGATION_BAR_SEARCH_INPUT_FOCUS_CHANGED
];
const MINI_PROGRAM_PAGE_RUNTIME_HOOKS = /* @__PURE__ */ (() => {
  return {
    onPageScroll: 1,
    onShareAppMessage: 1 << 1,
    onShareTimeline: 1 << 2
  };
})();
function isUniLifecycleHook(name, value, checkType = true) {
  if (checkType && !isFunction$1(value)) {
    return false;
  }
  if (UniLifecycleHooks.indexOf(name) > -1) {
    return true;
  } else if (name.indexOf("on") === 0) {
    return true;
  }
  return false;
}
let vueApp;
const createVueAppHooks = [];
function onCreateVueApp(hook) {
  if (vueApp) {
    return hook(vueApp);
  }
  createVueAppHooks.push(hook);
}
function invokeCreateVueAppHook(app) {
  vueApp = app;
  createVueAppHooks.forEach((hook) => hook(app));
}
const invokeCreateErrorHandler = once((app, createErrorHandler2) => {
  if (isFunction$1(app._component.onError)) {
    return createErrorHandler2(app);
  }
});
const E = function() {
};
E.prototype = {
  on: function(name, callback, ctx) {
    var e2 = this.e || (this.e = {});
    (e2[name] || (e2[name] = [])).push({
      fn: callback,
      ctx
    });
    return this;
  },
  once: function(name, callback, ctx) {
    var self2 = this;
    function listener() {
      self2.off(name, listener);
      callback.apply(ctx, arguments);
    }
    listener._ = callback;
    return this.on(name, listener, ctx);
  },
  emit: function(name) {
    var data = [].slice.call(arguments, 1);
    var evtArr = ((this.e || (this.e = {}))[name] || []).slice();
    var i = 0;
    var len = evtArr.length;
    for (i; i < len; i++) {
      evtArr[i].fn.apply(evtArr[i].ctx, data);
    }
    return this;
  },
  off: function(name, callback) {
    var e2 = this.e || (this.e = {});
    var evts = e2[name];
    var liveEvents = [];
    if (evts && callback) {
      for (var i = 0, len = evts.length; i < len; i++) {
        if (evts[i].fn !== callback && evts[i].fn._ !== callback)
          liveEvents.push(evts[i]);
      }
    }
    liveEvents.length ? e2[name] = liveEvents : delete e2[name];
    return this;
  }
};
var E$1 = E;
const isObject$1 = (val) => val !== null && typeof val === "object";
const defaultDelimiters = ["{", "}"];
class BaseFormatter {
  constructor() {
    this._caches = /* @__PURE__ */ Object.create(null);
  }
  interpolate(message, values, delimiters = defaultDelimiters) {
    if (!values) {
      return [message];
    }
    let tokens2 = this._caches[message];
    if (!tokens2) {
      tokens2 = parse(message, delimiters);
      this._caches[message] = tokens2;
    }
    return compile$1(tokens2, values);
  }
}
const RE_TOKEN_LIST_VALUE = /^(?:\d)+/;
const RE_TOKEN_NAMED_VALUE = /^(?:\w)+/;
function parse(format2, [startDelimiter, endDelimiter]) {
  const tokens2 = [];
  let position = 0;
  let text = "";
  while (position < format2.length) {
    let char = format2[position++];
    if (char === startDelimiter) {
      if (text) {
        tokens2.push({ type: "text", value: text });
      }
      text = "";
      let sub = "";
      char = format2[position++];
      while (char !== void 0 && char !== endDelimiter) {
        sub += char;
        char = format2[position++];
      }
      const isClosed = char === endDelimiter;
      const type = RE_TOKEN_LIST_VALUE.test(sub) ? "list" : isClosed && RE_TOKEN_NAMED_VALUE.test(sub) ? "named" : "unknown";
      tokens2.push({ value: sub, type });
    } else {
      text += char;
    }
  }
  text && tokens2.push({ type: "text", value: text });
  return tokens2;
}
function compile$1(tokens2, values) {
  const compiled = [];
  let index2 = 0;
  const mode = Array.isArray(values) ? "list" : isObject$1(values) ? "named" : "unknown";
  if (mode === "unknown") {
    return compiled;
  }
  while (index2 < tokens2.length) {
    const token2 = tokens2[index2];
    switch (token2.type) {
      case "text":
        compiled.push(token2.value);
        break;
      case "list":
        compiled.push(values[parseInt(token2.value, 10)]);
        break;
      case "named":
        if (mode === "named") {
          compiled.push(values[token2.value]);
        } else {
          {
            console.warn(`Type of token '${token2.type}' and format of value '${mode}' don't match!`);
          }
        }
        break;
      case "unknown":
        {
          console.warn(`Detect 'unknown' type of token!`);
        }
        break;
    }
    index2++;
  }
  return compiled;
}
const LOCALE_ZH_HANS = "zh-Hans";
const LOCALE_ZH_HANT = "zh-Hant";
const LOCALE_EN = "en";
const LOCALE_FR = "fr";
const LOCALE_ES = "es";
const hasOwnProperty$1 = Object.prototype.hasOwnProperty;
const hasOwn = (val, key) => hasOwnProperty$1.call(val, key);
const defaultFormatter = new BaseFormatter();
function include(str, parts) {
  return !!parts.find((part) => str.indexOf(part) !== -1);
}
function startsWith(str, parts) {
  return parts.find((part) => str.indexOf(part) === 0);
}
function normalizeLocale$1(locale2, messages) {
  if (!locale2) {
    return;
  }
  locale2 = locale2.trim().replace(/_/g, "-");
  if (messages && messages[locale2]) {
    return locale2;
  }
  locale2 = locale2.toLowerCase();
  if (locale2 === "chinese") {
    return LOCALE_ZH_HANS;
  }
  if (locale2.indexOf("zh") === 0) {
    if (locale2.indexOf("-hans") > -1) {
      return LOCALE_ZH_HANS;
    }
    if (locale2.indexOf("-hant") > -1) {
      return LOCALE_ZH_HANT;
    }
    if (include(locale2, ["-tw", "-hk", "-mo", "-cht"])) {
      return LOCALE_ZH_HANT;
    }
    return LOCALE_ZH_HANS;
  }
  let locales2 = [LOCALE_EN, LOCALE_FR, LOCALE_ES];
  if (messages && Object.keys(messages).length > 0) {
    locales2 = Object.keys(messages);
  }
  const lang2 = startsWith(locale2, locales2);
  if (lang2) {
    return lang2;
  }
}
class I18n {
  constructor({ locale: locale2, fallbackLocale, messages, watcher, formater }) {
    this.locale = LOCALE_EN;
    this.fallbackLocale = LOCALE_EN;
    this.message = {};
    this.messages = {};
    this.watchers = [];
    if (fallbackLocale) {
      this.fallbackLocale = fallbackLocale;
    }
    this.formater = formater || defaultFormatter;
    this.messages = messages || {};
    this.setLocale(locale2 || LOCALE_EN);
    if (watcher) {
      this.watchLocale(watcher);
    }
  }
  setLocale(locale2) {
    const oldLocale = this.locale;
    this.locale = normalizeLocale$1(locale2, this.messages) || this.fallbackLocale;
    if (!this.messages[this.locale]) {
      this.messages[this.locale] = {};
    }
    this.message = this.messages[this.locale];
    if (oldLocale !== this.locale) {
      this.watchers.forEach((watcher) => {
        watcher(this.locale, oldLocale);
      });
    }
  }
  getLocale() {
    return this.locale;
  }
  watchLocale(fn) {
    const index2 = this.watchers.push(fn) - 1;
    return () => {
      this.watchers.splice(index2, 1);
    };
  }
  add(locale2, message, override = true) {
    const curMessages = this.messages[locale2];
    if (curMessages) {
      if (override) {
        Object.assign(curMessages, message);
      } else {
        Object.keys(message).forEach((key) => {
          if (!hasOwn(curMessages, key)) {
            curMessages[key] = message[key];
          }
        });
      }
    } else {
      this.messages[locale2] = message;
    }
  }
  f(message, values, delimiters) {
    return this.formater.interpolate(message, values, delimiters).join("");
  }
  t(key, locale2, values) {
    let message = this.message;
    if (typeof locale2 === "string") {
      locale2 = normalizeLocale$1(locale2, this.messages);
      locale2 && (message = this.messages[locale2]);
    } else {
      values = locale2;
    }
    if (!hasOwn(message, key)) {
      console.warn(`Cannot translate the value of keypath ${key}. Use the value of keypath as default.`);
      return key;
    }
    return this.formater.interpolate(message[key], values).join("");
  }
}
function watchAppLocale(appVm, i18n) {
  if (appVm.$watchLocale) {
    appVm.$watchLocale((newLocale) => {
      i18n.setLocale(newLocale);
    });
  } else {
    appVm.$watch(() => appVm.$locale, (newLocale) => {
      i18n.setLocale(newLocale);
    });
  }
}
function getDefaultLocale() {
  if (typeof index !== "undefined" && index.getLocale) {
    return index.getLocale();
  }
  if (typeof global !== "undefined" && global.getLocale) {
    return global.getLocale();
  }
  return LOCALE_EN;
}
function initVueI18n(locale2, messages = {}, fallbackLocale, watcher) {
  if (typeof locale2 !== "string") {
    [locale2, messages] = [
      messages,
      locale2
    ];
  }
  if (typeof locale2 !== "string") {
    locale2 = getDefaultLocale();
  }
  if (typeof fallbackLocale !== "string") {
    fallbackLocale = typeof __uniConfig !== "undefined" && __uniConfig.fallbackLocale || LOCALE_EN;
  }
  const i18n = new I18n({
    locale: locale2,
    fallbackLocale,
    messages,
    watcher
  });
  let t2 = (key, values) => {
    if (typeof getApp !== "function") {
      t2 = function(key2, values2) {
        return i18n.t(key2, values2);
      };
    } else {
      let isWatchedAppLocale = false;
      t2 = function(key2, values2) {
        const appVm = getApp().$vm;
        if (appVm) {
          appVm.$locale;
          if (!isWatchedAppLocale) {
            isWatchedAppLocale = true;
            watchAppLocale(appVm, i18n);
          }
        }
        return i18n.t(key2, values2);
      };
    }
    return t2(key, values);
  };
  return {
    i18n,
    f(message, values, delimiters) {
      return i18n.f(message, values, delimiters);
    },
    t(key, values) {
      return t2(key, values);
    },
    add(locale3, message, override = true) {
      return i18n.add(locale3, message, override);
    },
    watch(fn) {
      return i18n.watchLocale(fn);
    },
    getLocale() {
      return i18n.getLocale();
    },
    setLocale(newLocale) {
      return i18n.setLocale(newLocale);
    }
  };
}
function getBaseSystemInfo() {
  return wx.getSystemInfoSync();
}
function validateProtocolFail(name, msg) {
  console.warn(`${name}: ${msg}`);
}
function validateProtocol(name, data, protocol, onFail) {
  if (!onFail) {
    onFail = validateProtocolFail;
  }
  for (const key in protocol) {
    const errMsg = validateProp$1(key, data[key], protocol[key], !hasOwn$1(data, key));
    if (isString$1(errMsg)) {
      onFail(name, errMsg);
    }
  }
}
function validateProtocols(name, args, protocol, onFail) {
  if (!protocol) {
    return;
  }
  if (!isArray$2(protocol)) {
    return validateProtocol(name, args[0] || /* @__PURE__ */ Object.create(null), protocol, onFail);
  }
  const len = protocol.length;
  const argsLen = args.length;
  for (let i = 0; i < len; i++) {
    const opts = protocol[i];
    const data = /* @__PURE__ */ Object.create(null);
    if (argsLen > i) {
      data[opts.name] = args[i];
    }
    validateProtocol(name, data, { [opts.name]: opts }, onFail);
  }
}
function validateProp$1(name, value, prop, isAbsent) {
  if (!isPlainObject$1(prop)) {
    prop = { type: prop };
  }
  const { type, required, validator } = prop;
  if (required && isAbsent) {
    return 'Missing required args: "' + name + '"';
  }
  if (value == null && !required) {
    return;
  }
  if (type != null) {
    let isValid2 = false;
    const types = isArray$2(type) ? type : [type];
    const expectedTypes = [];
    for (let i = 0; i < types.length && !isValid2; i++) {
      const { valid, expectedType } = assertType$1(value, types[i]);
      expectedTypes.push(expectedType || "");
      isValid2 = valid;
    }
    if (!isValid2) {
      return getInvalidTypeMessage$1(name, value, expectedTypes);
    }
  }
  if (validator) {
    return validator(value);
  }
}
const isSimpleType$1 = /* @__PURE__ */ makeMap("String,Number,Boolean,Function,Symbol");
function assertType$1(value, type) {
  let valid;
  const expectedType = getType$1(type);
  if (isSimpleType$1(expectedType)) {
    const t2 = typeof value;
    valid = t2 === expectedType.toLowerCase();
    if (!valid && t2 === "object") {
      valid = value instanceof type;
    }
  } else if (expectedType === "Object") {
    valid = isObject$2(value);
  } else if (expectedType === "Array") {
    valid = isArray$2(value);
  } else {
    {
      valid = value instanceof type;
    }
  }
  return {
    valid,
    expectedType
  };
}
function getInvalidTypeMessage$1(name, value, expectedTypes) {
  let message = `Invalid args: type check failed for args "${name}". Expected ${expectedTypes.map(capitalize).join(", ")}`;
  const expectedType = expectedTypes[0];
  const receivedType = toRawType(value);
  const expectedValue = styleValue$1(value, expectedType);
  const receivedValue = styleValue$1(value, receivedType);
  if (expectedTypes.length === 1 && isExplicable$1(expectedType) && !isBoolean$1(expectedType, receivedType)) {
    message += ` with value ${expectedValue}`;
  }
  message += `, got ${receivedType} `;
  if (isExplicable$1(receivedType)) {
    message += `with value ${receivedValue}.`;
  }
  return message;
}
function getType$1(ctor) {
  const match = ctor && ctor.toString().match(/^\s*function (\w+)/);
  return match ? match[1] : "";
}
function styleValue$1(value, type) {
  if (type === "String") {
    return `"${value}"`;
  } else if (type === "Number") {
    return `${Number(value)}`;
  } else {
    return `${value}`;
  }
}
function isExplicable$1(type) {
  const explicitTypes = ["string", "number", "boolean"];
  return explicitTypes.some((elem) => type.toLowerCase() === elem);
}
function isBoolean$1(...args) {
  return args.some((elem) => elem.toLowerCase() === "boolean");
}
function tryCatch(fn) {
  return function() {
    try {
      return fn.apply(fn, arguments);
    } catch (e2) {
      console.error(e2);
    }
  };
}
let invokeCallbackId = 1;
const invokeCallbacks = {};
function addInvokeCallback(id, name, callback, keepAlive = false) {
  invokeCallbacks[id] = {
    name,
    keepAlive,
    callback
  };
  return id;
}
function invokeCallback(id, res, extras) {
  if (typeof id === "number") {
    const opts = invokeCallbacks[id];
    if (opts) {
      if (!opts.keepAlive) {
        delete invokeCallbacks[id];
      }
      return opts.callback(res, extras);
    }
  }
  return res;
}
const API_SUCCESS = "success";
const API_FAIL = "fail";
const API_COMPLETE = "complete";
function getApiCallbacks(args) {
  const apiCallbacks = {};
  for (const name in args) {
    const fn = args[name];
    if (isFunction$1(fn)) {
      apiCallbacks[name] = tryCatch(fn);
      delete args[name];
    }
  }
  return apiCallbacks;
}
function normalizeErrMsg$1(errMsg, name) {
  if (!errMsg || errMsg.indexOf(":fail") === -1) {
    return name + ":ok";
  }
  return name + errMsg.substring(errMsg.indexOf(":fail"));
}
function createAsyncApiCallback(name, args = {}, { beforeAll, beforeSuccess } = {}) {
  if (!isPlainObject$1(args)) {
    args = {};
  }
  const { success, fail, complete } = getApiCallbacks(args);
  const hasSuccess = isFunction$1(success);
  const hasFail = isFunction$1(fail);
  const hasComplete = isFunction$1(complete);
  const callbackId = invokeCallbackId++;
  addInvokeCallback(callbackId, name, (res) => {
    res = res || {};
    res.errMsg = normalizeErrMsg$1(res.errMsg, name);
    isFunction$1(beforeAll) && beforeAll(res);
    if (res.errMsg === name + ":ok") {
      isFunction$1(beforeSuccess) && beforeSuccess(res, args);
      hasSuccess && success(res);
    } else {
      hasFail && fail(res);
    }
    hasComplete && complete(res);
  });
  return callbackId;
}
const HOOK_SUCCESS = "success";
const HOOK_FAIL = "fail";
const HOOK_COMPLETE = "complete";
const globalInterceptors = {};
const scopedInterceptors = {};
function wrapperHook(hook, params) {
  return function(data) {
    return hook(data, params) || data;
  };
}
function queue$1(hooks2, data, params) {
  let promise = false;
  for (let i = 0; i < hooks2.length; i++) {
    const hook = hooks2[i];
    if (promise) {
      promise = Promise.resolve(wrapperHook(hook, params));
    } else {
      const res = hook(data, params);
      if (isPromise(res)) {
        promise = Promise.resolve(res);
      }
      if (res === false) {
        return {
          then() {
          },
          catch() {
          }
        };
      }
    }
  }
  return promise || {
    then(callback) {
      return callback(data);
    },
    catch() {
    }
  };
}
function wrapperOptions(interceptors2, options = {}) {
  [HOOK_SUCCESS, HOOK_FAIL, HOOK_COMPLETE].forEach((name) => {
    const hooks2 = interceptors2[name];
    if (!isArray$2(hooks2)) {
      return;
    }
    const oldCallback = options[name];
    options[name] = function callbackInterceptor(res) {
      queue$1(hooks2, res, options).then((res2) => {
        return isFunction$1(oldCallback) && oldCallback(res2) || res2;
      });
    };
  });
  return options;
}
function wrapperReturnValue(method, returnValue) {
  const returnValueHooks = [];
  if (isArray$2(globalInterceptors.returnValue)) {
    returnValueHooks.push(...globalInterceptors.returnValue);
  }
  const interceptor = scopedInterceptors[method];
  if (interceptor && isArray$2(interceptor.returnValue)) {
    returnValueHooks.push(...interceptor.returnValue);
  }
  returnValueHooks.forEach((hook) => {
    returnValue = hook(returnValue) || returnValue;
  });
  return returnValue;
}
function getApiInterceptorHooks(method) {
  const interceptor = /* @__PURE__ */ Object.create(null);
  Object.keys(globalInterceptors).forEach((hook) => {
    if (hook !== "returnValue") {
      interceptor[hook] = globalInterceptors[hook].slice();
    }
  });
  const scopedInterceptor = scopedInterceptors[method];
  if (scopedInterceptor) {
    Object.keys(scopedInterceptor).forEach((hook) => {
      if (hook !== "returnValue") {
        interceptor[hook] = (interceptor[hook] || []).concat(scopedInterceptor[hook]);
      }
    });
  }
  return interceptor;
}
function invokeApi(method, api, options, params) {
  const interceptor = getApiInterceptorHooks(method);
  if (interceptor && Object.keys(interceptor).length) {
    if (isArray$2(interceptor.invoke)) {
      const res = queue$1(interceptor.invoke, options);
      return res.then((options2) => {
        return api(wrapperOptions(getApiInterceptorHooks(method), options2), ...params);
      });
    } else {
      return api(wrapperOptions(interceptor, options), ...params);
    }
  }
  return api(options, ...params);
}
function hasCallback(args) {
  if (isPlainObject$1(args) && [API_SUCCESS, API_FAIL, API_COMPLETE].find((cb) => isFunction$1(args[cb]))) {
    return true;
  }
  return false;
}
function handlePromise(promise) {
  return promise;
}
function promisify$1(name, fn) {
  return (args = {}, ...rest) => {
    if (hasCallback(args)) {
      return wrapperReturnValue(name, invokeApi(name, fn, args, rest));
    }
    return wrapperReturnValue(name, handlePromise(new Promise((resolve2, reject) => {
      invokeApi(name, fn, extend$1(args, { success: resolve2, fail: reject }), rest);
    })));
  };
}
function formatApiArgs(args, options) {
  const params = args[0];
  if (!options || !isPlainObject$1(options.formatArgs) && isPlainObject$1(params)) {
    return;
  }
  const formatArgs = options.formatArgs;
  const keys2 = Object.keys(formatArgs);
  for (let i = 0; i < keys2.length; i++) {
    const name = keys2[i];
    const formatterOrDefaultValue = formatArgs[name];
    if (isFunction$1(formatterOrDefaultValue)) {
      const errMsg = formatterOrDefaultValue(args[0][name], params);
      if (isString$1(errMsg)) {
        return errMsg;
      }
    } else {
      if (!hasOwn$1(params, name)) {
        params[name] = formatterOrDefaultValue;
      }
    }
  }
}
function invokeSuccess(id, name, res) {
  return invokeCallback(id, extend$1(res || {}, { errMsg: name + ":ok" }));
}
function invokeFail(id, name, errMsg, errRes) {
  return invokeCallback(id, extend$1({ errMsg: name + ":fail" + (errMsg ? " " + errMsg : "") }, errRes));
}
function beforeInvokeApi(name, args, protocol, options) {
  {
    validateProtocols(name, args, protocol);
  }
  if (options && options.beforeInvoke) {
    const errMsg2 = options.beforeInvoke(args);
    if (isString$1(errMsg2)) {
      return errMsg2;
    }
  }
  const errMsg = formatApiArgs(args, options);
  if (errMsg) {
    return errMsg;
  }
}
function normalizeErrMsg(errMsg) {
  if (!errMsg || isString$1(errMsg)) {
    return errMsg;
  }
  if (errMsg.stack) {
    console.error(errMsg.message + LINEFEED + errMsg.stack);
    return errMsg.message;
  }
  return errMsg;
}
function wrapperTaskApi(name, fn, protocol, options) {
  return (args) => {
    const id = createAsyncApiCallback(name, args, options);
    const errMsg = beforeInvokeApi(name, [args], protocol, options);
    if (errMsg) {
      return invokeFail(id, name, errMsg);
    }
    return fn(args, {
      resolve: (res) => invokeSuccess(id, name, res),
      reject: (errMsg2, errRes) => invokeFail(id, name, normalizeErrMsg(errMsg2), errRes)
    });
  };
}
function wrapperSyncApi(name, fn, protocol, options) {
  return (...args) => {
    const errMsg = beforeInvokeApi(name, args, protocol, options);
    if (errMsg) {
      throw new Error(errMsg);
    }
    return fn.apply(null, args);
  };
}
function wrapperAsyncApi(name, fn, protocol, options) {
  return wrapperTaskApi(name, fn, protocol, options);
}
function defineSyncApi(name, fn, protocol, options) {
  return wrapperSyncApi(name, fn, protocol, options);
}
function defineAsyncApi(name, fn, protocol, options) {
  return promisify$1(name, wrapperAsyncApi(name, fn, protocol, options));
}
const API_UPX2PX = "upx2px";
const Upx2pxProtocol = [
  {
    name: "upx",
    type: [Number, String],
    required: true
  }
];
const EPS = 1e-4;
const BASE_DEVICE_WIDTH = 750;
let isIOS = false;
let deviceWidth = 0;
let deviceDPR = 0;
function checkDeviceWidth() {
  const { platform: platform2, pixelRatio, windowWidth } = getBaseSystemInfo();
  deviceWidth = windowWidth;
  deviceDPR = pixelRatio;
  isIOS = platform2 === "ios";
}
const upx2px = defineSyncApi(API_UPX2PX, (number, newDeviceWidth) => {
  if (deviceWidth === 0) {
    checkDeviceWidth();
  }
  number = Number(number);
  if (number === 0) {
    return 0;
  }
  let width = newDeviceWidth || deviceWidth;
  let result = number / BASE_DEVICE_WIDTH * width;
  if (result < 0) {
    result = -result;
  }
  result = Math.floor(result + EPS);
  if (result === 0) {
    if (deviceDPR === 1 || !isIOS) {
      result = 1;
    } else {
      result = 0.5;
    }
  }
  return number < 0 ? -result : result;
}, Upx2pxProtocol);
const API_ADD_INTERCEPTOR = "addInterceptor";
const API_REMOVE_INTERCEPTOR = "removeInterceptor";
const AddInterceptorProtocol = [
  {
    name: "method",
    type: [String, Object],
    required: true
  }
];
const RemoveInterceptorProtocol = AddInterceptorProtocol;
function mergeInterceptorHook(interceptors2, interceptor) {
  Object.keys(interceptor).forEach((hook) => {
    if (isFunction$1(interceptor[hook])) {
      interceptors2[hook] = mergeHook(interceptors2[hook], interceptor[hook]);
    }
  });
}
function removeInterceptorHook(interceptors2, interceptor) {
  if (!interceptors2 || !interceptor) {
    return;
  }
  Object.keys(interceptor).forEach((name) => {
    const hooks2 = interceptors2[name];
    const hook = interceptor[name];
    if (isArray$2(hooks2) && isFunction$1(hook)) {
      remove(hooks2, hook);
    }
  });
}
function mergeHook(parentVal, childVal) {
  const res = childVal ? parentVal ? parentVal.concat(childVal) : isArray$2(childVal) ? childVal : [childVal] : parentVal;
  return res ? dedupeHooks(res) : res;
}
function dedupeHooks(hooks2) {
  const res = [];
  for (let i = 0; i < hooks2.length; i++) {
    if (res.indexOf(hooks2[i]) === -1) {
      res.push(hooks2[i]);
    }
  }
  return res;
}
const addInterceptor = defineSyncApi(API_ADD_INTERCEPTOR, (method, interceptor) => {
  if (isString$1(method) && isPlainObject$1(interceptor)) {
    mergeInterceptorHook(scopedInterceptors[method] || (scopedInterceptors[method] = {}), interceptor);
  } else if (isPlainObject$1(method)) {
    mergeInterceptorHook(globalInterceptors, method);
  }
}, AddInterceptorProtocol);
const removeInterceptor = defineSyncApi(API_REMOVE_INTERCEPTOR, (method, interceptor) => {
  if (isString$1(method)) {
    if (isPlainObject$1(interceptor)) {
      removeInterceptorHook(scopedInterceptors[method], interceptor);
    } else {
      delete scopedInterceptors[method];
    }
  } else if (isPlainObject$1(method)) {
    removeInterceptorHook(globalInterceptors, method);
  }
}, RemoveInterceptorProtocol);
const interceptors = {};
const API_ON = "$on";
const OnProtocol = [
  {
    name: "event",
    type: String,
    required: true
  },
  {
    name: "callback",
    type: Function,
    required: true
  }
];
const API_ONCE = "$once";
const OnceProtocol = OnProtocol;
const API_OFF = "$off";
const OffProtocol = [
  {
    name: "event",
    type: [String, Array]
  },
  {
    name: "callback",
    type: Function
  }
];
const API_EMIT = "$emit";
const EmitProtocol = [
  {
    name: "event",
    type: String,
    required: true
  }
];
const emitter = new E$1();
const $on = defineSyncApi(API_ON, (name, callback) => {
  emitter.on(name, callback);
  return () => emitter.off(name, callback);
}, OnProtocol);
const $once = defineSyncApi(API_ONCE, (name, callback) => {
  emitter.once(name, callback);
  return () => emitter.off(name, callback);
}, OnceProtocol);
const $off = defineSyncApi(API_OFF, (name, callback) => {
  if (!name) {
    emitter.e = {};
    return;
  }
  if (!isArray$2(name))
    name = [name];
  name.forEach((n2) => emitter.off(n2, callback));
}, OffProtocol);
const $emit = defineSyncApi(API_EMIT, (name, ...args) => {
  emitter.emit(name, ...args);
}, EmitProtocol);
let cid;
let cidErrMsg;
let enabled;
function normalizePushMessage(message) {
  try {
    return JSON.parse(message);
  } catch (e2) {
  }
  return message;
}
function invokePushCallback(args) {
  if (args.type === "enabled") {
    enabled = true;
  } else if (args.type === "clientId") {
    cid = args.cid;
    cidErrMsg = args.errMsg;
    invokeGetPushCidCallbacks(cid, args.errMsg);
  } else if (args.type === "pushMsg") {
    const message = {
      type: "receive",
      data: normalizePushMessage(args.message)
    };
    for (let i = 0; i < onPushMessageCallbacks.length; i++) {
      const callback = onPushMessageCallbacks[i];
      callback(message);
      if (message.stopped) {
        break;
      }
    }
  } else if (args.type === "click") {
    onPushMessageCallbacks.forEach((callback) => {
      callback({
        type: "click",
        data: normalizePushMessage(args.message)
      });
    });
  }
}
const getPushCidCallbacks = [];
function invokeGetPushCidCallbacks(cid2, errMsg) {
  getPushCidCallbacks.forEach((callback) => {
    callback(cid2, errMsg);
  });
  getPushCidCallbacks.length = 0;
}
const API_GET_PUSH_CLIENT_ID = "getPushClientId";
const getPushClientId = defineAsyncApi(API_GET_PUSH_CLIENT_ID, (_, { resolve: resolve2, reject }) => {
  Promise.resolve().then(() => {
    if (typeof enabled === "undefined") {
      enabled = false;
      cid = "";
      cidErrMsg = "uniPush is not enabled";
    }
    getPushCidCallbacks.push((cid2, errMsg) => {
      if (cid2) {
        resolve2({ cid: cid2 });
      } else {
        reject(errMsg);
      }
    });
    if (typeof cid !== "undefined") {
      invokeGetPushCidCallbacks(cid, cidErrMsg);
    }
  });
});
const onPushMessageCallbacks = [];
const onPushMessage = (fn) => {
  if (onPushMessageCallbacks.indexOf(fn) === -1) {
    onPushMessageCallbacks.push(fn);
  }
};
const offPushMessage = (fn) => {
  if (!fn) {
    onPushMessageCallbacks.length = 0;
  } else {
    const index2 = onPushMessageCallbacks.indexOf(fn);
    if (index2 > -1) {
      onPushMessageCallbacks.splice(index2, 1);
    }
  }
};
const SYNC_API_RE = /^\$|getLocale|setLocale|sendNativeEvent|restoreGlobal|requireGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64|getDeviceInfo|getAppBaseInfo|getWindowInfo|getSystemSetting|getAppAuthorizeSetting/;
const CONTEXT_API_RE = /^create|Manager$/;
const CONTEXT_API_RE_EXC = ["createBLEConnection"];
const ASYNC_API = ["createBLEConnection"];
const CALLBACK_API_RE = /^on|^off/;
function isContextApi(name) {
  return CONTEXT_API_RE.test(name) && CONTEXT_API_RE_EXC.indexOf(name) === -1;
}
function isSyncApi(name) {
  return SYNC_API_RE.test(name) && ASYNC_API.indexOf(name) === -1;
}
function isCallbackApi(name) {
  return CALLBACK_API_RE.test(name) && name !== "onPush";
}
function shouldPromise(name) {
  if (isContextApi(name) || isSyncApi(name) || isCallbackApi(name)) {
    return false;
  }
  return true;
}
if (!Promise.prototype.finally) {
  Promise.prototype.finally = function(onfinally) {
    const promise = this.constructor;
    return this.then((value) => promise.resolve(onfinally && onfinally()).then(() => value), (reason) => promise.resolve(onfinally && onfinally()).then(() => {
      throw reason;
    }));
  };
}
function promisify(name, api) {
  if (!shouldPromise(name)) {
    return api;
  }
  if (!isFunction$1(api)) {
    return api;
  }
  return function promiseApi(options = {}, ...rest) {
    if (isFunction$1(options.success) || isFunction$1(options.fail) || isFunction$1(options.complete)) {
      return wrapperReturnValue(name, invokeApi(name, api, options, rest));
    }
    return wrapperReturnValue(name, handlePromise(new Promise((resolve2, reject) => {
      invokeApi(name, api, extend$1({}, options, {
        success: resolve2,
        fail: reject
      }), rest);
    })));
  };
}
const CALLBACKS = ["success", "fail", "cancel", "complete"];
function initWrapper(protocols2) {
  function processCallback(methodName, method, returnValue) {
    return function(res) {
      return method(processReturnValue(methodName, res, returnValue));
    };
  }
  function processArgs(methodName, fromArgs, argsOption = {}, returnValue = {}, keepFromArgs = false) {
    if (isPlainObject$1(fromArgs)) {
      const toArgs = keepFromArgs === true ? fromArgs : {};
      if (isFunction$1(argsOption)) {
        argsOption = argsOption(fromArgs, toArgs) || {};
      }
      for (const key in fromArgs) {
        if (hasOwn$1(argsOption, key)) {
          let keyOption = argsOption[key];
          if (isFunction$1(keyOption)) {
            keyOption = keyOption(fromArgs[key], fromArgs, toArgs);
          }
          if (!keyOption) {
            console.warn(`微信小程序 ${methodName} 暂不支持 ${key}`);
          } else if (isString$1(keyOption)) {
            toArgs[keyOption] = fromArgs[key];
          } else if (isPlainObject$1(keyOption)) {
            toArgs[keyOption.name ? keyOption.name : key] = keyOption.value;
          }
        } else if (CALLBACKS.indexOf(key) !== -1) {
          const callback = fromArgs[key];
          if (isFunction$1(callback)) {
            toArgs[key] = processCallback(methodName, callback, returnValue);
          }
        } else {
          if (!keepFromArgs && !hasOwn$1(toArgs, key)) {
            toArgs[key] = fromArgs[key];
          }
        }
      }
      return toArgs;
    } else if (isFunction$1(fromArgs)) {
      fromArgs = processCallback(methodName, fromArgs, returnValue);
    }
    return fromArgs;
  }
  function processReturnValue(methodName, res, returnValue, keepReturnValue = false) {
    if (isFunction$1(protocols2.returnValue)) {
      res = protocols2.returnValue(methodName, res);
    }
    return processArgs(methodName, res, returnValue, {}, keepReturnValue);
  }
  return function wrapper(methodName, method) {
    if (!hasOwn$1(protocols2, methodName)) {
      return method;
    }
    const protocol = protocols2[methodName];
    if (!protocol) {
      return function() {
        console.error(`微信小程序 暂不支持${methodName}`);
      };
    }
    return function(arg1, arg2) {
      let options = protocol;
      if (isFunction$1(protocol)) {
        options = protocol(arg1);
      }
      arg1 = processArgs(methodName, arg1, options.args, options.returnValue);
      const args = [arg1];
      if (typeof arg2 !== "undefined") {
        args.push(arg2);
      }
      const returnValue = wx[options.name || methodName].apply(wx, args);
      if (isSyncApi(methodName)) {
        return processReturnValue(methodName, returnValue, options.returnValue, isContextApi(methodName));
      }
      return returnValue;
    };
  };
}
const getLocale$1 = () => {
  const app = isFunction$1(getApp) && getApp({ allowDefault: true });
  if (app && app.$vm) {
    return app.$vm.$locale;
  }
  return normalizeLocale$1(wx.getSystemInfoSync().language) || LOCALE_EN;
};
const setLocale = (locale2) => {
  const app = isFunction$1(getApp) && getApp();
  if (!app) {
    return false;
  }
  const oldLocale = app.$vm.$locale;
  if (oldLocale !== locale2) {
    app.$vm.$locale = locale2;
    onLocaleChangeCallbacks.forEach((fn) => fn({ locale: locale2 }));
    return true;
  }
  return false;
};
const onLocaleChangeCallbacks = [];
const onLocaleChange = (fn) => {
  if (onLocaleChangeCallbacks.indexOf(fn) === -1) {
    onLocaleChangeCallbacks.push(fn);
  }
};
if (typeof global !== "undefined") {
  global.getLocale = getLocale$1;
}
const UUID_KEY = "__DC_STAT_UUID";
let deviceId;
function useDeviceId(global2 = wx) {
  return function addDeviceId(_, toRes) {
    deviceId = deviceId || global2.getStorageSync(UUID_KEY);
    if (!deviceId) {
      deviceId = Date.now() + "" + Math.floor(Math.random() * 1e7);
      wx.setStorage({
        key: UUID_KEY,
        data: deviceId
      });
    }
    toRes.deviceId = deviceId;
  };
}
function addSafeAreaInsets(fromRes, toRes) {
  if (fromRes.safeArea) {
    const safeArea = fromRes.safeArea;
    toRes.safeAreaInsets = {
      top: safeArea.top,
      left: safeArea.left,
      right: fromRes.windowWidth - safeArea.right,
      bottom: fromRes.screenHeight - safeArea.bottom
    };
  }
}
function populateParameters(fromRes, toRes) {
  const { brand = "", model = "", system = "", language = "", theme, version: version2, platform: platform2, fontSizeSetting, SDKVersion, pixelRatio, deviceOrientation } = fromRes;
  let osName = "";
  let osVersion = "";
  {
    osName = system.split(" ")[0] || "";
    osVersion = system.split(" ")[1] || "";
  }
  let hostVersion = version2;
  let deviceType = getGetDeviceType(fromRes, model);
  let deviceBrand = getDeviceBrand(brand);
  let _hostName = getHostName(fromRes);
  let _deviceOrientation = deviceOrientation;
  let _devicePixelRatio = pixelRatio;
  let _SDKVersion = SDKVersion;
  const hostLanguage = language.replace(/_/g, "-");
  const parameters = {
    appId: "__UNI__54737E5",
    appName: "dormitory",
    appVersion: "1.0.0",
    appVersionCode: "100",
    appLanguage: getAppLanguage(hostLanguage),
    uniCompileVersion: "3.8.12",
    uniRuntimeVersion: "3.8.12",
    uniPlatform: "mp-weixin",
    deviceBrand,
    deviceModel: model,
    deviceType,
    devicePixelRatio: _devicePixelRatio,
    deviceOrientation: _deviceOrientation,
    osName: osName.toLocaleLowerCase(),
    osVersion,
    hostTheme: theme,
    hostVersion,
    hostLanguage,
    hostName: _hostName,
    hostSDKVersion: _SDKVersion,
    hostFontSizeSetting: fontSizeSetting,
    windowTop: 0,
    windowBottom: 0,
    // TODO
    osLanguage: void 0,
    osTheme: void 0,
    ua: void 0,
    hostPackageName: void 0,
    browserName: void 0,
    browserVersion: void 0
  };
  extend$1(toRes, parameters);
}
function getGetDeviceType(fromRes, model) {
  let deviceType = fromRes.deviceType || "phone";
  {
    const deviceTypeMaps = {
      ipad: "pad",
      windows: "pc",
      mac: "pc"
    };
    const deviceTypeMapsKeys = Object.keys(deviceTypeMaps);
    const _model = model.toLocaleLowerCase();
    for (let index2 = 0; index2 < deviceTypeMapsKeys.length; index2++) {
      const _m = deviceTypeMapsKeys[index2];
      if (_model.indexOf(_m) !== -1) {
        deviceType = deviceTypeMaps[_m];
        break;
      }
    }
  }
  return deviceType;
}
function getDeviceBrand(brand) {
  let deviceBrand = brand;
  if (deviceBrand) {
    deviceBrand = deviceBrand.toLocaleLowerCase();
  }
  return deviceBrand;
}
function getAppLanguage(defaultLanguage) {
  return getLocale$1 ? getLocale$1() : defaultLanguage;
}
function getHostName(fromRes) {
  const _platform = "WeChat";
  let _hostName = fromRes.hostName || _platform;
  {
    if (fromRes.environment) {
      _hostName = fromRes.environment;
    } else if (fromRes.host && fromRes.host.env) {
      _hostName = fromRes.host.env;
    }
  }
  return _hostName;
}
const getSystemInfo = {
  returnValue: (fromRes, toRes) => {
    addSafeAreaInsets(fromRes, toRes);
    useDeviceId()(fromRes, toRes);
    populateParameters(fromRes, toRes);
  }
};
const getSystemInfoSync = getSystemInfo;
const redirectTo = {};
const previewImage = {
  args(fromArgs, toArgs) {
    let currentIndex = parseInt(fromArgs.current);
    if (isNaN(currentIndex)) {
      return;
    }
    const urls = fromArgs.urls;
    if (!isArray$2(urls)) {
      return;
    }
    const len = urls.length;
    if (!len) {
      return;
    }
    if (currentIndex < 0) {
      currentIndex = 0;
    } else if (currentIndex >= len) {
      currentIndex = len - 1;
    }
    if (currentIndex > 0) {
      toArgs.current = urls[currentIndex];
      toArgs.urls = urls.filter((item, index2) => index2 < currentIndex ? item !== urls[currentIndex] : true);
    } else {
      toArgs.current = urls[0];
    }
    return {
      indicator: false,
      loop: false
    };
  }
};
const showActionSheet = {
  args(fromArgs, toArgs) {
    toArgs.alertText = fromArgs.title;
  }
};
const getDeviceInfo = {
  returnValue: (fromRes, toRes) => {
    const { brand, model } = fromRes;
    let deviceType = getGetDeviceType(fromRes, model);
    let deviceBrand = getDeviceBrand(brand);
    useDeviceId()(fromRes, toRes);
    toRes = sortObject(extend$1(toRes, {
      deviceType,
      deviceBrand,
      deviceModel: model
    }));
  }
};
const getAppBaseInfo = {
  returnValue: (fromRes, toRes) => {
    const { version: version2, language, SDKVersion, theme } = fromRes;
    let _hostName = getHostName(fromRes);
    let hostLanguage = language.replace(/_/g, "-");
    toRes = sortObject(extend$1(toRes, {
      hostVersion: version2,
      hostLanguage,
      hostName: _hostName,
      hostSDKVersion: SDKVersion,
      hostTheme: theme,
      appId: "__UNI__54737E5",
      appName: "dormitory",
      appVersion: "1.0.0",
      appVersionCode: "100",
      appLanguage: getAppLanguage(hostLanguage)
    }));
  }
};
const getWindowInfo = {
  returnValue: (fromRes, toRes) => {
    addSafeAreaInsets(fromRes, toRes);
    toRes = sortObject(extend$1(toRes, {
      windowTop: 0,
      windowBottom: 0
    }));
  }
};
const getAppAuthorizeSetting = {
  returnValue: function(fromRes, toRes) {
    const { locationReducedAccuracy } = fromRes;
    toRes.locationAccuracy = "unsupported";
    if (locationReducedAccuracy === true) {
      toRes.locationAccuracy = "reduced";
    } else if (locationReducedAccuracy === false) {
      toRes.locationAccuracy = "full";
    }
  }
};
const baseApis = {
  $on,
  $off,
  $once,
  $emit,
  upx2px,
  interceptors,
  addInterceptor,
  removeInterceptor,
  onCreateVueApp,
  invokeCreateVueAppHook,
  getLocale: getLocale$1,
  setLocale,
  onLocaleChange,
  getPushClientId,
  onPushMessage,
  offPushMessage,
  invokePushCallback
};
function initUni(api, protocols2, platform2 = wx) {
  const wrapper = initWrapper(protocols2);
  const UniProxyHandlers = {
    get(target, key) {
      if (hasOwn$1(target, key)) {
        return target[key];
      }
      if (hasOwn$1(api, key)) {
        return promisify(key, api[key]);
      }
      if (hasOwn$1(baseApis, key)) {
        return promisify(key, baseApis[key]);
      }
      return promisify(key, wrapper(key, platform2[key]));
    }
  };
  return new Proxy({}, UniProxyHandlers);
}
function initGetProvider(providers) {
  return function getProvider2({ service, success, fail, complete }) {
    let res;
    if (providers[service]) {
      res = {
        errMsg: "getProvider:ok",
        service,
        provider: providers[service]
      };
      isFunction$1(success) && success(res);
    } else {
      res = {
        errMsg: "getProvider:fail:服务[" + service + "]不存在"
      };
      isFunction$1(fail) && fail(res);
    }
    isFunction$1(complete) && complete(res);
  };
}
const objectKeys = [
  "qy",
  "env",
  "error",
  "version",
  "lanDebug",
  "cloud",
  "serviceMarket",
  "router",
  "worklet",
  "__webpack_require_UNI_MP_PLUGIN__"
];
const singlePageDisableKey = ["lanDebug", "router", "worklet"];
const launchOption = wx.getLaunchOptionsSync ? wx.getLaunchOptionsSync() : null;
function isWxKey(key) {
  if (launchOption && launchOption.scene === 1154 && singlePageDisableKey.includes(key)) {
    return false;
  }
  return objectKeys.indexOf(key) > -1 || typeof wx[key] === "function";
}
function initWx() {
  const newWx = {};
  for (const key in wx) {
    if (isWxKey(key)) {
      newWx[key] = wx[key];
    }
  }
  if (typeof globalThis !== "undefined" && typeof requireMiniProgram === "undefined") {
    globalThis.wx = newWx;
  }
  return newWx;
}
const mocks$1 = ["__route__", "__wxExparserNodeId__", "__wxWebviewId__"];
const getProvider = initGetProvider({
  oauth: ["weixin"],
  share: ["weixin"],
  payment: ["wxpay"],
  push: ["weixin"]
});
function initComponentMocks(component) {
  const res = /* @__PURE__ */ Object.create(null);
  mocks$1.forEach((name) => {
    res[name] = component[name];
  });
  return res;
}
function createSelectorQuery() {
  const query = wx$2.createSelectorQuery();
  const oldIn = query.in;
  query.in = function newIn(component) {
    return oldIn.call(this, initComponentMocks(component));
  };
  return query;
}
const wx$2 = initWx();
let baseInfo = wx$2.getAppBaseInfo && wx$2.getAppBaseInfo();
if (!baseInfo) {
  baseInfo = wx$2.getSystemInfoSync();
}
const host = baseInfo ? baseInfo.host : null;
const shareVideoMessage = host && host.env === "SAAASDK" ? wx$2.miniapp.shareVideoMessage : wx$2.shareVideoMessage;
var shims = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  createSelectorQuery,
  getProvider,
  shareVideoMessage
});
const compressImage = {
  args(fromArgs, toArgs) {
    if (fromArgs.compressedHeight && !toArgs.compressHeight) {
      toArgs.compressHeight = fromArgs.compressedHeight;
    }
    if (fromArgs.compressedWidth && !toArgs.compressWidth) {
      toArgs.compressWidth = fromArgs.compressedWidth;
    }
  }
};
var protocols = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  compressImage,
  getAppAuthorizeSetting,
  getAppBaseInfo,
  getDeviceInfo,
  getSystemInfo,
  getSystemInfoSync,
  getWindowInfo,
  previewImage,
  redirectTo,
  showActionSheet
});
const wx$1 = initWx();
var index = initUni(shims, protocols, wx$1);
function warn$1(msg, ...args) {
  console.warn(`[Vue warn] ${msg}`, ...args);
}
let activeEffectScope;
class EffectScope {
  constructor(detached = false) {
    this.detached = detached;
    this._active = true;
    this.effects = [];
    this.cleanups = [];
    this.parent = activeEffectScope;
    if (!detached && activeEffectScope) {
      this.index = (activeEffectScope.scopes || (activeEffectScope.scopes = [])).push(this) - 1;
    }
  }
  get active() {
    return this._active;
  }
  run(fn) {
    if (this._active) {
      const currentEffectScope = activeEffectScope;
      try {
        activeEffectScope = this;
        return fn();
      } finally {
        activeEffectScope = currentEffectScope;
      }
    } else {
      warn$1(`cannot run an inactive effect scope.`);
    }
  }
  /**
   * This should only be called on non-detached scopes
   * @internal
   */
  on() {
    activeEffectScope = this;
  }
  /**
   * This should only be called on non-detached scopes
   * @internal
   */
  off() {
    activeEffectScope = this.parent;
  }
  stop(fromParent) {
    if (this._active) {
      let i, l;
      for (i = 0, l = this.effects.length; i < l; i++) {
        this.effects[i].stop();
      }
      for (i = 0, l = this.cleanups.length; i < l; i++) {
        this.cleanups[i]();
      }
      if (this.scopes) {
        for (i = 0, l = this.scopes.length; i < l; i++) {
          this.scopes[i].stop(true);
        }
      }
      if (!this.detached && this.parent && !fromParent) {
        const last = this.parent.scopes.pop();
        if (last && last !== this) {
          this.parent.scopes[this.index] = last;
          last.index = this.index;
        }
      }
      this.parent = void 0;
      this._active = false;
    }
  }
}
function effectScope(detached) {
  return new EffectScope(detached);
}
function recordEffectScope(effect, scope = activeEffectScope) {
  if (scope && scope.active) {
    scope.effects.push(effect);
  }
}
function getCurrentScope() {
  return activeEffectScope;
}
function onScopeDispose(fn) {
  if (activeEffectScope) {
    activeEffectScope.cleanups.push(fn);
  } else {
    warn$1(`onScopeDispose() is called when there is no active effect scope to be associated with.`);
  }
}
const createDep = (effects) => {
  const dep = new Set(effects);
  dep.w = 0;
  dep.n = 0;
  return dep;
};
const wasTracked = (dep) => (dep.w & trackOpBit) > 0;
const newTracked = (dep) => (dep.n & trackOpBit) > 0;
const initDepMarkers = ({ deps }) => {
  if (deps.length) {
    for (let i = 0; i < deps.length; i++) {
      deps[i].w |= trackOpBit;
    }
  }
};
const finalizeDepMarkers = (effect) => {
  const { deps } = effect;
  if (deps.length) {
    let ptr = 0;
    for (let i = 0; i < deps.length; i++) {
      const dep = deps[i];
      if (wasTracked(dep) && !newTracked(dep)) {
        dep.delete(effect);
      } else {
        deps[ptr++] = dep;
      }
      dep.w &= ~trackOpBit;
      dep.n &= ~trackOpBit;
    }
    deps.length = ptr;
  }
};
const targetMap = /* @__PURE__ */ new WeakMap();
let effectTrackDepth = 0;
let trackOpBit = 1;
const maxMarkerBits = 30;
let activeEffect;
const ITERATE_KEY = Symbol("iterate");
const MAP_KEY_ITERATE_KEY = Symbol("Map key iterate");
class ReactiveEffect {
  constructor(fn, scheduler = null, scope) {
    this.fn = fn;
    this.scheduler = scheduler;
    this.active = true;
    this.deps = [];
    this.parent = void 0;
    recordEffectScope(this, scope);
  }
  run() {
    if (!this.active) {
      return this.fn();
    }
    let parent = activeEffect;
    let lastShouldTrack = shouldTrack;
    while (parent) {
      if (parent === this) {
        return;
      }
      parent = parent.parent;
    }
    try {
      this.parent = activeEffect;
      activeEffect = this;
      shouldTrack = true;
      trackOpBit = 1 << ++effectTrackDepth;
      if (effectTrackDepth <= maxMarkerBits) {
        initDepMarkers(this);
      } else {
        cleanupEffect(this);
      }
      return this.fn();
    } finally {
      if (effectTrackDepth <= maxMarkerBits) {
        finalizeDepMarkers(this);
      }
      trackOpBit = 1 << --effectTrackDepth;
      activeEffect = this.parent;
      shouldTrack = lastShouldTrack;
      this.parent = void 0;
      if (this.deferStop) {
        this.stop();
      }
    }
  }
  stop() {
    if (activeEffect === this) {
      this.deferStop = true;
    } else if (this.active) {
      cleanupEffect(this);
      if (this.onStop) {
        this.onStop();
      }
      this.active = false;
    }
  }
}
function cleanupEffect(effect) {
  const { deps } = effect;
  if (deps.length) {
    for (let i = 0; i < deps.length; i++) {
      deps[i].delete(effect);
    }
    deps.length = 0;
  }
}
let shouldTrack = true;
const trackStack = [];
function pauseTracking() {
  trackStack.push(shouldTrack);
  shouldTrack = false;
}
function resetTracking() {
  const last = trackStack.pop();
  shouldTrack = last === void 0 ? true : last;
}
function track(target, type, key) {
  if (shouldTrack && activeEffect) {
    let depsMap = targetMap.get(target);
    if (!depsMap) {
      targetMap.set(target, depsMap = /* @__PURE__ */ new Map());
    }
    let dep = depsMap.get(key);
    if (!dep) {
      depsMap.set(key, dep = createDep());
    }
    const eventInfo = { effect: activeEffect, target, type, key };
    trackEffects(dep, eventInfo);
  }
}
function trackEffects(dep, debuggerEventExtraInfo) {
  let shouldTrack2 = false;
  if (effectTrackDepth <= maxMarkerBits) {
    if (!newTracked(dep)) {
      dep.n |= trackOpBit;
      shouldTrack2 = !wasTracked(dep);
    }
  } else {
    shouldTrack2 = !dep.has(activeEffect);
  }
  if (shouldTrack2) {
    dep.add(activeEffect);
    activeEffect.deps.push(dep);
    if (activeEffect.onTrack) {
      activeEffect.onTrack(Object.assign({ effect: activeEffect }, debuggerEventExtraInfo));
    }
  }
}
function trigger(target, type, key, newValue, oldValue, oldTarget) {
  const depsMap = targetMap.get(target);
  if (!depsMap) {
    return;
  }
  let deps = [];
  if (type === "clear") {
    deps = [...depsMap.values()];
  } else if (key === "length" && isArray$2(target)) {
    const newLength = Number(newValue);
    depsMap.forEach((dep, key2) => {
      if (key2 === "length" || key2 >= newLength) {
        deps.push(dep);
      }
    });
  } else {
    if (key !== void 0) {
      deps.push(depsMap.get(key));
    }
    switch (type) {
      case "add":
        if (!isArray$2(target)) {
          deps.push(depsMap.get(ITERATE_KEY));
          if (isMap(target)) {
            deps.push(depsMap.get(MAP_KEY_ITERATE_KEY));
          }
        } else if (isIntegerKey(key)) {
          deps.push(depsMap.get("length"));
        }
        break;
      case "delete":
        if (!isArray$2(target)) {
          deps.push(depsMap.get(ITERATE_KEY));
          if (isMap(target)) {
            deps.push(depsMap.get(MAP_KEY_ITERATE_KEY));
          }
        }
        break;
      case "set":
        if (isMap(target)) {
          deps.push(depsMap.get(ITERATE_KEY));
        }
        break;
    }
  }
  const eventInfo = { target, type, key, newValue, oldValue, oldTarget };
  if (deps.length === 1) {
    if (deps[0]) {
      {
        triggerEffects(deps[0], eventInfo);
      }
    }
  } else {
    const effects = [];
    for (const dep of deps) {
      if (dep) {
        effects.push(...dep);
      }
    }
    {
      triggerEffects(createDep(effects), eventInfo);
    }
  }
}
function triggerEffects(dep, debuggerEventExtraInfo) {
  const effects = isArray$2(dep) ? dep : [...dep];
  for (const effect of effects) {
    if (effect.computed) {
      triggerEffect(effect, debuggerEventExtraInfo);
    }
  }
  for (const effect of effects) {
    if (!effect.computed) {
      triggerEffect(effect, debuggerEventExtraInfo);
    }
  }
}
function triggerEffect(effect, debuggerEventExtraInfo) {
  if (effect !== activeEffect || effect.allowRecurse) {
    if (effect.onTrigger) {
      effect.onTrigger(extend$1({ effect }, debuggerEventExtraInfo));
    }
    if (effect.scheduler) {
      effect.scheduler();
    } else {
      effect.run();
    }
  }
}
function getDepFromReactive(object, key) {
  var _a2;
  return (_a2 = targetMap.get(object)) === null || _a2 === void 0 ? void 0 : _a2.get(key);
}
const isNonTrackableKeys = /* @__PURE__ */ makeMap(`__proto__,__v_isRef,__isVue`);
const builtInSymbols = new Set(
  /* @__PURE__ */ Object.getOwnPropertyNames(Symbol).filter((key) => key !== "arguments" && key !== "caller").map((key) => Symbol[key]).filter(isSymbol)
);
const get$1$1 = /* @__PURE__ */ createGetter();
const shallowGet = /* @__PURE__ */ createGetter(false, true);
const readonlyGet = /* @__PURE__ */ createGetter(true);
const shallowReadonlyGet = /* @__PURE__ */ createGetter(true, true);
const arrayInstrumentations = /* @__PURE__ */ createArrayInstrumentations();
function createArrayInstrumentations() {
  const instrumentations = {};
  ["includes", "indexOf", "lastIndexOf"].forEach((key) => {
    instrumentations[key] = function(...args) {
      const arr = toRaw(this);
      for (let i = 0, l = this.length; i < l; i++) {
        track(arr, "get", i + "");
      }
      const res = arr[key](...args);
      if (res === -1 || res === false) {
        return arr[key](...args.map(toRaw));
      } else {
        return res;
      }
    };
  });
  ["push", "pop", "shift", "unshift", "splice"].forEach((key) => {
    instrumentations[key] = function(...args) {
      pauseTracking();
      const res = toRaw(this)[key].apply(this, args);
      resetTracking();
      return res;
    };
  });
  return instrumentations;
}
function hasOwnProperty(key) {
  const obj = toRaw(this);
  track(obj, "has", key);
  return obj.hasOwnProperty(key);
}
function createGetter(isReadonly2 = false, shallow = false) {
  return function get2(target, key, receiver) {
    if (key === "__v_isReactive") {
      return !isReadonly2;
    } else if (key === "__v_isReadonly") {
      return isReadonly2;
    } else if (key === "__v_isShallow") {
      return shallow;
    } else if (key === "__v_raw" && receiver === (isReadonly2 ? shallow ? shallowReadonlyMap : readonlyMap : shallow ? shallowReactiveMap : reactiveMap).get(target)) {
      return target;
    }
    const targetIsArray = isArray$2(target);
    if (!isReadonly2) {
      if (targetIsArray && hasOwn$1(arrayInstrumentations, key)) {
        return Reflect.get(arrayInstrumentations, key, receiver);
      }
      if (key === "hasOwnProperty") {
        return hasOwnProperty;
      }
    }
    const res = Reflect.get(target, key, receiver);
    if (isSymbol(key) ? builtInSymbols.has(key) : isNonTrackableKeys(key)) {
      return res;
    }
    if (!isReadonly2) {
      track(target, "get", key);
    }
    if (shallow) {
      return res;
    }
    if (isRef(res)) {
      return targetIsArray && isIntegerKey(key) ? res : res.value;
    }
    if (isObject$2(res)) {
      return isReadonly2 ? readonly(res) : reactive(res);
    }
    return res;
  };
}
const set$1$1 = /* @__PURE__ */ createSetter();
const shallowSet = /* @__PURE__ */ createSetter(true);
function createSetter(shallow = false) {
  return function set2(target, key, value, receiver) {
    let oldValue = target[key];
    if (isReadonly(oldValue) && isRef(oldValue) && !isRef(value)) {
      return false;
    }
    if (!shallow) {
      if (!isShallow(value) && !isReadonly(value)) {
        oldValue = toRaw(oldValue);
        value = toRaw(value);
      }
      if (!isArray$2(target) && isRef(oldValue) && !isRef(value)) {
        oldValue.value = value;
        return true;
      }
    }
    const hadKey = isArray$2(target) && isIntegerKey(key) ? Number(key) < target.length : hasOwn$1(target, key);
    const result = Reflect.set(target, key, value, receiver);
    if (target === toRaw(receiver)) {
      if (!hadKey) {
        trigger(target, "add", key, value);
      } else if (hasChanged(value, oldValue)) {
        trigger(target, "set", key, value, oldValue);
      }
    }
    return result;
  };
}
function deleteProperty(target, key) {
  const hadKey = hasOwn$1(target, key);
  const oldValue = target[key];
  const result = Reflect.deleteProperty(target, key);
  if (result && hadKey) {
    trigger(target, "delete", key, void 0, oldValue);
  }
  return result;
}
function has$1(target, key) {
  const result = Reflect.has(target, key);
  if (!isSymbol(key) || !builtInSymbols.has(key)) {
    track(target, "has", key);
  }
  return result;
}
function ownKeys(target) {
  track(target, "iterate", isArray$2(target) ? "length" : ITERATE_KEY);
  return Reflect.ownKeys(target);
}
const mutableHandlers = {
  get: get$1$1,
  set: set$1$1,
  deleteProperty,
  has: has$1,
  ownKeys
};
const readonlyHandlers = {
  get: readonlyGet,
  set(target, key) {
    {
      warn$1(`Set operation on key "${String(key)}" failed: target is readonly.`, target);
    }
    return true;
  },
  deleteProperty(target, key) {
    {
      warn$1(`Delete operation on key "${String(key)}" failed: target is readonly.`, target);
    }
    return true;
  }
};
const shallowReactiveHandlers = /* @__PURE__ */ extend$1({}, mutableHandlers, {
  get: shallowGet,
  set: shallowSet
});
const shallowReadonlyHandlers = /* @__PURE__ */ extend$1({}, readonlyHandlers, {
  get: shallowReadonlyGet
});
const toShallow = (value) => value;
const getProto = (v) => Reflect.getPrototypeOf(v);
function get$3(target, key, isReadonly2 = false, isShallow2 = false) {
  target = target[
    "__v_raw"
    /* ReactiveFlags.RAW */
  ];
  const rawTarget = toRaw(target);
  const rawKey = toRaw(key);
  if (!isReadonly2) {
    if (key !== rawKey) {
      track(rawTarget, "get", key);
    }
    track(rawTarget, "get", rawKey);
  }
  const { has: has2 } = getProto(rawTarget);
  const wrap = isShallow2 ? toShallow : isReadonly2 ? toReadonly : toReactive;
  if (has2.call(rawTarget, key)) {
    return wrap(target.get(key));
  } else if (has2.call(rawTarget, rawKey)) {
    return wrap(target.get(rawKey));
  } else if (target !== rawTarget) {
    target.get(key);
  }
}
function has(key, isReadonly2 = false) {
  const target = this[
    "__v_raw"
    /* ReactiveFlags.RAW */
  ];
  const rawTarget = toRaw(target);
  const rawKey = toRaw(key);
  if (!isReadonly2) {
    if (key !== rawKey) {
      track(rawTarget, "has", key);
    }
    track(rawTarget, "has", rawKey);
  }
  return key === rawKey ? target.has(key) : target.has(key) || target.has(rawKey);
}
function size(target, isReadonly2 = false) {
  target = target[
    "__v_raw"
    /* ReactiveFlags.RAW */
  ];
  !isReadonly2 && track(toRaw(target), "iterate", ITERATE_KEY);
  return Reflect.get(target, "size", target);
}
function add$2(value) {
  value = toRaw(value);
  const target = toRaw(this);
  const proto2 = getProto(target);
  const hadKey = proto2.has.call(target, value);
  if (!hadKey) {
    target.add(value);
    trigger(target, "add", value, value);
  }
  return this;
}
function set$2$1(key, value) {
  value = toRaw(value);
  const target = toRaw(this);
  const { has: has2, get: get2 } = getProto(target);
  let hadKey = has2.call(target, key);
  if (!hadKey) {
    key = toRaw(key);
    hadKey = has2.call(target, key);
  } else {
    checkIdentityKeys(target, has2, key);
  }
  const oldValue = get2.call(target, key);
  target.set(key, value);
  if (!hadKey) {
    trigger(target, "add", key, value);
  } else if (hasChanged(value, oldValue)) {
    trigger(target, "set", key, value, oldValue);
  }
  return this;
}
function deleteEntry(key) {
  const target = toRaw(this);
  const { has: has2, get: get2 } = getProto(target);
  let hadKey = has2.call(target, key);
  if (!hadKey) {
    key = toRaw(key);
    hadKey = has2.call(target, key);
  } else {
    checkIdentityKeys(target, has2, key);
  }
  const oldValue = get2 ? get2.call(target, key) : void 0;
  const result = target.delete(key);
  if (hadKey) {
    trigger(target, "delete", key, void 0, oldValue);
  }
  return result;
}
function clear() {
  const target = toRaw(this);
  const hadItems = target.size !== 0;
  const oldTarget = isMap(target) ? new Map(target) : new Set(target);
  const result = target.clear();
  if (hadItems) {
    trigger(target, "clear", void 0, void 0, oldTarget);
  }
  return result;
}
function createForEach(isReadonly2, isShallow2) {
  return function forEach(callback, thisArg) {
    const observed = this;
    const target = observed[
      "__v_raw"
      /* ReactiveFlags.RAW */
    ];
    const rawTarget = toRaw(target);
    const wrap = isShallow2 ? toShallow : isReadonly2 ? toReadonly : toReactive;
    !isReadonly2 && track(rawTarget, "iterate", ITERATE_KEY);
    return target.forEach((value, key) => {
      return callback.call(thisArg, wrap(value), wrap(key), observed);
    });
  };
}
function createIterableMethod(method, isReadonly2, isShallow2) {
  return function(...args) {
    const target = this[
      "__v_raw"
      /* ReactiveFlags.RAW */
    ];
    const rawTarget = toRaw(target);
    const targetIsMap = isMap(rawTarget);
    const isPair = method === "entries" || method === Symbol.iterator && targetIsMap;
    const isKeyOnly = method === "keys" && targetIsMap;
    const innerIterator = target[method](...args);
    const wrap = isShallow2 ? toShallow : isReadonly2 ? toReadonly : toReactive;
    !isReadonly2 && track(rawTarget, "iterate", isKeyOnly ? MAP_KEY_ITERATE_KEY : ITERATE_KEY);
    return {
      // iterator protocol
      next() {
        const { value, done } = innerIterator.next();
        return done ? { value, done } : {
          value: isPair ? [wrap(value[0]), wrap(value[1])] : wrap(value),
          done
        };
      },
      // iterable protocol
      [Symbol.iterator]() {
        return this;
      }
    };
  };
}
function createReadonlyMethod(type) {
  return function(...args) {
    {
      const key = args[0] ? `on key "${args[0]}" ` : ``;
      console.warn(`${capitalize(type)} operation ${key}failed: target is readonly.`, toRaw(this));
    }
    return type === "delete" ? false : this;
  };
}
function createInstrumentations() {
  const mutableInstrumentations2 = {
    get(key) {
      return get$3(this, key);
    },
    get size() {
      return size(this);
    },
    has,
    add: add$2,
    set: set$2$1,
    delete: deleteEntry,
    clear,
    forEach: createForEach(false, false)
  };
  const shallowInstrumentations2 = {
    get(key) {
      return get$3(this, key, false, true);
    },
    get size() {
      return size(this);
    },
    has,
    add: add$2,
    set: set$2$1,
    delete: deleteEntry,
    clear,
    forEach: createForEach(false, true)
  };
  const readonlyInstrumentations2 = {
    get(key) {
      return get$3(this, key, true);
    },
    get size() {
      return size(this, true);
    },
    has(key) {
      return has.call(this, key, true);
    },
    add: createReadonlyMethod(
      "add"
      /* TriggerOpTypes.ADD */
    ),
    set: createReadonlyMethod(
      "set"
      /* TriggerOpTypes.SET */
    ),
    delete: createReadonlyMethod(
      "delete"
      /* TriggerOpTypes.DELETE */
    ),
    clear: createReadonlyMethod(
      "clear"
      /* TriggerOpTypes.CLEAR */
    ),
    forEach: createForEach(true, false)
  };
  const shallowReadonlyInstrumentations2 = {
    get(key) {
      return get$3(this, key, true, true);
    },
    get size() {
      return size(this, true);
    },
    has(key) {
      return has.call(this, key, true);
    },
    add: createReadonlyMethod(
      "add"
      /* TriggerOpTypes.ADD */
    ),
    set: createReadonlyMethod(
      "set"
      /* TriggerOpTypes.SET */
    ),
    delete: createReadonlyMethod(
      "delete"
      /* TriggerOpTypes.DELETE */
    ),
    clear: createReadonlyMethod(
      "clear"
      /* TriggerOpTypes.CLEAR */
    ),
    forEach: createForEach(true, true)
  };
  const iteratorMethods = ["keys", "values", "entries", Symbol.iterator];
  iteratorMethods.forEach((method) => {
    mutableInstrumentations2[method] = createIterableMethod(method, false, false);
    readonlyInstrumentations2[method] = createIterableMethod(method, true, false);
    shallowInstrumentations2[method] = createIterableMethod(method, false, true);
    shallowReadonlyInstrumentations2[method] = createIterableMethod(method, true, true);
  });
  return [
    mutableInstrumentations2,
    readonlyInstrumentations2,
    shallowInstrumentations2,
    shallowReadonlyInstrumentations2
  ];
}
const [mutableInstrumentations, readonlyInstrumentations, shallowInstrumentations, shallowReadonlyInstrumentations] = /* @__PURE__ */ createInstrumentations();
function createInstrumentationGetter(isReadonly2, shallow) {
  const instrumentations = shallow ? isReadonly2 ? shallowReadonlyInstrumentations : shallowInstrumentations : isReadonly2 ? readonlyInstrumentations : mutableInstrumentations;
  return (target, key, receiver) => {
    if (key === "__v_isReactive") {
      return !isReadonly2;
    } else if (key === "__v_isReadonly") {
      return isReadonly2;
    } else if (key === "__v_raw") {
      return target;
    }
    return Reflect.get(hasOwn$1(instrumentations, key) && key in target ? instrumentations : target, key, receiver);
  };
}
const mutableCollectionHandlers = {
  get: /* @__PURE__ */ createInstrumentationGetter(false, false)
};
const shallowCollectionHandlers = {
  get: /* @__PURE__ */ createInstrumentationGetter(false, true)
};
const readonlyCollectionHandlers = {
  get: /* @__PURE__ */ createInstrumentationGetter(true, false)
};
const shallowReadonlyCollectionHandlers = {
  get: /* @__PURE__ */ createInstrumentationGetter(true, true)
};
function checkIdentityKeys(target, has2, key) {
  const rawKey = toRaw(key);
  if (rawKey !== key && has2.call(target, rawKey)) {
    const type = toRawType(target);
    console.warn(`Reactive ${type} contains both the raw and reactive versions of the same object${type === `Map` ? ` as keys` : ``}, which can lead to inconsistencies. Avoid differentiating between the raw and reactive versions of an object and only use the reactive version if possible.`);
  }
}
const reactiveMap = /* @__PURE__ */ new WeakMap();
const shallowReactiveMap = /* @__PURE__ */ new WeakMap();
const readonlyMap = /* @__PURE__ */ new WeakMap();
const shallowReadonlyMap = /* @__PURE__ */ new WeakMap();
function targetTypeMap(rawType) {
  switch (rawType) {
    case "Object":
    case "Array":
      return 1;
    case "Map":
    case "Set":
    case "WeakMap":
    case "WeakSet":
      return 2;
    default:
      return 0;
  }
}
function getTargetType(value) {
  return value[
    "__v_skip"
    /* ReactiveFlags.SKIP */
  ] || !Object.isExtensible(value) ? 0 : targetTypeMap(toRawType(value));
}
function reactive(target) {
  if (isReadonly(target)) {
    return target;
  }
  return createReactiveObject(target, false, mutableHandlers, mutableCollectionHandlers, reactiveMap);
}
function shallowReactive(target) {
  return createReactiveObject(target, false, shallowReactiveHandlers, shallowCollectionHandlers, shallowReactiveMap);
}
function readonly(target) {
  return createReactiveObject(target, true, readonlyHandlers, readonlyCollectionHandlers, readonlyMap);
}
function shallowReadonly(target) {
  return createReactiveObject(target, true, shallowReadonlyHandlers, shallowReadonlyCollectionHandlers, shallowReadonlyMap);
}
function createReactiveObject(target, isReadonly2, baseHandlers, collectionHandlers, proxyMap) {
  if (!isObject$2(target)) {
    {
      console.warn(`value cannot be made reactive: ${String(target)}`);
    }
    return target;
  }
  if (target[
    "__v_raw"
    /* ReactiveFlags.RAW */
  ] && !(isReadonly2 && target[
    "__v_isReactive"
    /* ReactiveFlags.IS_REACTIVE */
  ])) {
    return target;
  }
  const existingProxy = proxyMap.get(target);
  if (existingProxy) {
    return existingProxy;
  }
  const targetType = getTargetType(target);
  if (targetType === 0) {
    return target;
  }
  const proxy = new Proxy(target, targetType === 2 ? collectionHandlers : baseHandlers);
  proxyMap.set(target, proxy);
  return proxy;
}
function isReactive(value) {
  if (isReadonly(value)) {
    return isReactive(value[
      "__v_raw"
      /* ReactiveFlags.RAW */
    ]);
  }
  return !!(value && value[
    "__v_isReactive"
    /* ReactiveFlags.IS_REACTIVE */
  ]);
}
function isReadonly(value) {
  return !!(value && value[
    "__v_isReadonly"
    /* ReactiveFlags.IS_READONLY */
  ]);
}
function isShallow(value) {
  return !!(value && value[
    "__v_isShallow"
    /* ReactiveFlags.IS_SHALLOW */
  ]);
}
function isProxy(value) {
  return isReactive(value) || isReadonly(value);
}
function toRaw(observed) {
  const raw = observed && observed[
    "__v_raw"
    /* ReactiveFlags.RAW */
  ];
  return raw ? toRaw(raw) : observed;
}
function markRaw(value) {
  def(value, "__v_skip", true);
  return value;
}
const toReactive = (value) => isObject$2(value) ? reactive(value) : value;
const toReadonly = (value) => isObject$2(value) ? readonly(value) : value;
function trackRefValue(ref2) {
  if (shouldTrack && activeEffect) {
    ref2 = toRaw(ref2);
    {
      trackEffects(ref2.dep || (ref2.dep = createDep()), {
        target: ref2,
        type: "get",
        key: "value"
      });
    }
  }
}
function triggerRefValue(ref2, newVal) {
  ref2 = toRaw(ref2);
  const dep = ref2.dep;
  if (dep) {
    {
      triggerEffects(dep, {
        target: ref2,
        type: "set",
        key: "value",
        newValue: newVal
      });
    }
  }
}
function isRef(r) {
  return !!(r && r.__v_isRef === true);
}
function ref(value) {
  return createRef(value, false);
}
function createRef(rawValue, shallow) {
  if (isRef(rawValue)) {
    return rawValue;
  }
  return new RefImpl(rawValue, shallow);
}
class RefImpl {
  constructor(value, __v_isShallow) {
    this.__v_isShallow = __v_isShallow;
    this.dep = void 0;
    this.__v_isRef = true;
    this._rawValue = __v_isShallow ? value : toRaw(value);
    this._value = __v_isShallow ? value : toReactive(value);
  }
  get value() {
    trackRefValue(this);
    return this._value;
  }
  set value(newVal) {
    const useDirectValue = this.__v_isShallow || isShallow(newVal) || isReadonly(newVal);
    newVal = useDirectValue ? newVal : toRaw(newVal);
    if (hasChanged(newVal, this._rawValue)) {
      this._rawValue = newVal;
      this._value = useDirectValue ? newVal : toReactive(newVal);
      triggerRefValue(this, newVal);
    }
  }
}
function unref(ref2) {
  return isRef(ref2) ? ref2.value : ref2;
}
const shallowUnwrapHandlers = {
  get: (target, key, receiver) => unref(Reflect.get(target, key, receiver)),
  set: (target, key, value, receiver) => {
    const oldValue = target[key];
    if (isRef(oldValue) && !isRef(value)) {
      oldValue.value = value;
      return true;
    } else {
      return Reflect.set(target, key, value, receiver);
    }
  }
};
function proxyRefs(objectWithRefs) {
  return isReactive(objectWithRefs) ? objectWithRefs : new Proxy(objectWithRefs, shallowUnwrapHandlers);
}
function toRefs(object) {
  if (!isProxy(object)) {
    console.warn(`toRefs() expects a reactive object but received a plain one.`);
  }
  const ret = isArray$2(object) ? new Array(object.length) : {};
  for (const key in object) {
    ret[key] = toRef(object, key);
  }
  return ret;
}
class ObjectRefImpl {
  constructor(_object, _key, _defaultValue) {
    this._object = _object;
    this._key = _key;
    this._defaultValue = _defaultValue;
    this.__v_isRef = true;
  }
  get value() {
    const val = this._object[this._key];
    return val === void 0 ? this._defaultValue : val;
  }
  set value(newVal) {
    this._object[this._key] = newVal;
  }
  get dep() {
    return getDepFromReactive(toRaw(this._object), this._key);
  }
}
function toRef(object, key, defaultValue) {
  const val = object[key];
  return isRef(val) ? val : new ObjectRefImpl(object, key, defaultValue);
}
var _a;
class ComputedRefImpl {
  constructor(getter, _setter, isReadonly2, isSSR) {
    this._setter = _setter;
    this.dep = void 0;
    this.__v_isRef = true;
    this[_a] = false;
    this._dirty = true;
    this.effect = new ReactiveEffect(getter, () => {
      if (!this._dirty) {
        this._dirty = true;
        triggerRefValue(this);
      }
    });
    this.effect.computed = this;
    this.effect.active = this._cacheable = !isSSR;
    this[
      "__v_isReadonly"
      /* ReactiveFlags.IS_READONLY */
    ] = isReadonly2;
  }
  get value() {
    const self2 = toRaw(this);
    trackRefValue(self2);
    if (self2._dirty || !self2._cacheable) {
      self2._dirty = false;
      self2._value = self2.effect.run();
    }
    return self2._value;
  }
  set value(newValue) {
    this._setter(newValue);
  }
}
_a = "__v_isReadonly";
function computed$1(getterOrOptions, debugOptions, isSSR = false) {
  let getter;
  let setter;
  const onlyGetter = isFunction$1(getterOrOptions);
  if (onlyGetter) {
    getter = getterOrOptions;
    setter = () => {
      console.warn("Write operation failed: computed value is readonly");
    };
  } else {
    getter = getterOrOptions.get;
    setter = getterOrOptions.set;
  }
  const cRef = new ComputedRefImpl(getter, setter, onlyGetter || !setter, isSSR);
  if (debugOptions && !isSSR) {
    cRef.effect.onTrack = debugOptions.onTrack;
    cRef.effect.onTrigger = debugOptions.onTrigger;
  }
  return cRef;
}
const stack = [];
function pushWarningContext(vnode) {
  stack.push(vnode);
}
function popWarningContext() {
  stack.pop();
}
function warn$2(msg, ...args) {
  pauseTracking();
  const instance = stack.length ? stack[stack.length - 1].component : null;
  const appWarnHandler = instance && instance.appContext.config.warnHandler;
  const trace = getComponentTrace();
  if (appWarnHandler) {
    callWithErrorHandling(appWarnHandler, instance, 11, [
      msg + args.join(""),
      instance && instance.proxy,
      trace.map(({ vnode }) => `at <${formatComponentName(instance, vnode.type)}>`).join("\n"),
      trace
    ]);
  } else {
    const warnArgs = [`[Vue warn]: ${msg}`, ...args];
    if (trace.length && // avoid spamming console during tests
    true) {
      warnArgs.push(`
`, ...formatTrace(trace));
    }
    console.warn(...warnArgs);
  }
  resetTracking();
}
function getComponentTrace() {
  let currentVNode = stack[stack.length - 1];
  if (!currentVNode) {
    return [];
  }
  const normalizedStack = [];
  while (currentVNode) {
    const last = normalizedStack[0];
    if (last && last.vnode === currentVNode) {
      last.recurseCount++;
    } else {
      normalizedStack.push({
        vnode: currentVNode,
        recurseCount: 0
      });
    }
    const parentInstance = currentVNode.component && currentVNode.component.parent;
    currentVNode = parentInstance && parentInstance.vnode;
  }
  return normalizedStack;
}
function formatTrace(trace) {
  const logs = [];
  trace.forEach((entry, i) => {
    logs.push(...i === 0 ? [] : [`
`], ...formatTraceEntry(entry));
  });
  return logs;
}
function formatTraceEntry({ vnode, recurseCount }) {
  const postfix = recurseCount > 0 ? `... (${recurseCount} recursive calls)` : ``;
  const isRoot = vnode.component ? vnode.component.parent == null : false;
  const open = ` at <${formatComponentName(vnode.component, vnode.type, isRoot)}`;
  const close = `>` + postfix;
  return vnode.props ? [open, ...formatProps(vnode.props), close] : [open + close];
}
function formatProps(props) {
  const res = [];
  const keys2 = Object.keys(props);
  keys2.slice(0, 3).forEach((key) => {
    res.push(...formatProp(key, props[key]));
  });
  if (keys2.length > 3) {
    res.push(` ...`);
  }
  return res;
}
function formatProp(key, value, raw) {
  if (isString$1(value)) {
    value = JSON.stringify(value);
    return raw ? value : [`${key}=${value}`];
  } else if (typeof value === "number" || typeof value === "boolean" || value == null) {
    return raw ? value : [`${key}=${value}`];
  } else if (isRef(value)) {
    value = formatProp(key, toRaw(value.value), true);
    return raw ? value : [`${key}=Ref<`, value, `>`];
  } else if (isFunction$1(value)) {
    return [`${key}=fn${value.name ? `<${value.name}>` : ``}`];
  } else {
    value = toRaw(value);
    return raw ? value : [`${key}=`, value];
  }
}
const ErrorTypeStrings = {
  [
    "sp"
    /* LifecycleHooks.SERVER_PREFETCH */
  ]: "serverPrefetch hook",
  [
    "bc"
    /* LifecycleHooks.BEFORE_CREATE */
  ]: "beforeCreate hook",
  [
    "c"
    /* LifecycleHooks.CREATED */
  ]: "created hook",
  [
    "bm"
    /* LifecycleHooks.BEFORE_MOUNT */
  ]: "beforeMount hook",
  [
    "m"
    /* LifecycleHooks.MOUNTED */
  ]: "mounted hook",
  [
    "bu"
    /* LifecycleHooks.BEFORE_UPDATE */
  ]: "beforeUpdate hook",
  [
    "u"
    /* LifecycleHooks.UPDATED */
  ]: "updated",
  [
    "bum"
    /* LifecycleHooks.BEFORE_UNMOUNT */
  ]: "beforeUnmount hook",
  [
    "um"
    /* LifecycleHooks.UNMOUNTED */
  ]: "unmounted hook",
  [
    "a"
    /* LifecycleHooks.ACTIVATED */
  ]: "activated hook",
  [
    "da"
    /* LifecycleHooks.DEACTIVATED */
  ]: "deactivated hook",
  [
    "ec"
    /* LifecycleHooks.ERROR_CAPTURED */
  ]: "errorCaptured hook",
  [
    "rtc"
    /* LifecycleHooks.RENDER_TRACKED */
  ]: "renderTracked hook",
  [
    "rtg"
    /* LifecycleHooks.RENDER_TRIGGERED */
  ]: "renderTriggered hook",
  [
    0
    /* ErrorCodes.SETUP_FUNCTION */
  ]: "setup function",
  [
    1
    /* ErrorCodes.RENDER_FUNCTION */
  ]: "render function",
  [
    2
    /* ErrorCodes.WATCH_GETTER */
  ]: "watcher getter",
  [
    3
    /* ErrorCodes.WATCH_CALLBACK */
  ]: "watcher callback",
  [
    4
    /* ErrorCodes.WATCH_CLEANUP */
  ]: "watcher cleanup function",
  [
    5
    /* ErrorCodes.NATIVE_EVENT_HANDLER */
  ]: "native event handler",
  [
    6
    /* ErrorCodes.COMPONENT_EVENT_HANDLER */
  ]: "component event handler",
  [
    7
    /* ErrorCodes.VNODE_HOOK */
  ]: "vnode hook",
  [
    8
    /* ErrorCodes.DIRECTIVE_HOOK */
  ]: "directive hook",
  [
    9
    /* ErrorCodes.TRANSITION_HOOK */
  ]: "transition hook",
  [
    10
    /* ErrorCodes.APP_ERROR_HANDLER */
  ]: "app errorHandler",
  [
    11
    /* ErrorCodes.APP_WARN_HANDLER */
  ]: "app warnHandler",
  [
    12
    /* ErrorCodes.FUNCTION_REF */
  ]: "ref function",
  [
    13
    /* ErrorCodes.ASYNC_COMPONENT_LOADER */
  ]: "async component loader",
  [
    14
    /* ErrorCodes.SCHEDULER */
  ]: "scheduler flush. This is likely a Vue internals bug. Please open an issue at https://new-issue.vuejs.org/?repo=vuejs/core"
};
function callWithErrorHandling(fn, instance, type, args) {
  let res;
  try {
    res = args ? fn(...args) : fn();
  } catch (err) {
    handleError(err, instance, type);
  }
  return res;
}
function callWithAsyncErrorHandling(fn, instance, type, args) {
  if (isFunction$1(fn)) {
    const res = callWithErrorHandling(fn, instance, type, args);
    if (res && isPromise(res)) {
      res.catch((err) => {
        handleError(err, instance, type);
      });
    }
    return res;
  }
  const values = [];
  for (let i = 0; i < fn.length; i++) {
    values.push(callWithAsyncErrorHandling(fn[i], instance, type, args));
  }
  return values;
}
function handleError(err, instance, type, throwInDev = true) {
  const contextVNode = instance ? instance.vnode : null;
  if (instance) {
    let cur = instance.parent;
    const exposedInstance = instance.proxy;
    const errorInfo = ErrorTypeStrings[type] || type;
    while (cur) {
      const errorCapturedHooks = cur.ec;
      if (errorCapturedHooks) {
        for (let i = 0; i < errorCapturedHooks.length; i++) {
          if (errorCapturedHooks[i](err, exposedInstance, errorInfo) === false) {
            return;
          }
        }
      }
      cur = cur.parent;
    }
    const appErrorHandler = instance.appContext.config.errorHandler;
    if (appErrorHandler) {
      callWithErrorHandling(appErrorHandler, null, 10, [err, exposedInstance, errorInfo]);
      return;
    }
  }
  logError(err, type, contextVNode, throwInDev);
}
function logError(err, type, contextVNode, throwInDev = true) {
  {
    const info = ErrorTypeStrings[type] || type;
    if (contextVNode) {
      pushWarningContext(contextVNode);
    }
    warn$2(`Unhandled error${info ? ` during execution of ${info}` : ``}`);
    if (contextVNode) {
      popWarningContext();
    }
    if (throwInDev) {
      console.error(err);
    } else {
      console.error(err);
    }
  }
}
let isFlushing = false;
let isFlushPending = false;
const queue = [];
let flushIndex = 0;
const pendingPostFlushCbs = [];
let activePostFlushCbs = null;
let postFlushIndex = 0;
const resolvedPromise = /* @__PURE__ */ Promise.resolve();
let currentFlushPromise = null;
const RECURSION_LIMIT = 100;
function nextTick$1(fn) {
  const p2 = currentFlushPromise || resolvedPromise;
  return fn ? p2.then(this ? fn.bind(this) : fn) : p2;
}
function findInsertionIndex(id) {
  let start = flushIndex + 1;
  let end = queue.length;
  while (start < end) {
    const middle = start + end >>> 1;
    const middleJobId = getId(queue[middle]);
    middleJobId < id ? start = middle + 1 : end = middle;
  }
  return start;
}
function queueJob(job) {
  if (!queue.length || !queue.includes(job, isFlushing && job.allowRecurse ? flushIndex + 1 : flushIndex)) {
    if (job.id == null) {
      queue.push(job);
    } else {
      queue.splice(findInsertionIndex(job.id), 0, job);
    }
    queueFlush();
  }
}
function queueFlush() {
  if (!isFlushing && !isFlushPending) {
    isFlushPending = true;
    currentFlushPromise = resolvedPromise.then(flushJobs);
  }
}
function hasQueueJob(job) {
  return queue.indexOf(job) > -1;
}
function invalidateJob(job) {
  const i = queue.indexOf(job);
  if (i > flushIndex) {
    queue.splice(i, 1);
  }
}
function queuePostFlushCb(cb) {
  if (!isArray$2(cb)) {
    if (!activePostFlushCbs || !activePostFlushCbs.includes(cb, cb.allowRecurse ? postFlushIndex + 1 : postFlushIndex)) {
      pendingPostFlushCbs.push(cb);
    }
  } else {
    pendingPostFlushCbs.push(...cb);
  }
  queueFlush();
}
function flushPreFlushCbs(seen, i = isFlushing ? flushIndex + 1 : 0) {
  {
    seen = seen || /* @__PURE__ */ new Map();
  }
  for (; i < queue.length; i++) {
    const cb = queue[i];
    if (cb && cb.pre) {
      if (checkRecursiveUpdates(seen, cb)) {
        continue;
      }
      queue.splice(i, 1);
      i--;
      cb();
    }
  }
}
function flushPostFlushCbs(seen) {
  if (pendingPostFlushCbs.length) {
    const deduped = [...new Set(pendingPostFlushCbs)];
    pendingPostFlushCbs.length = 0;
    if (activePostFlushCbs) {
      activePostFlushCbs.push(...deduped);
      return;
    }
    activePostFlushCbs = deduped;
    {
      seen = seen || /* @__PURE__ */ new Map();
    }
    activePostFlushCbs.sort((a, b) => getId(a) - getId(b));
    for (postFlushIndex = 0; postFlushIndex < activePostFlushCbs.length; postFlushIndex++) {
      if (checkRecursiveUpdates(seen, activePostFlushCbs[postFlushIndex])) {
        continue;
      }
      activePostFlushCbs[postFlushIndex]();
    }
    activePostFlushCbs = null;
    postFlushIndex = 0;
  }
}
const getId = (job) => job.id == null ? Infinity : job.id;
const comparator = (a, b) => {
  const diff2 = getId(a) - getId(b);
  if (diff2 === 0) {
    if (a.pre && !b.pre)
      return -1;
    if (b.pre && !a.pre)
      return 1;
  }
  return diff2;
};
function flushJobs(seen) {
  isFlushPending = false;
  isFlushing = true;
  {
    seen = seen || /* @__PURE__ */ new Map();
  }
  queue.sort(comparator);
  const check = (job) => checkRecursiveUpdates(seen, job);
  try {
    for (flushIndex = 0; flushIndex < queue.length; flushIndex++) {
      const job = queue[flushIndex];
      if (job && job.active !== false) {
        if (check(job)) {
          continue;
        }
        callWithErrorHandling(
          job,
          null,
          14
          /* ErrorCodes.SCHEDULER */
        );
      }
    }
  } finally {
    flushIndex = 0;
    queue.length = 0;
    flushPostFlushCbs(seen);
    isFlushing = false;
    currentFlushPromise = null;
    if (queue.length || pendingPostFlushCbs.length) {
      flushJobs(seen);
    }
  }
}
function checkRecursiveUpdates(seen, fn) {
  if (!seen.has(fn)) {
    seen.set(fn, 1);
  } else {
    const count = seen.get(fn);
    if (count > RECURSION_LIMIT) {
      const instance = fn.ownerInstance;
      const componentName = instance && getComponentName(instance.type);
      warn$2(`Maximum recursive updates exceeded${componentName ? ` in component <${componentName}>` : ``}. This means you have a reactive effect that is mutating its own dependencies and thus recursively triggering itself. Possible sources include component template, render function, updated hook or watcher source function.`);
      return true;
    } else {
      seen.set(fn, count + 1);
    }
  }
}
let devtools;
let buffer = [];
let devtoolsNotInstalled = false;
function emit$1(event, ...args) {
  if (devtools) {
    devtools.emit(event, ...args);
  } else if (!devtoolsNotInstalled) {
    buffer.push({ event, args });
  }
}
function setDevtoolsHook(hook, target) {
  var _a2, _b;
  devtools = hook;
  if (devtools) {
    devtools.enabled = true;
    buffer.forEach(({ event, args }) => devtools.emit(event, ...args));
    buffer = [];
  } else if (
    // handle late devtools injection - only do this if we are in an actual
    // browser environment to avoid the timer handle stalling test runner exit
    // (#4815)
    typeof window !== "undefined" && // some envs mock window but not fully
    // eslint-disable-next-line no-restricted-globals
    window.HTMLElement && // also exclude jsdom
    // eslint-disable-next-line no-restricted-globals
    !((_b = (_a2 = window.navigator) === null || _a2 === void 0 ? void 0 : _a2.userAgent) === null || _b === void 0 ? void 0 : _b.includes("jsdom"))
  ) {
    const replay = target.__VUE_DEVTOOLS_HOOK_REPLAY__ = target.__VUE_DEVTOOLS_HOOK_REPLAY__ || [];
    replay.push((newHook) => {
      setDevtoolsHook(newHook, target);
    });
    setTimeout(() => {
      if (!devtools) {
        target.__VUE_DEVTOOLS_HOOK_REPLAY__ = null;
        devtoolsNotInstalled = true;
        buffer = [];
      }
    }, 3e3);
  } else {
    devtoolsNotInstalled = true;
    buffer = [];
  }
}
function devtoolsInitApp(app, version2) {
  emit$1("app:init", app, version2, {
    Fragment,
    Text,
    Comment,
    Static
  });
}
const devtoolsComponentAdded = /* @__PURE__ */ createDevtoolsComponentHook(
  "component:added"
  /* DevtoolsHooks.COMPONENT_ADDED */
);
const devtoolsComponentUpdated = /* @__PURE__ */ createDevtoolsComponentHook(
  "component:updated"
  /* DevtoolsHooks.COMPONENT_UPDATED */
);
const _devtoolsComponentRemoved = /* @__PURE__ */ createDevtoolsComponentHook(
  "component:removed"
  /* DevtoolsHooks.COMPONENT_REMOVED */
);
const devtoolsComponentRemoved = (component) => {
  if (devtools && typeof devtools.cleanupBuffer === "function" && // remove the component if it wasn't buffered
  !devtools.cleanupBuffer(component)) {
    _devtoolsComponentRemoved(component);
  }
};
function createDevtoolsComponentHook(hook) {
  return (component) => {
    emit$1(
      hook,
      component.appContext.app,
      component.uid,
      // fixed by xxxxxx
      // 为 0 是 App，无 parent 是 Page 指向 App
      component.uid === 0 ? void 0 : component.parent ? component.parent.uid : 0,
      component
    );
  };
}
const devtoolsPerfStart = /* @__PURE__ */ createDevtoolsPerformanceHook(
  "perf:start"
  /* DevtoolsHooks.PERFORMANCE_START */
);
const devtoolsPerfEnd = /* @__PURE__ */ createDevtoolsPerformanceHook(
  "perf:end"
  /* DevtoolsHooks.PERFORMANCE_END */
);
function createDevtoolsPerformanceHook(hook) {
  return (component, type, time) => {
    emit$1(hook, component.appContext.app, component.uid, component, type, time);
  };
}
function devtoolsComponentEmit(component, event, params) {
  emit$1("component:emit", component.appContext.app, component, event, params);
}
function emit(instance, event, ...rawArgs) {
  if (instance.isUnmounted)
    return;
  const props = instance.vnode.props || EMPTY_OBJ;
  {
    const { emitsOptions, propsOptions: [propsOptions] } = instance;
    if (emitsOptions) {
      if (!(event in emitsOptions) && true) {
        if (!propsOptions || !(toHandlerKey(event) in propsOptions)) {
          warn$2(`Component emitted event "${event}" but it is neither declared in the emits option nor as an "${toHandlerKey(event)}" prop.`);
        }
      } else {
        const validator = emitsOptions[event];
        if (isFunction$1(validator)) {
          const isValid2 = validator(...rawArgs);
          if (!isValid2) {
            warn$2(`Invalid event arguments: event validation failed for event "${event}".`);
          }
        }
      }
    }
  }
  let args = rawArgs;
  const isModelListener2 = event.startsWith("update:");
  const modelArg = isModelListener2 && event.slice(7);
  if (modelArg && modelArg in props) {
    const modifiersKey = `${modelArg === "modelValue" ? "model" : modelArg}Modifiers`;
    const { number, trim } = props[modifiersKey] || EMPTY_OBJ;
    if (trim) {
      args = rawArgs.map((a) => isString$1(a) ? a.trim() : a);
    }
    if (number) {
      args = rawArgs.map(looseToNumber);
    }
  }
  {
    devtoolsComponentEmit(instance, event, args);
  }
  {
    const lowerCaseEvent = event.toLowerCase();
    if (lowerCaseEvent !== event && props[toHandlerKey(lowerCaseEvent)]) {
      warn$2(`Event "${lowerCaseEvent}" is emitted in component ${formatComponentName(instance, instance.type)} but the handler is registered for "${event}". Note that HTML attributes are case-insensitive and you cannot use v-on to listen to camelCase events when using in-DOM templates. You should probably use "${hyphenate(event)}" instead of "${event}".`);
    }
  }
  let handlerName;
  let handler = props[handlerName = toHandlerKey(event)] || // also try camelCase event handler (#2249)
  props[handlerName = toHandlerKey(camelize(event))];
  if (!handler && isModelListener2) {
    handler = props[handlerName = toHandlerKey(hyphenate(event))];
  }
  if (handler) {
    callWithAsyncErrorHandling(handler, instance, 6, args);
  }
  const onceHandler = props[handlerName + `Once`];
  if (onceHandler) {
    if (!instance.emitted) {
      instance.emitted = {};
    } else if (instance.emitted[handlerName]) {
      return;
    }
    instance.emitted[handlerName] = true;
    callWithAsyncErrorHandling(onceHandler, instance, 6, args);
  }
}
function normalizeEmitsOptions(comp, appContext, asMixin = false) {
  const cache = appContext.emitsCache;
  const cached = cache.get(comp);
  if (cached !== void 0) {
    return cached;
  }
  const raw = comp.emits;
  let normalized = {};
  let hasExtends = false;
  if (!isFunction$1(comp)) {
    const extendEmits = (raw2) => {
      const normalizedFromExtend = normalizeEmitsOptions(raw2, appContext, true);
      if (normalizedFromExtend) {
        hasExtends = true;
        extend$1(normalized, normalizedFromExtend);
      }
    };
    if (!asMixin && appContext.mixins.length) {
      appContext.mixins.forEach(extendEmits);
    }
    if (comp.extends) {
      extendEmits(comp.extends);
    }
    if (comp.mixins) {
      comp.mixins.forEach(extendEmits);
    }
  }
  if (!raw && !hasExtends) {
    if (isObject$2(comp)) {
      cache.set(comp, null);
    }
    return null;
  }
  if (isArray$2(raw)) {
    raw.forEach((key) => normalized[key] = null);
  } else {
    extend$1(normalized, raw);
  }
  if (isObject$2(comp)) {
    cache.set(comp, normalized);
  }
  return normalized;
}
function isEmitListener(options, key) {
  if (!options || !isOn(key)) {
    return false;
  }
  key = key.slice(2).replace(/Once$/, "");
  return hasOwn$1(options, key[0].toLowerCase() + key.slice(1)) || hasOwn$1(options, hyphenate(key)) || hasOwn$1(options, key);
}
let currentRenderingInstance = null;
function setCurrentRenderingInstance(instance) {
  const prev = currentRenderingInstance;
  currentRenderingInstance = instance;
  instance && instance.type.__scopeId || null;
  return prev;
}
function provide(key, value) {
  if (!currentInstance) {
    {
      warn$2(`provide() can only be used inside setup().`);
    }
  } else {
    let provides = currentInstance.provides;
    const parentProvides = currentInstance.parent && currentInstance.parent.provides;
    if (parentProvides === provides) {
      provides = currentInstance.provides = Object.create(parentProvides);
    }
    provides[key] = value;
    if (currentInstance.type.mpType === "app") {
      currentInstance.appContext.app.provide(key, value);
    }
  }
}
function inject(key, defaultValue, treatDefaultAsFactory = false) {
  const instance = currentInstance || currentRenderingInstance;
  if (instance) {
    const provides = instance.parent == null ? instance.vnode.appContext && instance.vnode.appContext.provides : instance.parent.provides;
    if (provides && key in provides) {
      return provides[key];
    } else if (arguments.length > 1) {
      return treatDefaultAsFactory && isFunction$1(defaultValue) ? defaultValue.call(instance.proxy) : defaultValue;
    } else {
      warn$2(`injection "${String(key)}" not found.`);
    }
  } else {
    warn$2(`inject() can only be used inside setup() or functional components.`);
  }
}
function watchEffect(effect, options) {
  return doWatch(effect, null, options);
}
const INITIAL_WATCHER_VALUE = {};
function watch(source, cb, options) {
  if (!isFunction$1(cb)) {
    warn$2(`\`watch(fn, options?)\` signature has been moved to a separate API. Use \`watchEffect(fn, options?)\` instead. \`watch\` now only supports \`watch(source, cb, options?) signature.`);
  }
  return doWatch(source, cb, options);
}
function doWatch(source, cb, { immediate, deep, flush, onTrack, onTrigger } = EMPTY_OBJ) {
  if (!cb) {
    if (immediate !== void 0) {
      warn$2(`watch() "immediate" option is only respected when using the watch(source, callback, options?) signature.`);
    }
    if (deep !== void 0) {
      warn$2(`watch() "deep" option is only respected when using the watch(source, callback, options?) signature.`);
    }
  }
  const warnInvalidSource = (s2) => {
    warn$2(`Invalid watch source: `, s2, `A watch source can only be a getter/effect function, a ref, a reactive object, or an array of these types.`);
  };
  const instance = getCurrentScope() === (currentInstance === null || currentInstance === void 0 ? void 0 : currentInstance.scope) ? currentInstance : null;
  let getter;
  let forceTrigger = false;
  let isMultiSource = false;
  if (isRef(source)) {
    getter = () => source.value;
    forceTrigger = isShallow(source);
  } else if (isReactive(source)) {
    getter = () => source;
    deep = true;
  } else if (isArray$2(source)) {
    isMultiSource = true;
    forceTrigger = source.some((s2) => isReactive(s2) || isShallow(s2));
    getter = () => source.map((s2) => {
      if (isRef(s2)) {
        return s2.value;
      } else if (isReactive(s2)) {
        return traverse(s2);
      } else if (isFunction$1(s2)) {
        return callWithErrorHandling(
          s2,
          instance,
          2
          /* ErrorCodes.WATCH_GETTER */
        );
      } else {
        warnInvalidSource(s2);
      }
    });
  } else if (isFunction$1(source)) {
    if (cb) {
      getter = () => callWithErrorHandling(
        source,
        instance,
        2
        /* ErrorCodes.WATCH_GETTER */
      );
    } else {
      getter = () => {
        if (instance && instance.isUnmounted) {
          return;
        }
        if (cleanup) {
          cleanup();
        }
        return callWithAsyncErrorHandling(source, instance, 3, [onCleanup]);
      };
    }
  } else {
    getter = NOOP;
    warnInvalidSource(source);
  }
  if (cb && deep) {
    const baseGetter = getter;
    getter = () => traverse(baseGetter());
  }
  let cleanup;
  let onCleanup = (fn) => {
    cleanup = effect.onStop = () => {
      callWithErrorHandling(
        fn,
        instance,
        4
        /* ErrorCodes.WATCH_CLEANUP */
      );
    };
  };
  let oldValue = isMultiSource ? new Array(source.length).fill(INITIAL_WATCHER_VALUE) : INITIAL_WATCHER_VALUE;
  const job = () => {
    if (!effect.active) {
      return;
    }
    if (cb) {
      const newValue = effect.run();
      if (deep || forceTrigger || (isMultiSource ? newValue.some((v, i) => hasChanged(v, oldValue[i])) : hasChanged(newValue, oldValue)) || false) {
        if (cleanup) {
          cleanup();
        }
        callWithAsyncErrorHandling(cb, instance, 3, [
          newValue,
          // pass undefined as the old value when it's changed for the first time
          oldValue === INITIAL_WATCHER_VALUE ? void 0 : isMultiSource && oldValue[0] === INITIAL_WATCHER_VALUE ? [] : oldValue,
          onCleanup
        ]);
        oldValue = newValue;
      }
    } else {
      effect.run();
    }
  };
  job.allowRecurse = !!cb;
  let scheduler;
  if (flush === "sync") {
    scheduler = job;
  } else if (flush === "post") {
    scheduler = () => queuePostRenderEffect$1(job, instance && instance.suspense);
  } else {
    job.pre = true;
    if (instance)
      job.id = instance.uid;
    scheduler = () => queueJob(job);
  }
  const effect = new ReactiveEffect(getter, scheduler);
  {
    effect.onTrack = onTrack;
    effect.onTrigger = onTrigger;
  }
  if (cb) {
    if (immediate) {
      job();
    } else {
      oldValue = effect.run();
    }
  } else if (flush === "post") {
    queuePostRenderEffect$1(effect.run.bind(effect), instance && instance.suspense);
  } else {
    effect.run();
  }
  const unwatch = () => {
    effect.stop();
    if (instance && instance.scope) {
      remove(instance.scope.effects, effect);
    }
  };
  return unwatch;
}
function instanceWatch(source, value, options) {
  const publicThis = this.proxy;
  const getter = isString$1(source) ? source.includes(".") ? createPathGetter(publicThis, source) : () => publicThis[source] : source.bind(publicThis, publicThis);
  let cb;
  if (isFunction$1(value)) {
    cb = value;
  } else {
    cb = value.handler;
    options = value;
  }
  const cur = currentInstance;
  setCurrentInstance(this);
  const res = doWatch(getter, cb.bind(publicThis), options);
  if (cur) {
    setCurrentInstance(cur);
  } else {
    unsetCurrentInstance();
  }
  return res;
}
function createPathGetter(ctx, path) {
  const segments = path.split(".");
  return () => {
    let cur = ctx;
    for (let i = 0; i < segments.length && cur; i++) {
      cur = cur[segments[i]];
    }
    return cur;
  };
}
function traverse(value, seen) {
  if (!isObject$2(value) || value[
    "__v_skip"
    /* ReactiveFlags.SKIP */
  ]) {
    return value;
  }
  seen = seen || /* @__PURE__ */ new Set();
  if (seen.has(value)) {
    return value;
  }
  seen.add(value);
  if (isRef(value)) {
    traverse(value.value, seen);
  } else if (isArray$2(value)) {
    for (let i = 0; i < value.length; i++) {
      traverse(value[i], seen);
    }
  } else if (isSet(value) || isMap(value)) {
    value.forEach((v) => {
      traverse(v, seen);
    });
  } else if (isPlainObject$1(value)) {
    for (const key in value) {
      traverse(value[key], seen);
    }
  }
  return value;
}
const isKeepAlive = (vnode) => vnode.type.__isKeepAlive;
function onActivated(hook, target) {
  registerKeepAliveHook(hook, "a", target);
}
function onDeactivated(hook, target) {
  registerKeepAliveHook(hook, "da", target);
}
function registerKeepAliveHook(hook, type, target = currentInstance) {
  const wrappedHook = hook.__wdc || (hook.__wdc = () => {
    let current = target;
    while (current) {
      if (current.isDeactivated) {
        return;
      }
      current = current.parent;
    }
    return hook();
  });
  injectHook(type, wrappedHook, target);
  if (target) {
    let current = target.parent;
    while (current && current.parent) {
      if (isKeepAlive(current.parent.vnode)) {
        injectToKeepAliveRoot(wrappedHook, type, target, current);
      }
      current = current.parent;
    }
  }
}
function injectToKeepAliveRoot(hook, type, target, keepAliveRoot) {
  const injected = injectHook(
    type,
    hook,
    keepAliveRoot,
    true
    /* prepend */
  );
  onUnmounted(() => {
    remove(keepAliveRoot[type], injected);
  }, target);
}
function injectHook(type, hook, target = currentInstance, prepend = false) {
  if (target) {
    if (isRootHook(type)) {
      target = target.root;
    }
    const hooks2 = target[type] || (target[type] = []);
    const wrappedHook = hook.__weh || (hook.__weh = (...args) => {
      if (target.isUnmounted) {
        return;
      }
      pauseTracking();
      setCurrentInstance(target);
      const res = callWithAsyncErrorHandling(hook, target, type, args);
      unsetCurrentInstance();
      resetTracking();
      return res;
    });
    if (prepend) {
      hooks2.unshift(wrappedHook);
    } else {
      hooks2.push(wrappedHook);
    }
    return wrappedHook;
  } else {
    const apiName = toHandlerKey((ErrorTypeStrings[type] || type.replace(/^on/, "")).replace(/ hook$/, ""));
    warn$2(`${apiName} is called when there is no active component instance to be associated with. Lifecycle injection APIs can only be used during execution of setup().`);
  }
}
const createHook$1 = (lifecycle) => (hook, target = currentInstance) => (
  // post-create lifecycle registrations are noops during SSR (except for serverPrefetch)
  (!isInSSRComponentSetup || lifecycle === "sp") && injectHook(lifecycle, (...args) => hook(...args), target)
);
const onBeforeMount = createHook$1(
  "bm"
  /* LifecycleHooks.BEFORE_MOUNT */
);
const onMounted = createHook$1(
  "m"
  /* LifecycleHooks.MOUNTED */
);
const onBeforeUpdate = createHook$1(
  "bu"
  /* LifecycleHooks.BEFORE_UPDATE */
);
const onUpdated = createHook$1(
  "u"
  /* LifecycleHooks.UPDATED */
);
const onBeforeUnmount = createHook$1(
  "bum"
  /* LifecycleHooks.BEFORE_UNMOUNT */
);
const onUnmounted = createHook$1(
  "um"
  /* LifecycleHooks.UNMOUNTED */
);
const onServerPrefetch = createHook$1(
  "sp"
  /* LifecycleHooks.SERVER_PREFETCH */
);
const onRenderTriggered = createHook$1(
  "rtg"
  /* LifecycleHooks.RENDER_TRIGGERED */
);
const onRenderTracked = createHook$1(
  "rtc"
  /* LifecycleHooks.RENDER_TRACKED */
);
function onErrorCaptured(hook, target = currentInstance) {
  injectHook("ec", hook, target);
}
function validateDirectiveName(name) {
  if (isBuiltInDirective(name)) {
    warn$2("Do not use built-in directive ids as custom directive id: " + name);
  }
}
const COMPONENTS = "components";
function resolveComponent(name, maybeSelfReference) {
  return resolveAsset(COMPONENTS, name, true, maybeSelfReference) || name;
}
function resolveAsset(type, name, warnMissing = true, maybeSelfReference = false) {
  const instance = currentRenderingInstance || currentInstance;
  if (instance) {
    const Component2 = instance.type;
    if (type === COMPONENTS) {
      const selfName = getComponentName(
        Component2,
        false
        /* do not include inferred name to avoid breaking existing code */
      );
      if (selfName && (selfName === name || selfName === camelize(name) || selfName === capitalize(camelize(name)))) {
        return Component2;
      }
    }
    const res = (
      // local registration
      // check instance[type] first which is resolved for options API
      resolve(instance[type] || Component2[type], name) || // global registration
      resolve(instance.appContext[type], name)
    );
    if (!res && maybeSelfReference) {
      return Component2;
    }
    if (warnMissing && !res) {
      const extra = type === COMPONENTS ? `
If this is a native custom element, make sure to exclude it from component resolution via compilerOptions.isCustomElement.` : ``;
      warn$2(`Failed to resolve ${type.slice(0, -1)}: ${name}${extra}`);
    }
    return res;
  } else {
    warn$2(`resolve${capitalize(type.slice(0, -1))} can only be used in render() or setup().`);
  }
}
function resolve(registry, name) {
  return registry && (registry[name] || registry[camelize(name)] || registry[capitalize(camelize(name))]);
}
const getPublicInstance = (i) => {
  if (!i)
    return null;
  if (isStatefulComponent(i))
    return getExposeProxy(i) || i.proxy;
  return getPublicInstance(i.parent);
};
const publicPropertiesMap = (
  // Move PURE marker to new line to workaround compiler discarding it
  // due to type annotation
  /* @__PURE__ */ extend$1(/* @__PURE__ */ Object.create(null), {
    $: (i) => i,
    // fixed by xxxxxx vue-i18n 在 dev 模式，访问了 $el，故模拟一个假的
    // $el: i => i.vnode.el,
    $el: (i) => i.__$el || (i.__$el = {}),
    $data: (i) => i.data,
    $props: (i) => shallowReadonly(i.props),
    $attrs: (i) => shallowReadonly(i.attrs),
    $slots: (i) => shallowReadonly(i.slots),
    $refs: (i) => shallowReadonly(i.refs),
    $parent: (i) => getPublicInstance(i.parent),
    $root: (i) => getPublicInstance(i.root),
    $emit: (i) => i.emit,
    $options: (i) => resolveMergedOptions(i),
    $forceUpdate: (i) => i.f || (i.f = () => queueJob(i.update)),
    // $nextTick: i => i.n || (i.n = nextTick.bind(i.proxy!)),// fixed by xxxxxx
    $watch: (i) => instanceWatch.bind(i)
  })
);
const isReservedPrefix = (key) => key === "_" || key === "$";
const hasSetupBinding = (state, key) => state !== EMPTY_OBJ && !state.__isScriptSetup && hasOwn$1(state, key);
const PublicInstanceProxyHandlers = {
  get({ _: instance }, key) {
    const { ctx, setupState, data, props, accessCache, type, appContext } = instance;
    if (key === "__isVue") {
      return true;
    }
    let normalizedProps;
    if (key[0] !== "$") {
      const n2 = accessCache[key];
      if (n2 !== void 0) {
        switch (n2) {
          case 1:
            return setupState[key];
          case 2:
            return data[key];
          case 4:
            return ctx[key];
          case 3:
            return props[key];
        }
      } else if (hasSetupBinding(setupState, key)) {
        accessCache[key] = 1;
        return setupState[key];
      } else if (data !== EMPTY_OBJ && hasOwn$1(data, key)) {
        accessCache[key] = 2;
        return data[key];
      } else if (
        // only cache other properties when instance has declared (thus stable)
        // props
        (normalizedProps = instance.propsOptions[0]) && hasOwn$1(normalizedProps, key)
      ) {
        accessCache[key] = 3;
        return props[key];
      } else if (ctx !== EMPTY_OBJ && hasOwn$1(ctx, key)) {
        accessCache[key] = 4;
        return ctx[key];
      } else if (shouldCacheAccess) {
        accessCache[key] = 0;
      }
    }
    const publicGetter = publicPropertiesMap[key];
    let cssModule, globalProperties;
    if (publicGetter) {
      if (key === "$attrs") {
        track(instance, "get", key);
      }
      return publicGetter(instance);
    } else if (
      // css module (injected by vue-loader)
      (cssModule = type.__cssModules) && (cssModule = cssModule[key])
    ) {
      return cssModule;
    } else if (ctx !== EMPTY_OBJ && hasOwn$1(ctx, key)) {
      accessCache[key] = 4;
      return ctx[key];
    } else if (
      // global properties
      globalProperties = appContext.config.globalProperties, hasOwn$1(globalProperties, key)
    ) {
      {
        return globalProperties[key];
      }
    } else if (currentRenderingInstance && (!isString$1(key) || // #1091 avoid internal isRef/isVNode checks on component instance leading
    // to infinite warning loop
    key.indexOf("__v") !== 0)) {
      if (data !== EMPTY_OBJ && isReservedPrefix(key[0]) && hasOwn$1(data, key)) {
        warn$2(`Property ${JSON.stringify(key)} must be accessed via $data because it starts with a reserved character ("$" or "_") and is not proxied on the render context.`);
      } else if (instance === currentRenderingInstance) {
        warn$2(`Property ${JSON.stringify(key)} was accessed during render but is not defined on instance.`);
      }
    }
  },
  set({ _: instance }, key, value) {
    const { data, setupState, ctx } = instance;
    if (hasSetupBinding(setupState, key)) {
      setupState[key] = value;
      return true;
    } else if (setupState.__isScriptSetup && hasOwn$1(setupState, key)) {
      warn$2(`Cannot mutate <script setup> binding "${key}" from Options API.`);
      return false;
    } else if (data !== EMPTY_OBJ && hasOwn$1(data, key)) {
      data[key] = value;
      return true;
    } else if (hasOwn$1(instance.props, key)) {
      warn$2(`Attempting to mutate prop "${key}". Props are readonly.`);
      return false;
    }
    if (key[0] === "$" && key.slice(1) in instance) {
      warn$2(`Attempting to mutate public property "${key}". Properties starting with $ are reserved and readonly.`);
      return false;
    } else {
      if (key in instance.appContext.config.globalProperties) {
        Object.defineProperty(ctx, key, {
          enumerable: true,
          configurable: true,
          value
        });
      } else {
        ctx[key] = value;
      }
    }
    return true;
  },
  has({ _: { data, setupState, accessCache, ctx, appContext, propsOptions } }, key) {
    let normalizedProps;
    return !!accessCache[key] || data !== EMPTY_OBJ && hasOwn$1(data, key) || hasSetupBinding(setupState, key) || (normalizedProps = propsOptions[0]) && hasOwn$1(normalizedProps, key) || hasOwn$1(ctx, key) || hasOwn$1(publicPropertiesMap, key) || hasOwn$1(appContext.config.globalProperties, key);
  },
  defineProperty(target, key, descriptor) {
    if (descriptor.get != null) {
      target._.accessCache[key] = 0;
    } else if (hasOwn$1(descriptor, "value")) {
      this.set(target, key, descriptor.value, null);
    }
    return Reflect.defineProperty(target, key, descriptor);
  }
};
{
  PublicInstanceProxyHandlers.ownKeys = (target) => {
    warn$2(`Avoid app logic that relies on enumerating keys on a component instance. The keys will be empty in production mode to avoid performance overhead.`);
    return Reflect.ownKeys(target);
  };
}
function createDevRenderContext(instance) {
  const target = {};
  Object.defineProperty(target, `_`, {
    configurable: true,
    enumerable: false,
    get: () => instance
  });
  Object.keys(publicPropertiesMap).forEach((key) => {
    Object.defineProperty(target, key, {
      configurable: true,
      enumerable: false,
      get: () => publicPropertiesMap[key](instance),
      // intercepted by the proxy so no need for implementation,
      // but needed to prevent set errors
      set: NOOP
    });
  });
  return target;
}
function exposePropsOnRenderContext(instance) {
  const { ctx, propsOptions: [propsOptions] } = instance;
  if (propsOptions) {
    Object.keys(propsOptions).forEach((key) => {
      Object.defineProperty(ctx, key, {
        enumerable: true,
        configurable: true,
        get: () => instance.props[key],
        set: NOOP
      });
    });
  }
}
function exposeSetupStateOnRenderContext(instance) {
  const { ctx, setupState } = instance;
  Object.keys(toRaw(setupState)).forEach((key) => {
    if (!setupState.__isScriptSetup) {
      if (isReservedPrefix(key[0])) {
        warn$2(`setup() return property ${JSON.stringify(key)} should not start with "$" or "_" which are reserved prefixes for Vue internals.`);
        return;
      }
      Object.defineProperty(ctx, key, {
        enumerable: true,
        configurable: true,
        get: () => setupState[key],
        set: NOOP
      });
    }
  });
}
function createDuplicateChecker() {
  const cache = /* @__PURE__ */ Object.create(null);
  return (type, key) => {
    if (cache[key]) {
      warn$2(`${type} property "${key}" is already defined in ${cache[key]}.`);
    } else {
      cache[key] = type;
    }
  };
}
let shouldCacheAccess = true;
function applyOptions$1(instance) {
  const options = resolveMergedOptions(instance);
  const publicThis = instance.proxy;
  const ctx = instance.ctx;
  shouldCacheAccess = false;
  if (options.beforeCreate) {
    callHook$1(
      options.beforeCreate,
      instance,
      "bc"
      /* LifecycleHooks.BEFORE_CREATE */
    );
  }
  const {
    // state
    data: dataOptions,
    computed: computedOptions,
    methods,
    watch: watchOptions,
    provide: provideOptions,
    inject: injectOptions,
    // lifecycle
    created,
    beforeMount,
    mounted,
    beforeUpdate,
    updated,
    activated,
    deactivated,
    beforeDestroy,
    beforeUnmount,
    destroyed,
    unmounted,
    render,
    renderTracked,
    renderTriggered,
    errorCaptured,
    serverPrefetch,
    // public API
    expose,
    inheritAttrs,
    // assets
    components,
    directives,
    filters
  } = options;
  const checkDuplicateProperties = createDuplicateChecker();
  {
    const [propsOptions] = instance.propsOptions;
    if (propsOptions) {
      for (const key in propsOptions) {
        checkDuplicateProperties("Props", key);
      }
    }
  }
  if (injectOptions) {
    resolveInjections(injectOptions, ctx, checkDuplicateProperties, instance.appContext.config.unwrapInjectedRef);
  }
  if (methods) {
    for (const key in methods) {
      const methodHandler = methods[key];
      if (isFunction$1(methodHandler)) {
        {
          Object.defineProperty(ctx, key, {
            value: methodHandler.bind(publicThis),
            configurable: true,
            enumerable: true,
            writable: true
          });
        }
        {
          checkDuplicateProperties("Methods", key);
        }
      } else {
        warn$2(`Method "${key}" has type "${typeof methodHandler}" in the component definition. Did you reference the function correctly?`);
      }
    }
  }
  if (dataOptions) {
    if (!isFunction$1(dataOptions)) {
      warn$2(`The data option must be a function. Plain object usage is no longer supported.`);
    }
    const data = dataOptions.call(publicThis, publicThis);
    if (isPromise(data)) {
      warn$2(`data() returned a Promise - note data() cannot be async; If you intend to perform data fetching before component renders, use async setup() + <Suspense>.`);
    }
    if (!isObject$2(data)) {
      warn$2(`data() should return an object.`);
    } else {
      instance.data = reactive(data);
      {
        for (const key in data) {
          checkDuplicateProperties("Data", key);
          if (!isReservedPrefix(key[0])) {
            Object.defineProperty(ctx, key, {
              configurable: true,
              enumerable: true,
              get: () => data[key],
              set: NOOP
            });
          }
        }
      }
    }
  }
  shouldCacheAccess = true;
  if (computedOptions) {
    for (const key in computedOptions) {
      const opt = computedOptions[key];
      const get2 = isFunction$1(opt) ? opt.bind(publicThis, publicThis) : isFunction$1(opt.get) ? opt.get.bind(publicThis, publicThis) : NOOP;
      if (get2 === NOOP) {
        warn$2(`Computed property "${key}" has no getter.`);
      }
      const set2 = !isFunction$1(opt) && isFunction$1(opt.set) ? opt.set.bind(publicThis) : () => {
        warn$2(`Write operation failed: computed property "${key}" is readonly.`);
      };
      const c = computed({
        get: get2,
        set: set2
      });
      Object.defineProperty(ctx, key, {
        enumerable: true,
        configurable: true,
        get: () => c.value,
        set: (v) => c.value = v
      });
      {
        checkDuplicateProperties("Computed", key);
      }
    }
  }
  if (watchOptions) {
    for (const key in watchOptions) {
      createWatcher(watchOptions[key], ctx, publicThis, key);
    }
  }
  {
    if (provideOptions) {
      const provides = isFunction$1(provideOptions) ? provideOptions.call(publicThis) : provideOptions;
      Reflect.ownKeys(provides).forEach((key) => {
        provide(key, provides[key]);
      });
    }
  }
  {
    if (created) {
      callHook$1(
        created,
        instance,
        "c"
        /* LifecycleHooks.CREATED */
      );
    }
  }
  function registerLifecycleHook(register, hook) {
    if (isArray$2(hook)) {
      hook.forEach((_hook) => register(_hook.bind(publicThis)));
    } else if (hook) {
      register(hook.bind(publicThis));
    }
  }
  registerLifecycleHook(onBeforeMount, beforeMount);
  registerLifecycleHook(onMounted, mounted);
  registerLifecycleHook(onBeforeUpdate, beforeUpdate);
  registerLifecycleHook(onUpdated, updated);
  registerLifecycleHook(onActivated, activated);
  registerLifecycleHook(onDeactivated, deactivated);
  registerLifecycleHook(onErrorCaptured, errorCaptured);
  registerLifecycleHook(onRenderTracked, renderTracked);
  registerLifecycleHook(onRenderTriggered, renderTriggered);
  registerLifecycleHook(onBeforeUnmount, beforeUnmount);
  registerLifecycleHook(onUnmounted, unmounted);
  registerLifecycleHook(onServerPrefetch, serverPrefetch);
  if (isArray$2(expose)) {
    if (expose.length) {
      const exposed = instance.exposed || (instance.exposed = {});
      expose.forEach((key) => {
        Object.defineProperty(exposed, key, {
          get: () => publicThis[key],
          set: (val) => publicThis[key] = val
        });
      });
    } else if (!instance.exposed) {
      instance.exposed = {};
    }
  }
  if (render && instance.render === NOOP) {
    instance.render = render;
  }
  if (inheritAttrs != null) {
    instance.inheritAttrs = inheritAttrs;
  }
  if (components)
    instance.components = components;
  if (directives)
    instance.directives = directives;
  if (instance.ctx.$onApplyOptions) {
    instance.ctx.$onApplyOptions(options, instance, publicThis);
  }
}
function resolveInjections(injectOptions, ctx, checkDuplicateProperties = NOOP, unwrapRef = false) {
  if (isArray$2(injectOptions)) {
    injectOptions = normalizeInject(injectOptions);
  }
  for (const key in injectOptions) {
    const opt = injectOptions[key];
    let injected;
    if (isObject$2(opt)) {
      if ("default" in opt) {
        injected = inject(
          opt.from || key,
          opt.default,
          true
          /* treat default function as factory */
        );
      } else {
        injected = inject(opt.from || key);
      }
    } else {
      injected = inject(opt);
    }
    if (isRef(injected)) {
      if (unwrapRef) {
        Object.defineProperty(ctx, key, {
          enumerable: true,
          configurable: true,
          get: () => injected.value,
          set: (v) => injected.value = v
        });
      } else {
        {
          warn$2(`injected property "${key}" is a ref and will be auto-unwrapped and no longer needs \`.value\` in the next minor release. To opt-in to the new behavior now, set \`app.config.unwrapInjectedRef = true\` (this config is temporary and will not be needed in the future.)`);
        }
        ctx[key] = injected;
      }
    } else {
      ctx[key] = injected;
    }
    {
      checkDuplicateProperties("Inject", key);
    }
  }
}
function callHook$1(hook, instance, type) {
  callWithAsyncErrorHandling(isArray$2(hook) ? hook.map((h) => h.bind(instance.proxy)) : hook.bind(instance.proxy), instance, type);
}
function createWatcher(raw, ctx, publicThis, key) {
  const getter = key.includes(".") ? createPathGetter(publicThis, key) : () => publicThis[key];
  if (isString$1(raw)) {
    const handler = ctx[raw];
    if (isFunction$1(handler)) {
      watch(getter, handler);
    } else {
      warn$2(`Invalid watch handler specified by key "${raw}"`, handler);
    }
  } else if (isFunction$1(raw)) {
    watch(getter, raw.bind(publicThis));
  } else if (isObject$2(raw)) {
    if (isArray$2(raw)) {
      raw.forEach((r) => createWatcher(r, ctx, publicThis, key));
    } else {
      const handler = isFunction$1(raw.handler) ? raw.handler.bind(publicThis) : ctx[raw.handler];
      if (isFunction$1(handler)) {
        watch(getter, handler, raw);
      } else {
        warn$2(`Invalid watch handler specified by key "${raw.handler}"`, handler);
      }
    }
  } else {
    warn$2(`Invalid watch option: "${key}"`, raw);
  }
}
function resolveMergedOptions(instance) {
  const base = instance.type;
  const { mixins, extends: extendsOptions } = base;
  const { mixins: globalMixins, optionsCache: cache, config: { optionMergeStrategies } } = instance.appContext;
  const cached = cache.get(base);
  let resolved;
  if (cached) {
    resolved = cached;
  } else if (!globalMixins.length && !mixins && !extendsOptions) {
    {
      resolved = base;
    }
  } else {
    resolved = {};
    if (globalMixins.length) {
      globalMixins.forEach((m) => mergeOptions(resolved, m, optionMergeStrategies, true));
    }
    mergeOptions(resolved, base, optionMergeStrategies);
  }
  if (isObject$2(base)) {
    cache.set(base, resolved);
  }
  return resolved;
}
function mergeOptions(to2, from2, strats, asMixin = false) {
  const { mixins, extends: extendsOptions } = from2;
  if (extendsOptions) {
    mergeOptions(to2, extendsOptions, strats, true);
  }
  if (mixins) {
    mixins.forEach((m) => mergeOptions(to2, m, strats, true));
  }
  for (const key in from2) {
    if (asMixin && key === "expose") {
      warn$2(`"expose" option is ignored when declared in mixins or extends. It should only be declared in the base component itself.`);
    } else {
      const strat = internalOptionMergeStrats[key] || strats && strats[key];
      to2[key] = strat ? strat(to2[key], from2[key]) : from2[key];
    }
  }
  return to2;
}
const internalOptionMergeStrats = {
  data: mergeDataFn,
  props: mergeObjectOptions,
  emits: mergeObjectOptions,
  // objects
  methods: mergeObjectOptions,
  computed: mergeObjectOptions,
  // lifecycle
  beforeCreate: mergeAsArray$1,
  created: mergeAsArray$1,
  beforeMount: mergeAsArray$1,
  mounted: mergeAsArray$1,
  beforeUpdate: mergeAsArray$1,
  updated: mergeAsArray$1,
  beforeDestroy: mergeAsArray$1,
  beforeUnmount: mergeAsArray$1,
  destroyed: mergeAsArray$1,
  unmounted: mergeAsArray$1,
  activated: mergeAsArray$1,
  deactivated: mergeAsArray$1,
  errorCaptured: mergeAsArray$1,
  serverPrefetch: mergeAsArray$1,
  // assets
  components: mergeObjectOptions,
  directives: mergeObjectOptions,
  // watch
  watch: mergeWatchOptions,
  // provide / inject
  provide: mergeDataFn,
  inject: mergeInject
};
function mergeDataFn(to2, from2) {
  if (!from2) {
    return to2;
  }
  if (!to2) {
    return from2;
  }
  return function mergedDataFn() {
    return extend$1(isFunction$1(to2) ? to2.call(this, this) : to2, isFunction$1(from2) ? from2.call(this, this) : from2);
  };
}
function mergeInject(to2, from2) {
  return mergeObjectOptions(normalizeInject(to2), normalizeInject(from2));
}
function normalizeInject(raw) {
  if (isArray$2(raw)) {
    const res = {};
    for (let i = 0; i < raw.length; i++) {
      res[raw[i]] = raw[i];
    }
    return res;
  }
  return raw;
}
function mergeAsArray$1(to2, from2) {
  return to2 ? [...new Set([].concat(to2, from2))] : from2;
}
function mergeObjectOptions(to2, from2) {
  return to2 ? extend$1(extend$1(/* @__PURE__ */ Object.create(null), to2), from2) : from2;
}
function mergeWatchOptions(to2, from2) {
  if (!to2)
    return from2;
  if (!from2)
    return to2;
  const merged = extend$1(/* @__PURE__ */ Object.create(null), to2);
  for (const key in from2) {
    merged[key] = mergeAsArray$1(to2[key], from2[key]);
  }
  return merged;
}
function initProps$1(instance, rawProps, isStateful, isSSR = false) {
  const props = {};
  const attrs = {};
  instance.propsDefaults = /* @__PURE__ */ Object.create(null);
  setFullProps(instance, rawProps, props, attrs);
  for (const key in instance.propsOptions[0]) {
    if (!(key in props)) {
      props[key] = void 0;
    }
  }
  {
    validateProps(rawProps || {}, props, instance);
  }
  if (isStateful) {
    instance.props = isSSR ? props : shallowReactive(props);
  } else {
    if (!instance.type.props) {
      instance.props = attrs;
    } else {
      instance.props = props;
    }
  }
  instance.attrs = attrs;
}
function isInHmrContext(instance) {
  while (instance) {
    if (instance.type.__hmrId)
      return true;
    instance = instance.parent;
  }
}
function updateProps(instance, rawProps, rawPrevProps, optimized) {
  const { props, attrs, vnode: { patchFlag } } = instance;
  const rawCurrentProps = toRaw(props);
  const [options] = instance.propsOptions;
  let hasAttrsChanged = false;
  if (
    // always force full diff in dev
    // - #1942 if hmr is enabled with sfc component
    // - vite#872 non-sfc component used by sfc component
    !isInHmrContext(instance) && (optimized || patchFlag > 0) && !(patchFlag & 16)
  ) {
    if (patchFlag & 8) {
      const propsToUpdate = instance.vnode.dynamicProps;
      for (let i = 0; i < propsToUpdate.length; i++) {
        let key = propsToUpdate[i];
        if (isEmitListener(instance.emitsOptions, key)) {
          continue;
        }
        const value = rawProps[key];
        if (options) {
          if (hasOwn$1(attrs, key)) {
            if (value !== attrs[key]) {
              attrs[key] = value;
              hasAttrsChanged = true;
            }
          } else {
            const camelizedKey = camelize(key);
            props[camelizedKey] = resolvePropValue(
              options,
              rawCurrentProps,
              camelizedKey,
              value,
              instance,
              false
              /* isAbsent */
            );
          }
        } else {
          if (value !== attrs[key]) {
            attrs[key] = value;
            hasAttrsChanged = true;
          }
        }
      }
    }
  } else {
    if (setFullProps(instance, rawProps, props, attrs)) {
      hasAttrsChanged = true;
    }
    let kebabKey;
    for (const key in rawCurrentProps) {
      if (!rawProps || // for camelCase
      !hasOwn$1(rawProps, key) && // it's possible the original props was passed in as kebab-case
      // and converted to camelCase (#955)
      ((kebabKey = hyphenate(key)) === key || !hasOwn$1(rawProps, kebabKey))) {
        if (options) {
          if (rawPrevProps && // for camelCase
          (rawPrevProps[key] !== void 0 || // for kebab-case
          rawPrevProps[kebabKey] !== void 0)) {
            props[key] = resolvePropValue(
              options,
              rawCurrentProps,
              key,
              void 0,
              instance,
              true
              /* isAbsent */
            );
          }
        } else {
          delete props[key];
        }
      }
    }
    if (attrs !== rawCurrentProps) {
      for (const key in attrs) {
        if (!rawProps || !hasOwn$1(rawProps, key) && true) {
          delete attrs[key];
          hasAttrsChanged = true;
        }
      }
    }
  }
  if (hasAttrsChanged) {
    trigger(instance, "set", "$attrs");
  }
  {
    validateProps(rawProps || {}, props, instance);
  }
}
function setFullProps(instance, rawProps, props, attrs) {
  const [options, needCastKeys] = instance.propsOptions;
  let hasAttrsChanged = false;
  let rawCastValues;
  if (rawProps) {
    for (let key in rawProps) {
      if (isReservedProp(key)) {
        continue;
      }
      const value = rawProps[key];
      let camelKey;
      if (options && hasOwn$1(options, camelKey = camelize(key))) {
        if (!needCastKeys || !needCastKeys.includes(camelKey)) {
          props[camelKey] = value;
        } else {
          (rawCastValues || (rawCastValues = {}))[camelKey] = value;
        }
      } else if (!isEmitListener(instance.emitsOptions, key)) {
        if (!(key in attrs) || value !== attrs[key]) {
          attrs[key] = value;
          hasAttrsChanged = true;
        }
      }
    }
  }
  if (needCastKeys) {
    const rawCurrentProps = toRaw(props);
    const castValues = rawCastValues || EMPTY_OBJ;
    for (let i = 0; i < needCastKeys.length; i++) {
      const key = needCastKeys[i];
      props[key] = resolvePropValue(options, rawCurrentProps, key, castValues[key], instance, !hasOwn$1(castValues, key));
    }
  }
  return hasAttrsChanged;
}
function resolvePropValue(options, props, key, value, instance, isAbsent) {
  const opt = options[key];
  if (opt != null) {
    const hasDefault = hasOwn$1(opt, "default");
    if (hasDefault && value === void 0) {
      const defaultValue = opt.default;
      if (opt.type !== Function && isFunction$1(defaultValue)) {
        const { propsDefaults } = instance;
        if (key in propsDefaults) {
          value = propsDefaults[key];
        } else {
          setCurrentInstance(instance);
          value = propsDefaults[key] = defaultValue.call(null, props);
          unsetCurrentInstance();
        }
      } else {
        value = defaultValue;
      }
    }
    if (opt[
      0
      /* BooleanFlags.shouldCast */
    ]) {
      if (isAbsent && !hasDefault) {
        value = false;
      } else if (opt[
        1
        /* BooleanFlags.shouldCastTrue */
      ] && (value === "" || value === hyphenate(key))) {
        value = true;
      }
    }
  }
  return value;
}
function normalizePropsOptions(comp, appContext, asMixin = false) {
  const cache = appContext.propsCache;
  const cached = cache.get(comp);
  if (cached) {
    return cached;
  }
  const raw = comp.props;
  const normalized = {};
  const needCastKeys = [];
  let hasExtends = false;
  if (!isFunction$1(comp)) {
    const extendProps = (raw2) => {
      hasExtends = true;
      const [props, keys2] = normalizePropsOptions(raw2, appContext, true);
      extend$1(normalized, props);
      if (keys2)
        needCastKeys.push(...keys2);
    };
    if (!asMixin && appContext.mixins.length) {
      appContext.mixins.forEach(extendProps);
    }
    if (comp.extends) {
      extendProps(comp.extends);
    }
    if (comp.mixins) {
      comp.mixins.forEach(extendProps);
    }
  }
  if (!raw && !hasExtends) {
    if (isObject$2(comp)) {
      cache.set(comp, EMPTY_ARR);
    }
    return EMPTY_ARR;
  }
  if (isArray$2(raw)) {
    for (let i = 0; i < raw.length; i++) {
      if (!isString$1(raw[i])) {
        warn$2(`props must be strings when using array syntax.`, raw[i]);
      }
      const normalizedKey = camelize(raw[i]);
      if (validatePropName(normalizedKey)) {
        normalized[normalizedKey] = EMPTY_OBJ;
      }
    }
  } else if (raw) {
    if (!isObject$2(raw)) {
      warn$2(`invalid props options`, raw);
    }
    for (const key in raw) {
      const normalizedKey = camelize(key);
      if (validatePropName(normalizedKey)) {
        const opt = raw[key];
        const prop = normalized[normalizedKey] = isArray$2(opt) || isFunction$1(opt) ? { type: opt } : Object.assign({}, opt);
        if (prop) {
          const booleanIndex = getTypeIndex(Boolean, prop.type);
          const stringIndex = getTypeIndex(String, prop.type);
          prop[
            0
            /* BooleanFlags.shouldCast */
          ] = booleanIndex > -1;
          prop[
            1
            /* BooleanFlags.shouldCastTrue */
          ] = stringIndex < 0 || booleanIndex < stringIndex;
          if (booleanIndex > -1 || hasOwn$1(prop, "default")) {
            needCastKeys.push(normalizedKey);
          }
        }
      }
    }
  }
  const res = [normalized, needCastKeys];
  if (isObject$2(comp)) {
    cache.set(comp, res);
  }
  return res;
}
function validatePropName(key) {
  if (key[0] !== "$") {
    return true;
  } else {
    warn$2(`Invalid prop name: "${key}" is a reserved property.`);
  }
  return false;
}
function getType(ctor) {
  const match = ctor && ctor.toString().match(/^\s*(function|class) (\w+)/);
  return match ? match[2] : ctor === null ? "null" : "";
}
function isSameType(a, b) {
  return getType(a) === getType(b);
}
function getTypeIndex(type, expectedTypes) {
  if (isArray$2(expectedTypes)) {
    return expectedTypes.findIndex((t2) => isSameType(t2, type));
  } else if (isFunction$1(expectedTypes)) {
    return isSameType(expectedTypes, type) ? 0 : -1;
  }
  return -1;
}
function validateProps(rawProps, props, instance) {
  const resolvedValues = toRaw(props);
  const options = instance.propsOptions[0];
  for (const key in options) {
    let opt = options[key];
    if (opt == null)
      continue;
    validateProp(key, resolvedValues[key], opt, !hasOwn$1(rawProps, key) && !hasOwn$1(rawProps, hyphenate(key)));
  }
}
function validateProp(name, value, prop, isAbsent) {
  const { type, required, validator } = prop;
  if (required && isAbsent) {
    warn$2('Missing required prop: "' + name + '"');
    return;
  }
  if (value == null && !prop.required) {
    return;
  }
  if (type != null && type !== true) {
    let isValid2 = false;
    const types = isArray$2(type) ? type : [type];
    const expectedTypes = [];
    for (let i = 0; i < types.length && !isValid2; i++) {
      const { valid, expectedType } = assertType(value, types[i]);
      expectedTypes.push(expectedType || "");
      isValid2 = valid;
    }
    if (!isValid2) {
      warn$2(getInvalidTypeMessage(name, value, expectedTypes));
      return;
    }
  }
  if (validator && !validator(value)) {
    warn$2('Invalid prop: custom validator check failed for prop "' + name + '".');
  }
}
const isSimpleType = /* @__PURE__ */ makeMap("String,Number,Boolean,Function,Symbol,BigInt");
function assertType(value, type) {
  let valid;
  const expectedType = getType(type);
  if (isSimpleType(expectedType)) {
    const t2 = typeof value;
    valid = t2 === expectedType.toLowerCase();
    if (!valid && t2 === "object") {
      valid = value instanceof type;
    }
  } else if (expectedType === "Object") {
    valid = isObject$2(value);
  } else if (expectedType === "Array") {
    valid = isArray$2(value);
  } else if (expectedType === "null") {
    valid = value === null;
  } else {
    valid = value instanceof type;
  }
  return {
    valid,
    expectedType
  };
}
function getInvalidTypeMessage(name, value, expectedTypes) {
  let message = `Invalid prop: type check failed for prop "${name}". Expected ${expectedTypes.map(capitalize).join(" | ")}`;
  const expectedType = expectedTypes[0];
  const receivedType = toRawType(value);
  const expectedValue = styleValue(value, expectedType);
  const receivedValue = styleValue(value, receivedType);
  if (expectedTypes.length === 1 && isExplicable(expectedType) && !isBoolean(expectedType, receivedType)) {
    message += ` with value ${expectedValue}`;
  }
  message += `, got ${receivedType} `;
  if (isExplicable(receivedType)) {
    message += `with value ${receivedValue}.`;
  }
  return message;
}
function styleValue(value, type) {
  if (type === "String") {
    return `"${value}"`;
  } else if (type === "Number") {
    return `${Number(value)}`;
  } else {
    return `${value}`;
  }
}
function isExplicable(type) {
  const explicitTypes = ["string", "number", "boolean"];
  return explicitTypes.some((elem) => type.toLowerCase() === elem);
}
function isBoolean(...args) {
  return args.some((elem) => elem.toLowerCase() === "boolean");
}
function createAppContext() {
  return {
    app: null,
    config: {
      isNativeTag: NO,
      performance: false,
      globalProperties: {},
      optionMergeStrategies: {},
      errorHandler: void 0,
      warnHandler: void 0,
      compilerOptions: {}
    },
    mixins: [],
    components: {},
    directives: {},
    provides: /* @__PURE__ */ Object.create(null),
    optionsCache: /* @__PURE__ */ new WeakMap(),
    propsCache: /* @__PURE__ */ new WeakMap(),
    emitsCache: /* @__PURE__ */ new WeakMap()
  };
}
let uid$1 = 0;
function createAppAPI(render, hydrate) {
  return function createApp2(rootComponent, rootProps = null) {
    if (!isFunction$1(rootComponent)) {
      rootComponent = Object.assign({}, rootComponent);
    }
    if (rootProps != null && !isObject$2(rootProps)) {
      warn$2(`root props passed to app.mount() must be an object.`);
      rootProps = null;
    }
    const context = createAppContext();
    const installedPlugins = /* @__PURE__ */ new Set();
    const app = context.app = {
      _uid: uid$1++,
      _component: rootComponent,
      _props: rootProps,
      _container: null,
      _context: context,
      _instance: null,
      version,
      get config() {
        return context.config;
      },
      set config(v) {
        {
          warn$2(`app.config cannot be replaced. Modify individual options instead.`);
        }
      },
      use(plugin2, ...options) {
        if (installedPlugins.has(plugin2)) {
          warn$2(`Plugin has already been applied to target app.`);
        } else if (plugin2 && isFunction$1(plugin2.install)) {
          installedPlugins.add(plugin2);
          plugin2.install(app, ...options);
        } else if (isFunction$1(plugin2)) {
          installedPlugins.add(plugin2);
          plugin2(app, ...options);
        } else {
          warn$2(`A plugin must either be a function or an object with an "install" function.`);
        }
        return app;
      },
      mixin(mixin) {
        {
          if (!context.mixins.includes(mixin)) {
            context.mixins.push(mixin);
          } else {
            warn$2("Mixin has already been applied to target app" + (mixin.name ? `: ${mixin.name}` : ""));
          }
        }
        return app;
      },
      component(name, component) {
        {
          validateComponentName(name, context.config);
        }
        if (!component) {
          return context.components[name];
        }
        if (context.components[name]) {
          warn$2(`Component "${name}" has already been registered in target app.`);
        }
        context.components[name] = component;
        return app;
      },
      directive(name, directive) {
        {
          validateDirectiveName(name);
        }
        if (!directive) {
          return context.directives[name];
        }
        if (context.directives[name]) {
          warn$2(`Directive "${name}" has already been registered in target app.`);
        }
        context.directives[name] = directive;
        return app;
      },
      // fixed by xxxxxx
      mount() {
      },
      // fixed by xxxxxx
      unmount() {
      },
      provide(key, value) {
        if (key in context.provides) {
          warn$2(`App already provides property with key "${String(key)}". It will be overwritten with the new value.`);
        }
        context.provides[key] = value;
        return app;
      }
    };
    return app;
  };
}
let supported;
let perf;
function startMeasure(instance, type) {
  if (instance.appContext.config.performance && isSupported()) {
    perf.mark(`vue-${type}-${instance.uid}`);
  }
  {
    devtoolsPerfStart(instance, type, isSupported() ? perf.now() : Date.now());
  }
}
function endMeasure(instance, type) {
  if (instance.appContext.config.performance && isSupported()) {
    const startTag = `vue-${type}-${instance.uid}`;
    const endTag = startTag + `:end`;
    perf.mark(endTag);
    perf.measure(`<${formatComponentName(instance, instance.type)}> ${type}`, startTag, endTag);
    perf.clearMarks(startTag);
    perf.clearMarks(endTag);
  }
  {
    devtoolsPerfEnd(instance, type, isSupported() ? perf.now() : Date.now());
  }
}
function isSupported() {
  if (supported !== void 0) {
    return supported;
  }
  if (typeof window !== "undefined" && window.performance) {
    supported = true;
    perf = window.performance;
  } else {
    supported = false;
  }
  return supported;
}
const queuePostRenderEffect$1 = queuePostFlushCb;
const Fragment = Symbol("Fragment");
const Text = Symbol("Text");
const Comment = Symbol("Comment");
const Static = Symbol("Static");
function isVNode(value) {
  return value ? value.__v_isVNode === true : false;
}
const InternalObjectKey = `__vInternal`;
function guardReactiveProps(props) {
  if (!props)
    return null;
  return isProxy(props) || InternalObjectKey in props ? extend$1({}, props) : props;
}
const emptyAppContext = createAppContext();
let uid = 0;
function createComponentInstance(vnode, parent, suspense) {
  const type = vnode.type;
  const appContext = (parent ? parent.appContext : vnode.appContext) || emptyAppContext;
  const instance = {
    uid: uid++,
    vnode,
    type,
    parent,
    appContext,
    root: null,
    next: null,
    subTree: null,
    effect: null,
    update: null,
    scope: new EffectScope(
      true
      /* detached */
    ),
    render: null,
    proxy: null,
    exposed: null,
    exposeProxy: null,
    withProxy: null,
    provides: parent ? parent.provides : Object.create(appContext.provides),
    accessCache: null,
    renderCache: [],
    // local resolved assets
    components: null,
    directives: null,
    // resolved props and emits options
    propsOptions: normalizePropsOptions(type, appContext),
    emitsOptions: normalizeEmitsOptions(type, appContext),
    // emit
    emit: null,
    emitted: null,
    // props default value
    propsDefaults: EMPTY_OBJ,
    // inheritAttrs
    inheritAttrs: type.inheritAttrs,
    // state
    ctx: EMPTY_OBJ,
    data: EMPTY_OBJ,
    props: EMPTY_OBJ,
    attrs: EMPTY_OBJ,
    slots: EMPTY_OBJ,
    refs: EMPTY_OBJ,
    setupState: EMPTY_OBJ,
    setupContext: null,
    // suspense related
    suspense,
    suspenseId: suspense ? suspense.pendingId : 0,
    asyncDep: null,
    asyncResolved: false,
    // lifecycle hooks
    // not using enums here because it results in computed properties
    isMounted: false,
    isUnmounted: false,
    isDeactivated: false,
    bc: null,
    c: null,
    bm: null,
    m: null,
    bu: null,
    u: null,
    um: null,
    bum: null,
    da: null,
    a: null,
    rtg: null,
    rtc: null,
    ec: null,
    sp: null
  };
  {
    instance.ctx = createDevRenderContext(instance);
  }
  instance.root = parent ? parent.root : instance;
  instance.emit = emit.bind(null, instance);
  if (vnode.ce) {
    vnode.ce(instance);
  }
  return instance;
}
let currentInstance = null;
const getCurrentInstance = () => currentInstance || currentRenderingInstance;
const setCurrentInstance = (instance) => {
  currentInstance = instance;
  instance.scope.on();
};
const unsetCurrentInstance = () => {
  currentInstance && currentInstance.scope.off();
  currentInstance = null;
};
const isBuiltInTag = /* @__PURE__ */ makeMap("slot,component");
function validateComponentName(name, config) {
  const appIsNativeTag = config.isNativeTag || NO;
  if (isBuiltInTag(name) || appIsNativeTag(name)) {
    warn$2("Do not use built-in or reserved HTML elements as component id: " + name);
  }
}
function isStatefulComponent(instance) {
  return instance.vnode.shapeFlag & 4;
}
let isInSSRComponentSetup = false;
function setupComponent(instance, isSSR = false) {
  isInSSRComponentSetup = isSSR;
  const {
    props
    /*, children*/
  } = instance.vnode;
  const isStateful = isStatefulComponent(instance);
  initProps$1(instance, props, isStateful, isSSR);
  const setupResult = isStateful ? setupStatefulComponent(instance, isSSR) : void 0;
  isInSSRComponentSetup = false;
  return setupResult;
}
function setupStatefulComponent(instance, isSSR) {
  const Component2 = instance.type;
  {
    if (Component2.name) {
      validateComponentName(Component2.name, instance.appContext.config);
    }
    if (Component2.components) {
      const names = Object.keys(Component2.components);
      for (let i = 0; i < names.length; i++) {
        validateComponentName(names[i], instance.appContext.config);
      }
    }
    if (Component2.directives) {
      const names = Object.keys(Component2.directives);
      for (let i = 0; i < names.length; i++) {
        validateDirectiveName(names[i]);
      }
    }
    if (Component2.compilerOptions && isRuntimeOnly()) {
      warn$2(`"compilerOptions" is only supported when using a build of Vue that includes the runtime compiler. Since you are using a runtime-only build, the options should be passed via your build tool config instead.`);
    }
  }
  instance.accessCache = /* @__PURE__ */ Object.create(null);
  instance.proxy = markRaw(new Proxy(instance.ctx, PublicInstanceProxyHandlers));
  {
    exposePropsOnRenderContext(instance);
  }
  const { setup } = Component2;
  if (setup) {
    const setupContext = instance.setupContext = setup.length > 1 ? createSetupContext(instance) : null;
    setCurrentInstance(instance);
    pauseTracking();
    const setupResult = callWithErrorHandling(setup, instance, 0, [shallowReadonly(instance.props), setupContext]);
    resetTracking();
    unsetCurrentInstance();
    if (isPromise(setupResult)) {
      setupResult.then(unsetCurrentInstance, unsetCurrentInstance);
      {
        warn$2(`setup() returned a Promise, but the version of Vue you are using does not support it yet.`);
      }
    } else {
      handleSetupResult(instance, setupResult, isSSR);
    }
  } else {
    finishComponentSetup(instance, isSSR);
  }
}
function handleSetupResult(instance, setupResult, isSSR) {
  if (isFunction$1(setupResult)) {
    {
      instance.render = setupResult;
    }
  } else if (isObject$2(setupResult)) {
    if (isVNode(setupResult)) {
      warn$2(`setup() should not return VNodes directly - return a render function instead.`);
    }
    {
      instance.devtoolsRawSetupState = setupResult;
    }
    instance.setupState = proxyRefs(setupResult);
    {
      exposeSetupStateOnRenderContext(instance);
    }
  } else if (setupResult !== void 0) {
    warn$2(`setup() should return an object. Received: ${setupResult === null ? "null" : typeof setupResult}`);
  }
  finishComponentSetup(instance, isSSR);
}
let compile;
const isRuntimeOnly = () => !compile;
function finishComponentSetup(instance, isSSR, skipOptions) {
  const Component2 = instance.type;
  if (!instance.render) {
    instance.render = Component2.render || NOOP;
  }
  {
    setCurrentInstance(instance);
    pauseTracking();
    applyOptions$1(instance);
    resetTracking();
    unsetCurrentInstance();
  }
  if (!Component2.render && instance.render === NOOP && !isSSR) {
    if (Component2.template) {
      warn$2(
        `Component provided template option but runtime compilation is not supported in this build of Vue. Configure your bundler to alias "vue" to "vue/dist/vue.esm-bundler.js".`
        /* should not happen */
      );
    } else {
      warn$2(`Component is missing template or render function.`);
    }
  }
}
function createAttrsProxy(instance) {
  return new Proxy(
    instance.attrs,
    {
      get(target, key) {
        track(instance, "get", "$attrs");
        return target[key];
      },
      set() {
        warn$2(`setupContext.attrs is readonly.`);
        return false;
      },
      deleteProperty() {
        warn$2(`setupContext.attrs is readonly.`);
        return false;
      }
    }
  );
}
function createSetupContext(instance) {
  const expose = (exposed) => {
    {
      if (instance.exposed) {
        warn$2(`expose() should be called only once per setup().`);
      }
      if (exposed != null) {
        let exposedType = typeof exposed;
        if (exposedType === "object") {
          if (isArray$2(exposed)) {
            exposedType = "array";
          } else if (isRef(exposed)) {
            exposedType = "ref";
          }
        }
        if (exposedType !== "object") {
          warn$2(`expose() should be passed a plain object, received ${exposedType}.`);
        }
      }
    }
    instance.exposed = exposed || {};
  };
  let attrs;
  {
    return Object.freeze({
      get attrs() {
        return attrs || (attrs = createAttrsProxy(instance));
      },
      get slots() {
        return shallowReadonly(instance.slots);
      },
      get emit() {
        return (event, ...args) => instance.emit(event, ...args);
      },
      expose
    });
  }
}
function getExposeProxy(instance) {
  if (instance.exposed) {
    return instance.exposeProxy || (instance.exposeProxy = new Proxy(proxyRefs(markRaw(instance.exposed)), {
      get(target, key) {
        if (key in target) {
          return target[key];
        }
        return instance.proxy[key];
      },
      has(target, key) {
        return key in target || key in publicPropertiesMap;
      }
    }));
  }
}
const classifyRE = /(?:^|[-_])(\w)/g;
const classify = (str) => str.replace(classifyRE, (c) => c.toUpperCase()).replace(/[-_]/g, "");
function getComponentName(Component2, includeInferred = true) {
  return isFunction$1(Component2) ? Component2.displayName || Component2.name : Component2.name || includeInferred && Component2.__name;
}
function formatComponentName(instance, Component2, isRoot = false) {
  let name = getComponentName(Component2);
  if (!name && Component2.__file) {
    const match = Component2.__file.match(/([^/\\]+)\.\w+$/);
    if (match) {
      name = match[1];
    }
  }
  if (!name && instance && instance.parent) {
    const inferFromRegistry = (registry) => {
      for (const key in registry) {
        if (registry[key] === Component2) {
          return key;
        }
      }
    };
    name = inferFromRegistry(instance.components || instance.parent.type.components) || inferFromRegistry(instance.appContext.components);
  }
  return name ? classify(name) : isRoot ? `App` : `Anonymous`;
}
const computed = (getterOrOptions, debugOptions) => {
  return computed$1(getterOrOptions, debugOptions, isInSSRComponentSetup);
};
const version = "3.2.47";
function unwrapper(target) {
  return unref(target);
}
const ARRAYTYPE = "[object Array]";
const OBJECTTYPE = "[object Object]";
function diff$1(current, pre) {
  const result = {};
  syncKeys(current, pre);
  _diff(current, pre, "", result);
  return result;
}
function syncKeys(current, pre) {
  current = unwrapper(current);
  if (current === pre)
    return;
  const rootCurrentType = toTypeString(current);
  const rootPreType = toTypeString(pre);
  if (rootCurrentType == OBJECTTYPE && rootPreType == OBJECTTYPE) {
    for (let key in pre) {
      const currentValue = current[key];
      if (currentValue === void 0) {
        current[key] = null;
      } else {
        syncKeys(currentValue, pre[key]);
      }
    }
  } else if (rootCurrentType == ARRAYTYPE && rootPreType == ARRAYTYPE) {
    if (current.length >= pre.length) {
      pre.forEach((item, index2) => {
        syncKeys(current[index2], item);
      });
    }
  }
}
function _diff(current, pre, path, result) {
  current = unwrapper(current);
  if (current === pre)
    return;
  const rootCurrentType = toTypeString(current);
  const rootPreType = toTypeString(pre);
  if (rootCurrentType == OBJECTTYPE) {
    if (rootPreType != OBJECTTYPE || Object.keys(current).length < Object.keys(pre).length) {
      setResult(result, path, current);
    } else {
      for (let key in current) {
        const currentValue = unwrapper(current[key]);
        const preValue = pre[key];
        const currentType = toTypeString(currentValue);
        const preType = toTypeString(preValue);
        if (currentType != ARRAYTYPE && currentType != OBJECTTYPE) {
          if (currentValue != preValue) {
            setResult(result, (path == "" ? "" : path + ".") + key, currentValue);
          }
        } else if (currentType == ARRAYTYPE) {
          if (preType != ARRAYTYPE) {
            setResult(result, (path == "" ? "" : path + ".") + key, currentValue);
          } else {
            if (currentValue.length < preValue.length) {
              setResult(result, (path == "" ? "" : path + ".") + key, currentValue);
            } else {
              currentValue.forEach((item, index2) => {
                _diff(item, preValue[index2], (path == "" ? "" : path + ".") + key + "[" + index2 + "]", result);
              });
            }
          }
        } else if (currentType == OBJECTTYPE) {
          if (preType != OBJECTTYPE || Object.keys(currentValue).length < Object.keys(preValue).length) {
            setResult(result, (path == "" ? "" : path + ".") + key, currentValue);
          } else {
            for (let subKey in currentValue) {
              _diff(currentValue[subKey], preValue[subKey], (path == "" ? "" : path + ".") + key + "." + subKey, result);
            }
          }
        }
      }
    }
  } else if (rootCurrentType == ARRAYTYPE) {
    if (rootPreType != ARRAYTYPE) {
      setResult(result, path, current);
    } else {
      if (current.length < pre.length) {
        setResult(result, path, current);
      } else {
        current.forEach((item, index2) => {
          _diff(item, pre[index2], path + "[" + index2 + "]", result);
        });
      }
    }
  } else {
    setResult(result, path, current);
  }
}
function setResult(result, k, v) {
  result[k] = v;
}
function hasComponentEffect(instance) {
  return queue.includes(instance.update);
}
function flushCallbacks(instance) {
  const ctx = instance.ctx;
  const callbacks = ctx.__next_tick_callbacks;
  if (callbacks && callbacks.length) {
    const copies = callbacks.slice(0);
    callbacks.length = 0;
    for (let i = 0; i < copies.length; i++) {
      copies[i]();
    }
  }
}
function nextTick(instance, fn) {
  const ctx = instance.ctx;
  if (!ctx.__next_tick_pending && !hasComponentEffect(instance)) {
    return nextTick$1(fn && fn.bind(instance.proxy));
  }
  let _resolve;
  if (!ctx.__next_tick_callbacks) {
    ctx.__next_tick_callbacks = [];
  }
  ctx.__next_tick_callbacks.push(() => {
    if (fn) {
      callWithErrorHandling(
        fn.bind(instance.proxy),
        instance,
        14
        /* ErrorCodes.SCHEDULER */
      );
    } else if (_resolve) {
      _resolve(instance.proxy);
    }
  });
  return new Promise((resolve2) => {
    _resolve = resolve2;
  });
}
function clone$2(src, seen) {
  src = unwrapper(src);
  const type = typeof src;
  if (type === "object" && src !== null) {
    let copy = seen.get(src);
    if (typeof copy !== "undefined") {
      return copy;
    }
    if (isArray$2(src)) {
      const len = src.length;
      copy = new Array(len);
      seen.set(src, copy);
      for (let i = 0; i < len; i++) {
        copy[i] = clone$2(src[i], seen);
      }
    } else {
      copy = {};
      seen.set(src, copy);
      for (const name in src) {
        if (hasOwn$1(src, name)) {
          copy[name] = clone$2(src[name], seen);
        }
      }
    }
    return copy;
  }
  if (type !== "symbol") {
    return src;
  }
}
function deepCopy(src) {
  return clone$2(src, typeof WeakMap !== "undefined" ? /* @__PURE__ */ new WeakMap() : /* @__PURE__ */ new Map());
}
function getMPInstanceData(instance, keys2) {
  const data = instance.data;
  const ret = /* @__PURE__ */ Object.create(null);
  keys2.forEach((key) => {
    ret[key] = data[key];
  });
  return ret;
}
function patch(instance, data, oldData) {
  if (!data) {
    return;
  }
  data = deepCopy(data);
  const ctx = instance.ctx;
  const mpType = ctx.mpType;
  if (mpType === "page" || mpType === "component") {
    data.r0 = 1;
    const mpInstance = ctx.$scope;
    const keys2 = Object.keys(data);
    const diffData = diff$1(data, oldData || getMPInstanceData(mpInstance, keys2));
    if (Object.keys(diffData).length) {
      ctx.__next_tick_pending = true;
      mpInstance.setData(diffData, () => {
        ctx.__next_tick_pending = false;
        flushCallbacks(instance);
      });
      flushPreFlushCbs();
    } else {
      flushCallbacks(instance);
    }
  }
}
function initAppConfig(appConfig) {
  appConfig.globalProperties.$nextTick = function $nextTick(fn) {
    return nextTick(this.$, fn);
  };
}
function onApplyOptions(options, instance, publicThis) {
  instance.appContext.config.globalProperties.$applyOptions(options, instance, publicThis);
  const computedOptions = options.computed;
  if (computedOptions) {
    const keys2 = Object.keys(computedOptions);
    if (keys2.length) {
      const ctx = instance.ctx;
      if (!ctx.$computedKeys) {
        ctx.$computedKeys = [];
      }
      ctx.$computedKeys.push(...keys2);
    }
  }
  delete instance.ctx.$onApplyOptions;
}
function setRef$1(instance, isUnmount = false) {
  const { setupState, $templateRefs, ctx: { $scope, $mpPlatform } } = instance;
  if ($mpPlatform === "mp-alipay") {
    return;
  }
  if (!$templateRefs || !$scope) {
    return;
  }
  if (isUnmount) {
    return $templateRefs.forEach((templateRef) => setTemplateRef(templateRef, null, setupState));
  }
  const check = $mpPlatform === "mp-baidu" || $mpPlatform === "mp-toutiao";
  const doSetByRefs = (refs) => {
    const mpComponents = (
      // 字节小程序 selectAllComponents 可能返回 null
      // https://github.com/dcloudio/uni-app/issues/3954
      ($scope.selectAllComponents(".r") || []).concat($scope.selectAllComponents(".r-i-f") || [])
    );
    return refs.filter((templateRef) => {
      const refValue = findComponentPublicInstance(mpComponents, templateRef.i);
      if (check && refValue === null) {
        return true;
      }
      setTemplateRef(templateRef, refValue, setupState);
      return false;
    });
  };
  const doSet = () => {
    const refs = doSetByRefs($templateRefs);
    if (refs.length && instance.proxy && instance.proxy.$scope) {
      instance.proxy.$scope.setData({ r1: 1 }, () => {
        doSetByRefs(refs);
      });
    }
  };
  if ($scope._$setRef) {
    $scope._$setRef(doSet);
  } else {
    nextTick(instance, doSet);
  }
}
function toSkip(value) {
  if (isObject$2(value)) {
    markRaw(value);
  }
  return value;
}
function findComponentPublicInstance(mpComponents, id) {
  const mpInstance = mpComponents.find((com) => com && (com.properties || com.props).uI === id);
  if (mpInstance) {
    const vm = mpInstance.$vm;
    if (vm) {
      return getExposeProxy(vm.$) || vm;
    }
    return toSkip(mpInstance);
  }
  return null;
}
function setTemplateRef({ r, f: f2 }, refValue, setupState) {
  if (isFunction$1(r)) {
    r(refValue, {});
  } else {
    const _isString = isString$1(r);
    const _isRef = isRef(r);
    if (_isString || _isRef) {
      if (f2) {
        if (!_isRef) {
          return;
        }
        if (!isArray$2(r.value)) {
          r.value = [];
        }
        const existing = r.value;
        if (existing.indexOf(refValue) === -1) {
          existing.push(refValue);
          if (!refValue) {
            return;
          }
          onBeforeUnmount(() => remove(existing, refValue), refValue.$);
        }
      } else if (_isString) {
        if (hasOwn$1(setupState, r)) {
          setupState[r] = refValue;
        }
      } else if (isRef(r)) {
        r.value = refValue;
      } else {
        warnRef(r);
      }
    } else {
      warnRef(r);
    }
  }
}
function warnRef(ref2) {
  warn$2("Invalid template ref type:", ref2, `(${typeof ref2})`);
}
var MPType;
(function(MPType2) {
  MPType2["APP"] = "app";
  MPType2["PAGE"] = "page";
  MPType2["COMPONENT"] = "component";
})(MPType || (MPType = {}));
const queuePostRenderEffect = queuePostFlushCb;
function mountComponent(initialVNode, options) {
  const instance = initialVNode.component = createComponentInstance(initialVNode, options.parentComponent, null);
  {
    instance.ctx.$onApplyOptions = onApplyOptions;
    instance.ctx.$children = [];
  }
  if (options.mpType === "app") {
    instance.render = NOOP;
  }
  if (options.onBeforeSetup) {
    options.onBeforeSetup(instance, options);
  }
  {
    pushWarningContext(initialVNode);
    startMeasure(instance, `mount`);
  }
  {
    startMeasure(instance, `init`);
  }
  setupComponent(instance);
  {
    endMeasure(instance, `init`);
  }
  {
    if (options.parentComponent && instance.proxy) {
      options.parentComponent.ctx.$children.push(getExposeProxy(instance) || instance.proxy);
    }
  }
  setupRenderEffect(instance);
  {
    popWarningContext();
    endMeasure(instance, `mount`);
  }
  return instance.proxy;
}
const getFunctionalFallthrough = (attrs) => {
  let res;
  for (const key in attrs) {
    if (key === "class" || key === "style" || isOn(key)) {
      (res || (res = {}))[key] = attrs[key];
    }
  }
  return res;
};
function renderComponentRoot(instance) {
  const { type: Component2, vnode, proxy, withProxy, props, propsOptions: [propsOptions], slots, attrs, emit: emit2, render, renderCache, data, setupState, ctx, uid: uid2, appContext: { app: { config: { globalProperties: { pruneComponentPropsCache: pruneComponentPropsCache2 } } } }, inheritAttrs } = instance;
  instance.$templateRefs = [];
  instance.$ei = 0;
  pruneComponentPropsCache2(uid2);
  instance.__counter = instance.__counter === 0 ? 1 : 0;
  let result;
  const prev = setCurrentRenderingInstance(instance);
  try {
    if (vnode.shapeFlag & 4) {
      fallthroughAttrs(inheritAttrs, props, propsOptions, attrs);
      const proxyToUse = withProxy || proxy;
      result = render.call(proxyToUse, proxyToUse, renderCache, props, setupState, data, ctx);
    } else {
      fallthroughAttrs(inheritAttrs, props, propsOptions, Component2.props ? attrs : getFunctionalFallthrough(attrs));
      const render2 = Component2;
      result = render2.length > 1 ? render2(props, { attrs, slots, emit: emit2 }) : render2(
        props,
        null
        /* we know it doesn't need it */
      );
    }
  } catch (err) {
    handleError(
      err,
      instance,
      1
      /* ErrorCodes.RENDER_FUNCTION */
    );
    result = false;
  }
  setRef$1(instance);
  setCurrentRenderingInstance(prev);
  return result;
}
function fallthroughAttrs(inheritAttrs, props, propsOptions, fallthroughAttrs2) {
  if (props && fallthroughAttrs2 && inheritAttrs !== false) {
    const keys2 = Object.keys(fallthroughAttrs2).filter((key) => key !== "class" && key !== "style");
    if (!keys2.length) {
      return;
    }
    if (propsOptions && keys2.some(isModelListener)) {
      keys2.forEach((key) => {
        if (!isModelListener(key) || !(key.slice(9) in propsOptions)) {
          props[key] = fallthroughAttrs2[key];
        }
      });
    } else {
      keys2.forEach((key) => props[key] = fallthroughAttrs2[key]);
    }
  }
}
const updateComponentPreRender = (instance) => {
  pauseTracking();
  flushPreFlushCbs();
  resetTracking();
};
function componentUpdateScopedSlotsFn() {
  const scopedSlotsData = this.$scopedSlotsData;
  if (!scopedSlotsData || scopedSlotsData.length === 0) {
    return;
  }
  const mpInstance = this.ctx.$scope;
  const oldData = mpInstance.data;
  const diffData = /* @__PURE__ */ Object.create(null);
  scopedSlotsData.forEach(({ path, index: index2, data }) => {
    const oldScopedSlotData = getValueByDataPath(oldData, path);
    const diffPath = isString$1(index2) ? `${path}.${index2}` : `${path}[${index2}]`;
    if (typeof oldScopedSlotData === "undefined" || typeof oldScopedSlotData[index2] === "undefined") {
      diffData[diffPath] = data;
    } else {
      const diffScopedSlotData = diff$1(data, oldScopedSlotData[index2]);
      Object.keys(diffScopedSlotData).forEach((name) => {
        diffData[diffPath + "." + name] = diffScopedSlotData[name];
      });
    }
  });
  scopedSlotsData.length = 0;
  if (Object.keys(diffData).length) {
    mpInstance.setData(diffData);
  }
}
function toggleRecurse({ effect, update }, allowed) {
  effect.allowRecurse = update.allowRecurse = allowed;
}
function setupRenderEffect(instance) {
  const updateScopedSlots = componentUpdateScopedSlotsFn.bind(instance);
  instance.$updateScopedSlots = () => nextTick$1(() => queueJob(updateScopedSlots));
  const componentUpdateFn = () => {
    if (!instance.isMounted) {
      onBeforeUnmount(() => {
        setRef$1(instance, true);
      }, instance);
      {
        startMeasure(instance, `patch`);
      }
      patch(instance, renderComponentRoot(instance));
      {
        endMeasure(instance, `patch`);
      }
      {
        devtoolsComponentAdded(instance);
      }
    } else {
      const { next, bu, u } = instance;
      {
        pushWarningContext(next || instance.vnode);
      }
      toggleRecurse(instance, false);
      updateComponentPreRender();
      if (bu) {
        invokeArrayFns$1(bu);
      }
      toggleRecurse(instance, true);
      {
        startMeasure(instance, `patch`);
      }
      patch(instance, renderComponentRoot(instance));
      {
        endMeasure(instance, `patch`);
      }
      if (u) {
        queuePostRenderEffect(u);
      }
      {
        devtoolsComponentUpdated(instance);
      }
      {
        popWarningContext();
      }
    }
  };
  const effect = instance.effect = new ReactiveEffect(
    componentUpdateFn,
    () => queueJob(instance.update),
    instance.scope
    // track it in component's effect scope
  );
  const update = instance.update = effect.run.bind(effect);
  update.id = instance.uid;
  toggleRecurse(instance, true);
  {
    effect.onTrack = instance.rtc ? (e2) => invokeArrayFns$1(instance.rtc, e2) : void 0;
    effect.onTrigger = instance.rtg ? (e2) => invokeArrayFns$1(instance.rtg, e2) : void 0;
    update.ownerInstance = instance;
  }
  update();
}
function unmountComponent(instance) {
  const { bum, scope, update, um } = instance;
  if (bum) {
    invokeArrayFns$1(bum);
  }
  scope.stop();
  if (update) {
    update.active = false;
  }
  if (um) {
    queuePostRenderEffect(um);
  }
  queuePostRenderEffect(() => {
    instance.isUnmounted = true;
  });
  {
    devtoolsComponentRemoved(instance);
  }
}
const oldCreateApp = createAppAPI();
function getTarget() {
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  if (typeof my !== "undefined") {
    return my;
  }
}
function createVueApp(rootComponent, rootProps = null) {
  const target = getTarget();
  target.__VUE__ = true;
  {
    setDevtoolsHook(target.__VUE_DEVTOOLS_GLOBAL_HOOK__, target);
  }
  const app = oldCreateApp(rootComponent, rootProps);
  const appContext = app._context;
  initAppConfig(appContext.config);
  const createVNode = (initialVNode) => {
    initialVNode.appContext = appContext;
    initialVNode.shapeFlag = 6;
    return initialVNode;
  };
  const createComponent2 = function createComponent3(initialVNode, options) {
    return mountComponent(createVNode(initialVNode), options);
  };
  const destroyComponent = function destroyComponent2(component) {
    return component && unmountComponent(component.$);
  };
  app.mount = function mount() {
    rootComponent.render = NOOP;
    const instance = mountComponent(createVNode({ type: rootComponent }), {
      mpType: MPType.APP,
      mpInstance: null,
      parentComponent: null,
      slots: [],
      props: null
    });
    app._instance = instance.$;
    {
      devtoolsInitApp(app, version);
    }
    instance.$app = app;
    instance.$createComponent = createComponent2;
    instance.$destroyComponent = destroyComponent;
    appContext.$appInstance = instance;
    return instance;
  };
  app.unmount = function unmount() {
    warn$2(`Cannot unmount an app.`);
  };
  return app;
}
function useCssVars(getter) {
  const instance = getCurrentInstance();
  if (!instance) {
    warn$2(`useCssVars is called without current active component instance.`);
    return;
  }
  initCssVarsRender(instance, getter);
}
function initCssVarsRender(instance, getter) {
  instance.ctx.__cssVars = () => {
    const vars = getter(instance.proxy);
    const cssVars = {};
    for (const key in vars) {
      cssVars[`--${key}`] = vars[key];
    }
    return cssVars;
  };
}
function injectLifecycleHook(name, hook, publicThis, instance) {
  if (isFunction$1(hook)) {
    injectHook(name, hook.bind(publicThis), instance);
  }
}
function initHooks$1(options, instance, publicThis) {
  const mpType = options.mpType || publicThis.$mpType;
  if (!mpType || mpType === "component") {
    return;
  }
  Object.keys(options).forEach((name) => {
    if (isUniLifecycleHook(name, options[name], false)) {
      const hooks2 = options[name];
      if (isArray$2(hooks2)) {
        hooks2.forEach((hook) => injectLifecycleHook(name, hook, publicThis, instance));
      } else {
        injectLifecycleHook(name, hooks2, publicThis, instance);
      }
    }
  });
}
function applyOptions$2(options, instance, publicThis) {
  initHooks$1(options, instance, publicThis);
}
function set$3(target, key, val) {
  return target[key] = val;
}
function createErrorHandler(app) {
  return function errorHandler(err, instance, _info) {
    if (!instance) {
      throw err;
    }
    const appInstance = app._instance;
    if (!appInstance || !appInstance.proxy) {
      throw err;
    }
    {
      appInstance.proxy.$callHook(ON_ERROR, err);
    }
  };
}
function mergeAsArray(to2, from2) {
  return to2 ? [...new Set([].concat(to2, from2))] : from2;
}
function initOptionMergeStrategies(optionMergeStrategies) {
  UniLifecycleHooks.forEach((name) => {
    optionMergeStrategies[name] = mergeAsArray;
  });
}
let realAtob;
const b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
const b64re = /^(?:[A-Za-z\d+/]{4})*?(?:[A-Za-z\d+/]{2}(?:==)?|[A-Za-z\d+/]{3}=?)?$/;
if (typeof atob !== "function") {
  realAtob = function(str) {
    str = String(str).replace(/[\t\n\f\r ]+/g, "");
    if (!b64re.test(str)) {
      throw new Error("Failed to execute 'atob' on 'Window': The string to be decoded is not correctly encoded.");
    }
    str += "==".slice(2 - (str.length & 3));
    var bitmap;
    var result = "";
    var r1;
    var r2;
    var i = 0;
    for (; i < str.length; ) {
      bitmap = b64.indexOf(str.charAt(i++)) << 18 | b64.indexOf(str.charAt(i++)) << 12 | (r1 = b64.indexOf(str.charAt(i++))) << 6 | (r2 = b64.indexOf(str.charAt(i++)));
      result += r1 === 64 ? String.fromCharCode(bitmap >> 16 & 255) : r2 === 64 ? String.fromCharCode(bitmap >> 16 & 255, bitmap >> 8 & 255) : String.fromCharCode(bitmap >> 16 & 255, bitmap >> 8 & 255, bitmap & 255);
    }
    return result;
  };
} else {
  realAtob = atob;
}
function b64DecodeUnicode(str) {
  return decodeURIComponent(realAtob(str).split("").map(function(c) {
    return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(""));
}
function getCurrentUserInfo() {
  const token2 = index.getStorageSync("uni_id_token") || "";
  const tokenArr = token2.split(".");
  if (!token2 || tokenArr.length !== 3) {
    return {
      uid: null,
      role: [],
      permission: [],
      tokenExpired: 0
    };
  }
  let userInfo;
  try {
    userInfo = JSON.parse(b64DecodeUnicode(tokenArr[1]));
  } catch (error) {
    throw new Error("获取当前用户信息出错，详细错误信息为：" + error.message);
  }
  userInfo.tokenExpired = userInfo.exp * 1e3;
  delete userInfo.exp;
  delete userInfo.iat;
  return userInfo;
}
function uniIdMixin(globalProperties) {
  globalProperties.uniIDHasRole = function(roleId) {
    const { role } = getCurrentUserInfo();
    return role.indexOf(roleId) > -1;
  };
  globalProperties.uniIDHasPermission = function(permissionId) {
    const { permission } = getCurrentUserInfo();
    return this.uniIDHasRole("admin") || permission.indexOf(permissionId) > -1;
  };
  globalProperties.uniIDTokenValid = function() {
    const { tokenExpired } = getCurrentUserInfo();
    return tokenExpired > Date.now();
  };
}
function initApp(app) {
  const appConfig = app._context.config;
  appConfig.errorHandler = invokeCreateErrorHandler(app, createErrorHandler);
  initOptionMergeStrategies(appConfig.optionMergeStrategies);
  const globalProperties = appConfig.globalProperties;
  {
    uniIdMixin(globalProperties);
  }
  {
    globalProperties.$set = set$3;
    globalProperties.$applyOptions = applyOptions$2;
  }
  {
    index.invokeCreateVueAppHook(app);
  }
}
const propsCaches = /* @__PURE__ */ Object.create(null);
function renderProps(props) {
  const { uid: uid2, __counter } = getCurrentInstance();
  const propsId = (propsCaches[uid2] || (propsCaches[uid2] = [])).push(guardReactiveProps(props)) - 1;
  return uid2 + "," + propsId + "," + __counter;
}
function pruneComponentPropsCache(uid2) {
  delete propsCaches[uid2];
}
function findComponentPropsData(up) {
  if (!up) {
    return;
  }
  const [uid2, propsId] = up.split(",");
  if (!propsCaches[uid2]) {
    return;
  }
  return propsCaches[uid2][parseInt(propsId)];
}
var plugin = {
  install(app) {
    initApp(app);
    app.config.globalProperties.pruneComponentPropsCache = pruneComponentPropsCache;
    const oldMount = app.mount;
    app.mount = function mount(rootContainer) {
      const instance = oldMount.call(app, rootContainer);
      const createApp2 = getCreateApp();
      if (createApp2) {
        createApp2(instance);
      } else {
        if (typeof createMiniProgramApp !== "undefined") {
          createMiniProgramApp(instance);
        }
      }
      return instance;
    };
  }
};
function getCreateApp() {
  const method = "createApp";
  if (typeof global !== "undefined") {
    return global[method];
  } else if (typeof my !== "undefined") {
    return my[method];
  }
}
function vOn(value, key) {
  const instance = getCurrentInstance();
  const ctx = instance.ctx;
  const extraKey = typeof key !== "undefined" && (ctx.$mpPlatform === "mp-weixin" || ctx.$mpPlatform === "mp-qq") && (isString$1(key) || typeof key === "number") ? "_" + key : "";
  const name = "e" + instance.$ei++ + extraKey;
  const mpInstance = ctx.$scope;
  if (!value) {
    delete mpInstance[name];
    return name;
  }
  const existingInvoker = mpInstance[name];
  if (existingInvoker) {
    existingInvoker.value = value;
  } else {
    mpInstance[name] = createInvoker(value, instance);
  }
  return name;
}
function createInvoker(initialValue, instance) {
  const invoker = (e2) => {
    patchMPEvent(e2);
    let args = [e2];
    if (e2.detail && e2.detail.__args__) {
      args = e2.detail.__args__;
    }
    const eventValue = invoker.value;
    const invoke = () => callWithAsyncErrorHandling(patchStopImmediatePropagation(e2, eventValue), instance, 5, args);
    const eventTarget = e2.target;
    const eventSync = eventTarget ? eventTarget.dataset ? String(eventTarget.dataset.eventsync) === "true" : false : false;
    if (bubbles.includes(e2.type) && !eventSync) {
      setTimeout(invoke);
    } else {
      const res = invoke();
      if (e2.type === "input" && (isArray$2(res) || isPromise(res))) {
        return;
      }
      return res;
    }
  };
  invoker.value = initialValue;
  return invoker;
}
const bubbles = [
  // touch事件暂不做延迟，否则在 Android 上会影响性能，比如一些拖拽跟手手势等
  // 'touchstart',
  // 'touchmove',
  // 'touchcancel',
  // 'touchend',
  "tap",
  "longpress",
  "longtap",
  "transitionend",
  "animationstart",
  "animationiteration",
  "animationend",
  "touchforcechange"
];
function patchMPEvent(event) {
  if (event.type && event.target) {
    event.preventDefault = NOOP;
    event.stopPropagation = NOOP;
    event.stopImmediatePropagation = NOOP;
    if (!hasOwn$1(event, "detail")) {
      event.detail = {};
    }
    if (hasOwn$1(event, "markerId")) {
      event.detail = typeof event.detail === "object" ? event.detail : {};
      event.detail.markerId = event.markerId;
    }
    if (isPlainObject$1(event.detail) && hasOwn$1(event.detail, "checked") && !hasOwn$1(event.detail, "value")) {
      event.detail.value = event.detail.checked;
    }
    if (isPlainObject$1(event.detail)) {
      event.target = extend$1({}, event.target, event.detail);
    }
  }
}
function patchStopImmediatePropagation(e2, value) {
  if (isArray$2(value)) {
    const originalStop = e2.stopImmediatePropagation;
    e2.stopImmediatePropagation = () => {
      originalStop && originalStop.call(e2);
      e2._stopped = true;
    };
    return value.map((fn) => (e3) => !e3._stopped && fn(e3));
  } else {
    return value;
  }
}
function vFor(source, renderItem) {
  let ret;
  if (isArray$2(source) || isString$1(source)) {
    ret = new Array(source.length);
    for (let i = 0, l = source.length; i < l; i++) {
      ret[i] = renderItem(source[i], i, i);
    }
  } else if (typeof source === "number") {
    if (!Number.isInteger(source)) {
      warn$2(`The v-for range expect an integer value but got ${source}.`);
      return [];
    }
    ret = new Array(source);
    for (let i = 0; i < source; i++) {
      ret[i] = renderItem(i + 1, i, i);
    }
  } else if (isObject$2(source)) {
    if (source[Symbol.iterator]) {
      ret = Array.from(source, (item, i) => renderItem(item, i, i));
    } else {
      const keys2 = Object.keys(source);
      ret = new Array(keys2.length);
      for (let i = 0, l = keys2.length; i < l; i++) {
        const key = keys2[i];
        ret[i] = renderItem(source[key], key, i);
      }
    }
  } else {
    ret = [];
  }
  return ret;
}
function stringifyStyle(value) {
  if (isString$1(value)) {
    return value;
  }
  return stringify(normalizeStyle(value));
}
function stringify(styles) {
  let ret = "";
  if (!styles || isString$1(styles)) {
    return ret;
  }
  for (const key in styles) {
    ret += `${key.startsWith(`--`) ? key : hyphenate(key)}:${styles[key]};`;
  }
  return ret;
}
function dynamicSlot(names) {
  if (isString$1(names)) {
    return dynamicSlotName(names);
  }
  return names.map((name) => dynamicSlotName(name));
}
function setRef(ref2, id, opts = {}) {
  const { $templateRefs } = getCurrentInstance();
  $templateRefs.push({ i: id, r: ref2, k: opts.k, f: opts.f });
}
const o = (value, key) => vOn(value, key);
const f = (source, renderItem) => vFor(source, renderItem);
const d = (names) => dynamicSlot(names);
const s = (value) => stringifyStyle(value);
const e = (target, ...sources) => extend$1(target, ...sources);
const n = (value) => normalizeClass(value);
const t = (val) => toDisplayString(val);
const p = (props) => renderProps(props);
const sr = (ref2, id, opts) => setRef(ref2, id, opts);
function createApp$1(rootComponent, rootProps = null) {
  rootComponent && (rootComponent.mpType = "app");
  return createVueApp(rootComponent, rootProps).use(plugin);
}
const createSSRApp = createApp$1;
const MP_METHODS = [
  "createSelectorQuery",
  "createIntersectionObserver",
  "selectAllComponents",
  "selectComponent"
];
function createEmitFn(oldEmit, ctx) {
  return function emit2(event, ...args) {
    const scope = ctx.$scope;
    if (scope && event) {
      const detail = { __args__: args };
      {
        scope.triggerEvent(event, detail);
      }
    }
    return oldEmit.apply(this, [event, ...args]);
  };
}
function initBaseInstance(instance, options) {
  const ctx = instance.ctx;
  ctx.mpType = options.mpType;
  ctx.$mpType = options.mpType;
  ctx.$mpPlatform = "mp-weixin";
  ctx.$scope = options.mpInstance;
  ctx.$mp = {};
  {
    ctx._self = {};
  }
  instance.slots = {};
  if (isArray$2(options.slots) && options.slots.length) {
    options.slots.forEach((name) => {
      instance.slots[name] = true;
    });
    if (instance.slots[SLOT_DEFAULT_NAME]) {
      instance.slots.default = true;
    }
  }
  ctx.getOpenerEventChannel = function() {
    {
      return options.mpInstance.getOpenerEventChannel();
    }
  };
  ctx.$hasHook = hasHook;
  ctx.$callHook = callHook;
  instance.emit = createEmitFn(instance.emit, ctx);
}
function initComponentInstance(instance, options) {
  initBaseInstance(instance, options);
  const ctx = instance.ctx;
  MP_METHODS.forEach((method) => {
    ctx[method] = function(...args) {
      const mpInstance = ctx.$scope;
      if (mpInstance && mpInstance[method]) {
        return mpInstance[method].apply(mpInstance, args);
      }
    };
  });
}
function initMocks(instance, mpInstance, mocks2) {
  const ctx = instance.ctx;
  mocks2.forEach((mock) => {
    if (hasOwn$1(mpInstance, mock)) {
      instance[mock] = ctx[mock] = mpInstance[mock];
    }
  });
}
function hasHook(name) {
  const hooks2 = this.$[name];
  if (hooks2 && hooks2.length) {
    return true;
  }
  return false;
}
function callHook(name, args) {
  if (name === "mounted") {
    callHook.call(this, "bm");
    this.$.isMounted = true;
    name = "m";
  }
  const hooks2 = this.$[name];
  return hooks2 && invokeArrayFns(hooks2, args);
}
const PAGE_INIT_HOOKS = [
  ON_LOAD,
  ON_SHOW,
  ON_HIDE,
  ON_UNLOAD,
  ON_RESIZE,
  ON_TAB_ITEM_TAP,
  ON_REACH_BOTTOM,
  ON_PULL_DOWN_REFRESH,
  ON_ADD_TO_FAVORITES
  // 'onReady', // lifetimes.ready
  // 'onPageScroll', // 影响性能，开发者手动注册
  // 'onShareTimeline', // 右上角菜单，开发者手动注册
  // 'onShareAppMessage' // 右上角菜单，开发者手动注册
];
function findHooks(vueOptions, hooks2 = /* @__PURE__ */ new Set()) {
  if (vueOptions) {
    Object.keys(vueOptions).forEach((name) => {
      if (isUniLifecycleHook(name, vueOptions[name])) {
        hooks2.add(name);
      }
    });
    {
      const { extends: extendsOptions, mixins } = vueOptions;
      if (mixins) {
        mixins.forEach((mixin) => findHooks(mixin, hooks2));
      }
      if (extendsOptions) {
        findHooks(extendsOptions, hooks2);
      }
    }
  }
  return hooks2;
}
function initHook(mpOptions, hook, excludes) {
  if (excludes.indexOf(hook) === -1 && !hasOwn$1(mpOptions, hook)) {
    mpOptions[hook] = function(args) {
      return this.$vm && this.$vm.$callHook(hook, args);
    };
  }
}
const EXCLUDE_HOOKS = [ON_READY];
function initHooks(mpOptions, hooks2, excludes = EXCLUDE_HOOKS) {
  hooks2.forEach((hook) => initHook(mpOptions, hook, excludes));
}
function initUnknownHooks(mpOptions, vueOptions, excludes = EXCLUDE_HOOKS) {
  findHooks(vueOptions).forEach((hook) => initHook(mpOptions, hook, excludes));
}
function initRuntimeHooks(mpOptions, runtimeHooks) {
  if (!runtimeHooks) {
    return;
  }
  const hooks2 = Object.keys(MINI_PROGRAM_PAGE_RUNTIME_HOOKS);
  hooks2.forEach((hook) => {
    if (runtimeHooks & MINI_PROGRAM_PAGE_RUNTIME_HOOKS[hook]) {
      initHook(mpOptions, hook, []);
    }
  });
}
const findMixinRuntimeHooks = /* @__PURE__ */ once(() => {
  const runtimeHooks = [];
  const app = isFunction$1(getApp) && getApp({ allowDefault: true });
  if (app && app.$vm && app.$vm.$) {
    const mixins = app.$vm.$.appContext.mixins;
    if (isArray$2(mixins)) {
      const hooks2 = Object.keys(MINI_PROGRAM_PAGE_RUNTIME_HOOKS);
      mixins.forEach((mixin) => {
        hooks2.forEach((hook) => {
          if (hasOwn$1(mixin, hook) && !runtimeHooks.includes(hook)) {
            runtimeHooks.push(hook);
          }
        });
      });
    }
  }
  return runtimeHooks;
});
function initMixinRuntimeHooks(mpOptions) {
  initHooks(mpOptions, findMixinRuntimeHooks());
}
const HOOKS = [
  ON_SHOW,
  ON_HIDE,
  ON_ERROR,
  ON_THEME_CHANGE,
  ON_PAGE_NOT_FOUND,
  ON_UNHANDLE_REJECTION
];
function parseApp(instance, parseAppOptions) {
  const internalInstance = instance.$;
  const appOptions = {
    globalData: instance.$options && instance.$options.globalData || {},
    $vm: instance,
    onLaunch(options) {
      this.$vm = instance;
      const ctx = internalInstance.ctx;
      if (this.$vm && ctx.$scope) {
        return;
      }
      initBaseInstance(internalInstance, {
        mpType: "app",
        mpInstance: this,
        slots: []
      });
      ctx.globalData = this.globalData;
      instance.$callHook(ON_LAUNCH, options);
    }
  };
  initLocale(instance);
  const vueOptions = instance.$.type;
  initHooks(appOptions, HOOKS);
  initUnknownHooks(appOptions, vueOptions);
  {
    const methods = vueOptions.methods;
    methods && extend$1(appOptions, methods);
  }
  if (parseAppOptions) {
    parseAppOptions.parse(appOptions);
  }
  return appOptions;
}
function initCreateApp(parseAppOptions) {
  return function createApp2(vm) {
    return App(parseApp(vm, parseAppOptions));
  };
}
function initCreateSubpackageApp(parseAppOptions) {
  return function createApp2(vm) {
    const appOptions = parseApp(vm, parseAppOptions);
    const app = isFunction$1(getApp) && getApp({
      allowDefault: true
    });
    if (!app)
      return;
    vm.$.ctx.$scope = app;
    const globalData = app.globalData;
    if (globalData) {
      Object.keys(appOptions.globalData).forEach((name) => {
        if (!hasOwn$1(globalData, name)) {
          globalData[name] = appOptions.globalData[name];
        }
      });
    }
    Object.keys(appOptions).forEach((name) => {
      if (!hasOwn$1(app, name)) {
        app[name] = appOptions[name];
      }
    });
    initAppLifecycle(appOptions, vm);
  };
}
function initAppLifecycle(appOptions, vm) {
  if (isFunction$1(appOptions.onLaunch)) {
    const args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    appOptions.onLaunch(args);
  }
  if (isFunction$1(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow((args) => {
      vm.$callHook("onShow", args);
    });
  }
  if (isFunction$1(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide((args) => {
      vm.$callHook("onHide", args);
    });
  }
}
function initLocale(appVm) {
  const locale2 = ref(normalizeLocale$1(wx.getSystemInfoSync().language) || LOCALE_EN);
  Object.defineProperty(appVm, "$locale", {
    get() {
      return locale2.value;
    },
    set(v) {
      locale2.value = v;
    }
  });
}
function initVueIds(vueIds, mpInstance) {
  if (!vueIds) {
    return;
  }
  const ids = vueIds.split(",");
  const len = ids.length;
  if (len === 1) {
    mpInstance._$vueId = ids[0];
  } else if (len === 2) {
    mpInstance._$vueId = ids[0];
    mpInstance._$vuePid = ids[1];
  }
}
const EXTRAS = ["externalClasses"];
function initExtraOptions(miniProgramComponentOptions, vueOptions) {
  EXTRAS.forEach((name) => {
    if (hasOwn$1(vueOptions, name)) {
      miniProgramComponentOptions[name] = vueOptions[name];
    }
  });
}
const WORKLET_RE = /_(.*)_worklet_factory_/;
function initWorkletMethods(mpMethods, vueMethods) {
  if (vueMethods) {
    Object.keys(vueMethods).forEach((name) => {
      const matches = name.match(WORKLET_RE);
      if (matches) {
        const workletName = matches[1];
        mpMethods[name] = vueMethods[name];
        mpMethods[workletName] = vueMethods[workletName];
      }
    });
  }
}
function initWxsCallMethods(methods, wxsCallMethods) {
  if (!isArray$2(wxsCallMethods)) {
    return;
  }
  wxsCallMethods.forEach((callMethod) => {
    methods[callMethod] = function(args) {
      return this.$vm[callMethod](args);
    };
  });
}
function selectAllComponents(mpInstance, selector, $refs) {
  const components = mpInstance.selectAllComponents(selector);
  components.forEach((component) => {
    const ref2 = component.properties.uR;
    $refs[ref2] = component.$vm || component;
  });
}
function initRefs(instance, mpInstance) {
  Object.defineProperty(instance, "refs", {
    get() {
      const $refs = {};
      selectAllComponents(mpInstance, ".r", $refs);
      const forComponents = mpInstance.selectAllComponents(".r-i-f");
      forComponents.forEach((component) => {
        const ref2 = component.properties.uR;
        if (!ref2) {
          return;
        }
        if (!$refs[ref2]) {
          $refs[ref2] = [];
        }
        $refs[ref2].push(component.$vm || component);
      });
      return $refs;
    }
  });
}
function findVmByVueId(instance, vuePid) {
  const $children = instance.$children;
  for (let i = $children.length - 1; i >= 0; i--) {
    const childVm = $children[i];
    if (childVm.$scope._$vueId === vuePid) {
      return childVm;
    }
  }
  let parentVm;
  for (let i = $children.length - 1; i >= 0; i--) {
    parentVm = findVmByVueId($children[i], vuePid);
    if (parentVm) {
      return parentVm;
    }
  }
}
const builtInProps = [
  // 百度小程序,快手小程序自定义组件不支持绑定动态事件，动态dataset，故通过props传递事件信息
  // event-opts
  "eO",
  // 组件 ref
  "uR",
  // 组件 ref-in-for
  "uRIF",
  // 组件 id
  "uI",
  // 组件类型 m: 小程序组件
  "uT",
  // 组件 props
  "uP",
  // 小程序不能直接定义 $slots 的 props，所以通过 vueSlots 转换到 $slots
  "uS"
];
function initDefaultProps(options, isBehavior = false) {
  const properties = {};
  if (!isBehavior) {
    builtInProps.forEach((name) => {
      properties[name] = {
        type: null,
        value: ""
      };
    });
    properties.uS = {
      type: null,
      value: [],
      observer: function(newVal) {
        const $slots = /* @__PURE__ */ Object.create(null);
        newVal && newVal.forEach((slotName) => {
          $slots[slotName] = true;
        });
        this.setData({
          $slots
        });
      }
    };
  }
  if (options.behaviors) {
    if (options.behaviors.includes("wx://form-field")) {
      if (!options.properties || !options.properties.name) {
        properties.name = {
          type: null,
          value: ""
        };
      }
      if (!options.properties || !options.properties.value) {
        properties.value = {
          type: null,
          value: ""
        };
      }
    }
  }
  return properties;
}
function initVirtualHostProps(options) {
  const properties = {};
  {
    if (options && options.virtualHost) {
      properties.virtualHostStyle = {
        type: null,
        value: ""
      };
      properties.virtualHostClass = {
        type: null,
        value: ""
      };
    }
  }
  return properties;
}
function initProps(mpComponentOptions) {
  if (!mpComponentOptions.properties) {
    mpComponentOptions.properties = {};
  }
  extend$1(mpComponentOptions.properties, initDefaultProps(mpComponentOptions), initVirtualHostProps(mpComponentOptions.options));
}
const PROP_TYPES = [String, Number, Boolean, Object, Array, null];
function parsePropType(type, defaultValue) {
  if (isArray$2(type) && type.length === 1) {
    return type[0];
  }
  return type;
}
function normalizePropType(type, defaultValue) {
  const res = parsePropType(type);
  return PROP_TYPES.indexOf(res) !== -1 ? res : null;
}
function initPageProps({ properties }, rawProps) {
  if (isArray$2(rawProps)) {
    rawProps.forEach((key) => {
      properties[key] = {
        type: String,
        value: ""
      };
    });
  } else if (isPlainObject$1(rawProps)) {
    Object.keys(rawProps).forEach((key) => {
      const opts = rawProps[key];
      if (isPlainObject$1(opts)) {
        let value = opts.default;
        if (isFunction$1(value)) {
          value = value();
        }
        const type = opts.type;
        opts.type = normalizePropType(type);
        properties[key] = {
          type: opts.type,
          value
        };
      } else {
        properties[key] = {
          type: normalizePropType(opts)
        };
      }
    });
  }
}
function findPropsData(properties, isPage2) {
  return (isPage2 ? findPagePropsData(properties) : findComponentPropsData(properties.uP)) || {};
}
function findPagePropsData(properties) {
  const propsData = {};
  if (isPlainObject$1(properties)) {
    Object.keys(properties).forEach((name) => {
      if (builtInProps.indexOf(name) === -1) {
        propsData[name] = properties[name];
      }
    });
  }
  return propsData;
}
function initFormField(vm) {
  const vueOptions = vm.$options;
  if (isArray$2(vueOptions.behaviors) && vueOptions.behaviors.includes("uni://form-field")) {
    vm.$watch("modelValue", () => {
      vm.$scope && vm.$scope.setData({
        name: vm.name,
        value: vm.modelValue
      });
    }, {
      immediate: true
    });
  }
}
function initData(_) {
  return {};
}
function initPropsObserver(componentOptions) {
  const observe = function observe2() {
    const up = this.properties.uP;
    if (!up) {
      return;
    }
    if (this.$vm) {
      updateComponentProps(up, this.$vm.$);
    } else if (this.properties.uT === "m") {
      updateMiniProgramComponentProperties(up, this);
    }
  };
  {
    if (!componentOptions.observers) {
      componentOptions.observers = {};
    }
    componentOptions.observers.uP = observe;
  }
}
function updateMiniProgramComponentProperties(up, mpInstance) {
  const prevProps = mpInstance.properties;
  const nextProps = findComponentPropsData(up) || {};
  if (hasPropsChanged(prevProps, nextProps, false)) {
    mpInstance.setData(nextProps);
  }
}
function updateComponentProps(up, instance) {
  const prevProps = toRaw(instance.props);
  const nextProps = findComponentPropsData(up) || {};
  if (hasPropsChanged(prevProps, nextProps)) {
    updateProps(instance, nextProps, prevProps, false);
    if (hasQueueJob(instance.update)) {
      invalidateJob(instance.update);
    }
    {
      instance.update();
    }
  }
}
function hasPropsChanged(prevProps, nextProps, checkLen = true) {
  const nextKeys = Object.keys(nextProps);
  if (checkLen && nextKeys.length !== Object.keys(prevProps).length) {
    return true;
  }
  for (let i = 0; i < nextKeys.length; i++) {
    const key = nextKeys[i];
    if (nextProps[key] !== prevProps[key]) {
      return true;
    }
  }
  return false;
}
function initBehaviors(vueOptions) {
  const vueBehaviors = vueOptions.behaviors;
  let vueProps = vueOptions.props;
  if (!vueProps) {
    vueOptions.props = vueProps = [];
  }
  const behaviors = [];
  if (isArray$2(vueBehaviors)) {
    vueBehaviors.forEach((behavior) => {
      behaviors.push(behavior.replace("uni://", "wx://"));
      if (behavior === "uni://form-field") {
        if (isArray$2(vueProps)) {
          vueProps.push("name");
          vueProps.push("modelValue");
        } else {
          vueProps.name = {
            type: String,
            default: ""
          };
          vueProps.modelValue = {
            type: [String, Number, Boolean, Array, Object, Date],
            default: ""
          };
        }
      }
    });
  }
  return behaviors;
}
function applyOptions(componentOptions, vueOptions) {
  componentOptions.data = initData();
  componentOptions.behaviors = initBehaviors(vueOptions);
}
function parseComponent(vueOptions, { parse: parse2, mocks: mocks2, isPage: isPage2, initRelation: initRelation2, handleLink: handleLink2, initLifetimes: initLifetimes2 }) {
  vueOptions = vueOptions.default || vueOptions;
  const options = {
    multipleSlots: true,
    // styleIsolation: 'apply-shared',
    addGlobalClass: true,
    pureDataPattern: /^uP$/
  };
  if (isArray$2(vueOptions.mixins)) {
    vueOptions.mixins.forEach((item) => {
      if (isObject$2(item.options)) {
        extend$1(options, item.options);
      }
    });
  }
  if (vueOptions.options) {
    extend$1(options, vueOptions.options);
  }
  const mpComponentOptions = {
    options,
    lifetimes: initLifetimes2({ mocks: mocks2, isPage: isPage2, initRelation: initRelation2, vueOptions }),
    pageLifetimes: {
      show() {
        this.$vm && this.$vm.$callHook("onPageShow");
      },
      hide() {
        this.$vm && this.$vm.$callHook("onPageHide");
      },
      resize(size2) {
        this.$vm && this.$vm.$callHook("onPageResize", size2);
      }
    },
    methods: {
      __l: handleLink2
    }
  };
  {
    applyOptions(mpComponentOptions, vueOptions);
  }
  initProps(mpComponentOptions);
  initPropsObserver(mpComponentOptions);
  initExtraOptions(mpComponentOptions, vueOptions);
  initWxsCallMethods(mpComponentOptions.methods, vueOptions.wxsCallMethods);
  {
    initWorkletMethods(mpComponentOptions.methods, vueOptions.methods);
  }
  if (parse2) {
    parse2(mpComponentOptions, { handleLink: handleLink2 });
  }
  return mpComponentOptions;
}
function initCreateComponent(parseOptions2) {
  return function createComponent2(vueComponentOptions) {
    return Component(parseComponent(vueComponentOptions, parseOptions2));
  };
}
let $createComponentFn;
let $destroyComponentFn;
function getAppVm() {
  return getApp().$vm;
}
function $createComponent(initialVNode, options) {
  if (!$createComponentFn) {
    $createComponentFn = getAppVm().$createComponent;
  }
  const proxy = $createComponentFn(initialVNode, options);
  return getExposeProxy(proxy.$) || proxy;
}
function $destroyComponent(instance) {
  if (!$destroyComponentFn) {
    $destroyComponentFn = getAppVm().$destroyComponent;
  }
  return $destroyComponentFn(instance);
}
function parsePage(vueOptions, parseOptions2) {
  const { parse: parse2, mocks: mocks2, isPage: isPage2, initRelation: initRelation2, handleLink: handleLink2, initLifetimes: initLifetimes2 } = parseOptions2;
  const miniProgramPageOptions = parseComponent(vueOptions, {
    mocks: mocks2,
    isPage: isPage2,
    initRelation: initRelation2,
    handleLink: handleLink2,
    initLifetimes: initLifetimes2
  });
  initPageProps(miniProgramPageOptions, (vueOptions.default || vueOptions).props);
  const methods = miniProgramPageOptions.methods;
  methods.onLoad = function(query) {
    this.options = query;
    this.$page = {
      fullPath: addLeadingSlash(this.route + stringifyQuery(query))
    };
    return this.$vm && this.$vm.$callHook(ON_LOAD, query);
  };
  initHooks(methods, PAGE_INIT_HOOKS);
  {
    initUnknownHooks(methods, vueOptions);
  }
  initRuntimeHooks(methods, vueOptions.__runtimeHooks);
  initMixinRuntimeHooks(methods);
  parse2 && parse2(miniProgramPageOptions, { handleLink: handleLink2 });
  return miniProgramPageOptions;
}
function initCreatePage(parseOptions2) {
  return function createPage2(vuePageOptions) {
    return Component(parsePage(vuePageOptions, parseOptions2));
  };
}
function initCreatePluginApp(parseAppOptions) {
  return function createApp2(vm) {
    initAppLifecycle(parseApp(vm, parseAppOptions), vm);
  };
}
const MPPage = Page;
const MPComponent = Component;
function initTriggerEvent(mpInstance) {
  const oldTriggerEvent = mpInstance.triggerEvent;
  const newTriggerEvent = function(event, ...args) {
    return oldTriggerEvent.apply(mpInstance, [customizeEvent(event), ...args]);
  };
  try {
    mpInstance.triggerEvent = newTriggerEvent;
  } catch (error) {
    mpInstance._triggerEvent = newTriggerEvent;
  }
}
function initMiniProgramHook(name, options, isComponent) {
  const oldHook = options[name];
  if (!oldHook) {
    options[name] = function() {
      initTriggerEvent(this);
    };
  } else {
    options[name] = function(...args) {
      initTriggerEvent(this);
      return oldHook.apply(this, args);
    };
  }
}
Page = function(options) {
  initMiniProgramHook(ON_LOAD, options);
  return MPPage(options);
};
Component = function(options) {
  initMiniProgramHook("created", options);
  const isVueComponent = options.properties && options.properties.uP;
  if (!isVueComponent) {
    initProps(options);
    initPropsObserver(options);
  }
  return MPComponent(options);
};
function initLifetimes({ mocks: mocks2, isPage: isPage2, initRelation: initRelation2, vueOptions }) {
  return {
    attached() {
      let properties = this.properties;
      initVueIds(properties.uI, this);
      const relationOptions = {
        vuePid: this._$vuePid
      };
      initRelation2(this, relationOptions);
      const mpInstance = this;
      const isMiniProgramPage = isPage2(mpInstance);
      let propsData = properties;
      this.$vm = $createComponent({
        type: vueOptions,
        props: findPropsData(propsData, isMiniProgramPage)
      }, {
        mpType: isMiniProgramPage ? "page" : "component",
        mpInstance,
        slots: properties.uS || {},
        parentComponent: relationOptions.parent && relationOptions.parent.$,
        onBeforeSetup(instance, options) {
          initRefs(instance, mpInstance);
          initMocks(instance, mpInstance, mocks2);
          initComponentInstance(instance, options);
        }
      });
      if (!isMiniProgramPage) {
        initFormField(this.$vm);
      }
    },
    ready() {
      if (this.$vm) {
        {
          this.$vm.$callHook("mounted");
          this.$vm.$callHook(ON_READY);
        }
      }
    },
    detached() {
      if (this.$vm) {
        pruneComponentPropsCache(this.$vm.$.uid);
        $destroyComponent(this.$vm);
      }
    }
  };
}
const mocks = ["__route__", "__wxExparserNodeId__", "__wxWebviewId__"];
function isPage(mpInstance) {
  return !!mpInstance.route;
}
function initRelation(mpInstance, detail) {
  mpInstance.triggerEvent("__l", detail);
}
function handleLink(event) {
  const detail = event.detail || event.value;
  const vuePid = detail.vuePid;
  let parentVm;
  if (vuePid) {
    parentVm = findVmByVueId(this.$vm, vuePid);
  }
  if (!parentVm) {
    parentVm = this.$vm;
  }
  detail.parent = parentVm;
}
var parseOptions = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  handleLink,
  initLifetimes,
  initRelation,
  isPage,
  mocks
});
const createApp = initCreateApp();
const createPage = initCreatePage(parseOptions);
const createComponent = initCreateComponent(parseOptions);
const createPluginApp = initCreatePluginApp();
const createSubpackageApp = initCreateSubpackageApp();
{
  wx.createApp = global.createApp = createApp;
  wx.createPage = createPage;
  wx.createComponent = createComponent;
  wx.createPluginApp = global.createPluginApp = createPluginApp;
  wx.createSubpackageApp = global.createSubpackageApp = createSubpackageApp;
}
function getDefaultExportFromCjs(x) {
  return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, "default") ? x["default"] : x;
}
var gtpushMinExports = {};
var gtpushMin = {
  get exports() {
    return gtpushMinExports;
  },
  set exports(v) {
    gtpushMinExports = v;
  }
};
/*! For license information please see gtpush-min.js.LICENSE.txt */
(function(module2, exports2) {
  (function t2(e2, r) {
    module2.exports = r();
  })(self, () => (() => {
    var t2 = { 4736: (t3, e3, r2) => {
      t3 = r2.nmd(t3);
      var i2;
      var n2 = function(t4) {
        var e4 = 1e7, r3 = 7, i3 = 9007199254740992, s2 = d2(i3), a = "0123456789abcdefghijklmnopqrstuvwxyz";
        var o2 = "function" === typeof BigInt;
        function u(t5, e5, r4, i4) {
          if ("undefined" === typeof t5)
            return u[0];
          if ("undefined" !== typeof e5)
            return 10 === +e5 && !r4 ? st(t5) : X(t5, e5, r4, i4);
          return st(t5);
        }
        function c(t5, e5) {
          this.value = t5;
          this.sign = e5;
          this.isSmall = false;
        }
        c.prototype = Object.create(u.prototype);
        function l(t5) {
          this.value = t5;
          this.sign = t5 < 0;
          this.isSmall = true;
        }
        l.prototype = Object.create(u.prototype);
        function f2(t5) {
          this.value = t5;
        }
        f2.prototype = Object.create(u.prototype);
        function h(t5) {
          return -i3 < t5 && t5 < i3;
        }
        function d2(t5) {
          if (t5 < 1e7)
            return [t5];
          if (t5 < 1e14)
            return [t5 % 1e7, Math.floor(t5 / 1e7)];
          return [t5 % 1e7, Math.floor(t5 / 1e7) % 1e7, Math.floor(t5 / 1e14)];
        }
        function v(t5) {
          p2(t5);
          var r4 = t5.length;
          if (r4 < 4 && N(t5, s2) < 0)
            switch (r4) {
              case 0:
                return 0;
              case 1:
                return t5[0];
              case 2:
                return t5[0] + t5[1] * e4;
              default:
                return t5[0] + (t5[1] + t5[2] * e4) * e4;
            }
          return t5;
        }
        function p2(t5) {
          var e5 = t5.length;
          while (0 === t5[--e5])
            ;
          t5.length = e5 + 1;
        }
        function g(t5) {
          var e5 = new Array(t5);
          var r4 = -1;
          while (++r4 < t5)
            e5[r4] = 0;
          return e5;
        }
        function y(t5) {
          if (t5 > 0)
            return Math.floor(t5);
          return Math.ceil(t5);
        }
        function m(t5, r4) {
          var i4 = t5.length, n3 = r4.length, s3 = new Array(i4), a2 = 0, o3 = e4, u2, c2;
          for (c2 = 0; c2 < n3; c2++) {
            u2 = t5[c2] + r4[c2] + a2;
            a2 = u2 >= o3 ? 1 : 0;
            s3[c2] = u2 - a2 * o3;
          }
          while (c2 < i4) {
            u2 = t5[c2] + a2;
            a2 = u2 === o3 ? 1 : 0;
            s3[c2++] = u2 - a2 * o3;
          }
          if (a2 > 0)
            s3.push(a2);
          return s3;
        }
        function w(t5, e5) {
          if (t5.length >= e5.length)
            return m(t5, e5);
          return m(e5, t5);
        }
        function S(t5, r4) {
          var i4 = t5.length, n3 = new Array(i4), s3 = e4, a2, o3;
          for (o3 = 0; o3 < i4; o3++) {
            a2 = t5[o3] - s3 + r4;
            r4 = Math.floor(a2 / s3);
            n3[o3] = a2 - r4 * s3;
            r4 += 1;
          }
          while (r4 > 0) {
            n3[o3++] = r4 % s3;
            r4 = Math.floor(r4 / s3);
          }
          return n3;
        }
        c.prototype.add = function(t5) {
          var e5 = st(t5);
          if (this.sign !== e5.sign)
            return this.subtract(e5.negate());
          var r4 = this.value, i4 = e5.value;
          if (e5.isSmall)
            return new c(S(r4, Math.abs(i4)), this.sign);
          return new c(w(r4, i4), this.sign);
        };
        c.prototype.plus = c.prototype.add;
        l.prototype.add = function(t5) {
          var e5 = st(t5);
          var r4 = this.value;
          if (r4 < 0 !== e5.sign)
            return this.subtract(e5.negate());
          var i4 = e5.value;
          if (e5.isSmall) {
            if (h(r4 + i4))
              return new l(r4 + i4);
            i4 = d2(Math.abs(i4));
          }
          return new c(S(i4, Math.abs(r4)), r4 < 0);
        };
        l.prototype.plus = l.prototype.add;
        f2.prototype.add = function(t5) {
          return new f2(this.value + st(t5).value);
        };
        f2.prototype.plus = f2.prototype.add;
        function _(t5, r4) {
          var i4 = t5.length, n3 = r4.length, s3 = new Array(i4), a2 = 0, o3 = e4, u2, c2;
          for (u2 = 0; u2 < n3; u2++) {
            c2 = t5[u2] - a2 - r4[u2];
            if (c2 < 0) {
              c2 += o3;
              a2 = 1;
            } else
              a2 = 0;
            s3[u2] = c2;
          }
          for (u2 = n3; u2 < i4; u2++) {
            c2 = t5[u2] - a2;
            if (c2 < 0)
              c2 += o3;
            else {
              s3[u2++] = c2;
              break;
            }
            s3[u2] = c2;
          }
          for (; u2 < i4; u2++)
            s3[u2] = t5[u2];
          p2(s3);
          return s3;
        }
        function b(t5, e5, r4) {
          var i4;
          if (N(t5, e5) >= 0)
            i4 = _(t5, e5);
          else {
            i4 = _(e5, t5);
            r4 = !r4;
          }
          i4 = v(i4);
          if ("number" === typeof i4) {
            if (r4)
              i4 = -i4;
            return new l(i4);
          }
          return new c(i4, r4);
        }
        function E2(t5, r4, i4) {
          var n3 = t5.length, s3 = new Array(n3), a2 = -r4, o3 = e4, u2, f3;
          for (u2 = 0; u2 < n3; u2++) {
            f3 = t5[u2] + a2;
            a2 = Math.floor(f3 / o3);
            f3 %= o3;
            s3[u2] = f3 < 0 ? f3 + o3 : f3;
          }
          s3 = v(s3);
          if ("number" === typeof s3) {
            if (i4)
              s3 = -s3;
            return new l(s3);
          }
          return new c(s3, i4);
        }
        c.prototype.subtract = function(t5) {
          var e5 = st(t5);
          if (this.sign !== e5.sign)
            return this.add(e5.negate());
          var r4 = this.value, i4 = e5.value;
          if (e5.isSmall)
            return E2(r4, Math.abs(i4), this.sign);
          return b(r4, i4, this.sign);
        };
        c.prototype.minus = c.prototype.subtract;
        l.prototype.subtract = function(t5) {
          var e5 = st(t5);
          var r4 = this.value;
          if (r4 < 0 !== e5.sign)
            return this.add(e5.negate());
          var i4 = e5.value;
          if (e5.isSmall)
            return new l(r4 - i4);
          return E2(i4, Math.abs(r4), r4 >= 0);
        };
        l.prototype.minus = l.prototype.subtract;
        f2.prototype.subtract = function(t5) {
          return new f2(this.value - st(t5).value);
        };
        f2.prototype.minus = f2.prototype.subtract;
        c.prototype.negate = function() {
          return new c(this.value, !this.sign);
        };
        l.prototype.negate = function() {
          var t5 = this.sign;
          var e5 = new l(-this.value);
          e5.sign = !t5;
          return e5;
        };
        f2.prototype.negate = function() {
          return new f2(-this.value);
        };
        c.prototype.abs = function() {
          return new c(this.value, false);
        };
        l.prototype.abs = function() {
          return new l(Math.abs(this.value));
        };
        f2.prototype.abs = function() {
          return new f2(this.value >= 0 ? this.value : -this.value);
        };
        function D(t5, r4) {
          var i4 = t5.length, n3 = r4.length, s3 = i4 + n3, a2 = g(s3), o3 = e4, u2, c2, l2, f3, h2;
          for (l2 = 0; l2 < i4; ++l2) {
            f3 = t5[l2];
            for (var d3 = 0; d3 < n3; ++d3) {
              h2 = r4[d3];
              u2 = f3 * h2 + a2[l2 + d3];
              c2 = Math.floor(u2 / o3);
              a2[l2 + d3] = u2 - c2 * o3;
              a2[l2 + d3 + 1] += c2;
            }
          }
          p2(a2);
          return a2;
        }
        function M(t5, r4) {
          var i4 = t5.length, n3 = new Array(i4), s3 = e4, a2 = 0, o3, u2;
          for (u2 = 0; u2 < i4; u2++) {
            o3 = t5[u2] * r4 + a2;
            a2 = Math.floor(o3 / s3);
            n3[u2] = o3 - a2 * s3;
          }
          while (a2 > 0) {
            n3[u2++] = a2 % s3;
            a2 = Math.floor(a2 / s3);
          }
          return n3;
        }
        function T(t5, e5) {
          var r4 = [];
          while (e5-- > 0)
            r4.push(0);
          return r4.concat(t5);
        }
        function I(t5, e5) {
          var r4 = Math.max(t5.length, e5.length);
          if (r4 <= 30)
            return D(t5, e5);
          r4 = Math.ceil(r4 / 2);
          var i4 = t5.slice(r4), n3 = t5.slice(0, r4), s3 = e5.slice(r4), a2 = e5.slice(0, r4);
          var o3 = I(n3, a2), u2 = I(i4, s3), c2 = I(w(n3, i4), w(a2, s3));
          var l2 = w(w(o3, T(_(_(c2, o3), u2), r4)), T(u2, 2 * r4));
          p2(l2);
          return l2;
        }
        function A(t5, e5) {
          return -0.012 * t5 - 0.012 * e5 + 15e-6 * t5 * e5 > 0;
        }
        c.prototype.multiply = function(t5) {
          var r4 = st(t5), i4 = this.value, n3 = r4.value, s3 = this.sign !== r4.sign, a2;
          if (r4.isSmall) {
            if (0 === n3)
              return u[0];
            if (1 === n3)
              return this;
            if (-1 === n3)
              return this.negate();
            a2 = Math.abs(n3);
            if (a2 < e4)
              return new c(M(i4, a2), s3);
            n3 = d2(a2);
          }
          if (A(i4.length, n3.length))
            return new c(I(i4, n3), s3);
          return new c(D(i4, n3), s3);
        };
        c.prototype.times = c.prototype.multiply;
        function x(t5, r4, i4) {
          if (t5 < e4)
            return new c(M(r4, t5), i4);
          return new c(D(r4, d2(t5)), i4);
        }
        l.prototype._multiplyBySmall = function(t5) {
          if (h(t5.value * this.value))
            return new l(t5.value * this.value);
          return x(Math.abs(t5.value), d2(Math.abs(this.value)), this.sign !== t5.sign);
        };
        c.prototype._multiplyBySmall = function(t5) {
          if (0 === t5.value)
            return u[0];
          if (1 === t5.value)
            return this;
          if (-1 === t5.value)
            return this.negate();
          return x(Math.abs(t5.value), this.value, this.sign !== t5.sign);
        };
        l.prototype.multiply = function(t5) {
          return st(t5)._multiplyBySmall(this);
        };
        l.prototype.times = l.prototype.multiply;
        f2.prototype.multiply = function(t5) {
          return new f2(this.value * st(t5).value);
        };
        f2.prototype.times = f2.prototype.multiply;
        function R(t5) {
          var r4 = t5.length, i4 = g(r4 + r4), n3 = e4, s3, a2, o3, u2, c2;
          for (o3 = 0; o3 < r4; o3++) {
            u2 = t5[o3];
            a2 = 0 - u2 * u2;
            for (var l2 = o3; l2 < r4; l2++) {
              c2 = t5[l2];
              s3 = 2 * (u2 * c2) + i4[o3 + l2] + a2;
              a2 = Math.floor(s3 / n3);
              i4[o3 + l2] = s3 - a2 * n3;
            }
            i4[o3 + r4] = a2;
          }
          p2(i4);
          return i4;
        }
        c.prototype.square = function() {
          return new c(R(this.value), false);
        };
        l.prototype.square = function() {
          var t5 = this.value * this.value;
          if (h(t5))
            return new l(t5);
          return new c(R(d2(Math.abs(this.value))), false);
        };
        f2.prototype.square = function(t5) {
          return new f2(this.value * this.value);
        };
        function B(t5, r4) {
          var i4 = t5.length, n3 = r4.length, s3 = e4, a2 = g(r4.length), o3 = r4[n3 - 1], u2 = Math.ceil(s3 / (2 * o3)), c2 = M(t5, u2), l2 = M(r4, u2), f3, h2, d3, p3, y2, m2, w2;
          if (c2.length <= i4)
            c2.push(0);
          l2.push(0);
          o3 = l2[n3 - 1];
          for (h2 = i4 - n3; h2 >= 0; h2--) {
            f3 = s3 - 1;
            if (c2[h2 + n3] !== o3)
              f3 = Math.floor((c2[h2 + n3] * s3 + c2[h2 + n3 - 1]) / o3);
            d3 = 0;
            p3 = 0;
            m2 = l2.length;
            for (y2 = 0; y2 < m2; y2++) {
              d3 += f3 * l2[y2];
              w2 = Math.floor(d3 / s3);
              p3 += c2[h2 + y2] - (d3 - w2 * s3);
              d3 = w2;
              if (p3 < 0) {
                c2[h2 + y2] = p3 + s3;
                p3 = -1;
              } else {
                c2[h2 + y2] = p3;
                p3 = 0;
              }
            }
            while (0 !== p3) {
              f3 -= 1;
              d3 = 0;
              for (y2 = 0; y2 < m2; y2++) {
                d3 += c2[h2 + y2] - s3 + l2[y2];
                if (d3 < 0) {
                  c2[h2 + y2] = d3 + s3;
                  d3 = 0;
                } else {
                  c2[h2 + y2] = d3;
                  d3 = 1;
                }
              }
              p3 += d3;
            }
            a2[h2] = f3;
          }
          c2 = k(c2, u2)[0];
          return [v(a2), v(c2)];
        }
        function O(t5, r4) {
          var i4 = t5.length, n3 = r4.length, s3 = [], a2 = [], o3 = e4, u2, c2, l2, f3, h2;
          while (i4) {
            a2.unshift(t5[--i4]);
            p2(a2);
            if (N(a2, r4) < 0) {
              s3.push(0);
              continue;
            }
            c2 = a2.length;
            l2 = a2[c2 - 1] * o3 + a2[c2 - 2];
            f3 = r4[n3 - 1] * o3 + r4[n3 - 2];
            if (c2 > n3)
              l2 = (l2 + 1) * o3;
            u2 = Math.ceil(l2 / f3);
            do {
              h2 = M(r4, u2);
              if (N(h2, a2) <= 0)
                break;
              u2--;
            } while (u2);
            s3.push(u2);
            a2 = _(a2, h2);
          }
          s3.reverse();
          return [v(s3), v(a2)];
        }
        function k(t5, r4) {
          var i4 = t5.length, n3 = g(i4), s3 = e4, a2, o3, u2, c2;
          u2 = 0;
          for (a2 = i4 - 1; a2 >= 0; --a2) {
            c2 = u2 * s3 + t5[a2];
            o3 = y(c2 / r4);
            u2 = c2 - o3 * r4;
            n3[a2] = 0 | o3;
          }
          return [n3, 0 | u2];
        }
        function C(t5, r4) {
          var i4, n3 = st(r4);
          if (o2)
            return [new f2(t5.value / n3.value), new f2(t5.value % n3.value)];
          var s3 = t5.value, a2 = n3.value;
          var h2;
          if (0 === a2)
            throw new Error("Cannot divide by zero");
          if (t5.isSmall) {
            if (n3.isSmall)
              return [new l(y(s3 / a2)), new l(s3 % a2)];
            return [u[0], t5];
          }
          if (n3.isSmall) {
            if (1 === a2)
              return [t5, u[0]];
            if (-1 == a2)
              return [t5.negate(), u[0]];
            var p3 = Math.abs(a2);
            if (p3 < e4) {
              i4 = k(s3, p3);
              h2 = v(i4[0]);
              var g2 = i4[1];
              if (t5.sign)
                g2 = -g2;
              if ("number" === typeof h2) {
                if (t5.sign !== n3.sign)
                  h2 = -h2;
                return [new l(h2), new l(g2)];
              }
              return [new c(h2, t5.sign !== n3.sign), new l(g2)];
            }
            a2 = d2(p3);
          }
          var m2 = N(s3, a2);
          if (-1 === m2)
            return [u[0], t5];
          if (0 === m2)
            return [u[t5.sign === n3.sign ? 1 : -1], u[0]];
          if (s3.length + a2.length <= 200)
            i4 = B(s3, a2);
          else
            i4 = O(s3, a2);
          h2 = i4[0];
          var w2 = t5.sign !== n3.sign, S2 = i4[1], _2 = t5.sign;
          if ("number" === typeof h2) {
            if (w2)
              h2 = -h2;
            h2 = new l(h2);
          } else
            h2 = new c(h2, w2);
          if ("number" === typeof S2) {
            if (_2)
              S2 = -S2;
            S2 = new l(S2);
          } else
            S2 = new c(S2, _2);
          return [h2, S2];
        }
        c.prototype.divmod = function(t5) {
          var e5 = C(this, t5);
          return { quotient: e5[0], remainder: e5[1] };
        };
        f2.prototype.divmod = l.prototype.divmod = c.prototype.divmod;
        c.prototype.divide = function(t5) {
          return C(this, t5)[0];
        };
        f2.prototype.over = f2.prototype.divide = function(t5) {
          return new f2(this.value / st(t5).value);
        };
        l.prototype.over = l.prototype.divide = c.prototype.over = c.prototype.divide;
        c.prototype.mod = function(t5) {
          return C(this, t5)[1];
        };
        f2.prototype.mod = f2.prototype.remainder = function(t5) {
          return new f2(this.value % st(t5).value);
        };
        l.prototype.remainder = l.prototype.mod = c.prototype.remainder = c.prototype.mod;
        c.prototype.pow = function(t5) {
          var e5 = st(t5), r4 = this.value, i4 = e5.value, n3, s3, a2;
          if (0 === i4)
            return u[1];
          if (0 === r4)
            return u[0];
          if (1 === r4)
            return u[1];
          if (-1 === r4)
            return e5.isEven() ? u[1] : u[-1];
          if (e5.sign)
            return u[0];
          if (!e5.isSmall)
            throw new Error("The exponent " + e5.toString() + " is too large.");
          if (this.isSmall) {
            if (h(n3 = Math.pow(r4, i4)))
              return new l(y(n3));
          }
          s3 = this;
          a2 = u[1];
          while (true) {
            if (i4 & true) {
              a2 = a2.times(s3);
              --i4;
            }
            if (0 === i4)
              break;
            i4 /= 2;
            s3 = s3.square();
          }
          return a2;
        };
        l.prototype.pow = c.prototype.pow;
        f2.prototype.pow = function(t5) {
          var e5 = st(t5);
          var r4 = this.value, i4 = e5.value;
          var n3 = BigInt(0), s3 = BigInt(1), a2 = BigInt(2);
          if (i4 === n3)
            return u[1];
          if (r4 === n3)
            return u[0];
          if (r4 === s3)
            return u[1];
          if (r4 === BigInt(-1))
            return e5.isEven() ? u[1] : u[-1];
          if (e5.isNegative())
            return new f2(n3);
          var o3 = this;
          var c2 = u[1];
          while (true) {
            if ((i4 & s3) === s3) {
              c2 = c2.times(o3);
              --i4;
            }
            if (i4 === n3)
              break;
            i4 /= a2;
            o3 = o3.square();
          }
          return c2;
        };
        c.prototype.modPow = function(t5, e5) {
          t5 = st(t5);
          e5 = st(e5);
          if (e5.isZero())
            throw new Error("Cannot take modPow with modulus 0");
          var r4 = u[1], i4 = this.mod(e5);
          if (t5.isNegative()) {
            t5 = t5.multiply(u[-1]);
            i4 = i4.modInv(e5);
          }
          while (t5.isPositive()) {
            if (i4.isZero())
              return u[0];
            if (t5.isOdd())
              r4 = r4.multiply(i4).mod(e5);
            t5 = t5.divide(2);
            i4 = i4.square().mod(e5);
          }
          return r4;
        };
        f2.prototype.modPow = l.prototype.modPow = c.prototype.modPow;
        function N(t5, e5) {
          if (t5.length !== e5.length)
            return t5.length > e5.length ? 1 : -1;
          for (var r4 = t5.length - 1; r4 >= 0; r4--)
            if (t5[r4] !== e5[r4])
              return t5[r4] > e5[r4] ? 1 : -1;
          return 0;
        }
        c.prototype.compareAbs = function(t5) {
          var e5 = st(t5), r4 = this.value, i4 = e5.value;
          if (e5.isSmall)
            return 1;
          return N(r4, i4);
        };
        l.prototype.compareAbs = function(t5) {
          var e5 = st(t5), r4 = Math.abs(this.value), i4 = e5.value;
          if (e5.isSmall) {
            i4 = Math.abs(i4);
            return r4 === i4 ? 0 : r4 > i4 ? 1 : -1;
          }
          return -1;
        };
        f2.prototype.compareAbs = function(t5) {
          var e5 = this.value;
          var r4 = st(t5).value;
          e5 = e5 >= 0 ? e5 : -e5;
          r4 = r4 >= 0 ? r4 : -r4;
          return e5 === r4 ? 0 : e5 > r4 ? 1 : -1;
        };
        c.prototype.compare = function(t5) {
          if (t5 === 1 / 0)
            return -1;
          if (t5 === -1 / 0)
            return 1;
          var e5 = st(t5), r4 = this.value, i4 = e5.value;
          if (this.sign !== e5.sign)
            return e5.sign ? 1 : -1;
          if (e5.isSmall)
            return this.sign ? -1 : 1;
          return N(r4, i4) * (this.sign ? -1 : 1);
        };
        c.prototype.compareTo = c.prototype.compare;
        l.prototype.compare = function(t5) {
          if (t5 === 1 / 0)
            return -1;
          if (t5 === -1 / 0)
            return 1;
          var e5 = st(t5), r4 = this.value, i4 = e5.value;
          if (e5.isSmall)
            return r4 == i4 ? 0 : r4 > i4 ? 1 : -1;
          if (r4 < 0 !== e5.sign)
            return r4 < 0 ? -1 : 1;
          return r4 < 0 ? 1 : -1;
        };
        l.prototype.compareTo = l.prototype.compare;
        f2.prototype.compare = function(t5) {
          if (t5 === 1 / 0)
            return -1;
          if (t5 === -1 / 0)
            return 1;
          var e5 = this.value;
          var r4 = st(t5).value;
          return e5 === r4 ? 0 : e5 > r4 ? 1 : -1;
        };
        f2.prototype.compareTo = f2.prototype.compare;
        c.prototype.equals = function(t5) {
          return 0 === this.compare(t5);
        };
        f2.prototype.eq = f2.prototype.equals = l.prototype.eq = l.prototype.equals = c.prototype.eq = c.prototype.equals;
        c.prototype.notEquals = function(t5) {
          return 0 !== this.compare(t5);
        };
        f2.prototype.neq = f2.prototype.notEquals = l.prototype.neq = l.prototype.notEquals = c.prototype.neq = c.prototype.notEquals;
        c.prototype.greater = function(t5) {
          return this.compare(t5) > 0;
        };
        f2.prototype.gt = f2.prototype.greater = l.prototype.gt = l.prototype.greater = c.prototype.gt = c.prototype.greater;
        c.prototype.lesser = function(t5) {
          return this.compare(t5) < 0;
        };
        f2.prototype.lt = f2.prototype.lesser = l.prototype.lt = l.prototype.lesser = c.prototype.lt = c.prototype.lesser;
        c.prototype.greaterOrEquals = function(t5) {
          return this.compare(t5) >= 0;
        };
        f2.prototype.geq = f2.prototype.greaterOrEquals = l.prototype.geq = l.prototype.greaterOrEquals = c.prototype.geq = c.prototype.greaterOrEquals;
        c.prototype.lesserOrEquals = function(t5) {
          return this.compare(t5) <= 0;
        };
        f2.prototype.leq = f2.prototype.lesserOrEquals = l.prototype.leq = l.prototype.lesserOrEquals = c.prototype.leq = c.prototype.lesserOrEquals;
        c.prototype.isEven = function() {
          return 0 === (1 & this.value[0]);
        };
        l.prototype.isEven = function() {
          return 0 === (1 & this.value);
        };
        f2.prototype.isEven = function() {
          return (this.value & BigInt(1)) === BigInt(0);
        };
        c.prototype.isOdd = function() {
          return 1 === (1 & this.value[0]);
        };
        l.prototype.isOdd = function() {
          return 1 === (1 & this.value);
        };
        f2.prototype.isOdd = function() {
          return (this.value & BigInt(1)) === BigInt(1);
        };
        c.prototype.isPositive = function() {
          return !this.sign;
        };
        l.prototype.isPositive = function() {
          return this.value > 0;
        };
        f2.prototype.isPositive = l.prototype.isPositive;
        c.prototype.isNegative = function() {
          return this.sign;
        };
        l.prototype.isNegative = function() {
          return this.value < 0;
        };
        f2.prototype.isNegative = l.prototype.isNegative;
        c.prototype.isUnit = function() {
          return false;
        };
        l.prototype.isUnit = function() {
          return 1 === Math.abs(this.value);
        };
        f2.prototype.isUnit = function() {
          return this.abs().value === BigInt(1);
        };
        c.prototype.isZero = function() {
          return false;
        };
        l.prototype.isZero = function() {
          return 0 === this.value;
        };
        f2.prototype.isZero = function() {
          return this.value === BigInt(0);
        };
        c.prototype.isDivisibleBy = function(t5) {
          var e5 = st(t5);
          if (e5.isZero())
            return false;
          if (e5.isUnit())
            return true;
          if (0 === e5.compareAbs(2))
            return this.isEven();
          return this.mod(e5).isZero();
        };
        f2.prototype.isDivisibleBy = l.prototype.isDivisibleBy = c.prototype.isDivisibleBy;
        function P(t5) {
          var e5 = t5.abs();
          if (e5.isUnit())
            return false;
          if (e5.equals(2) || e5.equals(3) || e5.equals(5))
            return true;
          if (e5.isEven() || e5.isDivisibleBy(3) || e5.isDivisibleBy(5))
            return false;
          if (e5.lesser(49))
            return true;
        }
        function V(t5, e5) {
          var r4 = t5.prev(), i4 = r4, s3 = 0, a2, u2, c2;
          while (i4.isEven())
            i4 = i4.divide(2), s3++;
          t:
            for (u2 = 0; u2 < e5.length; u2++) {
              if (t5.lesser(e5[u2]))
                continue;
              c2 = n2(e5[u2]).modPow(i4, t5);
              if (c2.isUnit() || c2.equals(r4))
                continue;
              for (a2 = s3 - 1; 0 != a2; a2--) {
                c2 = c2.square().mod(t5);
                if (c2.isUnit())
                  return false;
                if (c2.equals(r4))
                  continue t;
              }
              return false;
            }
          return true;
        }
        c.prototype.isPrime = function(e5) {
          var r4 = P(this);
          if (r4 !== t4)
            return r4;
          var i4 = this.abs();
          var s3 = i4.bitLength();
          if (s3 <= 64)
            return V(i4, [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]);
          var a2 = Math.log(2) * s3.toJSNumber();
          var o3 = Math.ceil(true === e5 ? 2 * Math.pow(a2, 2) : a2);
          for (var u2 = [], c2 = 0; c2 < o3; c2++)
            u2.push(n2(c2 + 2));
          return V(i4, u2);
        };
        f2.prototype.isPrime = l.prototype.isPrime = c.prototype.isPrime;
        c.prototype.isProbablePrime = function(e5, r4) {
          var i4 = P(this);
          if (i4 !== t4)
            return i4;
          var s3 = this.abs();
          var a2 = e5 === t4 ? 5 : e5;
          for (var o3 = [], u2 = 0; u2 < a2; u2++)
            o3.push(n2.randBetween(2, s3.minus(2), r4));
          return V(s3, o3);
        };
        f2.prototype.isProbablePrime = l.prototype.isProbablePrime = c.prototype.isProbablePrime;
        c.prototype.modInv = function(t5) {
          var e5 = n2.zero, r4 = n2.one, i4 = st(t5), s3 = this.abs(), a2, o3, u2;
          while (!s3.isZero()) {
            a2 = i4.divide(s3);
            o3 = e5;
            u2 = i4;
            e5 = r4;
            i4 = s3;
            r4 = o3.subtract(a2.multiply(r4));
            s3 = u2.subtract(a2.multiply(s3));
          }
          if (!i4.isUnit())
            throw new Error(this.toString() + " and " + t5.toString() + " are not co-prime");
          if (-1 === e5.compare(0))
            e5 = e5.add(t5);
          if (this.isNegative())
            return e5.negate();
          return e5;
        };
        f2.prototype.modInv = l.prototype.modInv = c.prototype.modInv;
        c.prototype.next = function() {
          var t5 = this.value;
          if (this.sign)
            return E2(t5, 1, this.sign);
          return new c(S(t5, 1), this.sign);
        };
        l.prototype.next = function() {
          var t5 = this.value;
          if (t5 + 1 < i3)
            return new l(t5 + 1);
          return new c(s2, false);
        };
        f2.prototype.next = function() {
          return new f2(this.value + BigInt(1));
        };
        c.prototype.prev = function() {
          var t5 = this.value;
          if (this.sign)
            return new c(S(t5, 1), true);
          return E2(t5, 1, this.sign);
        };
        l.prototype.prev = function() {
          var t5 = this.value;
          if (t5 - 1 > -i3)
            return new l(t5 - 1);
          return new c(s2, true);
        };
        f2.prototype.prev = function() {
          return new f2(this.value - BigInt(1));
        };
        var L = [1];
        while (2 * L[L.length - 1] <= e4)
          L.push(2 * L[L.length - 1]);
        var H = L.length, U = L[H - 1];
        function K(t5) {
          return Math.abs(t5) <= e4;
        }
        c.prototype.shiftLeft = function(t5) {
          var e5 = st(t5).toJSNumber();
          if (!K(e5))
            throw new Error(String(e5) + " is too large for shifting.");
          if (e5 < 0)
            return this.shiftRight(-e5);
          var r4 = this;
          if (r4.isZero())
            return r4;
          while (e5 >= H) {
            r4 = r4.multiply(U);
            e5 -= H - 1;
          }
          return r4.multiply(L[e5]);
        };
        f2.prototype.shiftLeft = l.prototype.shiftLeft = c.prototype.shiftLeft;
        c.prototype.shiftRight = function(t5) {
          var e5;
          var r4 = st(t5).toJSNumber();
          if (!K(r4))
            throw new Error(String(r4) + " is too large for shifting.");
          if (r4 < 0)
            return this.shiftLeft(-r4);
          var i4 = this;
          while (r4 >= H) {
            if (i4.isZero() || i4.isNegative() && i4.isUnit())
              return i4;
            e5 = C(i4, U);
            i4 = e5[1].isNegative() ? e5[0].prev() : e5[0];
            r4 -= H - 1;
          }
          e5 = C(i4, L[r4]);
          return e5[1].isNegative() ? e5[0].prev() : e5[0];
        };
        f2.prototype.shiftRight = l.prototype.shiftRight = c.prototype.shiftRight;
        function j(t5, e5, r4) {
          e5 = st(e5);
          var i4 = t5.isNegative(), s3 = e5.isNegative();
          var a2 = i4 ? t5.not() : t5, o3 = s3 ? e5.not() : e5;
          var u2 = 0, c2 = 0;
          var l2 = null, f3 = null;
          var h2 = [];
          while (!a2.isZero() || !o3.isZero()) {
            l2 = C(a2, U);
            u2 = l2[1].toJSNumber();
            if (i4)
              u2 = U - 1 - u2;
            f3 = C(o3, U);
            c2 = f3[1].toJSNumber();
            if (s3)
              c2 = U - 1 - c2;
            a2 = l2[0];
            o3 = f3[0];
            h2.push(r4(u2, c2));
          }
          var d3 = 0 !== r4(i4 ? 1 : 0, s3 ? 1 : 0) ? n2(-1) : n2(0);
          for (var v2 = h2.length - 1; v2 >= 0; v2 -= 1)
            d3 = d3.multiply(U).add(n2(h2[v2]));
          return d3;
        }
        c.prototype.not = function() {
          return this.negate().prev();
        };
        f2.prototype.not = l.prototype.not = c.prototype.not;
        c.prototype.and = function(t5) {
          return j(this, t5, function(t6, e5) {
            return t6 & e5;
          });
        };
        f2.prototype.and = l.prototype.and = c.prototype.and;
        c.prototype.or = function(t5) {
          return j(this, t5, function(t6, e5) {
            return t6 | e5;
          });
        };
        f2.prototype.or = l.prototype.or = c.prototype.or;
        c.prototype.xor = function(t5) {
          return j(this, t5, function(t6, e5) {
            return t6 ^ e5;
          });
        };
        f2.prototype.xor = l.prototype.xor = c.prototype.xor;
        var q = 1 << 30, F = (e4 & -e4) * (e4 & -e4) | q;
        function z(t5) {
          var r4 = t5.value, i4 = "number" === typeof r4 ? r4 | q : "bigint" === typeof r4 ? r4 | BigInt(q) : r4[0] + r4[1] * e4 | F;
          return i4 & -i4;
        }
        function G(t5, e5) {
          if (e5.compareTo(t5) <= 0) {
            var r4 = G(t5, e5.square(e5));
            var i4 = r4.p;
            var s3 = r4.e;
            var a2 = i4.multiply(e5);
            return a2.compareTo(t5) <= 0 ? { p: a2, e: 2 * s3 + 1 } : { p: i4, e: 2 * s3 };
          }
          return { p: n2(1), e: 0 };
        }
        c.prototype.bitLength = function() {
          var t5 = this;
          if (t5.compareTo(n2(0)) < 0)
            t5 = t5.negate().subtract(n2(1));
          if (0 === t5.compareTo(n2(0)))
            return n2(0);
          return n2(G(t5, n2(2)).e).add(n2(1));
        };
        f2.prototype.bitLength = l.prototype.bitLength = c.prototype.bitLength;
        function Y(t5, e5) {
          t5 = st(t5);
          e5 = st(e5);
          return t5.greater(e5) ? t5 : e5;
        }
        function W(t5, e5) {
          t5 = st(t5);
          e5 = st(e5);
          return t5.lesser(e5) ? t5 : e5;
        }
        function J(t5, e5) {
          t5 = st(t5).abs();
          e5 = st(e5).abs();
          if (t5.equals(e5))
            return t5;
          if (t5.isZero())
            return e5;
          if (e5.isZero())
            return t5;
          var r4 = u[1], i4, n3;
          while (t5.isEven() && e5.isEven()) {
            i4 = W(z(t5), z(e5));
            t5 = t5.divide(i4);
            e5 = e5.divide(i4);
            r4 = r4.multiply(i4);
          }
          while (t5.isEven())
            t5 = t5.divide(z(t5));
          do {
            while (e5.isEven())
              e5 = e5.divide(z(e5));
            if (t5.greater(e5)) {
              n3 = e5;
              e5 = t5;
              t5 = n3;
            }
            e5 = e5.subtract(t5);
          } while (!e5.isZero());
          return r4.isUnit() ? t5 : t5.multiply(r4);
        }
        function Z(t5, e5) {
          t5 = st(t5).abs();
          e5 = st(e5).abs();
          return t5.divide(J(t5, e5)).multiply(e5);
        }
        function $(t5, r4, i4) {
          t5 = st(t5);
          r4 = st(r4);
          var n3 = i4 || Math.random;
          var s3 = W(t5, r4), a2 = Y(t5, r4);
          var o3 = a2.subtract(s3).add(1);
          if (o3.isSmall)
            return s3.add(Math.floor(n3() * o3));
          var c2 = et(o3, e4).value;
          var l2 = [], f3 = true;
          for (var h2 = 0; h2 < c2.length; h2++) {
            var d3 = f3 ? c2[h2] + (h2 + 1 < c2.length ? c2[h2 + 1] / e4 : 0) : e4;
            var v2 = y(n3() * d3);
            l2.push(v2);
            if (v2 < c2[h2])
              f3 = false;
          }
          return s3.add(u.fromArray(l2, e4, false));
        }
        var X = function(t5, e5, r4, i4) {
          r4 = r4 || a;
          t5 = String(t5);
          if (!i4) {
            t5 = t5.toLowerCase();
            r4 = r4.toLowerCase();
          }
          var n3 = t5.length;
          var s3;
          var o3 = Math.abs(e5);
          var u2 = {};
          for (s3 = 0; s3 < r4.length; s3++)
            u2[r4[s3]] = s3;
          for (s3 = 0; s3 < n3; s3++) {
            var c2 = t5[s3];
            if ("-" === c2)
              continue;
            if (c2 in u2) {
              if (u2[c2] >= o3) {
                if ("1" === c2 && 1 === o3)
                  continue;
                throw new Error(c2 + " is not a valid digit in base " + e5 + ".");
              }
            }
          }
          e5 = st(e5);
          var l2 = [];
          var f3 = "-" === t5[0];
          for (s3 = f3 ? 1 : 0; s3 < t5.length; s3++) {
            var c2 = t5[s3];
            if (c2 in u2)
              l2.push(st(u2[c2]));
            else if ("<" === c2) {
              var h2 = s3;
              do {
                s3++;
              } while (">" !== t5[s3] && s3 < t5.length);
              l2.push(st(t5.slice(h2 + 1, s3)));
            } else
              throw new Error(c2 + " is not a valid character");
          }
          return Q(l2, e5, f3);
        };
        function Q(t5, e5, r4) {
          var i4 = u[0], n3 = u[1], s3;
          for (s3 = t5.length - 1; s3 >= 0; s3--) {
            i4 = i4.add(t5[s3].times(n3));
            n3 = n3.times(e5);
          }
          return r4 ? i4.negate() : i4;
        }
        function tt2(t5, e5) {
          e5 = e5 || a;
          if (t5 < e5.length)
            return e5[t5];
          return "<" + t5 + ">";
        }
        function et(t5, e5) {
          e5 = n2(e5);
          if (e5.isZero()) {
            if (t5.isZero())
              return { value: [0], isNegative: false };
            throw new Error("Cannot convert nonzero numbers to base 0.");
          }
          if (e5.equals(-1)) {
            if (t5.isZero())
              return { value: [0], isNegative: false };
            if (t5.isNegative())
              return { value: [].concat.apply([], Array.apply(null, Array(-t5.toJSNumber())).map(Array.prototype.valueOf, [1, 0])), isNegative: false };
            var r4 = Array.apply(null, Array(t5.toJSNumber() - 1)).map(Array.prototype.valueOf, [0, 1]);
            r4.unshift([1]);
            return { value: [].concat.apply([], r4), isNegative: false };
          }
          var i4 = false;
          if (t5.isNegative() && e5.isPositive()) {
            i4 = true;
            t5 = t5.abs();
          }
          if (e5.isUnit()) {
            if (t5.isZero())
              return { value: [0], isNegative: false };
            return { value: Array.apply(null, Array(t5.toJSNumber())).map(Number.prototype.valueOf, 1), isNegative: i4 };
          }
          var s3 = [];
          var a2 = t5, o3;
          while (a2.isNegative() || a2.compareAbs(e5) >= 0) {
            o3 = a2.divmod(e5);
            a2 = o3.quotient;
            var u2 = o3.remainder;
            if (u2.isNegative()) {
              u2 = e5.minus(u2).abs();
              a2 = a2.next();
            }
            s3.push(u2.toJSNumber());
          }
          s3.push(a2.toJSNumber());
          return { value: s3.reverse(), isNegative: i4 };
        }
        function rt(t5, e5, r4) {
          var i4 = et(t5, e5);
          return (i4.isNegative ? "-" : "") + i4.value.map(function(t6) {
            return tt2(t6, r4);
          }).join("");
        }
        c.prototype.toArray = function(t5) {
          return et(this, t5);
        };
        l.prototype.toArray = function(t5) {
          return et(this, t5);
        };
        f2.prototype.toArray = function(t5) {
          return et(this, t5);
        };
        c.prototype.toString = function(e5, r4) {
          if (e5 === t4)
            e5 = 10;
          if (10 !== e5)
            return rt(this, e5, r4);
          var i4 = this.value, n3 = i4.length, s3 = String(i4[--n3]), a2 = "0000000", o3;
          while (--n3 >= 0) {
            o3 = String(i4[n3]);
            s3 += a2.slice(o3.length) + o3;
          }
          var u2 = this.sign ? "-" : "";
          return u2 + s3;
        };
        l.prototype.toString = function(e5, r4) {
          if (e5 === t4)
            e5 = 10;
          if (10 != e5)
            return rt(this, e5, r4);
          return String(this.value);
        };
        f2.prototype.toString = l.prototype.toString;
        f2.prototype.toJSON = c.prototype.toJSON = l.prototype.toJSON = function() {
          return this.toString();
        };
        c.prototype.valueOf = function() {
          return parseInt(this.toString(), 10);
        };
        c.prototype.toJSNumber = c.prototype.valueOf;
        l.prototype.valueOf = function() {
          return this.value;
        };
        l.prototype.toJSNumber = l.prototype.valueOf;
        f2.prototype.valueOf = f2.prototype.toJSNumber = function() {
          return parseInt(this.toString(), 10);
        };
        function it(t5) {
          if (h(+t5)) {
            var e5 = +t5;
            if (e5 === y(e5))
              return o2 ? new f2(BigInt(e5)) : new l(e5);
            throw new Error("Invalid integer: " + t5);
          }
          var i4 = "-" === t5[0];
          if (i4)
            t5 = t5.slice(1);
          var n3 = t5.split(/e/i);
          if (n3.length > 2)
            throw new Error("Invalid integer: " + n3.join("e"));
          if (2 === n3.length) {
            var s3 = n3[1];
            if ("+" === s3[0])
              s3 = s3.slice(1);
            s3 = +s3;
            if (s3 !== y(s3) || !h(s3))
              throw new Error("Invalid integer: " + s3 + " is not a valid exponent.");
            var a2 = n3[0];
            var u2 = a2.indexOf(".");
            if (u2 >= 0) {
              s3 -= a2.length - u2 - 1;
              a2 = a2.slice(0, u2) + a2.slice(u2 + 1);
            }
            if (s3 < 0)
              throw new Error("Cannot include negative exponent part for integers");
            a2 += new Array(s3 + 1).join("0");
            t5 = a2;
          }
          var d3 = /^([0-9][0-9]*)$/.test(t5);
          if (!d3)
            throw new Error("Invalid integer: " + t5);
          if (o2)
            return new f2(BigInt(i4 ? "-" + t5 : t5));
          var v2 = [], g2 = t5.length, m2 = r3, w2 = g2 - m2;
          while (g2 > 0) {
            v2.push(+t5.slice(w2, g2));
            w2 -= m2;
            if (w2 < 0)
              w2 = 0;
            g2 -= m2;
          }
          p2(v2);
          return new c(v2, i4);
        }
        function nt(t5) {
          if (o2)
            return new f2(BigInt(t5));
          if (h(t5)) {
            if (t5 !== y(t5))
              throw new Error(t5 + " is not an integer.");
            return new l(t5);
          }
          return it(t5.toString());
        }
        function st(t5) {
          if ("number" === typeof t5)
            return nt(t5);
          if ("string" === typeof t5)
            return it(t5);
          if ("bigint" === typeof t5)
            return new f2(t5);
          return t5;
        }
        for (var at = 0; at < 1e3; at++) {
          u[at] = st(at);
          if (at > 0)
            u[-at] = st(-at);
        }
        u.one = u[1];
        u.zero = u[0];
        u.minusOne = u[-1];
        u.max = Y;
        u.min = W;
        u.gcd = J;
        u.lcm = Z;
        u.isInstance = function(t5) {
          return t5 instanceof c || t5 instanceof l || t5 instanceof f2;
        };
        u.randBetween = $;
        u.fromArray = function(t5, e5, r4) {
          return Q(t5.map(st), st(e5 || 10), r4);
        };
        return u;
      }();
      if (t3.hasOwnProperty("exports"))
        t3.exports = n2;
      i2 = function() {
        return n2;
      }.call(e3, r2, e3, t3), void 0 !== i2 && (t3.exports = i2);
    }, 452: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(8269), r2(8214), r2(888), r2(5109));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.BlockCipher;
          var n2 = e4.algo;
          var s2 = [];
          var a = [];
          var o2 = [];
          var u = [];
          var c = [];
          var l = [];
          var f2 = [];
          var h = [];
          var d2 = [];
          var v = [];
          (function() {
            var t5 = [];
            for (var e5 = 0; e5 < 256; e5++)
              if (e5 < 128)
                t5[e5] = e5 << 1;
              else
                t5[e5] = e5 << 1 ^ 283;
            var r4 = 0;
            var i3 = 0;
            for (var e5 = 0; e5 < 256; e5++) {
              var n3 = i3 ^ i3 << 1 ^ i3 << 2 ^ i3 << 3 ^ i3 << 4;
              n3 = n3 >>> 8 ^ 255 & n3 ^ 99;
              s2[r4] = n3;
              a[n3] = r4;
              var p3 = t5[r4];
              var g2 = t5[p3];
              var y = t5[g2];
              var m = 257 * t5[n3] ^ 16843008 * n3;
              o2[r4] = m << 24 | m >>> 8;
              u[r4] = m << 16 | m >>> 16;
              c[r4] = m << 8 | m >>> 24;
              l[r4] = m;
              var m = 16843009 * y ^ 65537 * g2 ^ 257 * p3 ^ 16843008 * r4;
              f2[n3] = m << 24 | m >>> 8;
              h[n3] = m << 16 | m >>> 16;
              d2[n3] = m << 8 | m >>> 24;
              v[n3] = m;
              if (!r4)
                r4 = i3 = 1;
              else {
                r4 = p3 ^ t5[t5[t5[y ^ p3]]];
                i3 ^= t5[t5[i3]];
              }
            }
          })();
          var p2 = [0, 1, 2, 4, 8, 16, 32, 64, 128, 27, 54];
          var g = n2.AES = i2.extend({ _doReset: function() {
            var t5;
            if (this._nRounds && this._keyPriorReset === this._key)
              return;
            var e5 = this._keyPriorReset = this._key;
            var r4 = e5.words;
            var i3 = e5.sigBytes / 4;
            var n3 = this._nRounds = i3 + 6;
            var a2 = 4 * (n3 + 1);
            var o3 = this._keySchedule = [];
            for (var u2 = 0; u2 < a2; u2++)
              if (u2 < i3)
                o3[u2] = r4[u2];
              else {
                t5 = o3[u2 - 1];
                if (!(u2 % i3)) {
                  t5 = t5 << 8 | t5 >>> 24;
                  t5 = s2[t5 >>> 24] << 24 | s2[t5 >>> 16 & 255] << 16 | s2[t5 >>> 8 & 255] << 8 | s2[255 & t5];
                  t5 ^= p2[u2 / i3 | 0] << 24;
                } else if (i3 > 6 && u2 % i3 == 4)
                  t5 = s2[t5 >>> 24] << 24 | s2[t5 >>> 16 & 255] << 16 | s2[t5 >>> 8 & 255] << 8 | s2[255 & t5];
                o3[u2] = o3[u2 - i3] ^ t5;
              }
            var c2 = this._invKeySchedule = [];
            for (var l2 = 0; l2 < a2; l2++) {
              var u2 = a2 - l2;
              if (l2 % 4)
                var t5 = o3[u2];
              else
                var t5 = o3[u2 - 4];
              if (l2 < 4 || u2 <= 4)
                c2[l2] = t5;
              else
                c2[l2] = f2[s2[t5 >>> 24]] ^ h[s2[t5 >>> 16 & 255]] ^ d2[s2[t5 >>> 8 & 255]] ^ v[s2[255 & t5]];
            }
          }, encryptBlock: function(t5, e5) {
            this._doCryptBlock(t5, e5, this._keySchedule, o2, u, c, l, s2);
          }, decryptBlock: function(t5, e5) {
            var r4 = t5[e5 + 1];
            t5[e5 + 1] = t5[e5 + 3];
            t5[e5 + 3] = r4;
            this._doCryptBlock(t5, e5, this._invKeySchedule, f2, h, d2, v, a);
            var r4 = t5[e5 + 1];
            t5[e5 + 1] = t5[e5 + 3];
            t5[e5 + 3] = r4;
          }, _doCryptBlock: function(t5, e5, r4, i3, n3, s3, a2, o3) {
            var u2 = this._nRounds;
            var c2 = t5[e5] ^ r4[0];
            var l2 = t5[e5 + 1] ^ r4[1];
            var f3 = t5[e5 + 2] ^ r4[2];
            var h2 = t5[e5 + 3] ^ r4[3];
            var d3 = 4;
            for (var v2 = 1; v2 < u2; v2++) {
              var p3 = i3[c2 >>> 24] ^ n3[l2 >>> 16 & 255] ^ s3[f3 >>> 8 & 255] ^ a2[255 & h2] ^ r4[d3++];
              var g2 = i3[l2 >>> 24] ^ n3[f3 >>> 16 & 255] ^ s3[h2 >>> 8 & 255] ^ a2[255 & c2] ^ r4[d3++];
              var y = i3[f3 >>> 24] ^ n3[h2 >>> 16 & 255] ^ s3[c2 >>> 8 & 255] ^ a2[255 & l2] ^ r4[d3++];
              var m = i3[h2 >>> 24] ^ n3[c2 >>> 16 & 255] ^ s3[l2 >>> 8 & 255] ^ a2[255 & f3] ^ r4[d3++];
              c2 = p3;
              l2 = g2;
              f3 = y;
              h2 = m;
            }
            var p3 = (o3[c2 >>> 24] << 24 | o3[l2 >>> 16 & 255] << 16 | o3[f3 >>> 8 & 255] << 8 | o3[255 & h2]) ^ r4[d3++];
            var g2 = (o3[l2 >>> 24] << 24 | o3[f3 >>> 16 & 255] << 16 | o3[h2 >>> 8 & 255] << 8 | o3[255 & c2]) ^ r4[d3++];
            var y = (o3[f3 >>> 24] << 24 | o3[h2 >>> 16 & 255] << 16 | o3[c2 >>> 8 & 255] << 8 | o3[255 & l2]) ^ r4[d3++];
            var m = (o3[h2 >>> 24] << 24 | o3[c2 >>> 16 & 255] << 16 | o3[l2 >>> 8 & 255] << 8 | o3[255 & f3]) ^ r4[d3++];
            t5[e5] = p3;
            t5[e5 + 1] = g2;
            t5[e5 + 2] = y;
            t5[e5 + 3] = m;
          }, keySize: 256 / 32 });
          e4.AES = i2._createHelper(g);
        })();
        return t4.AES;
      });
    }, 5109: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(888));
      })(this, function(t4) {
        t4.lib.Cipher || function(e4) {
          var r3 = t4;
          var i2 = r3.lib;
          var n2 = i2.Base;
          var s2 = i2.WordArray;
          var a = i2.BufferedBlockAlgorithm;
          var o2 = r3.enc;
          o2.Utf8;
          var c = o2.Base64;
          var l = r3.algo;
          var f2 = l.EvpKDF;
          var h = i2.Cipher = a.extend({ cfg: n2.extend(), createEncryptor: function(t5, e5) {
            return this.create(this._ENC_XFORM_MODE, t5, e5);
          }, createDecryptor: function(t5, e5) {
            return this.create(this._DEC_XFORM_MODE, t5, e5);
          }, init: function(t5, e5, r4) {
            this.cfg = this.cfg.extend(r4);
            this._xformMode = t5;
            this._key = e5;
            this.reset();
          }, reset: function() {
            a.reset.call(this);
            this._doReset();
          }, process: function(t5) {
            this._append(t5);
            return this._process();
          }, finalize: function(t5) {
            if (t5)
              this._append(t5);
            var e5 = this._doFinalize();
            return e5;
          }, keySize: 128 / 32, ivSize: 128 / 32, _ENC_XFORM_MODE: 1, _DEC_XFORM_MODE: 2, _createHelper: function() {
            function t5(t6) {
              if ("string" == typeof t6)
                return T;
              else
                return E2;
            }
            return function(e5) {
              return { encrypt: function(r4, i3, n3) {
                return t5(i3).encrypt(e5, r4, i3, n3);
              }, decrypt: function(r4, i3, n3) {
                return t5(i3).decrypt(e5, r4, i3, n3);
              } };
            };
          }() });
          i2.StreamCipher = h.extend({ _doFinalize: function() {
            var t5 = this._process(true);
            return t5;
          }, blockSize: 1 });
          var v = r3.mode = {};
          var p2 = i2.BlockCipherMode = n2.extend({ createEncryptor: function(t5, e5) {
            return this.Encryptor.create(t5, e5);
          }, createDecryptor: function(t5, e5) {
            return this.Decryptor.create(t5, e5);
          }, init: function(t5, e5) {
            this._cipher = t5;
            this._iv = e5;
          } });
          var g = v.CBC = function() {
            var t5 = p2.extend();
            t5.Encryptor = t5.extend({ processBlock: function(t6, e5) {
              var i3 = this._cipher;
              var n3 = i3.blockSize;
              r4.call(this, t6, e5, n3);
              i3.encryptBlock(t6, e5);
              this._prevBlock = t6.slice(e5, e5 + n3);
            } });
            t5.Decryptor = t5.extend({ processBlock: function(t6, e5) {
              var i3 = this._cipher;
              var n3 = i3.blockSize;
              var s3 = t6.slice(e5, e5 + n3);
              i3.decryptBlock(t6, e5);
              r4.call(this, t6, e5, n3);
              this._prevBlock = s3;
            } });
            function r4(t6, r5, i3) {
              var n3;
              var s3 = this._iv;
              if (s3) {
                n3 = s3;
                this._iv = e4;
              } else
                n3 = this._prevBlock;
              for (var a2 = 0; a2 < i3; a2++)
                t6[r5 + a2] ^= n3[a2];
            }
            return t5;
          }();
          var y = r3.pad = {};
          var m = y.Pkcs7 = { pad: function(t5, e5) {
            var r4 = 4 * e5;
            var i3 = r4 - t5.sigBytes % r4;
            var n3 = i3 << 24 | i3 << 16 | i3 << 8 | i3;
            var a2 = [];
            for (var o3 = 0; o3 < i3; o3 += 4)
              a2.push(n3);
            var u = s2.create(a2, i3);
            t5.concat(u);
          }, unpad: function(t5) {
            var e5 = 255 & t5.words[t5.sigBytes - 1 >>> 2];
            t5.sigBytes -= e5;
          } };
          i2.BlockCipher = h.extend({ cfg: h.cfg.extend({ mode: g, padding: m }), reset: function() {
            var t5;
            h.reset.call(this);
            var e5 = this.cfg;
            var r4 = e5.iv;
            var i3 = e5.mode;
            if (this._xformMode == this._ENC_XFORM_MODE)
              t5 = i3.createEncryptor;
            else {
              t5 = i3.createDecryptor;
              this._minBufferSize = 1;
            }
            if (this._mode && this._mode.__creator == t5)
              this._mode.init(this, r4 && r4.words);
            else {
              this._mode = t5.call(i3, this, r4 && r4.words);
              this._mode.__creator = t5;
            }
          }, _doProcessBlock: function(t5, e5) {
            this._mode.processBlock(t5, e5);
          }, _doFinalize: function() {
            var t5;
            var e5 = this.cfg.padding;
            if (this._xformMode == this._ENC_XFORM_MODE) {
              e5.pad(this._data, this.blockSize);
              t5 = this._process(true);
            } else {
              t5 = this._process(true);
              e5.unpad(t5);
            }
            return t5;
          }, blockSize: 128 / 32 });
          var S = i2.CipherParams = n2.extend({ init: function(t5) {
            this.mixIn(t5);
          }, toString: function(t5) {
            return (t5 || this.formatter).stringify(this);
          } });
          var _ = r3.format = {};
          var b = _.OpenSSL = { stringify: function(t5) {
            var e5;
            var r4 = t5.ciphertext;
            var i3 = t5.salt;
            if (i3)
              e5 = s2.create([1398893684, 1701076831]).concat(i3).concat(r4);
            else
              e5 = r4;
            return e5.toString(c);
          }, parse: function(t5) {
            var e5;
            var r4 = c.parse(t5);
            var i3 = r4.words;
            if (1398893684 == i3[0] && 1701076831 == i3[1]) {
              e5 = s2.create(i3.slice(2, 4));
              i3.splice(0, 4);
              r4.sigBytes -= 16;
            }
            return S.create({ ciphertext: r4, salt: e5 });
          } };
          var E2 = i2.SerializableCipher = n2.extend({ cfg: n2.extend({ format: b }), encrypt: function(t5, e5, r4, i3) {
            i3 = this.cfg.extend(i3);
            var n3 = t5.createEncryptor(r4, i3);
            var s3 = n3.finalize(e5);
            var a2 = n3.cfg;
            return S.create({ ciphertext: s3, key: r4, iv: a2.iv, algorithm: t5, mode: a2.mode, padding: a2.padding, blockSize: t5.blockSize, formatter: i3.format });
          }, decrypt: function(t5, e5, r4, i3) {
            i3 = this.cfg.extend(i3);
            e5 = this._parse(e5, i3.format);
            var n3 = t5.createDecryptor(r4, i3).finalize(e5.ciphertext);
            return n3;
          }, _parse: function(t5, e5) {
            if ("string" == typeof t5)
              return e5.parse(t5, this);
            else
              return t5;
          } });
          var D = r3.kdf = {};
          var M = D.OpenSSL = { execute: function(t5, e5, r4, i3) {
            if (!i3)
              i3 = s2.random(64 / 8);
            var n3 = f2.create({ keySize: e5 + r4 }).compute(t5, i3);
            var a2 = s2.create(n3.words.slice(e5), 4 * r4);
            n3.sigBytes = 4 * e5;
            return S.create({ key: n3, iv: a2, salt: i3 });
          } };
          var T = i2.PasswordBasedCipher = E2.extend({ cfg: E2.cfg.extend({ kdf: M }), encrypt: function(t5, e5, r4, i3) {
            i3 = this.cfg.extend(i3);
            var n3 = i3.kdf.execute(r4, t5.keySize, t5.ivSize);
            i3.iv = n3.iv;
            var s3 = E2.encrypt.call(this, t5, e5, n3.key, i3);
            s3.mixIn(n3);
            return s3;
          }, decrypt: function(t5, e5, r4, i3) {
            i3 = this.cfg.extend(i3);
            e5 = this._parse(e5, i3.format);
            var n3 = i3.kdf.execute(r4, t5.keySize, t5.ivSize, e5.salt);
            i3.iv = n3.iv;
            var s3 = E2.decrypt.call(this, t5, e5, n3.key, i3);
            return s3;
          } });
        }();
      });
    }, 8249: function(t3, e3, r2) {
      (function(r3, i2) {
        t3.exports = i2();
      })(this, function() {
        var t4 = t4 || function(t5, e4) {
          var i2;
          if ("undefined" !== typeof window && window.crypto)
            i2 = window.crypto;
          if ("undefined" !== typeof self && self.crypto)
            i2 = self.crypto;
          if ("undefined" !== typeof globalThis && globalThis.crypto)
            i2 = globalThis.crypto;
          if (!i2 && "undefined" !== typeof window && window.msCrypto)
            i2 = window.msCrypto;
          if (!i2 && "undefined" !== typeof r2.g && r2.g.crypto)
            i2 = r2.g.crypto;
          if (!i2 && true)
            try {
              i2 = r2(2480);
            } catch (t6) {
            }
          var n2 = function() {
            if (i2) {
              if ("function" === typeof i2.getRandomValues)
                try {
                  return i2.getRandomValues(new Uint32Array(1))[0];
                } catch (t6) {
                }
              if ("function" === typeof i2.randomBytes)
                try {
                  return i2.randomBytes(4).readInt32LE();
                } catch (t6) {
                }
            }
            throw new Error("Native crypto module could not be used to get secure random number.");
          };
          var s2 = Object.create || function() {
            function t6() {
            }
            return function(e5) {
              var r3;
              t6.prototype = e5;
              r3 = new t6();
              t6.prototype = null;
              return r3;
            };
          }();
          var a = {};
          var o2 = a.lib = {};
          var u = o2.Base = function() {
            return { extend: function(t6) {
              var e5 = s2(this);
              if (t6)
                e5.mixIn(t6);
              if (!e5.hasOwnProperty("init") || this.init === e5.init)
                e5.init = function() {
                  e5.$super.init.apply(this, arguments);
                };
              e5.init.prototype = e5;
              e5.$super = this;
              return e5;
            }, create: function() {
              var t6 = this.extend();
              t6.init.apply(t6, arguments);
              return t6;
            }, init: function() {
            }, mixIn: function(t6) {
              for (var e5 in t6)
                if (t6.hasOwnProperty(e5))
                  this[e5] = t6[e5];
              if (t6.hasOwnProperty("toString"))
                this.toString = t6.toString;
            }, clone: function() {
              return this.init.prototype.extend(this);
            } };
          }();
          var c = o2.WordArray = u.extend({ init: function(t6, r3) {
            t6 = this.words = t6 || [];
            if (r3 != e4)
              this.sigBytes = r3;
            else
              this.sigBytes = 4 * t6.length;
          }, toString: function(t6) {
            return (t6 || f2).stringify(this);
          }, concat: function(t6) {
            var e5 = this.words;
            var r3 = t6.words;
            var i3 = this.sigBytes;
            var n3 = t6.sigBytes;
            this.clamp();
            if (i3 % 4)
              for (var s3 = 0; s3 < n3; s3++) {
                var a2 = r3[s3 >>> 2] >>> 24 - s3 % 4 * 8 & 255;
                e5[i3 + s3 >>> 2] |= a2 << 24 - (i3 + s3) % 4 * 8;
              }
            else
              for (var o3 = 0; o3 < n3; o3 += 4)
                e5[i3 + o3 >>> 2] = r3[o3 >>> 2];
            this.sigBytes += n3;
            return this;
          }, clamp: function() {
            var e5 = this.words;
            var r3 = this.sigBytes;
            e5[r3 >>> 2] &= 4294967295 << 32 - r3 % 4 * 8;
            e5.length = t5.ceil(r3 / 4);
          }, clone: function() {
            var t6 = u.clone.call(this);
            t6.words = this.words.slice(0);
            return t6;
          }, random: function(t6) {
            var e5 = [];
            for (var r3 = 0; r3 < t6; r3 += 4)
              e5.push(n2());
            return new c.init(e5, t6);
          } });
          var l = a.enc = {};
          var f2 = l.Hex = { stringify: function(t6) {
            var e5 = t6.words;
            var r3 = t6.sigBytes;
            var i3 = [];
            for (var n3 = 0; n3 < r3; n3++) {
              var s3 = e5[n3 >>> 2] >>> 24 - n3 % 4 * 8 & 255;
              i3.push((s3 >>> 4).toString(16));
              i3.push((15 & s3).toString(16));
            }
            return i3.join("");
          }, parse: function(t6) {
            var e5 = t6.length;
            var r3 = [];
            for (var i3 = 0; i3 < e5; i3 += 2)
              r3[i3 >>> 3] |= parseInt(t6.substr(i3, 2), 16) << 24 - i3 % 8 * 4;
            return new c.init(r3, e5 / 2);
          } };
          var h = l.Latin1 = { stringify: function(t6) {
            var e5 = t6.words;
            var r3 = t6.sigBytes;
            var i3 = [];
            for (var n3 = 0; n3 < r3; n3++) {
              var s3 = e5[n3 >>> 2] >>> 24 - n3 % 4 * 8 & 255;
              i3.push(String.fromCharCode(s3));
            }
            return i3.join("");
          }, parse: function(t6) {
            var e5 = t6.length;
            var r3 = [];
            for (var i3 = 0; i3 < e5; i3++)
              r3[i3 >>> 2] |= (255 & t6.charCodeAt(i3)) << 24 - i3 % 4 * 8;
            return new c.init(r3, e5);
          } };
          var d2 = l.Utf8 = { stringify: function(t6) {
            try {
              return decodeURIComponent(escape(h.stringify(t6)));
            } catch (t7) {
              throw new Error("Malformed UTF-8 data");
            }
          }, parse: function(t6) {
            return h.parse(unescape(encodeURIComponent(t6)));
          } };
          var v = o2.BufferedBlockAlgorithm = u.extend({ reset: function() {
            this._data = new c.init();
            this._nDataBytes = 0;
          }, _append: function(t6) {
            if ("string" == typeof t6)
              t6 = d2.parse(t6);
            this._data.concat(t6);
            this._nDataBytes += t6.sigBytes;
          }, _process: function(e5) {
            var r3;
            var i3 = this._data;
            var n3 = i3.words;
            var s3 = i3.sigBytes;
            var a2 = this.blockSize;
            var o3 = 4 * a2;
            var u2 = s3 / o3;
            if (e5)
              u2 = t5.ceil(u2);
            else
              u2 = t5.max((0 | u2) - this._minBufferSize, 0);
            var l2 = u2 * a2;
            var f3 = t5.min(4 * l2, s3);
            if (l2) {
              for (var h2 = 0; h2 < l2; h2 += a2)
                this._doProcessBlock(n3, h2);
              r3 = n3.splice(0, l2);
              i3.sigBytes -= f3;
            }
            return new c.init(r3, f3);
          }, clone: function() {
            var t6 = u.clone.call(this);
            t6._data = this._data.clone();
            return t6;
          }, _minBufferSize: 0 });
          o2.Hasher = v.extend({ cfg: u.extend(), init: function(t6) {
            this.cfg = this.cfg.extend(t6);
            this.reset();
          }, reset: function() {
            v.reset.call(this);
            this._doReset();
          }, update: function(t6) {
            this._append(t6);
            this._process();
            return this;
          }, finalize: function(t6) {
            if (t6)
              this._append(t6);
            var e5 = this._doFinalize();
            return e5;
          }, blockSize: 512 / 32, _createHelper: function(t6) {
            return function(e5, r3) {
              return new t6.init(r3).finalize(e5);
            };
          }, _createHmacHelper: function(t6) {
            return function(e5, r3) {
              return new g.HMAC.init(t6, r3).finalize(e5);
            };
          } });
          var g = a.algo = {};
          return a;
        }(Math);
        return t4;
      });
    }, 8269: function(t3, e3, r2) {
      (function(i2, n2) {
        t3.exports = n2(r2(8249));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.WordArray;
          var n2 = e4.enc;
          n2.Base64 = { stringify: function(t5) {
            var e5 = t5.words;
            var r4 = t5.sigBytes;
            var i3 = this._map;
            t5.clamp();
            var n3 = [];
            for (var s2 = 0; s2 < r4; s2 += 3) {
              var a2 = e5[s2 >>> 2] >>> 24 - s2 % 4 * 8 & 255;
              var o2 = e5[s2 + 1 >>> 2] >>> 24 - (s2 + 1) % 4 * 8 & 255;
              var u = e5[s2 + 2 >>> 2] >>> 24 - (s2 + 2) % 4 * 8 & 255;
              var c = a2 << 16 | o2 << 8 | u;
              for (var l = 0; l < 4 && s2 + 0.75 * l < r4; l++)
                n3.push(i3.charAt(c >>> 6 * (3 - l) & 63));
            }
            var f2 = i3.charAt(64);
            if (f2)
              while (n3.length % 4)
                n3.push(f2);
            return n3.join("");
          }, parse: function(t5) {
            var e5 = t5.length;
            var r4 = this._map;
            var i3 = this._reverseMap;
            if (!i3) {
              i3 = this._reverseMap = [];
              for (var n3 = 0; n3 < r4.length; n3++)
                i3[r4.charCodeAt(n3)] = n3;
            }
            var s2 = r4.charAt(64);
            if (s2) {
              var o2 = t5.indexOf(s2);
              if (-1 !== o2)
                e5 = o2;
            }
            return a(t5, e5, i3);
          }, _map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=" };
          function a(t5, e5, r4) {
            var n3 = [];
            var s2 = 0;
            for (var a2 = 0; a2 < e5; a2++)
              if (a2 % 4) {
                var o2 = r4[t5.charCodeAt(a2 - 1)] << a2 % 4 * 2;
                var u = r4[t5.charCodeAt(a2)] >>> 6 - a2 % 4 * 2;
                var c = o2 | u;
                n3[s2 >>> 2] |= c << 24 - s2 % 4 * 8;
                s2++;
              }
            return i2.create(n3, s2);
          }
        })();
        return t4.enc.Base64;
      });
    }, 3786: function(t3, e3, r2) {
      (function(i2, n2) {
        t3.exports = n2(r2(8249));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.WordArray;
          var n2 = e4.enc;
          n2.Base64url = { stringify: function(t5, e5 = true) {
            var r4 = t5.words;
            var i3 = t5.sigBytes;
            var n3 = e5 ? this._safe_map : this._map;
            t5.clamp();
            var s2 = [];
            for (var a2 = 0; a2 < i3; a2 += 3) {
              var o2 = r4[a2 >>> 2] >>> 24 - a2 % 4 * 8 & 255;
              var u = r4[a2 + 1 >>> 2] >>> 24 - (a2 + 1) % 4 * 8 & 255;
              var c = r4[a2 + 2 >>> 2] >>> 24 - (a2 + 2) % 4 * 8 & 255;
              var l = o2 << 16 | u << 8 | c;
              for (var f2 = 0; f2 < 4 && a2 + 0.75 * f2 < i3; f2++)
                s2.push(n3.charAt(l >>> 6 * (3 - f2) & 63));
            }
            var h = n3.charAt(64);
            if (h)
              while (s2.length % 4)
                s2.push(h);
            return s2.join("");
          }, parse: function(t5, e5 = true) {
            var r4 = t5.length;
            var i3 = e5 ? this._safe_map : this._map;
            var n3 = this._reverseMap;
            if (!n3) {
              n3 = this._reverseMap = [];
              for (var s2 = 0; s2 < i3.length; s2++)
                n3[i3.charCodeAt(s2)] = s2;
            }
            var o2 = i3.charAt(64);
            if (o2) {
              var u = t5.indexOf(o2);
              if (-1 !== u)
                r4 = u;
            }
            return a(t5, r4, n3);
          }, _map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", _safe_map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_" };
          function a(t5, e5, r4) {
            var n3 = [];
            var s2 = 0;
            for (var a2 = 0; a2 < e5; a2++)
              if (a2 % 4) {
                var o2 = r4[t5.charCodeAt(a2 - 1)] << a2 % 4 * 2;
                var u = r4[t5.charCodeAt(a2)] >>> 6 - a2 % 4 * 2;
                var c = o2 | u;
                n3[s2 >>> 2] |= c << 24 - s2 % 4 * 8;
                s2++;
              }
            return i2.create(n3, s2);
          }
        })();
        return t4.enc.Base64url;
      });
    }, 298: function(t3, e3, r2) {
      (function(i2, n2) {
        t3.exports = n2(r2(8249));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.WordArray;
          var n2 = e4.enc;
          n2.Utf16 = n2.Utf16BE = { stringify: function(t5) {
            var e5 = t5.words;
            var r4 = t5.sigBytes;
            var i3 = [];
            for (var n3 = 0; n3 < r4; n3 += 2) {
              var s2 = e5[n3 >>> 2] >>> 16 - n3 % 4 * 8 & 65535;
              i3.push(String.fromCharCode(s2));
            }
            return i3.join("");
          }, parse: function(t5) {
            var e5 = t5.length;
            var r4 = [];
            for (var n3 = 0; n3 < e5; n3++)
              r4[n3 >>> 1] |= t5.charCodeAt(n3) << 16 - n3 % 2 * 16;
            return i2.create(r4, 2 * e5);
          } };
          n2.Utf16LE = { stringify: function(t5) {
            var e5 = t5.words;
            var r4 = t5.sigBytes;
            var i3 = [];
            for (var n3 = 0; n3 < r4; n3 += 2) {
              var s2 = a(e5[n3 >>> 2] >>> 16 - n3 % 4 * 8 & 65535);
              i3.push(String.fromCharCode(s2));
            }
            return i3.join("");
          }, parse: function(t5) {
            var e5 = t5.length;
            var r4 = [];
            for (var n3 = 0; n3 < e5; n3++)
              r4[n3 >>> 1] |= a(t5.charCodeAt(n3) << 16 - n3 % 2 * 16);
            return i2.create(r4, 2 * e5);
          } };
          function a(t5) {
            return t5 << 8 & 4278255360 | t5 >>> 8 & 16711935;
          }
        })();
        return t4.enc.Utf16;
      });
    }, 888: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(2783), r2(9824));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.Base;
          var n2 = r3.WordArray;
          var s2 = e4.algo;
          var a = s2.MD5;
          var o2 = s2.EvpKDF = i2.extend({ cfg: i2.extend({ keySize: 128 / 32, hasher: a, iterations: 1 }), init: function(t5) {
            this.cfg = this.cfg.extend(t5);
          }, compute: function(t5, e5) {
            var r4;
            var i3 = this.cfg;
            var s3 = i3.hasher.create();
            var a2 = n2.create();
            var o3 = a2.words;
            var u = i3.keySize;
            var c = i3.iterations;
            while (o3.length < u) {
              if (r4)
                s3.update(r4);
              r4 = s3.update(t5).finalize(e5);
              s3.reset();
              for (var l = 1; l < c; l++) {
                r4 = s3.finalize(r4);
                s3.reset();
              }
              a2.concat(r4);
            }
            a2.sigBytes = 4 * u;
            return a2;
          } });
          e4.EvpKDF = function(t5, e5, r4) {
            return o2.create(r4).compute(t5, e5);
          };
        })();
        return t4.EvpKDF;
      });
    }, 2209: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(5109));
      })(this, function(t4) {
        (function(e4) {
          var r3 = t4;
          var i2 = r3.lib;
          var n2 = i2.CipherParams;
          var s2 = r3.enc;
          var a = s2.Hex;
          var o2 = r3.format;
          o2.Hex = { stringify: function(t5) {
            return t5.ciphertext.toString(a);
          }, parse: function(t5) {
            var e5 = a.parse(t5);
            return n2.create({ ciphertext: e5 });
          } };
        })();
        return t4.format.Hex;
      });
    }, 9824: function(t3, e3, r2) {
      (function(i2, n2) {
        t3.exports = n2(r2(8249));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.Base;
          var n2 = e4.enc;
          var s2 = n2.Utf8;
          var a = e4.algo;
          a.HMAC = i2.extend({ init: function(t5, e5) {
            t5 = this._hasher = new t5.init();
            if ("string" == typeof e5)
              e5 = s2.parse(e5);
            var r4 = t5.blockSize;
            var i3 = 4 * r4;
            if (e5.sigBytes > i3)
              e5 = t5.finalize(e5);
            e5.clamp();
            var n3 = this._oKey = e5.clone();
            var a2 = this._iKey = e5.clone();
            var o2 = n3.words;
            var u = a2.words;
            for (var c = 0; c < r4; c++) {
              o2[c] ^= 1549556828;
              u[c] ^= 909522486;
            }
            n3.sigBytes = a2.sigBytes = i3;
            this.reset();
          }, reset: function() {
            var t5 = this._hasher;
            t5.reset();
            t5.update(this._iKey);
          }, update: function(t5) {
            this._hasher.update(t5);
            return this;
          }, finalize: function(t5) {
            var e5 = this._hasher;
            var r4 = e5.finalize(t5);
            e5.reset();
            var i3 = e5.finalize(this._oKey.clone().concat(r4));
            return i3;
          } });
        })();
      });
    }, 1354: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(4938), r2(4433), r2(298), r2(8269), r2(3786), r2(8214), r2(2783), r2(2153), r2(7792), r2(34), r2(7460), r2(3327), r2(706), r2(9824), r2(2112), r2(888), r2(5109), r2(8568), r2(4242), r2(9968), r2(7660), r2(1148), r2(3615), r2(2807), r2(1077), r2(6475), r2(6991), r2(2209), r2(452), r2(4253), r2(1857), r2(4454), r2(3974));
      })(this, function(t4) {
        return t4;
      });
    }, 4433: function(t3, e3, r2) {
      (function(i2, n2) {
        t3.exports = n2(r2(8249));
      })(this, function(t4) {
        (function() {
          if ("function" != typeof ArrayBuffer)
            return;
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.WordArray;
          var n2 = i2.init;
          var s2 = i2.init = function(t5) {
            if (t5 instanceof ArrayBuffer)
              t5 = new Uint8Array(t5);
            if (t5 instanceof Int8Array || "undefined" !== typeof Uint8ClampedArray && t5 instanceof Uint8ClampedArray || t5 instanceof Int16Array || t5 instanceof Uint16Array || t5 instanceof Int32Array || t5 instanceof Uint32Array || t5 instanceof Float32Array || t5 instanceof Float64Array)
              t5 = new Uint8Array(t5.buffer, t5.byteOffset, t5.byteLength);
            if (t5 instanceof Uint8Array) {
              var e5 = t5.byteLength;
              var r4 = [];
              for (var i3 = 0; i3 < e5; i3++)
                r4[i3 >>> 2] |= t5[i3] << 24 - i3 % 4 * 8;
              n2.call(this, r4, e5);
            } else
              n2.apply(this, arguments);
          };
          s2.prototype = i2;
        })();
        return t4.lib.WordArray;
      });
    }, 8214: function(t3, e3, r2) {
      (function(i2, n2) {
        t3.exports = n2(r2(8249));
      })(this, function(t4) {
        (function(e4) {
          var r3 = t4;
          var i2 = r3.lib;
          var n2 = i2.WordArray;
          var s2 = i2.Hasher;
          var a = r3.algo;
          var o2 = [];
          (function() {
            for (var t5 = 0; t5 < 64; t5++)
              o2[t5] = 4294967296 * e4.abs(e4.sin(t5 + 1)) | 0;
          })();
          var u = a.MD5 = s2.extend({ _doReset: function() {
            this._hash = new n2.init([1732584193, 4023233417, 2562383102, 271733878]);
          }, _doProcessBlock: function(t5, e5) {
            for (var r4 = 0; r4 < 16; r4++) {
              var i3 = e5 + r4;
              var n3 = t5[i3];
              t5[i3] = 16711935 & (n3 << 8 | n3 >>> 24) | 4278255360 & (n3 << 24 | n3 >>> 8);
            }
            var s3 = this._hash.words;
            var a2 = t5[e5 + 0];
            var u2 = t5[e5 + 1];
            var d2 = t5[e5 + 2];
            var v = t5[e5 + 3];
            var p2 = t5[e5 + 4];
            var g = t5[e5 + 5];
            var y = t5[e5 + 6];
            var m = t5[e5 + 7];
            var w = t5[e5 + 8];
            var S = t5[e5 + 9];
            var _ = t5[e5 + 10];
            var b = t5[e5 + 11];
            var E2 = t5[e5 + 12];
            var D = t5[e5 + 13];
            var M = t5[e5 + 14];
            var T = t5[e5 + 15];
            var I = s3[0];
            var A = s3[1];
            var x = s3[2];
            var R = s3[3];
            I = c(I, A, x, R, a2, 7, o2[0]);
            R = c(R, I, A, x, u2, 12, o2[1]);
            x = c(x, R, I, A, d2, 17, o2[2]);
            A = c(A, x, R, I, v, 22, o2[3]);
            I = c(I, A, x, R, p2, 7, o2[4]);
            R = c(R, I, A, x, g, 12, o2[5]);
            x = c(x, R, I, A, y, 17, o2[6]);
            A = c(A, x, R, I, m, 22, o2[7]);
            I = c(I, A, x, R, w, 7, o2[8]);
            R = c(R, I, A, x, S, 12, o2[9]);
            x = c(x, R, I, A, _, 17, o2[10]);
            A = c(A, x, R, I, b, 22, o2[11]);
            I = c(I, A, x, R, E2, 7, o2[12]);
            R = c(R, I, A, x, D, 12, o2[13]);
            x = c(x, R, I, A, M, 17, o2[14]);
            A = c(A, x, R, I, T, 22, o2[15]);
            I = l(I, A, x, R, u2, 5, o2[16]);
            R = l(R, I, A, x, y, 9, o2[17]);
            x = l(x, R, I, A, b, 14, o2[18]);
            A = l(A, x, R, I, a2, 20, o2[19]);
            I = l(I, A, x, R, g, 5, o2[20]);
            R = l(R, I, A, x, _, 9, o2[21]);
            x = l(x, R, I, A, T, 14, o2[22]);
            A = l(A, x, R, I, p2, 20, o2[23]);
            I = l(I, A, x, R, S, 5, o2[24]);
            R = l(R, I, A, x, M, 9, o2[25]);
            x = l(x, R, I, A, v, 14, o2[26]);
            A = l(A, x, R, I, w, 20, o2[27]);
            I = l(I, A, x, R, D, 5, o2[28]);
            R = l(R, I, A, x, d2, 9, o2[29]);
            x = l(x, R, I, A, m, 14, o2[30]);
            A = l(A, x, R, I, E2, 20, o2[31]);
            I = f2(I, A, x, R, g, 4, o2[32]);
            R = f2(R, I, A, x, w, 11, o2[33]);
            x = f2(x, R, I, A, b, 16, o2[34]);
            A = f2(A, x, R, I, M, 23, o2[35]);
            I = f2(I, A, x, R, u2, 4, o2[36]);
            R = f2(R, I, A, x, p2, 11, o2[37]);
            x = f2(x, R, I, A, m, 16, o2[38]);
            A = f2(A, x, R, I, _, 23, o2[39]);
            I = f2(I, A, x, R, D, 4, o2[40]);
            R = f2(R, I, A, x, a2, 11, o2[41]);
            x = f2(x, R, I, A, v, 16, o2[42]);
            A = f2(A, x, R, I, y, 23, o2[43]);
            I = f2(I, A, x, R, S, 4, o2[44]);
            R = f2(R, I, A, x, E2, 11, o2[45]);
            x = f2(x, R, I, A, T, 16, o2[46]);
            A = f2(A, x, R, I, d2, 23, o2[47]);
            I = h(I, A, x, R, a2, 6, o2[48]);
            R = h(R, I, A, x, m, 10, o2[49]);
            x = h(x, R, I, A, M, 15, o2[50]);
            A = h(A, x, R, I, g, 21, o2[51]);
            I = h(I, A, x, R, E2, 6, o2[52]);
            R = h(R, I, A, x, v, 10, o2[53]);
            x = h(x, R, I, A, _, 15, o2[54]);
            A = h(A, x, R, I, u2, 21, o2[55]);
            I = h(I, A, x, R, w, 6, o2[56]);
            R = h(R, I, A, x, T, 10, o2[57]);
            x = h(x, R, I, A, y, 15, o2[58]);
            A = h(A, x, R, I, D, 21, o2[59]);
            I = h(I, A, x, R, p2, 6, o2[60]);
            R = h(R, I, A, x, b, 10, o2[61]);
            x = h(x, R, I, A, d2, 15, o2[62]);
            A = h(A, x, R, I, S, 21, o2[63]);
            s3[0] = s3[0] + I | 0;
            s3[1] = s3[1] + A | 0;
            s3[2] = s3[2] + x | 0;
            s3[3] = s3[3] + R | 0;
          }, _doFinalize: function() {
            var t5 = this._data;
            var r4 = t5.words;
            var i3 = 8 * this._nDataBytes;
            var n3 = 8 * t5.sigBytes;
            r4[n3 >>> 5] |= 128 << 24 - n3 % 32;
            var s3 = e4.floor(i3 / 4294967296);
            var a2 = i3;
            r4[(n3 + 64 >>> 9 << 4) + 15] = 16711935 & (s3 << 8 | s3 >>> 24) | 4278255360 & (s3 << 24 | s3 >>> 8);
            r4[(n3 + 64 >>> 9 << 4) + 14] = 16711935 & (a2 << 8 | a2 >>> 24) | 4278255360 & (a2 << 24 | a2 >>> 8);
            t5.sigBytes = 4 * (r4.length + 1);
            this._process();
            var o3 = this._hash;
            var u2 = o3.words;
            for (var c2 = 0; c2 < 4; c2++) {
              var l2 = u2[c2];
              u2[c2] = 16711935 & (l2 << 8 | l2 >>> 24) | 4278255360 & (l2 << 24 | l2 >>> 8);
            }
            return o3;
          }, clone: function() {
            var t5 = s2.clone.call(this);
            t5._hash = this._hash.clone();
            return t5;
          } });
          function c(t5, e5, r4, i3, n3, s3, a2) {
            var o3 = t5 + (e5 & r4 | ~e5 & i3) + n3 + a2;
            return (o3 << s3 | o3 >>> 32 - s3) + e5;
          }
          function l(t5, e5, r4, i3, n3, s3, a2) {
            var o3 = t5 + (e5 & i3 | r4 & ~i3) + n3 + a2;
            return (o3 << s3 | o3 >>> 32 - s3) + e5;
          }
          function f2(t5, e5, r4, i3, n3, s3, a2) {
            var o3 = t5 + (e5 ^ r4 ^ i3) + n3 + a2;
            return (o3 << s3 | o3 >>> 32 - s3) + e5;
          }
          function h(t5, e5, r4, i3, n3, s3, a2) {
            var o3 = t5 + (r4 ^ (e5 | ~i3)) + n3 + a2;
            return (o3 << s3 | o3 >>> 32 - s3) + e5;
          }
          r3.MD5 = s2._createHelper(u);
          r3.HmacMD5 = s2._createHmacHelper(u);
        })(Math);
        return t4.MD5;
      });
    }, 8568: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(5109));
      })(this, function(t4) {
        t4.mode.CFB = function() {
          var e4 = t4.lib.BlockCipherMode.extend();
          e4.Encryptor = e4.extend({ processBlock: function(t5, e5) {
            var i2 = this._cipher;
            var n2 = i2.blockSize;
            r3.call(this, t5, e5, n2, i2);
            this._prevBlock = t5.slice(e5, e5 + n2);
          } });
          e4.Decryptor = e4.extend({ processBlock: function(t5, e5) {
            var i2 = this._cipher;
            var n2 = i2.blockSize;
            var s2 = t5.slice(e5, e5 + n2);
            r3.call(this, t5, e5, n2, i2);
            this._prevBlock = s2;
          } });
          function r3(t5, e5, r4, i2) {
            var n2;
            var s2 = this._iv;
            if (s2) {
              n2 = s2.slice(0);
              this._iv = void 0;
            } else
              n2 = this._prevBlock;
            i2.encryptBlock(n2, 0);
            for (var a = 0; a < r4; a++)
              t5[e5 + a] ^= n2[a];
          }
          return e4;
        }();
        return t4.mode.CFB;
      });
    }, 9968: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(5109));
      })(this, function(t4) {
        t4.mode.CTRGladman = function() {
          var e4 = t4.lib.BlockCipherMode.extend();
          function r3(t5) {
            if (255 === (t5 >> 24 & 255)) {
              var e5 = t5 >> 16 & 255;
              var r4 = t5 >> 8 & 255;
              var i3 = 255 & t5;
              if (255 === e5) {
                e5 = 0;
                if (255 === r4) {
                  r4 = 0;
                  if (255 === i3)
                    i3 = 0;
                  else
                    ++i3;
                } else
                  ++r4;
              } else
                ++e5;
              t5 = 0;
              t5 += e5 << 16;
              t5 += r4 << 8;
              t5 += i3;
            } else
              t5 += 1 << 24;
            return t5;
          }
          function i2(t5) {
            if (0 === (t5[0] = r3(t5[0])))
              t5[1] = r3(t5[1]);
            return t5;
          }
          var n2 = e4.Encryptor = e4.extend({ processBlock: function(t5, e5) {
            var r4 = this._cipher;
            var n3 = r4.blockSize;
            var s2 = this._iv;
            var a = this._counter;
            if (s2) {
              a = this._counter = s2.slice(0);
              this._iv = void 0;
            }
            i2(a);
            var o2 = a.slice(0);
            r4.encryptBlock(o2, 0);
            for (var u = 0; u < n3; u++)
              t5[e5 + u] ^= o2[u];
          } });
          e4.Decryptor = n2;
          return e4;
        }();
        return t4.mode.CTRGladman;
      });
    }, 4242: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(5109));
      })(this, function(t4) {
        t4.mode.CTR = function() {
          var e4 = t4.lib.BlockCipherMode.extend();
          var r3 = e4.Encryptor = e4.extend({ processBlock: function(t5, e5) {
            var r4 = this._cipher;
            var i2 = r4.blockSize;
            var n2 = this._iv;
            var s2 = this._counter;
            if (n2) {
              s2 = this._counter = n2.slice(0);
              this._iv = void 0;
            }
            var a = s2.slice(0);
            r4.encryptBlock(a, 0);
            s2[i2 - 1] = s2[i2 - 1] + 1 | 0;
            for (var o2 = 0; o2 < i2; o2++)
              t5[e5 + o2] ^= a[o2];
          } });
          e4.Decryptor = r3;
          return e4;
        }();
        return t4.mode.CTR;
      });
    }, 1148: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(5109));
      })(this, function(t4) {
        t4.mode.ECB = function() {
          var e4 = t4.lib.BlockCipherMode.extend();
          e4.Encryptor = e4.extend({ processBlock: function(t5, e5) {
            this._cipher.encryptBlock(t5, e5);
          } });
          e4.Decryptor = e4.extend({ processBlock: function(t5, e5) {
            this._cipher.decryptBlock(t5, e5);
          } });
          return e4;
        }();
        return t4.mode.ECB;
      });
    }, 7660: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(5109));
      })(this, function(t4) {
        t4.mode.OFB = function() {
          var e4 = t4.lib.BlockCipherMode.extend();
          var r3 = e4.Encryptor = e4.extend({ processBlock: function(t5, e5) {
            var r4 = this._cipher;
            var i2 = r4.blockSize;
            var n2 = this._iv;
            var s2 = this._keystream;
            if (n2) {
              s2 = this._keystream = n2.slice(0);
              this._iv = void 0;
            }
            r4.encryptBlock(s2, 0);
            for (var a = 0; a < i2; a++)
              t5[e5 + a] ^= s2[a];
          } });
          e4.Decryptor = r3;
          return e4;
        }();
        return t4.mode.OFB;
      });
    }, 3615: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(5109));
      })(this, function(t4) {
        t4.pad.AnsiX923 = { pad: function(t5, e4) {
          var r3 = t5.sigBytes;
          var i2 = 4 * e4;
          var n2 = i2 - r3 % i2;
          var s2 = r3 + n2 - 1;
          t5.clamp();
          t5.words[s2 >>> 2] |= n2 << 24 - s2 % 4 * 8;
          t5.sigBytes += n2;
        }, unpad: function(t5) {
          var e4 = 255 & t5.words[t5.sigBytes - 1 >>> 2];
          t5.sigBytes -= e4;
        } };
        return t4.pad.Ansix923;
      });
    }, 2807: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(5109));
      })(this, function(t4) {
        t4.pad.Iso10126 = { pad: function(e4, r3) {
          var i2 = 4 * r3;
          var n2 = i2 - e4.sigBytes % i2;
          e4.concat(t4.lib.WordArray.random(n2 - 1)).concat(t4.lib.WordArray.create([n2 << 24], 1));
        }, unpad: function(t5) {
          var e4 = 255 & t5.words[t5.sigBytes - 1 >>> 2];
          t5.sigBytes -= e4;
        } };
        return t4.pad.Iso10126;
      });
    }, 1077: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(5109));
      })(this, function(t4) {
        t4.pad.Iso97971 = { pad: function(e4, r3) {
          e4.concat(t4.lib.WordArray.create([2147483648], 1));
          t4.pad.ZeroPadding.pad(e4, r3);
        }, unpad: function(e4) {
          t4.pad.ZeroPadding.unpad(e4);
          e4.sigBytes--;
        } };
        return t4.pad.Iso97971;
      });
    }, 6991: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(5109));
      })(this, function(t4) {
        t4.pad.NoPadding = { pad: function() {
        }, unpad: function() {
        } };
        return t4.pad.NoPadding;
      });
    }, 6475: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(5109));
      })(this, function(t4) {
        t4.pad.ZeroPadding = { pad: function(t5, e4) {
          var r3 = 4 * e4;
          t5.clamp();
          t5.sigBytes += r3 - (t5.sigBytes % r3 || r3);
        }, unpad: function(t5) {
          var e4 = t5.words;
          var r3 = t5.sigBytes - 1;
          for (var r3 = t5.sigBytes - 1; r3 >= 0; r3--)
            if (e4[r3 >>> 2] >>> 24 - r3 % 4 * 8 & 255) {
              t5.sigBytes = r3 + 1;
              break;
            }
        } };
        return t4.pad.ZeroPadding;
      });
    }, 2112: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(2783), r2(9824));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.Base;
          var n2 = r3.WordArray;
          var s2 = e4.algo;
          var a = s2.SHA1;
          var o2 = s2.HMAC;
          var u = s2.PBKDF2 = i2.extend({ cfg: i2.extend({ keySize: 128 / 32, hasher: a, iterations: 1 }), init: function(t5) {
            this.cfg = this.cfg.extend(t5);
          }, compute: function(t5, e5) {
            var r4 = this.cfg;
            var i3 = o2.create(r4.hasher, t5);
            var s3 = n2.create();
            var a2 = n2.create([1]);
            var u2 = s3.words;
            var c = a2.words;
            var l = r4.keySize;
            var f2 = r4.iterations;
            while (u2.length < l) {
              var h = i3.update(e5).finalize(a2);
              i3.reset();
              var d2 = h.words;
              var v = d2.length;
              var p2 = h;
              for (var g = 1; g < f2; g++) {
                p2 = i3.finalize(p2);
                i3.reset();
                var y = p2.words;
                for (var m = 0; m < v; m++)
                  d2[m] ^= y[m];
              }
              s3.concat(h);
              c[0]++;
            }
            s3.sigBytes = 4 * l;
            return s3;
          } });
          e4.PBKDF2 = function(t5, e5, r4) {
            return u.create(r4).compute(t5, e5);
          };
        })();
        return t4.PBKDF2;
      });
    }, 3974: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(8269), r2(8214), r2(888), r2(5109));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.StreamCipher;
          var n2 = e4.algo;
          var s2 = [];
          var a = [];
          var o2 = [];
          var u = n2.RabbitLegacy = i2.extend({ _doReset: function() {
            var t5 = this._key.words;
            var e5 = this.cfg.iv;
            var r4 = this._X = [t5[0], t5[3] << 16 | t5[2] >>> 16, t5[1], t5[0] << 16 | t5[3] >>> 16, t5[2], t5[1] << 16 | t5[0] >>> 16, t5[3], t5[2] << 16 | t5[1] >>> 16];
            var i3 = this._C = [t5[2] << 16 | t5[2] >>> 16, 4294901760 & t5[0] | 65535 & t5[1], t5[3] << 16 | t5[3] >>> 16, 4294901760 & t5[1] | 65535 & t5[2], t5[0] << 16 | t5[0] >>> 16, 4294901760 & t5[2] | 65535 & t5[3], t5[1] << 16 | t5[1] >>> 16, 4294901760 & t5[3] | 65535 & t5[0]];
            this._b = 0;
            for (var n3 = 0; n3 < 4; n3++)
              c.call(this);
            for (var n3 = 0; n3 < 8; n3++)
              i3[n3] ^= r4[n3 + 4 & 7];
            if (e5) {
              var s3 = e5.words;
              var a2 = s3[0];
              var o3 = s3[1];
              var u2 = 16711935 & (a2 << 8 | a2 >>> 24) | 4278255360 & (a2 << 24 | a2 >>> 8);
              var l = 16711935 & (o3 << 8 | o3 >>> 24) | 4278255360 & (o3 << 24 | o3 >>> 8);
              var f2 = u2 >>> 16 | 4294901760 & l;
              var h = l << 16 | 65535 & u2;
              i3[0] ^= u2;
              i3[1] ^= f2;
              i3[2] ^= l;
              i3[3] ^= h;
              i3[4] ^= u2;
              i3[5] ^= f2;
              i3[6] ^= l;
              i3[7] ^= h;
              for (var n3 = 0; n3 < 4; n3++)
                c.call(this);
            }
          }, _doProcessBlock: function(t5, e5) {
            var r4 = this._X;
            c.call(this);
            s2[0] = r4[0] ^ r4[5] >>> 16 ^ r4[3] << 16;
            s2[1] = r4[2] ^ r4[7] >>> 16 ^ r4[5] << 16;
            s2[2] = r4[4] ^ r4[1] >>> 16 ^ r4[7] << 16;
            s2[3] = r4[6] ^ r4[3] >>> 16 ^ r4[1] << 16;
            for (var i3 = 0; i3 < 4; i3++) {
              s2[i3] = 16711935 & (s2[i3] << 8 | s2[i3] >>> 24) | 4278255360 & (s2[i3] << 24 | s2[i3] >>> 8);
              t5[e5 + i3] ^= s2[i3];
            }
          }, blockSize: 128 / 32, ivSize: 64 / 32 });
          function c() {
            var t5 = this._X;
            var e5 = this._C;
            for (var r4 = 0; r4 < 8; r4++)
              a[r4] = e5[r4];
            e5[0] = e5[0] + 1295307597 + this._b | 0;
            e5[1] = e5[1] + 3545052371 + (e5[0] >>> 0 < a[0] >>> 0 ? 1 : 0) | 0;
            e5[2] = e5[2] + 886263092 + (e5[1] >>> 0 < a[1] >>> 0 ? 1 : 0) | 0;
            e5[3] = e5[3] + 1295307597 + (e5[2] >>> 0 < a[2] >>> 0 ? 1 : 0) | 0;
            e5[4] = e5[4] + 3545052371 + (e5[3] >>> 0 < a[3] >>> 0 ? 1 : 0) | 0;
            e5[5] = e5[5] + 886263092 + (e5[4] >>> 0 < a[4] >>> 0 ? 1 : 0) | 0;
            e5[6] = e5[6] + 1295307597 + (e5[5] >>> 0 < a[5] >>> 0 ? 1 : 0) | 0;
            e5[7] = e5[7] + 3545052371 + (e5[6] >>> 0 < a[6] >>> 0 ? 1 : 0) | 0;
            this._b = e5[7] >>> 0 < a[7] >>> 0 ? 1 : 0;
            for (var r4 = 0; r4 < 8; r4++) {
              var i3 = t5[r4] + e5[r4];
              var n3 = 65535 & i3;
              var s3 = i3 >>> 16;
              var u2 = ((n3 * n3 >>> 17) + n3 * s3 >>> 15) + s3 * s3;
              var c2 = ((4294901760 & i3) * i3 | 0) + ((65535 & i3) * i3 | 0);
              o2[r4] = u2 ^ c2;
            }
            t5[0] = o2[0] + (o2[7] << 16 | o2[7] >>> 16) + (o2[6] << 16 | o2[6] >>> 16) | 0;
            t5[1] = o2[1] + (o2[0] << 8 | o2[0] >>> 24) + o2[7] | 0;
            t5[2] = o2[2] + (o2[1] << 16 | o2[1] >>> 16) + (o2[0] << 16 | o2[0] >>> 16) | 0;
            t5[3] = o2[3] + (o2[2] << 8 | o2[2] >>> 24) + o2[1] | 0;
            t5[4] = o2[4] + (o2[3] << 16 | o2[3] >>> 16) + (o2[2] << 16 | o2[2] >>> 16) | 0;
            t5[5] = o2[5] + (o2[4] << 8 | o2[4] >>> 24) + o2[3] | 0;
            t5[6] = o2[6] + (o2[5] << 16 | o2[5] >>> 16) + (o2[4] << 16 | o2[4] >>> 16) | 0;
            t5[7] = o2[7] + (o2[6] << 8 | o2[6] >>> 24) + o2[5] | 0;
          }
          e4.RabbitLegacy = i2._createHelper(u);
        })();
        return t4.RabbitLegacy;
      });
    }, 4454: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(8269), r2(8214), r2(888), r2(5109));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.StreamCipher;
          var n2 = e4.algo;
          var s2 = [];
          var a = [];
          var o2 = [];
          var u = n2.Rabbit = i2.extend({ _doReset: function() {
            var t5 = this._key.words;
            var e5 = this.cfg.iv;
            for (var r4 = 0; r4 < 4; r4++)
              t5[r4] = 16711935 & (t5[r4] << 8 | t5[r4] >>> 24) | 4278255360 & (t5[r4] << 24 | t5[r4] >>> 8);
            var i3 = this._X = [t5[0], t5[3] << 16 | t5[2] >>> 16, t5[1], t5[0] << 16 | t5[3] >>> 16, t5[2], t5[1] << 16 | t5[0] >>> 16, t5[3], t5[2] << 16 | t5[1] >>> 16];
            var n3 = this._C = [t5[2] << 16 | t5[2] >>> 16, 4294901760 & t5[0] | 65535 & t5[1], t5[3] << 16 | t5[3] >>> 16, 4294901760 & t5[1] | 65535 & t5[2], t5[0] << 16 | t5[0] >>> 16, 4294901760 & t5[2] | 65535 & t5[3], t5[1] << 16 | t5[1] >>> 16, 4294901760 & t5[3] | 65535 & t5[0]];
            this._b = 0;
            for (var r4 = 0; r4 < 4; r4++)
              c.call(this);
            for (var r4 = 0; r4 < 8; r4++)
              n3[r4] ^= i3[r4 + 4 & 7];
            if (e5) {
              var s3 = e5.words;
              var a2 = s3[0];
              var o3 = s3[1];
              var u2 = 16711935 & (a2 << 8 | a2 >>> 24) | 4278255360 & (a2 << 24 | a2 >>> 8);
              var l = 16711935 & (o3 << 8 | o3 >>> 24) | 4278255360 & (o3 << 24 | o3 >>> 8);
              var f2 = u2 >>> 16 | 4294901760 & l;
              var h = l << 16 | 65535 & u2;
              n3[0] ^= u2;
              n3[1] ^= f2;
              n3[2] ^= l;
              n3[3] ^= h;
              n3[4] ^= u2;
              n3[5] ^= f2;
              n3[6] ^= l;
              n3[7] ^= h;
              for (var r4 = 0; r4 < 4; r4++)
                c.call(this);
            }
          }, _doProcessBlock: function(t5, e5) {
            var r4 = this._X;
            c.call(this);
            s2[0] = r4[0] ^ r4[5] >>> 16 ^ r4[3] << 16;
            s2[1] = r4[2] ^ r4[7] >>> 16 ^ r4[5] << 16;
            s2[2] = r4[4] ^ r4[1] >>> 16 ^ r4[7] << 16;
            s2[3] = r4[6] ^ r4[3] >>> 16 ^ r4[1] << 16;
            for (var i3 = 0; i3 < 4; i3++) {
              s2[i3] = 16711935 & (s2[i3] << 8 | s2[i3] >>> 24) | 4278255360 & (s2[i3] << 24 | s2[i3] >>> 8);
              t5[e5 + i3] ^= s2[i3];
            }
          }, blockSize: 128 / 32, ivSize: 64 / 32 });
          function c() {
            var t5 = this._X;
            var e5 = this._C;
            for (var r4 = 0; r4 < 8; r4++)
              a[r4] = e5[r4];
            e5[0] = e5[0] + 1295307597 + this._b | 0;
            e5[1] = e5[1] + 3545052371 + (e5[0] >>> 0 < a[0] >>> 0 ? 1 : 0) | 0;
            e5[2] = e5[2] + 886263092 + (e5[1] >>> 0 < a[1] >>> 0 ? 1 : 0) | 0;
            e5[3] = e5[3] + 1295307597 + (e5[2] >>> 0 < a[2] >>> 0 ? 1 : 0) | 0;
            e5[4] = e5[4] + 3545052371 + (e5[3] >>> 0 < a[3] >>> 0 ? 1 : 0) | 0;
            e5[5] = e5[5] + 886263092 + (e5[4] >>> 0 < a[4] >>> 0 ? 1 : 0) | 0;
            e5[6] = e5[6] + 1295307597 + (e5[5] >>> 0 < a[5] >>> 0 ? 1 : 0) | 0;
            e5[7] = e5[7] + 3545052371 + (e5[6] >>> 0 < a[6] >>> 0 ? 1 : 0) | 0;
            this._b = e5[7] >>> 0 < a[7] >>> 0 ? 1 : 0;
            for (var r4 = 0; r4 < 8; r4++) {
              var i3 = t5[r4] + e5[r4];
              var n3 = 65535 & i3;
              var s3 = i3 >>> 16;
              var u2 = ((n3 * n3 >>> 17) + n3 * s3 >>> 15) + s3 * s3;
              var c2 = ((4294901760 & i3) * i3 | 0) + ((65535 & i3) * i3 | 0);
              o2[r4] = u2 ^ c2;
            }
            t5[0] = o2[0] + (o2[7] << 16 | o2[7] >>> 16) + (o2[6] << 16 | o2[6] >>> 16) | 0;
            t5[1] = o2[1] + (o2[0] << 8 | o2[0] >>> 24) + o2[7] | 0;
            t5[2] = o2[2] + (o2[1] << 16 | o2[1] >>> 16) + (o2[0] << 16 | o2[0] >>> 16) | 0;
            t5[3] = o2[3] + (o2[2] << 8 | o2[2] >>> 24) + o2[1] | 0;
            t5[4] = o2[4] + (o2[3] << 16 | o2[3] >>> 16) + (o2[2] << 16 | o2[2] >>> 16) | 0;
            t5[5] = o2[5] + (o2[4] << 8 | o2[4] >>> 24) + o2[3] | 0;
            t5[6] = o2[6] + (o2[5] << 16 | o2[5] >>> 16) + (o2[4] << 16 | o2[4] >>> 16) | 0;
            t5[7] = o2[7] + (o2[6] << 8 | o2[6] >>> 24) + o2[5] | 0;
          }
          e4.Rabbit = i2._createHelper(u);
        })();
        return t4.Rabbit;
      });
    }, 1857: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(8269), r2(8214), r2(888), r2(5109));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.StreamCipher;
          var n2 = e4.algo;
          var s2 = n2.RC4 = i2.extend({ _doReset: function() {
            var t5 = this._key;
            var e5 = t5.words;
            var r4 = t5.sigBytes;
            var i3 = this._S = [];
            for (var n3 = 0; n3 < 256; n3++)
              i3[n3] = n3;
            for (var n3 = 0, s3 = 0; n3 < 256; n3++) {
              var a2 = n3 % r4;
              var o3 = e5[a2 >>> 2] >>> 24 - a2 % 4 * 8 & 255;
              s3 = (s3 + i3[n3] + o3) % 256;
              var u = i3[n3];
              i3[n3] = i3[s3];
              i3[s3] = u;
            }
            this._i = this._j = 0;
          }, _doProcessBlock: function(t5, e5) {
            t5[e5] ^= a.call(this);
          }, keySize: 256 / 32, ivSize: 0 });
          function a() {
            var t5 = this._S;
            var e5 = this._i;
            var r4 = this._j;
            var i3 = 0;
            for (var n3 = 0; n3 < 4; n3++) {
              e5 = (e5 + 1) % 256;
              r4 = (r4 + t5[e5]) % 256;
              var s3 = t5[e5];
              t5[e5] = t5[r4];
              t5[r4] = s3;
              i3 |= t5[(t5[e5] + t5[r4]) % 256] << 24 - 8 * n3;
            }
            this._i = e5;
            this._j = r4;
            return i3;
          }
          e4.RC4 = i2._createHelper(s2);
          var o2 = n2.RC4Drop = s2.extend({ cfg: s2.cfg.extend({ drop: 192 }), _doReset: function() {
            s2._doReset.call(this);
            for (var t5 = this.cfg.drop; t5 > 0; t5--)
              a.call(this);
          } });
          e4.RC4Drop = i2._createHelper(o2);
        })();
        return t4.RC4;
      });
    }, 706: function(t3, e3, r2) {
      (function(i2, n2) {
        t3.exports = n2(r2(8249));
      })(this, function(t4) {
        (function(e4) {
          var r3 = t4;
          var i2 = r3.lib;
          var n2 = i2.WordArray;
          var s2 = i2.Hasher;
          var a = r3.algo;
          var o2 = n2.create([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 7, 4, 13, 1, 10, 6, 15, 3, 12, 0, 9, 5, 2, 14, 11, 8, 3, 10, 14, 4, 9, 15, 8, 1, 2, 7, 0, 6, 13, 11, 5, 12, 1, 9, 11, 10, 0, 8, 12, 4, 13, 3, 7, 15, 14, 5, 6, 2, 4, 0, 5, 9, 7, 12, 2, 10, 14, 1, 3, 8, 11, 6, 15, 13]);
          var u = n2.create([5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 6, 11, 3, 7, 0, 13, 5, 10, 14, 15, 8, 12, 4, 9, 1, 2, 15, 5, 1, 3, 7, 14, 6, 9, 11, 8, 12, 2, 10, 0, 4, 13, 8, 6, 4, 1, 3, 11, 15, 0, 5, 12, 2, 13, 9, 7, 10, 14, 12, 15, 10, 4, 1, 5, 8, 7, 6, 2, 13, 14, 0, 3, 9, 11]);
          var c = n2.create([11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8, 7, 6, 8, 13, 11, 9, 7, 15, 7, 12, 15, 9, 11, 7, 13, 12, 11, 13, 6, 7, 14, 9, 13, 15, 14, 8, 13, 6, 5, 12, 7, 5, 11, 12, 14, 15, 14, 15, 9, 8, 9, 14, 5, 6, 8, 6, 5, 12, 9, 15, 5, 11, 6, 8, 13, 12, 5, 12, 13, 14, 11, 8, 5, 6]);
          var l = n2.create([8, 9, 9, 11, 13, 15, 15, 5, 7, 7, 8, 11, 14, 14, 12, 6, 9, 13, 15, 7, 12, 8, 9, 11, 7, 7, 12, 7, 6, 15, 13, 11, 9, 7, 15, 11, 8, 6, 6, 14, 12, 13, 5, 14, 13, 13, 7, 5, 15, 5, 8, 11, 14, 14, 6, 14, 6, 9, 12, 9, 12, 5, 15, 8, 8, 5, 12, 9, 12, 5, 14, 6, 8, 13, 6, 5, 15, 13, 11, 11]);
          var f2 = n2.create([0, 1518500249, 1859775393, 2400959708, 2840853838]);
          var h = n2.create([1352829926, 1548603684, 1836072691, 2053994217, 0]);
          var d2 = a.RIPEMD160 = s2.extend({ _doReset: function() {
            this._hash = n2.create([1732584193, 4023233417, 2562383102, 271733878, 3285377520]);
          }, _doProcessBlock: function(t5, e5) {
            for (var r4 = 0; r4 < 16; r4++) {
              var i3 = e5 + r4;
              var n3 = t5[i3];
              t5[i3] = 16711935 & (n3 << 8 | n3 >>> 24) | 4278255360 & (n3 << 24 | n3 >>> 8);
            }
            var s3 = this._hash.words;
            var a2 = f2.words;
            var d3 = h.words;
            var S = o2.words;
            var _ = u.words;
            var b = c.words;
            var E2 = l.words;
            var D, M, T, I, A;
            var x, R, B, O, k;
            x = D = s3[0];
            R = M = s3[1];
            B = T = s3[2];
            O = I = s3[3];
            k = A = s3[4];
            var C;
            for (var r4 = 0; r4 < 80; r4 += 1) {
              C = D + t5[e5 + S[r4]] | 0;
              if (r4 < 16)
                C += v(M, T, I) + a2[0];
              else if (r4 < 32)
                C += p2(M, T, I) + a2[1];
              else if (r4 < 48)
                C += g(M, T, I) + a2[2];
              else if (r4 < 64)
                C += y(M, T, I) + a2[3];
              else
                C += m(M, T, I) + a2[4];
              C |= 0;
              C = w(C, b[r4]);
              C = C + A | 0;
              D = A;
              A = I;
              I = w(T, 10);
              T = M;
              M = C;
              C = x + t5[e5 + _[r4]] | 0;
              if (r4 < 16)
                C += m(R, B, O) + d3[0];
              else if (r4 < 32)
                C += y(R, B, O) + d3[1];
              else if (r4 < 48)
                C += g(R, B, O) + d3[2];
              else if (r4 < 64)
                C += p2(R, B, O) + d3[3];
              else
                C += v(R, B, O) + d3[4];
              C |= 0;
              C = w(C, E2[r4]);
              C = C + k | 0;
              x = k;
              k = O;
              O = w(B, 10);
              B = R;
              R = C;
            }
            C = s3[1] + T + O | 0;
            s3[1] = s3[2] + I + k | 0;
            s3[2] = s3[3] + A + x | 0;
            s3[3] = s3[4] + D + R | 0;
            s3[4] = s3[0] + M + B | 0;
            s3[0] = C;
          }, _doFinalize: function() {
            var t5 = this._data;
            var e5 = t5.words;
            var r4 = 8 * this._nDataBytes;
            var i3 = 8 * t5.sigBytes;
            e5[i3 >>> 5] |= 128 << 24 - i3 % 32;
            e5[(i3 + 64 >>> 9 << 4) + 14] = 16711935 & (r4 << 8 | r4 >>> 24) | 4278255360 & (r4 << 24 | r4 >>> 8);
            t5.sigBytes = 4 * (e5.length + 1);
            this._process();
            var n3 = this._hash;
            var s3 = n3.words;
            for (var a2 = 0; a2 < 5; a2++) {
              var o3 = s3[a2];
              s3[a2] = 16711935 & (o3 << 8 | o3 >>> 24) | 4278255360 & (o3 << 24 | o3 >>> 8);
            }
            return n3;
          }, clone: function() {
            var t5 = s2.clone.call(this);
            t5._hash = this._hash.clone();
            return t5;
          } });
          function v(t5, e5, r4) {
            return t5 ^ e5 ^ r4;
          }
          function p2(t5, e5, r4) {
            return t5 & e5 | ~t5 & r4;
          }
          function g(t5, e5, r4) {
            return (t5 | ~e5) ^ r4;
          }
          function y(t5, e5, r4) {
            return t5 & r4 | e5 & ~r4;
          }
          function m(t5, e5, r4) {
            return t5 ^ (e5 | ~r4);
          }
          function w(t5, e5) {
            return t5 << e5 | t5 >>> 32 - e5;
          }
          r3.RIPEMD160 = s2._createHelper(d2);
          r3.HmacRIPEMD160 = s2._createHmacHelper(d2);
        })();
        return t4.RIPEMD160;
      });
    }, 2783: function(t3, e3, r2) {
      (function(i2, n2) {
        t3.exports = n2(r2(8249));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.WordArray;
          var n2 = r3.Hasher;
          var s2 = e4.algo;
          var a = [];
          var o2 = s2.SHA1 = n2.extend({ _doReset: function() {
            this._hash = new i2.init([1732584193, 4023233417, 2562383102, 271733878, 3285377520]);
          }, _doProcessBlock: function(t5, e5) {
            var r4 = this._hash.words;
            var i3 = r4[0];
            var n3 = r4[1];
            var s3 = r4[2];
            var o3 = r4[3];
            var u = r4[4];
            for (var c = 0; c < 80; c++) {
              if (c < 16)
                a[c] = 0 | t5[e5 + c];
              else {
                var l = a[c - 3] ^ a[c - 8] ^ a[c - 14] ^ a[c - 16];
                a[c] = l << 1 | l >>> 31;
              }
              var f2 = (i3 << 5 | i3 >>> 27) + u + a[c];
              if (c < 20)
                f2 += (n3 & s3 | ~n3 & o3) + 1518500249;
              else if (c < 40)
                f2 += (n3 ^ s3 ^ o3) + 1859775393;
              else if (c < 60)
                f2 += (n3 & s3 | n3 & o3 | s3 & o3) - 1894007588;
              else
                f2 += (n3 ^ s3 ^ o3) - 899497514;
              u = o3;
              o3 = s3;
              s3 = n3 << 30 | n3 >>> 2;
              n3 = i3;
              i3 = f2;
            }
            r4[0] = r4[0] + i3 | 0;
            r4[1] = r4[1] + n3 | 0;
            r4[2] = r4[2] + s3 | 0;
            r4[3] = r4[3] + o3 | 0;
            r4[4] = r4[4] + u | 0;
          }, _doFinalize: function() {
            var t5 = this._data;
            var e5 = t5.words;
            var r4 = 8 * this._nDataBytes;
            var i3 = 8 * t5.sigBytes;
            e5[i3 >>> 5] |= 128 << 24 - i3 % 32;
            e5[(i3 + 64 >>> 9 << 4) + 14] = Math.floor(r4 / 4294967296);
            e5[(i3 + 64 >>> 9 << 4) + 15] = r4;
            t5.sigBytes = 4 * e5.length;
            this._process();
            return this._hash;
          }, clone: function() {
            var t5 = n2.clone.call(this);
            t5._hash = this._hash.clone();
            return t5;
          } });
          e4.SHA1 = n2._createHelper(o2);
          e4.HmacSHA1 = n2._createHmacHelper(o2);
        })();
        return t4.SHA1;
      });
    }, 7792: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(2153));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.WordArray;
          var n2 = e4.algo;
          var s2 = n2.SHA256;
          var a = n2.SHA224 = s2.extend({ _doReset: function() {
            this._hash = new i2.init([3238371032, 914150663, 812702999, 4144912697, 4290775857, 1750603025, 1694076839, 3204075428]);
          }, _doFinalize: function() {
            var t5 = s2._doFinalize.call(this);
            t5.sigBytes -= 4;
            return t5;
          } });
          e4.SHA224 = s2._createHelper(a);
          e4.HmacSHA224 = s2._createHmacHelper(a);
        })();
        return t4.SHA224;
      });
    }, 2153: function(t3, e3, r2) {
      (function(i2, n2) {
        t3.exports = n2(r2(8249));
      })(this, function(t4) {
        (function(e4) {
          var r3 = t4;
          var i2 = r3.lib;
          var n2 = i2.WordArray;
          var s2 = i2.Hasher;
          var a = r3.algo;
          var o2 = [];
          var u = [];
          (function() {
            function t5(t6) {
              var r5 = e4.sqrt(t6);
              for (var i4 = 2; i4 <= r5; i4++)
                if (!(t6 % i4))
                  return false;
              return true;
            }
            function r4(t6) {
              return 4294967296 * (t6 - (0 | t6)) | 0;
            }
            var i3 = 2;
            var n3 = 0;
            while (n3 < 64) {
              if (t5(i3)) {
                if (n3 < 8)
                  o2[n3] = r4(e4.pow(i3, 1 / 2));
                u[n3] = r4(e4.pow(i3, 1 / 3));
                n3++;
              }
              i3++;
            }
          })();
          var c = [];
          var l = a.SHA256 = s2.extend({ _doReset: function() {
            this._hash = new n2.init(o2.slice(0));
          }, _doProcessBlock: function(t5, e5) {
            var r4 = this._hash.words;
            var i3 = r4[0];
            var n3 = r4[1];
            var s3 = r4[2];
            var a2 = r4[3];
            var o3 = r4[4];
            var l2 = r4[5];
            var f2 = r4[6];
            var h = r4[7];
            for (var d2 = 0; d2 < 64; d2++) {
              if (d2 < 16)
                c[d2] = 0 | t5[e5 + d2];
              else {
                var v = c[d2 - 15];
                var p2 = (v << 25 | v >>> 7) ^ (v << 14 | v >>> 18) ^ v >>> 3;
                var g = c[d2 - 2];
                var y = (g << 15 | g >>> 17) ^ (g << 13 | g >>> 19) ^ g >>> 10;
                c[d2] = p2 + c[d2 - 7] + y + c[d2 - 16];
              }
              var m = o3 & l2 ^ ~o3 & f2;
              var w = i3 & n3 ^ i3 & s3 ^ n3 & s3;
              var S = (i3 << 30 | i3 >>> 2) ^ (i3 << 19 | i3 >>> 13) ^ (i3 << 10 | i3 >>> 22);
              var _ = (o3 << 26 | o3 >>> 6) ^ (o3 << 21 | o3 >>> 11) ^ (o3 << 7 | o3 >>> 25);
              var b = h + _ + m + u[d2] + c[d2];
              var E2 = S + w;
              h = f2;
              f2 = l2;
              l2 = o3;
              o3 = a2 + b | 0;
              a2 = s3;
              s3 = n3;
              n3 = i3;
              i3 = b + E2 | 0;
            }
            r4[0] = r4[0] + i3 | 0;
            r4[1] = r4[1] + n3 | 0;
            r4[2] = r4[2] + s3 | 0;
            r4[3] = r4[3] + a2 | 0;
            r4[4] = r4[4] + o3 | 0;
            r4[5] = r4[5] + l2 | 0;
            r4[6] = r4[6] + f2 | 0;
            r4[7] = r4[7] + h | 0;
          }, _doFinalize: function() {
            var t5 = this._data;
            var r4 = t5.words;
            var i3 = 8 * this._nDataBytes;
            var n3 = 8 * t5.sigBytes;
            r4[n3 >>> 5] |= 128 << 24 - n3 % 32;
            r4[(n3 + 64 >>> 9 << 4) + 14] = e4.floor(i3 / 4294967296);
            r4[(n3 + 64 >>> 9 << 4) + 15] = i3;
            t5.sigBytes = 4 * r4.length;
            this._process();
            return this._hash;
          }, clone: function() {
            var t5 = s2.clone.call(this);
            t5._hash = this._hash.clone();
            return t5;
          } });
          r3.SHA256 = s2._createHelper(l);
          r3.HmacSHA256 = s2._createHmacHelper(l);
        })(Math);
        return t4.SHA256;
      });
    }, 3327: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(4938));
      })(this, function(t4) {
        (function(e4) {
          var r3 = t4;
          var i2 = r3.lib;
          var n2 = i2.WordArray;
          var s2 = i2.Hasher;
          var a = r3.x64;
          var o2 = a.Word;
          var u = r3.algo;
          var c = [];
          var l = [];
          var f2 = [];
          (function() {
            var t5 = 1, e5 = 0;
            for (var r4 = 0; r4 < 24; r4++) {
              c[t5 + 5 * e5] = (r4 + 1) * (r4 + 2) / 2 % 64;
              var i3 = e5 % 5;
              var n3 = (2 * t5 + 3 * e5) % 5;
              t5 = i3;
              e5 = n3;
            }
            for (var t5 = 0; t5 < 5; t5++)
              for (var e5 = 0; e5 < 5; e5++)
                l[t5 + 5 * e5] = e5 + (2 * t5 + 3 * e5) % 5 * 5;
            var s3 = 1;
            for (var a2 = 0; a2 < 24; a2++) {
              var u2 = 0;
              var h2 = 0;
              for (var d3 = 0; d3 < 7; d3++) {
                if (1 & s3) {
                  var v = (1 << d3) - 1;
                  if (v < 32)
                    h2 ^= 1 << v;
                  else
                    u2 ^= 1 << v - 32;
                }
                if (128 & s3)
                  s3 = s3 << 1 ^ 113;
                else
                  s3 <<= 1;
              }
              f2[a2] = o2.create(u2, h2);
            }
          })();
          var h = [];
          (function() {
            for (var t5 = 0; t5 < 25; t5++)
              h[t5] = o2.create();
          })();
          var d2 = u.SHA3 = s2.extend({ cfg: s2.cfg.extend({ outputLength: 512 }), _doReset: function() {
            var t5 = this._state = [];
            for (var e5 = 0; e5 < 25; e5++)
              t5[e5] = new o2.init();
            this.blockSize = (1600 - 2 * this.cfg.outputLength) / 32;
          }, _doProcessBlock: function(t5, e5) {
            var r4 = this._state;
            var i3 = this.blockSize / 2;
            for (var n3 = 0; n3 < i3; n3++) {
              var s3 = t5[e5 + 2 * n3];
              var a2 = t5[e5 + 2 * n3 + 1];
              s3 = 16711935 & (s3 << 8 | s3 >>> 24) | 4278255360 & (s3 << 24 | s3 >>> 8);
              a2 = 16711935 & (a2 << 8 | a2 >>> 24) | 4278255360 & (a2 << 24 | a2 >>> 8);
              var o3 = r4[n3];
              o3.high ^= a2;
              o3.low ^= s3;
            }
            for (var u2 = 0; u2 < 24; u2++) {
              for (var d3 = 0; d3 < 5; d3++) {
                var v = 0, p2 = 0;
                for (var g = 0; g < 5; g++) {
                  var o3 = r4[d3 + 5 * g];
                  v ^= o3.high;
                  p2 ^= o3.low;
                }
                var y = h[d3];
                y.high = v;
                y.low = p2;
              }
              for (var d3 = 0; d3 < 5; d3++) {
                var m = h[(d3 + 4) % 5];
                var w = h[(d3 + 1) % 5];
                var S = w.high;
                var _ = w.low;
                var v = m.high ^ (S << 1 | _ >>> 31);
                var p2 = m.low ^ (_ << 1 | S >>> 31);
                for (var g = 0; g < 5; g++) {
                  var o3 = r4[d3 + 5 * g];
                  o3.high ^= v;
                  o3.low ^= p2;
                }
              }
              for (var b = 1; b < 25; b++) {
                var v;
                var p2;
                var o3 = r4[b];
                var E2 = o3.high;
                var D = o3.low;
                var M = c[b];
                if (M < 32) {
                  v = E2 << M | D >>> 32 - M;
                  p2 = D << M | E2 >>> 32 - M;
                } else {
                  v = D << M - 32 | E2 >>> 64 - M;
                  p2 = E2 << M - 32 | D >>> 64 - M;
                }
                var T = h[l[b]];
                T.high = v;
                T.low = p2;
              }
              var I = h[0];
              var A = r4[0];
              I.high = A.high;
              I.low = A.low;
              for (var d3 = 0; d3 < 5; d3++)
                for (var g = 0; g < 5; g++) {
                  var b = d3 + 5 * g;
                  var o3 = r4[b];
                  var x = h[b];
                  var R = h[(d3 + 1) % 5 + 5 * g];
                  var B = h[(d3 + 2) % 5 + 5 * g];
                  o3.high = x.high ^ ~R.high & B.high;
                  o3.low = x.low ^ ~R.low & B.low;
                }
              var o3 = r4[0];
              var O = f2[u2];
              o3.high ^= O.high;
              o3.low ^= O.low;
            }
          }, _doFinalize: function() {
            var t5 = this._data;
            var r4 = t5.words;
            8 * this._nDataBytes;
            var s3 = 8 * t5.sigBytes;
            var a2 = 32 * this.blockSize;
            r4[s3 >>> 5] |= 1 << 24 - s3 % 32;
            r4[(e4.ceil((s3 + 1) / a2) * a2 >>> 5) - 1] |= 128;
            t5.sigBytes = 4 * r4.length;
            this._process();
            var o3 = this._state;
            var u2 = this.cfg.outputLength / 8;
            var c2 = u2 / 8;
            var l2 = [];
            for (var f3 = 0; f3 < c2; f3++) {
              var h2 = o3[f3];
              var d3 = h2.high;
              var v = h2.low;
              d3 = 16711935 & (d3 << 8 | d3 >>> 24) | 4278255360 & (d3 << 24 | d3 >>> 8);
              v = 16711935 & (v << 8 | v >>> 24) | 4278255360 & (v << 24 | v >>> 8);
              l2.push(v);
              l2.push(d3);
            }
            return new n2.init(l2, u2);
          }, clone: function() {
            var t5 = s2.clone.call(this);
            var e5 = t5._state = this._state.slice(0);
            for (var r4 = 0; r4 < 25; r4++)
              e5[r4] = e5[r4].clone();
            return t5;
          } });
          r3.SHA3 = s2._createHelper(d2);
          r3.HmacSHA3 = s2._createHmacHelper(d2);
        })(Math);
        return t4.SHA3;
      });
    }, 7460: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(4938), r2(34));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.x64;
          var i2 = r3.Word;
          var n2 = r3.WordArray;
          var s2 = e4.algo;
          var a = s2.SHA512;
          var o2 = s2.SHA384 = a.extend({ _doReset: function() {
            this._hash = new n2.init([new i2.init(3418070365, 3238371032), new i2.init(1654270250, 914150663), new i2.init(2438529370, 812702999), new i2.init(355462360, 4144912697), new i2.init(1731405415, 4290775857), new i2.init(2394180231, 1750603025), new i2.init(3675008525, 1694076839), new i2.init(1203062813, 3204075428)]);
          }, _doFinalize: function() {
            var t5 = a._doFinalize.call(this);
            t5.sigBytes -= 16;
            return t5;
          } });
          e4.SHA384 = a._createHelper(o2);
          e4.HmacSHA384 = a._createHmacHelper(o2);
        })();
        return t4.SHA384;
      });
    }, 34: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(4938));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.Hasher;
          var n2 = e4.x64;
          var s2 = n2.Word;
          var a = n2.WordArray;
          var o2 = e4.algo;
          function u() {
            return s2.create.apply(s2, arguments);
          }
          var c = [u(1116352408, 3609767458), u(1899447441, 602891725), u(3049323471, 3964484399), u(3921009573, 2173295548), u(961987163, 4081628472), u(1508970993, 3053834265), u(2453635748, 2937671579), u(2870763221, 3664609560), u(3624381080, 2734883394), u(310598401, 1164996542), u(607225278, 1323610764), u(1426881987, 3590304994), u(1925078388, 4068182383), u(2162078206, 991336113), u(2614888103, 633803317), u(3248222580, 3479774868), u(3835390401, 2666613458), u(4022224774, 944711139), u(264347078, 2341262773), u(604807628, 2007800933), u(770255983, 1495990901), u(1249150122, 1856431235), u(1555081692, 3175218132), u(1996064986, 2198950837), u(2554220882, 3999719339), u(2821834349, 766784016), u(2952996808, 2566594879), u(3210313671, 3203337956), u(3336571891, 1034457026), u(3584528711, 2466948901), u(113926993, 3758326383), u(338241895, 168717936), u(666307205, 1188179964), u(773529912, 1546045734), u(1294757372, 1522805485), u(1396182291, 2643833823), u(1695183700, 2343527390), u(1986661051, 1014477480), u(2177026350, 1206759142), u(2456956037, 344077627), u(2730485921, 1290863460), u(2820302411, 3158454273), u(3259730800, 3505952657), u(3345764771, 106217008), u(3516065817, 3606008344), u(3600352804, 1432725776), u(4094571909, 1467031594), u(275423344, 851169720), u(430227734, 3100823752), u(506948616, 1363258195), u(659060556, 3750685593), u(883997877, 3785050280), u(958139571, 3318307427), u(1322822218, 3812723403), u(1537002063, 2003034995), u(1747873779, 3602036899), u(1955562222, 1575990012), u(2024104815, 1125592928), u(2227730452, 2716904306), u(2361852424, 442776044), u(2428436474, 593698344), u(2756734187, 3733110249), u(3204031479, 2999351573), u(3329325298, 3815920427), u(3391569614, 3928383900), u(3515267271, 566280711), u(3940187606, 3454069534), u(4118630271, 4000239992), u(116418474, 1914138554), u(174292421, 2731055270), u(289380356, 3203993006), u(460393269, 320620315), u(685471733, 587496836), u(852142971, 1086792851), u(1017036298, 365543100), u(1126000580, 2618297676), u(1288033470, 3409855158), u(1501505948, 4234509866), u(1607167915, 987167468), u(1816402316, 1246189591)];
          var l = [];
          (function() {
            for (var t5 = 0; t5 < 80; t5++)
              l[t5] = u();
          })();
          var f2 = o2.SHA512 = i2.extend({ _doReset: function() {
            this._hash = new a.init([new s2.init(1779033703, 4089235720), new s2.init(3144134277, 2227873595), new s2.init(1013904242, 4271175723), new s2.init(2773480762, 1595750129), new s2.init(1359893119, 2917565137), new s2.init(2600822924, 725511199), new s2.init(528734635, 4215389547), new s2.init(1541459225, 327033209)]);
          }, _doProcessBlock: function(t5, e5) {
            var r4 = this._hash.words;
            var i3 = r4[0];
            var n3 = r4[1];
            var s3 = r4[2];
            var a2 = r4[3];
            var o3 = r4[4];
            var u2 = r4[5];
            var f3 = r4[6];
            var h = r4[7];
            var d2 = i3.high;
            var v = i3.low;
            var p2 = n3.high;
            var g = n3.low;
            var y = s3.high;
            var m = s3.low;
            var w = a2.high;
            var S = a2.low;
            var _ = o3.high;
            var b = o3.low;
            var E2 = u2.high;
            var D = u2.low;
            var M = f3.high;
            var T = f3.low;
            var I = h.high;
            var A = h.low;
            var x = d2;
            var R = v;
            var B = p2;
            var O = g;
            var k = y;
            var C = m;
            var N = w;
            var P = S;
            var V = _;
            var L = b;
            var H = E2;
            var U = D;
            var K = M;
            var j = T;
            var q = I;
            var F = A;
            for (var z = 0; z < 80; z++) {
              var G;
              var Y;
              var W = l[z];
              if (z < 16) {
                Y = W.high = 0 | t5[e5 + 2 * z];
                G = W.low = 0 | t5[e5 + 2 * z + 1];
              } else {
                var J = l[z - 15];
                var Z = J.high;
                var $ = J.low;
                var X = (Z >>> 1 | $ << 31) ^ (Z >>> 8 | $ << 24) ^ Z >>> 7;
                var Q = ($ >>> 1 | Z << 31) ^ ($ >>> 8 | Z << 24) ^ ($ >>> 7 | Z << 25);
                var tt2 = l[z - 2];
                var et = tt2.high;
                var rt = tt2.low;
                var it = (et >>> 19 | rt << 13) ^ (et << 3 | rt >>> 29) ^ et >>> 6;
                var nt = (rt >>> 19 | et << 13) ^ (rt << 3 | et >>> 29) ^ (rt >>> 6 | et << 26);
                var st = l[z - 7];
                var at = st.high;
                var ot = st.low;
                var ut = l[z - 16];
                var ct = ut.high;
                var lt = ut.low;
                G = Q + ot;
                Y = X + at + (G >>> 0 < Q >>> 0 ? 1 : 0);
                G += nt;
                Y = Y + it + (G >>> 0 < nt >>> 0 ? 1 : 0);
                G += lt;
                Y = Y + ct + (G >>> 0 < lt >>> 0 ? 1 : 0);
                W.high = Y;
                W.low = G;
              }
              var ft = V & H ^ ~V & K;
              var ht = L & U ^ ~L & j;
              var dt = x & B ^ x & k ^ B & k;
              var vt = R & O ^ R & C ^ O & C;
              var pt = (x >>> 28 | R << 4) ^ (x << 30 | R >>> 2) ^ (x << 25 | R >>> 7);
              var gt = (R >>> 28 | x << 4) ^ (R << 30 | x >>> 2) ^ (R << 25 | x >>> 7);
              var yt = (V >>> 14 | L << 18) ^ (V >>> 18 | L << 14) ^ (V << 23 | L >>> 9);
              var mt = (L >>> 14 | V << 18) ^ (L >>> 18 | V << 14) ^ (L << 23 | V >>> 9);
              var wt = c[z];
              var St = wt.high;
              var _t = wt.low;
              var bt = F + mt;
              var Et = q + yt + (bt >>> 0 < F >>> 0 ? 1 : 0);
              var bt = bt + ht;
              var Et = Et + ft + (bt >>> 0 < ht >>> 0 ? 1 : 0);
              var bt = bt + _t;
              var Et = Et + St + (bt >>> 0 < _t >>> 0 ? 1 : 0);
              var bt = bt + G;
              var Et = Et + Y + (bt >>> 0 < G >>> 0 ? 1 : 0);
              var Dt = gt + vt;
              var Mt = pt + dt + (Dt >>> 0 < gt >>> 0 ? 1 : 0);
              q = K;
              F = j;
              K = H;
              j = U;
              H = V;
              U = L;
              L = P + bt | 0;
              V = N + Et + (L >>> 0 < P >>> 0 ? 1 : 0) | 0;
              N = k;
              P = C;
              k = B;
              C = O;
              B = x;
              O = R;
              R = bt + Dt | 0;
              x = Et + Mt + (R >>> 0 < bt >>> 0 ? 1 : 0) | 0;
            }
            v = i3.low = v + R;
            i3.high = d2 + x + (v >>> 0 < R >>> 0 ? 1 : 0);
            g = n3.low = g + O;
            n3.high = p2 + B + (g >>> 0 < O >>> 0 ? 1 : 0);
            m = s3.low = m + C;
            s3.high = y + k + (m >>> 0 < C >>> 0 ? 1 : 0);
            S = a2.low = S + P;
            a2.high = w + N + (S >>> 0 < P >>> 0 ? 1 : 0);
            b = o3.low = b + L;
            o3.high = _ + V + (b >>> 0 < L >>> 0 ? 1 : 0);
            D = u2.low = D + U;
            u2.high = E2 + H + (D >>> 0 < U >>> 0 ? 1 : 0);
            T = f3.low = T + j;
            f3.high = M + K + (T >>> 0 < j >>> 0 ? 1 : 0);
            A = h.low = A + F;
            h.high = I + q + (A >>> 0 < F >>> 0 ? 1 : 0);
          }, _doFinalize: function() {
            var t5 = this._data;
            var e5 = t5.words;
            var r4 = 8 * this._nDataBytes;
            var i3 = 8 * t5.sigBytes;
            e5[i3 >>> 5] |= 128 << 24 - i3 % 32;
            e5[(i3 + 128 >>> 10 << 5) + 30] = Math.floor(r4 / 4294967296);
            e5[(i3 + 128 >>> 10 << 5) + 31] = r4;
            t5.sigBytes = 4 * e5.length;
            this._process();
            var n3 = this._hash.toX32();
            return n3;
          }, clone: function() {
            var t5 = i2.clone.call(this);
            t5._hash = this._hash.clone();
            return t5;
          }, blockSize: 1024 / 32 });
          e4.SHA512 = i2._createHelper(f2);
          e4.HmacSHA512 = i2._createHmacHelper(f2);
        })();
        return t4.SHA512;
      });
    }, 4253: function(t3, e3, r2) {
      (function(i2, n2, s2) {
        t3.exports = n2(r2(8249), r2(8269), r2(8214), r2(888), r2(5109));
      })(this, function(t4) {
        (function() {
          var e4 = t4;
          var r3 = e4.lib;
          var i2 = r3.WordArray;
          var n2 = r3.BlockCipher;
          var s2 = e4.algo;
          var a = [57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4];
          var o2 = [14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32];
          var u = [1, 2, 4, 6, 8, 10, 12, 14, 15, 17, 19, 21, 23, 25, 27, 28];
          var c = [{ 0: 8421888, 268435456: 32768, 536870912: 8421378, 805306368: 2, 1073741824: 512, 1342177280: 8421890, 1610612736: 8389122, 1879048192: 8388608, 2147483648: 514, 2415919104: 8389120, 2684354560: 33280, 2952790016: 8421376, 3221225472: 32770, 3489660928: 8388610, 3758096384: 0, 4026531840: 33282, 134217728: 0, 402653184: 8421890, 671088640: 33282, 939524096: 32768, 1207959552: 8421888, 1476395008: 512, 1744830464: 8421378, 2013265920: 2, 2281701376: 8389120, 2550136832: 33280, 2818572288: 8421376, 3087007744: 8389122, 3355443200: 8388610, 3623878656: 32770, 3892314112: 514, 4160749568: 8388608, 1: 32768, 268435457: 2, 536870913: 8421888, 805306369: 8388608, 1073741825: 8421378, 1342177281: 33280, 1610612737: 512, 1879048193: 8389122, 2147483649: 8421890, 2415919105: 8421376, 2684354561: 8388610, 2952790017: 33282, 3221225473: 514, 3489660929: 8389120, 3758096385: 32770, 4026531841: 0, 134217729: 8421890, 402653185: 8421376, 671088641: 8388608, 939524097: 512, 1207959553: 32768, 1476395009: 8388610, 1744830465: 2, 2013265921: 33282, 2281701377: 32770, 2550136833: 8389122, 2818572289: 514, 3087007745: 8421888, 3355443201: 8389120, 3623878657: 0, 3892314113: 33280, 4160749569: 8421378 }, { 0: 1074282512, 16777216: 16384, 33554432: 524288, 50331648: 1074266128, 67108864: 1073741840, 83886080: 1074282496, 100663296: 1073758208, 117440512: 16, 134217728: 540672, 150994944: 1073758224, 167772160: 1073741824, 184549376: 540688, 201326592: 524304, 218103808: 0, 234881024: 16400, 251658240: 1074266112, 8388608: 1073758208, 25165824: 540688, 41943040: 16, 58720256: 1073758224, 75497472: 1074282512, 92274688: 1073741824, 109051904: 524288, 125829120: 1074266128, 142606336: 524304, 159383552: 0, 176160768: 16384, 192937984: 1074266112, 209715200: 1073741840, 226492416: 540672, 243269632: 1074282496, 260046848: 16400, 268435456: 0, 285212672: 1074266128, 301989888: 1073758224, 318767104: 1074282496, 335544320: 1074266112, 352321536: 16, 369098752: 540688, 385875968: 16384, 402653184: 16400, 419430400: 524288, 436207616: 524304, 452984832: 1073741840, 469762048: 540672, 486539264: 1073758208, 503316480: 1073741824, 520093696: 1074282512, 276824064: 540688, 293601280: 524288, 310378496: 1074266112, 327155712: 16384, 343932928: 1073758208, 360710144: 1074282512, 377487360: 16, 394264576: 1073741824, 411041792: 1074282496, 427819008: 1073741840, 444596224: 1073758224, 461373440: 524304, 478150656: 0, 494927872: 16400, 511705088: 1074266128, 528482304: 540672 }, { 0: 260, 1048576: 0, 2097152: 67109120, 3145728: 65796, 4194304: 65540, 5242880: 67108868, 6291456: 67174660, 7340032: 67174400, 8388608: 67108864, 9437184: 67174656, 10485760: 65792, 11534336: 67174404, 12582912: 67109124, 13631488: 65536, 14680064: 4, 15728640: 256, 524288: 67174656, 1572864: 67174404, 2621440: 0, 3670016: 67109120, 4718592: 67108868, 5767168: 65536, 6815744: 65540, 7864320: 260, 8912896: 4, 9961472: 256, 11010048: 67174400, 12058624: 65796, 13107200: 65792, 14155776: 67109124, 15204352: 67174660, 16252928: 67108864, 16777216: 67174656, 17825792: 65540, 18874368: 65536, 19922944: 67109120, 20971520: 256, 22020096: 67174660, 23068672: 67108868, 24117248: 0, 25165824: 67109124, 26214400: 67108864, 27262976: 4, 28311552: 65792, 29360128: 67174400, 30408704: 260, 31457280: 65796, 32505856: 67174404, 17301504: 67108864, 18350080: 260, 19398656: 67174656, 20447232: 0, 21495808: 65540, 22544384: 67109120, 23592960: 256, 24641536: 67174404, 25690112: 65536, 26738688: 67174660, 27787264: 65796, 28835840: 67108868, 29884416: 67109124, 30932992: 67174400, 31981568: 4, 33030144: 65792 }, { 0: 2151682048, 65536: 2147487808, 131072: 4198464, 196608: 2151677952, 262144: 0, 327680: 4198400, 393216: 2147483712, 458752: 4194368, 524288: 2147483648, 589824: 4194304, 655360: 64, 720896: 2147487744, 786432: 2151678016, 851968: 4160, 917504: 4096, 983040: 2151682112, 32768: 2147487808, 98304: 64, 163840: 2151678016, 229376: 2147487744, 294912: 4198400, 360448: 2151682112, 425984: 0, 491520: 2151677952, 557056: 4096, 622592: 2151682048, 688128: 4194304, 753664: 4160, 819200: 2147483648, 884736: 4194368, 950272: 4198464, 1015808: 2147483712, 1048576: 4194368, 1114112: 4198400, 1179648: 2147483712, 1245184: 0, 1310720: 4160, 1376256: 2151678016, 1441792: 2151682048, 1507328: 2147487808, 1572864: 2151682112, 1638400: 2147483648, 1703936: 2151677952, 1769472: 4198464, 1835008: 2147487744, 1900544: 4194304, 1966080: 64, 2031616: 4096, 1081344: 2151677952, 1146880: 2151682112, 1212416: 0, 1277952: 4198400, 1343488: 4194368, 1409024: 2147483648, 1474560: 2147487808, 1540096: 64, 1605632: 2147483712, 1671168: 4096, 1736704: 2147487744, 1802240: 2151678016, 1867776: 4160, 1933312: 2151682048, 1998848: 4194304, 2064384: 4198464 }, { 0: 128, 4096: 17039360, 8192: 262144, 12288: 536870912, 16384: 537133184, 20480: 16777344, 24576: 553648256, 28672: 262272, 32768: 16777216, 36864: 537133056, 40960: 536871040, 45056: 553910400, 49152: 553910272, 53248: 0, 57344: 17039488, 61440: 553648128, 2048: 17039488, 6144: 553648256, 10240: 128, 14336: 17039360, 18432: 262144, 22528: 537133184, 26624: 553910272, 30720: 536870912, 34816: 537133056, 38912: 0, 43008: 553910400, 47104: 16777344, 51200: 536871040, 55296: 553648128, 59392: 16777216, 63488: 262272, 65536: 262144, 69632: 128, 73728: 536870912, 77824: 553648256, 81920: 16777344, 86016: 553910272, 90112: 537133184, 94208: 16777216, 98304: 553910400, 102400: 553648128, 106496: 17039360, 110592: 537133056, 114688: 262272, 118784: 536871040, 122880: 0, 126976: 17039488, 67584: 553648256, 71680: 16777216, 75776: 17039360, 79872: 537133184, 83968: 536870912, 88064: 17039488, 92160: 128, 96256: 553910272, 100352: 262272, 104448: 553910400, 108544: 0, 112640: 553648128, 116736: 16777344, 120832: 262144, 124928: 537133056, 129024: 536871040 }, { 0: 268435464, 256: 8192, 512: 270532608, 768: 270540808, 1024: 268443648, 1280: 2097152, 1536: 2097160, 1792: 268435456, 2048: 0, 2304: 268443656, 2560: 2105344, 2816: 8, 3072: 270532616, 3328: 2105352, 3584: 8200, 3840: 270540800, 128: 270532608, 384: 270540808, 640: 8, 896: 2097152, 1152: 2105352, 1408: 268435464, 1664: 268443648, 1920: 8200, 2176: 2097160, 2432: 8192, 2688: 268443656, 2944: 270532616, 3200: 0, 3456: 270540800, 3712: 2105344, 3968: 268435456, 4096: 268443648, 4352: 270532616, 4608: 270540808, 4864: 8200, 5120: 2097152, 5376: 268435456, 5632: 268435464, 5888: 2105344, 6144: 2105352, 6400: 0, 6656: 8, 6912: 270532608, 7168: 8192, 7424: 268443656, 7680: 270540800, 7936: 2097160, 4224: 8, 4480: 2105344, 4736: 2097152, 4992: 268435464, 5248: 268443648, 5504: 8200, 5760: 270540808, 6016: 270532608, 6272: 270540800, 6528: 270532616, 6784: 8192, 7040: 2105352, 7296: 2097160, 7552: 0, 7808: 268435456, 8064: 268443656 }, { 0: 1048576, 16: 33555457, 32: 1024, 48: 1049601, 64: 34604033, 80: 0, 96: 1, 112: 34603009, 128: 33555456, 144: 1048577, 160: 33554433, 176: 34604032, 192: 34603008, 208: 1025, 224: 1049600, 240: 33554432, 8: 34603009, 24: 0, 40: 33555457, 56: 34604032, 72: 1048576, 88: 33554433, 104: 33554432, 120: 1025, 136: 1049601, 152: 33555456, 168: 34603008, 184: 1048577, 200: 1024, 216: 34604033, 232: 1, 248: 1049600, 256: 33554432, 272: 1048576, 288: 33555457, 304: 34603009, 320: 1048577, 336: 33555456, 352: 34604032, 368: 1049601, 384: 1025, 400: 34604033, 416: 1049600, 432: 1, 448: 0, 464: 34603008, 480: 33554433, 496: 1024, 264: 1049600, 280: 33555457, 296: 34603009, 312: 1, 328: 33554432, 344: 1048576, 360: 1025, 376: 34604032, 392: 33554433, 408: 34603008, 424: 0, 440: 34604033, 456: 1049601, 472: 1024, 488: 33555456, 504: 1048577 }, { 0: 134219808, 1: 131072, 2: 134217728, 3: 32, 4: 131104, 5: 134350880, 6: 134350848, 7: 2048, 8: 134348800, 9: 134219776, 10: 133120, 11: 134348832, 12: 2080, 13: 0, 14: 134217760, 15: 133152, 2147483648: 2048, 2147483649: 134350880, 2147483650: 134219808, 2147483651: 134217728, 2147483652: 134348800, 2147483653: 133120, 2147483654: 133152, 2147483655: 32, 2147483656: 134217760, 2147483657: 2080, 2147483658: 131104, 2147483659: 134350848, 2147483660: 0, 2147483661: 134348832, 2147483662: 134219776, 2147483663: 131072, 16: 133152, 17: 134350848, 18: 32, 19: 2048, 20: 134219776, 21: 134217760, 22: 134348832, 23: 131072, 24: 0, 25: 131104, 26: 134348800, 27: 134219808, 28: 134350880, 29: 133120, 30: 2080, 31: 134217728, 2147483664: 131072, 2147483665: 2048, 2147483666: 134348832, 2147483667: 133152, 2147483668: 32, 2147483669: 134348800, 2147483670: 134217728, 2147483671: 134219808, 2147483672: 134350880, 2147483673: 134217760, 2147483674: 134219776, 2147483675: 0, 2147483676: 133120, 2147483677: 2080, 2147483678: 131104, 2147483679: 134350848 }];
          var l = [4160749569, 528482304, 33030144, 2064384, 129024, 8064, 504, 2147483679];
          var f2 = s2.DES = n2.extend({ _doReset: function() {
            var t5 = this._key;
            var e5 = t5.words;
            var r4 = [];
            for (var i3 = 0; i3 < 56; i3++) {
              var n3 = a[i3] - 1;
              r4[i3] = e5[n3 >>> 5] >>> 31 - n3 % 32 & 1;
            }
            var s3 = this._subKeys = [];
            for (var c2 = 0; c2 < 16; c2++) {
              var l2 = s3[c2] = [];
              var f3 = u[c2];
              for (var i3 = 0; i3 < 24; i3++) {
                l2[i3 / 6 | 0] |= r4[(o2[i3] - 1 + f3) % 28] << 31 - i3 % 6;
                l2[4 + (i3 / 6 | 0)] |= r4[28 + (o2[i3 + 24] - 1 + f3) % 28] << 31 - i3 % 6;
              }
              l2[0] = l2[0] << 1 | l2[0] >>> 31;
              for (var i3 = 1; i3 < 7; i3++)
                l2[i3] = l2[i3] >>> 4 * (i3 - 1) + 3;
              l2[7] = l2[7] << 5 | l2[7] >>> 27;
            }
            var h2 = this._invSubKeys = [];
            for (var i3 = 0; i3 < 16; i3++)
              h2[i3] = s3[15 - i3];
          }, encryptBlock: function(t5, e5) {
            this._doCryptBlock(t5, e5, this._subKeys);
          }, decryptBlock: function(t5, e5) {
            this._doCryptBlock(t5, e5, this._invSubKeys);
          }, _doCryptBlock: function(t5, e5, r4) {
            this._lBlock = t5[e5];
            this._rBlock = t5[e5 + 1];
            h.call(this, 4, 252645135);
            h.call(this, 16, 65535);
            d2.call(this, 2, 858993459);
            d2.call(this, 8, 16711935);
            h.call(this, 1, 1431655765);
            for (var i3 = 0; i3 < 16; i3++) {
              var n3 = r4[i3];
              var s3 = this._lBlock;
              var a2 = this._rBlock;
              var o3 = 0;
              for (var u2 = 0; u2 < 8; u2++)
                o3 |= c[u2][((a2 ^ n3[u2]) & l[u2]) >>> 0];
              this._lBlock = a2;
              this._rBlock = s3 ^ o3;
            }
            var f3 = this._lBlock;
            this._lBlock = this._rBlock;
            this._rBlock = f3;
            h.call(this, 1, 1431655765);
            d2.call(this, 8, 16711935);
            d2.call(this, 2, 858993459);
            h.call(this, 16, 65535);
            h.call(this, 4, 252645135);
            t5[e5] = this._lBlock;
            t5[e5 + 1] = this._rBlock;
          }, keySize: 64 / 32, ivSize: 64 / 32, blockSize: 64 / 32 });
          function h(t5, e5) {
            var r4 = (this._lBlock >>> t5 ^ this._rBlock) & e5;
            this._rBlock ^= r4;
            this._lBlock ^= r4 << t5;
          }
          function d2(t5, e5) {
            var r4 = (this._rBlock >>> t5 ^ this._lBlock) & e5;
            this._lBlock ^= r4;
            this._rBlock ^= r4 << t5;
          }
          e4.DES = n2._createHelper(f2);
          var v = s2.TripleDES = n2.extend({ _doReset: function() {
            var t5 = this._key;
            var e5 = t5.words;
            if (2 !== e5.length && 4 !== e5.length && e5.length < 6)
              throw new Error("Invalid key length - 3DES requires the key length to be 64, 128, 192 or >192.");
            var r4 = e5.slice(0, 2);
            var n3 = e5.length < 4 ? e5.slice(0, 2) : e5.slice(2, 4);
            var s3 = e5.length < 6 ? e5.slice(0, 2) : e5.slice(4, 6);
            this._des1 = f2.createEncryptor(i2.create(r4));
            this._des2 = f2.createEncryptor(i2.create(n3));
            this._des3 = f2.createEncryptor(i2.create(s3));
          }, encryptBlock: function(t5, e5) {
            this._des1.encryptBlock(t5, e5);
            this._des2.decryptBlock(t5, e5);
            this._des3.encryptBlock(t5, e5);
          }, decryptBlock: function(t5, e5) {
            this._des3.decryptBlock(t5, e5);
            this._des2.encryptBlock(t5, e5);
            this._des1.decryptBlock(t5, e5);
          }, keySize: 192 / 32, ivSize: 64 / 32, blockSize: 64 / 32 });
          e4.TripleDES = n2._createHelper(v);
        })();
        return t4.TripleDES;
      });
    }, 4938: function(t3, e3, r2) {
      (function(i2, n2) {
        t3.exports = n2(r2(8249));
      })(this, function(t4) {
        (function(e4) {
          var r3 = t4;
          var i2 = r3.lib;
          var n2 = i2.Base;
          var s2 = i2.WordArray;
          var a = r3.x64 = {};
          a.Word = n2.extend({ init: function(t5, e5) {
            this.high = t5;
            this.low = e5;
          } });
          a.WordArray = n2.extend({ init: function(t5, r4) {
            t5 = this.words = t5 || [];
            if (r4 != e4)
              this.sigBytes = r4;
            else
              this.sigBytes = 8 * t5.length;
          }, toX32: function() {
            var t5 = this.words;
            var e5 = t5.length;
            var r4 = [];
            for (var i3 = 0; i3 < e5; i3++) {
              var n3 = t5[i3];
              r4.push(n3.high);
              r4.push(n3.low);
            }
            return s2.create(r4, this.sigBytes);
          }, clone: function() {
            var t5 = n2.clone.call(this);
            var e5 = t5.words = this.words.slice(0);
            var r4 = e5.length;
            for (var i3 = 0; i3 < r4; i3++)
              e5[i3] = e5[i3].clone();
            return t5;
          } });
        })();
        return t4;
      });
    }, 4198: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      e3.ErrorCode = void 0;
      (function(t4) {
        t4[t4["SUCCESS"] = 0] = "SUCCESS";
        t4[t4["CLIENT_ID_NOT_FOUND"] = 1] = "CLIENT_ID_NOT_FOUND";
        t4[t4["OPERATION_TOO_OFTEN"] = 2] = "OPERATION_TOO_OFTEN";
        t4[t4["REPEAT_MESSAGE"] = 3] = "REPEAT_MESSAGE";
        t4[t4["TIME_OUT"] = 4] = "TIME_OUT";
      })(e3.ErrorCode || (e3.ErrorCode = {}));
    }, 9021: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      const n2 = i2(r2(6893));
      const s2 = i2(r2(7555));
      const a = i2(r2(6379));
      const o2 = i2(r2(529));
      var u;
      (function(t4) {
        function e4(t5) {
          o2.default.debugMode = t5;
          o2.default.info(`setDebugMode: ${t5}`);
        }
        t4.setDebugMode = e4;
        function r3(t5) {
          try {
            s2.default.init(t5);
          } catch (t6) {
            o2.default.error(`init error`, t6);
          }
        }
        t4.init = r3;
        function i3(t5) {
          try {
            if (!t5.url)
              throw new Error("invalid url");
            if (!t5.key || !t5.keyId)
              throw new Error("invalid key or keyId");
            a.default.socketUrl = t5.url;
            a.default.publicKeyId = t5.keyId;
            a.default.publicKey = t5.key;
          } catch (t6) {
            o2.default.error(`setSocketServer error`, t6);
          }
        }
        t4.setSocketServer = i3;
        function u2(t5) {
          try {
            s2.default.enableSocket(t5);
          } catch (t6) {
            o2.default.error(`enableSocket error`, t6);
          }
        }
        t4.enableSocket = u2;
        function c() {
          return n2.default.SDK_VERSION;
        }
        t4.getVersion = c;
      })(u || (u = {}));
      t3.exports = u;
    }, 9478: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(529));
      const s2 = i2(r2(496));
      const a = i2(r2(3555));
      const o2 = i2(r2(1929));
      const u = i2(r2(4379));
      const c = i2(r2(6899));
      const l = i2(r2(776));
      const f2 = i2(r2(2002));
      const h = i2(r2(5807));
      const d2 = i2(r2(9704));
      const v = i2(r2(6545));
      const p2 = i2(r2(3680));
      const g = i2(r2(7706));
      const y = i2(r2(4486));
      const m = i2(r2(5867));
      const w = i2(r2(7006));
      var S;
      (function(t4) {
        let e4;
        let r3;
        let i3;
        function S2() {
          let t5;
          try {
            if ("undefined" != typeof index) {
              e4 = new v.default();
              r3 = new p2.default();
              i3 = new g.default();
            } else if ("undefined" != typeof tt) {
              e4 = new f2.default();
              r3 = new h.default();
              i3 = new d2.default();
            } else if ("undefined" != typeof my) {
              e4 = new s2.default();
              r3 = new a.default();
              i3 = new o2.default();
            } else if ("undefined" != typeof wx$1) {
              e4 = new y.default();
              r3 = new m.default();
              i3 = new w.default();
            } else if ("undefined" != typeof window) {
              e4 = new u.default();
              r3 = new c.default();
              i3 = new l.default();
            }
          } catch (e5) {
            n2.default.error(`init am error: ${e5}`);
            t5 = e5;
          }
          if (!e4 || !r3 || !i3) {
            if ("undefined" != typeof window) {
              e4 = new u.default();
              r3 = new c.default();
              i3 = new l.default();
            }
          }
          if (!e4 || !r3 || !i3)
            throw new Error(`init am error: no api impl found, ${t5}`);
        }
        function _() {
          if (!e4)
            S2();
          return e4;
        }
        t4.getDevice = _;
        function b() {
          if (!r3)
            S2();
          return r3;
        }
        t4.getStorage = b;
        function E2() {
          if (!i3)
            S2();
          return i3;
        }
        t4.getWebSocket = E2;
      })(S || (S = {}));
      e3["default"] = S;
    }, 4685: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(9478));
      var s2;
      (function(t4) {
        function e4() {
          return n2.default.getDevice().os();
        }
        t4.os = e4;
        function r3() {
          return n2.default.getDevice().osVersion();
        }
        t4.osVersion = r3;
        function i3() {
          return n2.default.getDevice().model();
        }
        t4.model = i3;
        function s3() {
          return n2.default.getDevice().brand();
        }
        t4.brand = s3;
        function a() {
          return n2.default.getDevice().platform();
        }
        t4.platform = a;
        function o2() {
          return n2.default.getDevice().platformVersion();
        }
        t4.platformVersion = o2;
        function u() {
          return n2.default.getDevice().platformId();
        }
        t4.platformId = u;
        function c() {
          return n2.default.getDevice().language();
        }
        t4.language = c;
        function l() {
          let t5 = n2.default.getDevice().userAgent;
          if (t5)
            return t5();
          return "";
        }
        t4.userAgent = l;
        function f2(t5) {
          n2.default.getDevice().getNetworkType(t5);
        }
        t4.getNetworkType = f2;
        function h(t5) {
          n2.default.getDevice().onNetworkStatusChange(t5);
        }
        t4.onNetworkStatusChange = h;
      })(s2 || (s2 = {}));
      e3["default"] = s2;
    }, 7002: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(6379));
      const s2 = i2(r2(1386));
      const a = i2(r2(4054));
      const o2 = r2(2918);
      const u = i2(r2(7167));
      const c = i2(r2(529));
      const l = i2(r2(9478));
      const f2 = i2(r2(8506));
      var h;
      (function(t4) {
        let e4;
        let r3 = false;
        let i3 = false;
        let h2 = false;
        let d2 = [];
        const v = 10;
        let p2 = 0;
        t4.allowReconnect = true;
        function g() {
          return r3 && i3;
        }
        t4.isAvailable = g;
        function y(e5) {
          let r4 = (/* @__PURE__ */ new Date()).getTime();
          if (r4 - p2 < 1e3) {
            c.default.warn(`enableSocket ${e5} fail: this function can only be called once a second`);
            return;
          }
          p2 = r4;
          t4.allowReconnect = e5;
          if (e5)
            t4.reconnect(10);
          else
            t4.close(`enableSocket ${e5}`);
        }
        t4.enableSocket = y;
        function m(e5 = 0) {
          if (!t4.allowReconnect)
            return;
          if (!_())
            return;
          setTimeout(function() {
            w();
          }, e5);
        }
        t4.reconnect = m;
        function w() {
          t4.allowReconnect = true;
          if (!_())
            return;
          if (!b())
            return;
          h2 = true;
          let r4 = n2.default.socketUrl;
          try {
            let t5 = f2.default.getSync(f2.default.KEY_REDIRECT_SERVER, "");
            if (t5) {
              let e5 = o2.RedirectServerData.parse(t5);
              let i4 = e5.addressList[0].split(",");
              let n3 = i4[0];
              let s3 = Number(i4[1]);
              let a2 = (/* @__PURE__ */ new Date()).getTime();
              if (a2 - e5.time < 1e3 * s3)
                r4 = n3;
            }
          } catch (t5) {
          }
          e4 = l.default.getWebSocket().connect({ url: r4, success: function() {
            i3 = true;
            S();
          }, fail: function() {
            i3 = false;
            M();
            m(100);
          } });
          e4.onOpen(T);
          e4.onClose(x);
          e4.onError(A);
          e4.onMessage(I);
        }
        t4.connect = w;
        function S() {
          if (i3 && r3) {
            h2 = false;
            s2.default.create().send();
            u.default.getInstance().start();
          }
        }
        function _() {
          if (!n2.default.networkConnected) {
            c.default.error(`connect failed, network is not available`);
            return false;
          }
          if (h2) {
            c.default.warn(`connecting`);
            return false;
          }
          if (g()) {
            c.default.warn(`already connected`);
            return false;
          }
          return true;
        }
        function b() {
          var t5 = d2.length;
          let e5 = (/* @__PURE__ */ new Date()).getTime();
          if (t5 > 0) {
            for (var r4 = t5 - 1; r4 >= 0; r4--)
              if (e5 - d2[r4] > 5e3) {
                d2.splice(0, r4 + 1);
                break;
              }
          }
          t5 = d2.length;
          d2.push(e5);
          if (t5 >= v) {
            c.default.error("connect failed, connection limit reached");
            return false;
          }
          return true;
        }
        function E2(t5 = "") {
          null === e4 || void 0 === e4 || e4.close({ code: 1e3, reason: t5, success: function(t6) {
          }, fail: function(t6) {
          } });
          M();
        }
        t4.close = E2;
        function D(t5) {
          if (r3 && r3)
            null === e4 || void 0 === e4 || e4.send({ data: t5, success: function(t6) {
            }, fail: function(t6) {
            } });
          else
            throw new Error(`socket not connect`);
        }
        t4.send = D;
        function M(t5) {
          var e5;
          i3 = false;
          r3 = false;
          h2 = false;
          u.default.getInstance().cancel();
          if (n2.default.online) {
            n2.default.online = false;
            null === (e5 = n2.default.onlineState) || void 0 === e5 || e5.call(n2.default.onlineState, { online: n2.default.online });
          }
        }
        let T = function(t5) {
          r3 = true;
          S();
        };
        let I = function(t5) {
          try {
            t5.data;
            u.default.getInstance().refresh();
            a.default.receiveMessage(t5.data);
          } catch (t6) {
          }
        };
        let A = function(t5) {
          E2(`socket error`);
        };
        let x = function(t5) {
          M();
        };
      })(h || (h = {}));
      e3["default"] = h;
    }, 8506: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(9478));
      var s2;
      (function(t4) {
        t4.KEY_APPID = "getui_appid";
        t4.KEY_CID = "getui_cid";
        t4.KEY_SESSION = "getui_session";
        t4.KEY_REGID = "getui_regid";
        t4.KEY_SOCKET_URL = "getui_socket_url";
        t4.KEY_DEVICE_ID = "getui_deviceid";
        t4.KEY_ADD_PHONE_INFO_TIME = "getui_api_time";
        t4.KEY_BIND_ALIAS_TIME = "getui_ba_time";
        t4.KEY_SET_TAG_TIME = "getui_st_time";
        t4.KEY_REDIRECT_SERVER = "getui_redirect_server";
        t4.KEY_LAST_CONNECT_TIME = "getui_last_connect_time";
        function e4(t5) {
          n2.default.getStorage().set(t5);
        }
        t4.set = e4;
        function r3(t5, e5) {
          n2.default.getStorage().setSync(t5, e5);
        }
        t4.setSync = r3;
        function i3(t5) {
          n2.default.getStorage().get(t5);
        }
        t4.get = i3;
        function s3(t5, e5) {
          let r4 = n2.default.getStorage().getSync(t5);
          return r4 ? r4 : e5;
        }
        t4.getSync = s3;
      })(s2 || (s2 = {}));
      e3["default"] = s2;
    }, 496: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      const n2 = i2(r2(3854));
      class s2 {
        constructor() {
          this.systemInfo = my.getSystemInfoSync();
        }
        os() {
          return n2.default.getStr(this.systemInfo, "platform");
        }
        osVersion() {
          return n2.default.getStr(this.systemInfo, "system");
        }
        model() {
          return n2.default.getStr(this.systemInfo, "model");
        }
        brand() {
          return n2.default.getStr(this.systemInfo, "brand");
        }
        platform() {
          return "MP-ALIPAY";
        }
        platformVersion() {
          return n2.default.getStr(this.systemInfo, "app") + " " + n2.default.getStr(this.systemInfo, "version");
        }
        platformId() {
          return my.getAppIdSync();
        }
        language() {
          return n2.default.getStr(this.systemInfo, "language");
        }
        getNetworkType(t4) {
          my.getNetworkType({ success: (e4) => {
            var r3;
            null === (r3 = t4.success) || void 0 === r3 || r3.call(t4.success, { networkType: e4.networkType });
          }, fail: () => {
            var e4;
            null === (e4 = t4.fail) || void 0 === e4 || e4.call(t4.fail, "");
          } });
        }
        onNetworkStatusChange(t4) {
          my.onNetworkStatusChange(t4);
        }
      }
      t3.exports = s2;
    }, 3555: (t3) => {
      class e3 {
        set(t4) {
          my.setStorage({ key: t4.key, data: t4.data, success: t4.success, fail: t4.fail });
        }
        setSync(t4, e4) {
          my.setStorageSync({ key: t4, data: e4 });
        }
        get(t4) {
          my.getStorage({ key: t4.key, success: t4.success, fail: t4.fail, complete: t4.complete });
        }
        getSync(t4) {
          return my.getStorageSync({ key: t4 }).data;
        }
      }
      t3.exports = e3;
    }, 1929: (t3) => {
      class e3 {
        connect(t4) {
          my.connectSocket({ url: t4.url, header: t4.header, method: t4.method, success: t4.success, fail: t4.fail, complete: t4.complete });
          return { onOpen: my.onSocketOpen, send: my.sendSocketMessage, onMessage: (t5) => {
            my.onSocketMessage.call(my.onSocketMessage, (e4) => {
              t5.call(t5, { data: e4 ? e4.data : "" });
            });
          }, onError: my.onSocketError, onClose: my.onSocketClose, close: my.closeSocket };
        }
      }
      t3.exports = e3;
    }, 4379: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        os() {
          let t4 = window.navigator.userAgent.toLowerCase();
          if (t4.indexOf("android") > 0 || t4.indexOf("adr") > 0)
            return "android";
          if (!!t4.match(/\(i[^;]+;( u;)? cpu.+mac os x/))
            return "ios";
          if (t4.indexOf("windows") > 0 || t4.indexOf("win32") > 0 || t4.indexOf("win64") > 0)
            return "windows";
          if (t4.indexOf("macintosh") > 0 || t4.indexOf("mac os") > 0)
            return "mac os";
          if (t4.indexOf("linux") > 0)
            return "linux";
          if (t4.indexOf("unix") > 0)
            return "linux";
          return "other";
        }
        osVersion() {
          let t4 = window.navigator.userAgent.toLowerCase();
          let e4 = t4.substring(t4.indexOf(";") + 1).trim();
          if (e4.indexOf(";") > 0)
            return e4.substring(0, e4.indexOf(";")).trim();
          return e4.substring(0, e4.indexOf(")")).trim();
        }
        model() {
          return "";
        }
        brand() {
          return "";
        }
        platform() {
          return "H5";
        }
        platformVersion() {
          return "";
        }
        platformId() {
          return "";
        }
        language() {
          return window.navigator.language;
        }
        userAgent() {
          return window.navigator.userAgent;
        }
        getNetworkType(t4) {
          var e4;
          null === (e4 = t4.success) || void 0 === e4 || e4.call(t4.success, { networkType: window.navigator.onLine ? "unknown" : "none" });
        }
        onNetworkStatusChange(t4) {
        }
      }
      e3["default"] = r2;
    }, 6899: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        set(t4) {
          var e4;
          window.localStorage.setItem(t4.key, t4.data);
          null === (e4 = t4.success) || void 0 === e4 || e4.call(t4.success, "");
        }
        setSync(t4, e4) {
          window.localStorage.setItem(t4, e4);
        }
        get(t4) {
          var e4;
          let r3 = window.localStorage.getItem(t4.key);
          null === (e4 = t4.success) || void 0 === e4 || e4.call(t4.success, r3);
        }
        getSync(t4) {
          return window.localStorage.getItem(t4);
        }
      }
      e3["default"] = r2;
    }, 776: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        connect(t4) {
          let e4 = new WebSocket(t4.url);
          return { send: (t5) => {
            var r3, i2;
            try {
              e4.send(t5.data);
              null === (r3 = t5.success) || void 0 === r3 || r3.call(t5.success, { errMsg: "" });
            } catch (e5) {
              null === (i2 = t5.fail) || void 0 === i2 || i2.call(t5.fail, { errMsg: e5 + "" });
            }
          }, close: (t5) => {
            var r3, i2;
            try {
              e4.close(t5.code, t5.reason);
              null === (r3 = t5.success) || void 0 === r3 || r3.call(t5.success, { errMsg: "" });
            } catch (e5) {
              null === (i2 = t5.fail) || void 0 === i2 || i2.call(t5.fail, { errMsg: e5 + "" });
            }
          }, onOpen: (r3) => {
            e4.onopen = (e5) => {
              var i2;
              null === (i2 = t4.success) || void 0 === i2 || i2.call(t4.success, "");
              r3({ header: "" });
            };
          }, onError: (r3) => {
            e4.onerror = (e5) => {
              var i2;
              null === (i2 = t4.fail) || void 0 === i2 || i2.call(t4.fail, "");
              r3({ errMsg: "" });
            };
          }, onMessage: (t5) => {
            e4.onmessage = (e5) => {
              t5({ data: e5.data });
            };
          }, onClose: (t5) => {
            e4.onclose = (e5) => {
              t5(e5);
            };
          } };
        }
      }
      e3["default"] = r2;
    }, 2002: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(3854));
      class s2 {
        constructor() {
          this.systemInfo = tt.getSystemInfoSync();
        }
        os() {
          return n2.default.getStr(this.systemInfo, "platform");
        }
        osVersion() {
          return n2.default.getStr(this.systemInfo, "system");
        }
        model() {
          return n2.default.getStr(this.systemInfo, "model");
        }
        brand() {
          return n2.default.getStr(this.systemInfo, "brand");
        }
        platform() {
          return "MP-TOUTIAO";
        }
        platformVersion() {
          return n2.default.getStr(this.systemInfo, "appName") + " " + n2.default.getStr(this.systemInfo, "version");
        }
        language() {
          return "";
        }
        platformId() {
          return "";
        }
        getNetworkType(t4) {
          tt.getNetworkType(t4);
        }
        onNetworkStatusChange(t4) {
          tt.onNetworkStatusChange(t4);
        }
      }
      e3["default"] = s2;
    }, 5807: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        set(t4) {
          tt.setStorage(t4);
        }
        setSync(t4, e4) {
          tt.setStorageSync(t4, e4);
        }
        get(t4) {
          tt.getStorage(t4);
        }
        getSync(t4) {
          return tt.getStorageSync(t4);
        }
      }
      e3["default"] = r2;
    }, 9704: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        connect(t4) {
          let e4 = tt.connectSocket({ url: t4.url, header: t4.header, protocols: t4.protocols, success: t4.success, fail: t4.fail, complete: t4.complete });
          return { onOpen: e4.onOpen, send: e4.send, onMessage: e4.onMessage, onError: e4.onError, onClose: e4.onClose, close: e4.close };
        }
      }
      e3["default"] = r2;
    }, 6545: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(3854));
      class s2 {
        constructor() {
          try {
            this.systemInfo = index.getSystemInfoSync();
            this.accountInfo = index.getAccountInfoSync();
          } catch (t4) {
          }
        }
        os() {
          return n2.default.getStr(this.systemInfo, "platform");
        }
        model() {
          return n2.default.getStr(this.systemInfo, "model");
        }
        brand() {
          return n2.default.getStr(this.systemInfo, "brand");
        }
        osVersion() {
          return n2.default.getStr(this.systemInfo, "system");
        }
        platform() {
          let t4 = "";
          t4 = "MP-WEIXIN";
          return t4;
        }
        platformVersion() {
          return this.systemInfo ? this.systemInfo.version : "";
        }
        platformId() {
          return this.accountInfo ? this.accountInfo.miniProgram.appId : "";
        }
        language() {
          var t4;
          return (null === (t4 = this.systemInfo) || void 0 === t4 ? void 0 : t4.language) ? this.systemInfo.language : "";
        }
        userAgent() {
          return window ? window.navigator.userAgent : "";
        }
        getNetworkType(t4) {
          index.getNetworkType(t4);
        }
        onNetworkStatusChange(t4) {
          index.onNetworkStatusChange(t4);
        }
      }
      e3["default"] = s2;
    }, 3680: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        set(t4) {
          index.setStorage(t4);
        }
        setSync(t4, e4) {
          index.setStorageSync(t4, e4);
        }
        get(t4) {
          index.getStorage(t4);
        }
        getSync(t4) {
          return index.getStorageSync(t4);
        }
      }
      e3["default"] = r2;
    }, 7706: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        connect(t4) {
          let e4 = index.connectSocket(t4);
          return { send: (t5) => {
            null === e4 || void 0 === e4 || e4.send(t5);
          }, close: (t5) => {
            null === e4 || void 0 === e4 || e4.close(t5);
          }, onOpen: (t5) => {
            null === e4 || void 0 === e4 || e4.onOpen(t5);
          }, onError: (t5) => {
            null === e4 || void 0 === e4 || e4.onError(t5);
          }, onMessage: (t5) => {
            null === e4 || void 0 === e4 || e4.onMessage(t5);
          }, onClose: (t5) => {
            null === e4 || void 0 === e4 || e4.onClose(t5);
          } };
        }
      }
      e3["default"] = r2;
    }, 4486: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(3854));
      class s2 {
        constructor() {
          this.systemInfo = wx$1.getSystemInfoSync();
        }
        os() {
          return n2.default.getStr(this.systemInfo, "platform");
        }
        osVersion() {
          return n2.default.getStr(this.systemInfo, "system");
        }
        model() {
          return n2.default.getStr(this.systemInfo, "model");
        }
        brand() {
          return n2.default.getStr(this.systemInfo, "brand");
        }
        platform() {
          return "MP-WEIXIN";
        }
        platformVersion() {
          return n2.default.getStr(this.systemInfo, "version");
        }
        language() {
          return n2.default.getStr(this.systemInfo, "language");
        }
        platformId() {
          if (wx$1.canIUse("getAccountInfoSync"))
            return wx$1.getAccountInfoSync().miniProgram.appId;
          return "";
        }
        getNetworkType(t4) {
          wx$1.getNetworkType({ success: (e4) => {
            var r3;
            null === (r3 = t4.success) || void 0 === r3 || r3.call(t4.success, { networkType: e4.networkType });
          }, fail: t4.fail });
        }
        onNetworkStatusChange(t4) {
          wx$1.onNetworkStatusChange(t4);
        }
      }
      e3["default"] = s2;
    }, 5867: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        set(t4) {
          wx$1.setStorage(t4);
        }
        setSync(t4, e4) {
          wx$1.setStorageSync(t4, e4);
        }
        get(t4) {
          wx$1.getStorage(t4);
        }
        getSync(t4) {
          return wx$1.getStorageSync(t4);
        }
      }
      e3["default"] = r2;
    }, 7006: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        connect(t4) {
          let e4 = wx$1.connectSocket({ url: t4.url, header: t4.header, protocols: t4.protocols, success: t4.success, fail: t4.fail, complete: t4.complete });
          return { onOpen: e4.onOpen, send: e4.send, onMessage: e4.onMessage, onError: e4.onError, onClose: e4.onClose, close: e4.close };
        }
      }
      e3["default"] = r2;
    }, 6893: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      var r2;
      (function(t4) {
        t4.SDK_VERSION = "GTMP-2.0.4.dcloud";
        t4.DEFAULT_SOCKET_URL = "wss://wshzn.gepush.com:5223/nws";
        t4.SOCKET_PROTOCOL_VERSION = "1.0";
        t4.SERVER_PUBLIC_KEY = "MHwwDQYJKoZIhvcNAQEBBQADawAwaAJhAJp1rROuvBF7sBSnvLaesj2iFhMcY8aXyLvpnNLKs2wjL3JmEnyr++SlVa35liUlzi83tnAFkn3A9GB7pHBNzawyUkBh8WUhq5bnFIkk2RaDa6+5MpG84DEv52p7RR+aWwIDAQAB";
        t4.SERVER_PUBLIC_KEY_ID = "69d747c4b9f641baf4004be4297e9f3b";
        t4.ID_U_2_G = true;
      })(r2 || (r2 = {}));
      e3["default"] = r2;
    }, 7555: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(7002));
      const s2 = i2(r2(529));
      const a = i2(r2(6379));
      class o2 {
        static init(t4) {
          var e4;
          if (this.inited)
            return;
          try {
            this.checkAppid(t4.appid);
            this.inited = true;
            s2.default.info(`init: appid=${t4.appid}`);
            a.default.init(t4);
            n2.default.connect();
          } catch (r3) {
            this.inited = false;
            null === (e4 = t4.onError) || void 0 === e4 || e4.call(t4.onError, { error: r3 });
            throw r3;
          }
        }
        static enableSocket(t4) {
          this.checkInit();
          n2.default.enableSocket(t4);
        }
        static checkInit() {
          if (!this.inited)
            throw new Error(`not init, please invoke init method firstly`);
        }
        static checkAppid(t4) {
          if (null == t4 || void 0 == t4 || "" == t4.trim())
            throw new Error(`invalid appid ${t4}`);
        }
      }
      o2.inited = false;
      e3["default"] = o2;
    }, 6379: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(6667));
      const s2 = i2(r2(8506));
      const a = i2(r2(6893));
      const o2 = i2(r2(7002));
      const u = i2(r2(529));
      const c = i2(r2(4685));
      const l = i2(r2(2323));
      class f2 {
        static init(t4) {
          var e4;
          if (a.default.ID_U_2_G)
            this.appid = l.default.to_getui(t4.appid);
          else
            this.appid = t4.appid;
          this.onError = t4.onError;
          this.onClientId = t4.onClientId;
          this.onlineState = t4.onlineState;
          this.onPushMsg = t4.onPushMsg;
          if (this.appid != s2.default.getSync(s2.default.KEY_APPID, this.appid)) {
            u.default.info("appid changed, clear session and cid");
            s2.default.setSync(s2.default.KEY_CID, "");
            s2.default.setSync(s2.default.KEY_SESSION, "");
          }
          s2.default.setSync(s2.default.KEY_APPID, this.appid);
          this.cid = s2.default.getSync(s2.default.KEY_CID, this.cid);
          if (this.cid)
            null === (e4 = this.onClientId) || void 0 === e4 || e4.call(this.onClientId, { cid: f2.cid });
          this.session = s2.default.getSync(s2.default.KEY_SESSION, this.session);
          this.deviceId = s2.default.getSync(s2.default.KEY_DEVICE_ID, this.deviceId);
          this.regId = s2.default.getSync(s2.default.KEY_REGID, this.regId);
          if (!this.regId) {
            this.regId = this.createRegId();
            s2.default.set({ key: s2.default.KEY_REGID, data: this.regId });
          }
          this.socketUrl = s2.default.getSync(s2.default.KEY_SOCKET_URL, this.socketUrl);
          let r3 = this;
          c.default.getNetworkType({ success: (t5) => {
            r3.networkType = t5.networkType;
            r3.networkConnected = "none" != r3.networkType && "" != r3.networkType;
          } });
          c.default.onNetworkStatusChange((t5) => {
            r3.networkConnected = t5.isConnected;
            r3.networkType = t5.networkType;
            if (r3.networkConnected)
              o2.default.reconnect(100);
          });
        }
        static createRegId() {
          return `M-V${n2.default.md5Hex(this.getUuid())}-${(/* @__PURE__ */ new Date()).getTime()}`;
        }
        static getUuid() {
          return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(t4) {
            let e4 = 16 * Math.random() | 0, r3 = "x" === t4 ? e4 : 3 & e4 | 8;
            return r3.toString(16);
          });
        }
      }
      f2.appid = "";
      f2.cid = "";
      f2.regId = "";
      f2.session = "";
      f2.deviceId = "";
      f2.packetId = 1;
      f2.online = false;
      f2.socketUrl = a.default.DEFAULT_SOCKET_URL;
      f2.publicKeyId = a.default.SERVER_PUBLIC_KEY_ID;
      f2.publicKey = a.default.SERVER_PUBLIC_KEY;
      f2.lastAliasTime = 0;
      f2.networkConnected = true;
      f2.networkType = "none";
      e3["default"] = f2;
    }, 9586: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      var n2, s2;
      Object.defineProperty(e3, "__esModule", { value: true });
      const a = i2(r2(661));
      const o2 = r2(4198);
      const u = i2(r2(6379));
      class c extends a.default {
        constructor() {
          super(...arguments);
          this.actionMsgData = new l();
        }
        static initActionMsg(t4, ...e4) {
          super.initMsg(t4);
          t4.command = a.default.Command.CLIENT_MSG;
          t4.data = t4.actionMsgData = l.create();
          return t4;
        }
        static parseActionMsg(t4, e4) {
          super.parseMsg(t4, e4);
          t4.actionMsgData = l.parse(t4.data);
          return t4;
        }
        send() {
          setTimeout(() => {
            var t4;
            if (c.waitingLoginMsgMap.has(this.actionMsgData.msgId) || c.waitingResponseMsgMap.has(this.actionMsgData.msgId)) {
              c.waitingLoginMsgMap.delete(this.actionMsgData.msgId);
              c.waitingResponseMsgMap.delete(this.actionMsgData.msgId);
              null === (t4 = this.callback) || void 0 === t4 || t4.call(this.callback, { resultCode: o2.ErrorCode.TIME_OUT, message: "waiting time out" });
            }
          }, 1e4);
          if (!u.default.online) {
            c.waitingLoginMsgMap.set(this.actionMsgData.msgId, this);
            return;
          }
          if (this.actionMsgData.msgAction != c.ClientAction.RECEIVED)
            c.waitingResponseMsgMap.set(this.actionMsgData.msgId, this);
          super.send();
        }
        receive() {
        }
        static sendWaitingMessages() {
          let t4 = this.waitingLoginMsgMap.keys();
          let e4;
          while (e4 = t4.next(), !e4.done) {
            let t5 = this.waitingLoginMsgMap.get(e4.value);
            this.waitingLoginMsgMap.delete(e4.value);
            null === t5 || void 0 === t5 || t5.send();
          }
        }
        static getWaitingResponseMessage(t4) {
          return c.waitingResponseMsgMap.get(t4);
        }
        static removeWaitingResponseMessage(t4) {
          let e4 = c.waitingResponseMsgMap.get(t4);
          if (e4)
            c.waitingResponseMsgMap.delete(t4);
          return e4;
        }
      }
      c.ServerAction = (n2 = class {
      }, n2.PUSH_MESSAGE = "pushmessage", n2.REDIRECT_SERVER = "redirect_server", n2.ADD_PHONE_INFO_RESULT = "addphoneinfo", n2.SET_MODE_RESULT = "set_mode_result", n2.SET_TAG_RESULT = "settag_result", n2.BIND_ALIAS_RESULT = "response_bind", n2.UNBIND_ALIAS_RESULT = "response_unbind", n2.FEED_BACK_RESULT = "pushmessage_feedback", n2.RECEIVED = "received", n2);
      c.ClientAction = (s2 = class {
      }, s2.ADD_PHONE_INFO = "addphoneinfo", s2.SET_MODE = "set_mode", s2.FEED_BACK = "pushmessage_feedback", s2.SET_TAGS = "set_tag", s2.BIND_ALIAS = "bind_alias", s2.UNBIND_ALIAS = "unbind_alias", s2.RECEIVED = "received", s2);
      c.waitingLoginMsgMap = /* @__PURE__ */ new Map();
      c.waitingResponseMsgMap = /* @__PURE__ */ new Map();
      class l {
        constructor() {
          this.appId = "";
          this.cid = "";
          this.msgId = "";
          this.msgAction = "";
          this.msgData = "";
          this.msgExtraData = "";
        }
        static create() {
          let t4 = new l();
          t4.appId = u.default.appid;
          t4.cid = u.default.cid;
          t4.msgId = (2147483647 & (/* @__PURE__ */ new Date()).getTime()).toString();
          return t4;
        }
        static parse(t4) {
          let e4 = new l();
          let r3 = JSON.parse(t4);
          e4.appId = r3.appId;
          e4.cid = r3.cid;
          e4.msgId = r3.msgId;
          e4.msgAction = r3.msgAction;
          e4.msgData = r3.msgData;
          e4.msgExtraData = r3.msgExtraData;
          return e4;
        }
      }
      e3["default"] = c;
    }, 4516: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(4685));
      const s2 = i2(r2(8506));
      const a = i2(r2(6893));
      const o2 = r2(4198);
      const u = i2(r2(9586));
      const c = i2(r2(6379));
      class l extends u.default {
        constructor() {
          super(...arguments);
          this.addPhoneInfoData = new f2();
        }
        static create() {
          let t4 = new l();
          super.initActionMsg(t4);
          t4.callback = (e4) => {
            if (e4.resultCode != o2.ErrorCode.SUCCESS && e4.resultCode != o2.ErrorCode.REPEAT_MESSAGE)
              setTimeout(function() {
                t4.send();
              }, 30 * 1e3);
            else
              s2.default.set({ key: s2.default.KEY_ADD_PHONE_INFO_TIME, data: (/* @__PURE__ */ new Date()).getTime() });
          };
          t4.actionMsgData.msgAction = u.default.ClientAction.ADD_PHONE_INFO;
          t4.addPhoneInfoData = f2.create();
          t4.actionMsgData.msgData = JSON.stringify(t4.addPhoneInfoData);
          return t4;
        }
        send() {
          let t4 = (/* @__PURE__ */ new Date()).getTime();
          let e4 = s2.default.getSync(s2.default.KEY_ADD_PHONE_INFO_TIME, 0);
          if (t4 - e4 < 24 * 60 * 60 * 1e3)
            return;
          super.send();
        }
      }
      class f2 {
        constructor() {
          this.model = "";
          this.brand = "";
          this.system_version = "";
          this.version = "";
          this.deviceid = "";
          this.type = "";
        }
        static create() {
          let t4 = new f2();
          t4.model = n2.default.model();
          t4.brand = n2.default.brand();
          t4.system_version = n2.default.osVersion();
          t4.version = a.default.SDK_VERSION;
          t4.device_token = "";
          t4.imei = "";
          t4.oaid = "";
          t4.mac = "";
          t4.idfa = "";
          t4.type = "MINIPROGRAM";
          t4.deviceid = `${t4.type}-${c.default.deviceId}`;
          t4.extra = { os: n2.default.os(), platform: n2.default.platform(), platformVersion: n2.default.platformVersion(), platformId: n2.default.platformId(), language: n2.default.language(), userAgent: n2.default.userAgent() };
          return t4;
        }
      }
      e3["default"] = l;
    }, 8723: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      var n2, s2;
      Object.defineProperty(e3, "__esModule", { value: true });
      const a = i2(r2(6379));
      const o2 = r2(4198);
      const u = i2(r2(9586));
      class c extends u.default {
        constructor() {
          super(...arguments);
          this.feedbackData = new l();
        }
        static create(t4, e4) {
          let r3 = new c();
          super.initActionMsg(r3);
          r3.callback = (t5) => {
            if (t5.resultCode != o2.ErrorCode.SUCCESS && t5.resultCode != o2.ErrorCode.REPEAT_MESSAGE)
              setTimeout(function() {
                r3.send();
              }, 30 * 1e3);
          };
          r3.feedbackData = l.create(t4, e4);
          r3.actionMsgData.msgAction = u.default.ClientAction.FEED_BACK;
          r3.actionMsgData.msgData = JSON.stringify(r3.feedbackData);
          return r3;
        }
        send() {
          super.send();
        }
      }
      c.ActionId = (n2 = class {
      }, n2.RECEIVE = "0", n2.MP_RECEIVE = "210000", n2.WEB_RECEIVE = "220000", n2.BEGIN = "1", n2);
      c.RESULT = (s2 = class {
      }, s2.OK = "ok", s2);
      class l {
        constructor() {
          this.messageid = "";
          this.appkey = "";
          this.appid = "";
          this.taskid = "";
          this.actionid = "";
          this.result = "";
          this.timestamp = "";
        }
        static create(t4, e4) {
          let r3 = new l();
          r3.messageid = t4.pushMessageData.messageid;
          r3.appkey = t4.pushMessageData.appKey;
          r3.appid = a.default.appid;
          r3.taskid = t4.pushMessageData.taskId;
          r3.actionid = e4;
          r3.result = c.RESULT.OK;
          r3.timestamp = (/* @__PURE__ */ new Date()).getTime().toString();
          return r3;
        }
      }
      e3["default"] = c;
    }, 6362: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(661));
      class s2 extends n2.default {
        static create() {
          let t4 = new s2();
          super.initMsg(t4);
          t4.command = n2.default.Command.HEART_BEAT;
          return t4;
        }
      }
      e3["default"] = s2;
    }, 1386: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(6667));
      const s2 = i2(r2(6379));
      const a = i2(r2(661));
      class o2 extends a.default {
        constructor() {
          super(...arguments);
          this.keyNegotiateData = new u();
        }
        static create() {
          let t4 = new o2();
          super.initMsg(t4);
          t4.command = a.default.Command.KEY_NEGOTIATE;
          n2.default.resetKey();
          t4.data = t4.keyNegotiateData = u.create();
          return t4;
        }
        send() {
          super.send();
        }
      }
      class u {
        constructor() {
          this.appId = "";
          this.rsaPublicKeyId = "";
          this.algorithm = "";
          this.secretKey = "";
          this.iv = "";
        }
        static create() {
          let t4 = new u();
          t4.appId = s2.default.appid;
          t4.rsaPublicKeyId = s2.default.publicKeyId;
          t4.algorithm = "AES";
          t4.secretKey = n2.default.getEncryptedSecretKey();
          t4.iv = n2.default.getEncryptedIV();
          return t4;
        }
      }
      e3["default"] = o2;
    }, 1280: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(661));
      const s2 = i2(r2(6667));
      const a = i2(r2(8858));
      const o2 = i2(r2(529));
      const u = i2(r2(6379));
      class c extends n2.default {
        constructor() {
          super(...arguments);
          this.keyNegotiateResultData = new l();
        }
        static parse(t4) {
          let e4 = new c();
          super.parseMsg(e4, t4);
          e4.keyNegotiateResultData = l.parse(e4.data);
          return e4;
        }
        receive() {
          var t4, e4;
          if (0 != this.keyNegotiateResultData.errorCode) {
            o2.default.error(`key negotiate fail: ${this.data}`);
            null === (t4 = u.default.onError) || void 0 === t4 || t4.call(u.default.onError, { error: `key negotiate fail: ${this.data}` });
            return;
          }
          let r3 = this.keyNegotiateResultData.encryptType.split("/");
          if (!s2.default.algorithmMap.has(r3[0].trim().toLowerCase()) || !s2.default.modeMap.has(r3[1].trim().toLowerCase()) || !s2.default.paddingMap.has(r3[2].trim().toLowerCase())) {
            o2.default.error(`key negotiate fail: ${this.data}`);
            null === (e4 = u.default.onError) || void 0 === e4 || e4.call(u.default.onError, { error: `key negotiate fail: ${this.data}` });
            return;
          }
          s2.default.setEncryptParams(r3[0].trim().toLowerCase(), r3[1].trim().toLowerCase(), r3[2].trim().toLowerCase());
          a.default.create().send();
        }
      }
      class l {
        constructor() {
          this.errorCode = -1;
          this.errorMsg = "";
          this.encryptType = "";
        }
        static parse(t4) {
          let e4 = new l();
          let r3 = JSON.parse(t4);
          e4.errorCode = r3.errorCode;
          e4.errorMsg = r3.errorMsg;
          e4.encryptType = r3.encryptType;
          return e4;
        }
      }
      e3["default"] = c;
    }, 8858: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(6379));
      const s2 = i2(r2(6667));
      const a = i2(r2(661));
      const o2 = i2(r2(4534));
      class u extends a.default {
        constructor() {
          super(...arguments);
          this.loginData = new c();
        }
        static create() {
          let t4 = new u();
          super.initMsg(t4);
          t4.command = a.default.Command.LOGIN;
          t4.data = t4.loginData = c.create();
          return t4;
        }
        send() {
          if (!this.loginData.session || n2.default.cid != s2.default.md5Hex(this.loginData.session)) {
            o2.default.create().send();
            return;
          }
          super.send();
        }
      }
      class c {
        constructor() {
          this.appId = "";
          this.session = "";
        }
        static create() {
          let t4 = new c();
          t4.appId = n2.default.appid;
          t4.session = n2.default.session;
          return t4;
        }
      }
      e3["default"] = u;
    }, 1606: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(8506));
      const s2 = i2(r2(661));
      const a = i2(r2(6379));
      const o2 = i2(r2(9586));
      const u = i2(r2(4516));
      const c = i2(r2(8858));
      class l extends s2.default {
        constructor() {
          super(...arguments);
          this.loginResultData = new f2();
        }
        static parse(t4) {
          let e4 = new l();
          super.parseMsg(e4, t4);
          e4.loginResultData = f2.parse(e4.data);
          return e4;
        }
        receive() {
          var t4;
          if (0 != this.loginResultData.errorCode) {
            this.data;
            a.default.session = a.default.cid = "";
            n2.default.setSync(n2.default.KEY_CID, "");
            n2.default.setSync(n2.default.KEY_SESSION, "");
            c.default.create().send();
            return;
          }
          if (!a.default.online) {
            a.default.online = true;
            null === (t4 = a.default.onlineState) || void 0 === t4 || t4.call(a.default.onlineState, { online: a.default.online });
          }
          o2.default.sendWaitingMessages();
          u.default.create().send();
        }
      }
      class f2 {
        constructor() {
          this.errorCode = -1;
          this.errorMsg = "";
          this.session = "";
        }
        static parse(t4) {
          let e4 = new f2();
          let r3 = JSON.parse(t4);
          e4.errorCode = r3.errorCode;
          e4.errorMsg = r3.errorMsg;
          e4.session = r3.session;
          return e4;
        }
      }
      e3["default"] = l;
    }, 661: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      var n2;
      Object.defineProperty(e3, "__esModule", { value: true });
      const s2 = i2(r2(9593));
      const a = i2(r2(7002));
      const o2 = i2(r2(6893));
      const u = i2(r2(6379));
      class c {
        constructor() {
          this.version = "";
          this.command = 0;
          this.packetId = 0;
          this.timeStamp = 0;
          this.data = "";
          this.signature = "";
        }
        static initMsg(t4, ...e4) {
          t4.version = o2.default.SOCKET_PROTOCOL_VERSION;
          t4.command = 0;
          t4.timeStamp = (/* @__PURE__ */ new Date()).getTime();
          return t4;
        }
        static parseMsg(t4, e4) {
          let r3 = JSON.parse(e4);
          t4.version = r3.version;
          t4.command = r3.command;
          t4.packetId = r3.packetId;
          t4.timeStamp = r3.timeStamp;
          t4.data = r3.data;
          t4.signature = r3.signature;
          return t4;
        }
        stringify() {
          return JSON.stringify(this, ["version", "command", "packetId", "timeStamp", "data", "signature"]);
        }
        send() {
          if (!a.default.isAvailable())
            return;
          this.packetId = u.default.packetId++;
          if (this.temp)
            this.data = this.temp;
          else
            this.temp = this.data;
          this.data = JSON.stringify(this.data);
          this.stringify();
          if (this.command != c.Command.HEART_BEAT) {
            s2.default.sign(this);
            if (this.data && this.command != c.Command.KEY_NEGOTIATE)
              s2.default.encrypt(this);
          }
          a.default.send(this.stringify());
        }
      }
      c.Command = (n2 = class {
      }, n2.HEART_BEAT = 0, n2.KEY_NEGOTIATE = 1, n2.KEY_NEGOTIATE_RESULT = 16, n2.REGISTER = 2, n2.REGISTER_RESULT = 32, n2.LOGIN = 3, n2.LOGIN_RESULT = 48, n2.LOGOUT = 4, n2.LOGOUT_RESULT = 64, n2.CLIENT_MSG = 5, n2.SERVER_MSG = 80, n2.SERVER_CLOSE = 96, n2.REDIRECT_SERVER = 112, n2);
      e3["default"] = c;
    }, 9593: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(6667));
      var s2;
      (function(t4) {
        function e4(t5) {
          t5.data = n2.default.encrypt(t5.data);
        }
        t4.encrypt = e4;
        function r3(t5) {
          t5.data = n2.default.decrypt(t5.data);
        }
        t4.decrypt = r3;
        function i3(t5) {
          t5.signature = n2.default.sha256(`${t5.timeStamp}${t5.packetId}${t5.command}${t5.data}`);
        }
        t4.sign = i3;
        function s3(t5) {
          let e5 = n2.default.sha256(`${t5.timeStamp}${t5.packetId}${t5.command}${t5.data}`);
          if (t5.signature != e5)
            throw new Error(`msg signature vierfy failed`);
        }
        t4.verify = s3;
      })(s2 || (s2 = {}));
      e3["default"] = s2;
    }, 4054: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(1280));
      const s2 = i2(r2(1606));
      const a = i2(r2(661));
      const o2 = i2(r2(1277));
      const u = i2(r2(910));
      const c = i2(r2(9538));
      const l = i2(r2(9479));
      const f2 = i2(r2(6755));
      const h = i2(r2(2918));
      const d2 = i2(r2(9586));
      const v = i2(r2(9510));
      const p2 = i2(r2(4626));
      const g = i2(r2(7562));
      const y = i2(r2(9593));
      const m = i2(r2(9586));
      const w = i2(r2(9519));
      const S = i2(r2(8947));
      class _ {
        static receiveMessage(t4) {
          let e4 = a.default.parseMsg(new a.default(), t4);
          if (e4.command == a.default.Command.HEART_BEAT)
            return;
          if (e4.command != a.default.Command.KEY_NEGOTIATE_RESULT && e4.command != a.default.Command.SERVER_CLOSE && e4.command != a.default.Command.REDIRECT_SERVER)
            y.default.decrypt(e4);
          if (e4.command != a.default.Command.SERVER_CLOSE && e4.command != a.default.Command.REDIRECT_SERVER)
            y.default.verify(e4);
          switch (e4.command) {
            case a.default.Command.KEY_NEGOTIATE_RESULT:
              n2.default.parse(e4.stringify()).receive();
              break;
            case a.default.Command.REGISTER_RESULT:
              o2.default.parse(e4.stringify()).receive();
              break;
            case a.default.Command.LOGIN_RESULT:
              s2.default.parse(e4.stringify()).receive();
              break;
            case a.default.Command.SERVER_MSG:
              this.receiveActionMsg(e4.stringify());
              break;
            case a.default.Command.SERVER_CLOSE:
              S.default.parse(e4.stringify()).receive();
              break;
            case a.default.Command.REDIRECT_SERVER:
              h.default.parse(e4.stringify()).receive();
              break;
          }
        }
        static receiveActionMsg(t4) {
          let e4 = m.default.parseActionMsg(new m.default(), t4);
          if (e4.actionMsgData.msgAction != d2.default.ServerAction.RECEIVED && e4.actionMsgData.msgAction != d2.default.ServerAction.REDIRECT_SERVER) {
            let t5 = JSON.parse(e4.actionMsgData.msgData);
            w.default.create(t5.id).send();
          }
          switch (e4.actionMsgData.msgAction) {
            case d2.default.ServerAction.PUSH_MESSAGE:
              f2.default.parse(t4).receive();
              break;
            case d2.default.ServerAction.ADD_PHONE_INFO_RESULT:
              u.default.parse(t4).receive();
              break;
            case d2.default.ServerAction.SET_MODE_RESULT:
              v.default.parse(t4).receive();
              break;
            case d2.default.ServerAction.SET_TAG_RESULT:
              p2.default.parse(t4).receive();
              break;
            case d2.default.ServerAction.BIND_ALIAS_RESULT:
              c.default.parse(t4).receive();
              break;
            case d2.default.ServerAction.UNBIND_ALIAS_RESULT:
              g.default.parse(t4).receive();
              break;
            case d2.default.ServerAction.FEED_BACK_RESULT:
              l.default.parse(t4).receive();
              break;
            case d2.default.ServerAction.RECEIVED:
              w.default.parse(t4).receive();
              break;
          }
        }
      }
      e3["default"] = _;
    }, 9519: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = r2(4198);
      const s2 = i2(r2(6379));
      const a = i2(r2(9586));
      class o2 extends a.default {
        constructor() {
          super(...arguments);
          this.receivedData = new u();
        }
        static create(t4) {
          let e4 = new o2();
          super.initActionMsg(e4);
          e4.callback = (t5) => {
            if (t5.resultCode != n2.ErrorCode.SUCCESS && t5.resultCode != n2.ErrorCode.REPEAT_MESSAGE)
              setTimeout(function() {
                e4.send();
              }, 3 * 1e3);
          };
          e4.actionMsgData.msgAction = a.default.ClientAction.RECEIVED;
          e4.receivedData = u.create(t4);
          e4.actionMsgData.msgData = JSON.stringify(e4.receivedData);
          return e4;
        }
        static parse(t4) {
          let e4 = new o2();
          super.parseActionMsg(e4, t4);
          e4.receivedData = u.parse(e4.data);
          return e4;
        }
        receive() {
          var t4;
          let e4 = a.default.getWaitingResponseMessage(this.actionMsgData.msgId);
          if (e4 && e4.actionMsgData.msgAction == a.default.ClientAction.ADD_PHONE_INFO || e4 && e4.actionMsgData.msgAction == a.default.ClientAction.FEED_BACK) {
            a.default.removeWaitingResponseMessage(e4.actionMsgData.msgId);
            null === (t4 = e4.callback) || void 0 === t4 || t4.call(e4.callback, { resultCode: n2.ErrorCode.SUCCESS, message: "received" });
          }
        }
        send() {
          super.send();
        }
      }
      class u {
        constructor() {
          this.msgId = "";
          this.cid = "";
        }
        static create(t4) {
          let e4 = new u();
          e4.cid = s2.default.cid;
          e4.msgId = t4;
          return e4;
        }
        static parse(t4) {
          let e4 = new u();
          let r3 = JSON.parse(t4);
          e4.cid = r3.cid;
          e4.msgId = r3.msgId;
          return e4;
        }
      }
      e3["default"] = o2;
    }, 2918: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      e3.RedirectServerData = void 0;
      const n2 = i2(r2(7002));
      const s2 = i2(r2(8506));
      const a = i2(r2(661));
      class o2 extends a.default {
        constructor() {
          super(...arguments);
          this.redirectServerData = new u();
        }
        static parse(t4) {
          let e4 = new o2();
          super.parseMsg(e4, t4);
          e4.redirectServerData = u.parse(e4.data);
          return e4;
        }
        receive() {
          this.redirectServerData;
          s2.default.setSync(s2.default.KEY_REDIRECT_SERVER, JSON.stringify(this.redirectServerData));
          n2.default.close("redirect server");
          n2.default.reconnect(this.redirectServerData.delay);
        }
      }
      class u {
        constructor() {
          this.addressList = [];
          this.delay = 0;
          this.loc = "";
          this.conf = "";
          this.time = 0;
        }
        static parse(t4) {
          let e4 = new u();
          let r3 = JSON.parse(t4);
          e4.addressList = r3.addressList;
          e4.delay = r3.delay;
          e4.loc = r3.loc;
          e4.conf = r3.conf;
          e4.time = r3.time ? r3.time : (/* @__PURE__ */ new Date()).getTime();
          return e4;
        }
      }
      e3.RedirectServerData = u;
      e3["default"] = o2;
    }, 4534: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(6379));
      const s2 = i2(r2(661));
      class a extends s2.default {
        constructor() {
          super(...arguments);
          this.registerData = new o2();
        }
        static create() {
          let t4 = new a();
          super.initMsg(t4);
          t4.command = s2.default.Command.REGISTER;
          t4.data = t4.registerData = o2.create();
          return t4;
        }
        send() {
          super.send();
        }
      }
      class o2 {
        constructor() {
          this.appId = "";
          this.regId = "";
        }
        static create() {
          let t4 = new o2();
          t4.appId = n2.default.appid;
          t4.regId = n2.default.regId;
          return t4;
        }
      }
      e3["default"] = a;
    }, 1277: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(661));
      const s2 = i2(r2(8506));
      const a = i2(r2(6379));
      const o2 = i2(r2(8858));
      const u = i2(r2(529));
      class c extends n2.default {
        constructor() {
          super(...arguments);
          this.registerResultData = new l();
        }
        static parse(t4) {
          let e4 = new c();
          super.parseMsg(e4, t4);
          e4.registerResultData = l.parse(e4.data);
          return e4;
        }
        receive() {
          var t4, e4;
          if (0 != this.registerResultData.errorCode || !this.registerResultData.cid || !this.registerResultData.session) {
            u.default.error(`register fail: ${this.data}`);
            null === (t4 = a.default.onError) || void 0 === t4 || t4.call(a.default.onError, { error: `register fail: ${this.data}` });
            return;
          }
          if (a.default.cid != this.registerResultData.cid)
            s2.default.setSync(s2.default.KEY_ADD_PHONE_INFO_TIME, 0);
          a.default.cid = this.registerResultData.cid;
          null === (e4 = a.default.onClientId) || void 0 === e4 || e4.call(a.default.onClientId, { cid: a.default.cid });
          s2.default.set({ key: s2.default.KEY_CID, data: a.default.cid });
          a.default.session = this.registerResultData.session;
          s2.default.set({ key: s2.default.KEY_SESSION, data: a.default.session });
          a.default.deviceId = this.registerResultData.deviceId;
          s2.default.set({ key: s2.default.KEY_DEVICE_ID, data: a.default.deviceId });
          o2.default.create().send();
        }
      }
      class l {
        constructor() {
          this.errorCode = -1;
          this.errorMsg = "";
          this.cid = "";
          this.session = "";
          this.deviceId = "";
          this.regId = "";
        }
        static parse(t4) {
          let e4 = new l();
          let r3 = JSON.parse(t4);
          e4.errorCode = r3.errorCode;
          e4.errorMsg = r3.errorMsg;
          e4.cid = r3.cid;
          e4.session = r3.session;
          e4.deviceId = r3.deviceId;
          e4.regId = r3.regId;
          return e4;
        }
      }
      e3["default"] = c;
    }, 8947: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(7002));
      const s2 = i2(r2(529));
      const a = i2(r2(661));
      class o2 extends a.default {
        constructor() {
          super(...arguments);
          this.serverCloseData = new u();
        }
        static parse(t4) {
          let e4 = new o2();
          super.parseMsg(e4, t4);
          e4.serverCloseData = u.parse(e4.data);
          return e4;
        }
        receive() {
          JSON.stringify(this.serverCloseData);
          let t4 = `server close ${this.serverCloseData.code}`;
          if (20 == this.serverCloseData.code || 23 == this.serverCloseData.code || 24 == this.serverCloseData.code) {
            n2.default.allowReconnect = false;
            n2.default.close(t4);
          } else if (21 == this.serverCloseData.code)
            this.safeClose21(t4);
          else {
            n2.default.allowReconnect = true;
            n2.default.close(t4);
            n2.default.reconnect(10);
          }
        }
        safeClose21(t4) {
          try {
            if ("undefined" != typeof document) {
              if (document.hasFocus() && "visible" == document.visibilityState) {
                n2.default.allowReconnect = true;
                n2.default.close(t4);
                n2.default.reconnect(10);
                return;
              }
            }
            n2.default.allowReconnect = false;
            n2.default.close(t4);
          } catch (e4) {
            s2.default.error(`ServerClose t1`, e4);
            n2.default.allowReconnect = false;
            n2.default.close(`${t4} error`);
          }
        }
      }
      class u {
        constructor() {
          this.code = -1;
          this.msg = "";
        }
        static parse(t4) {
          let e4 = new u();
          let r3 = JSON.parse(t4);
          e4.code = r3.code;
          e4.msg = r3.msg;
          return e4;
        }
      }
      e3["default"] = o2;
    }, 910: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(8506));
      const s2 = i2(r2(9586));
      class a extends s2.default {
        constructor() {
          super(...arguments);
          this.addPhoneInfoResultData = new o2();
        }
        static parse(t4) {
          let e4 = new a();
          super.parseActionMsg(e4, t4);
          e4.addPhoneInfoResultData = o2.parse(e4.actionMsgData.msgData);
          return e4;
        }
        receive() {
          var t4;
          this.addPhoneInfoResultData;
          let e4 = s2.default.removeWaitingResponseMessage(this.actionMsgData.msgId);
          if (e4)
            null === (t4 = e4.callback) || void 0 === t4 || t4.call(e4.callback, { resultCode: this.addPhoneInfoResultData.errorCode, message: this.addPhoneInfoResultData.errorMsg });
          n2.default.set({ key: n2.default.KEY_ADD_PHONE_INFO_TIME, data: (/* @__PURE__ */ new Date()).getTime() });
        }
      }
      class o2 {
        constructor() {
          this.errorCode = -1;
          this.errorMsg = "";
        }
        static parse(t4) {
          let e4 = new o2();
          let r3 = JSON.parse(t4);
          e4.errorCode = r3.errorCode;
          e4.errorMsg = r3.errorMsg;
          return e4;
        }
      }
      e3["default"] = a;
    }, 9538: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(8506));
      const s2 = i2(r2(529));
      const a = i2(r2(9586));
      class o2 extends a.default {
        constructor() {
          super(...arguments);
          this.bindAliasResultData = new u();
        }
        static parse(t4) {
          let e4 = new o2();
          super.parseActionMsg(e4, t4);
          e4.bindAliasResultData = u.parse(e4.actionMsgData.msgData);
          return e4;
        }
        receive() {
          var t4;
          s2.default.info(`bind alias result`, this.bindAliasResultData);
          let e4 = a.default.removeWaitingResponseMessage(this.actionMsgData.msgId);
          if (e4)
            null === (t4 = e4.callback) || void 0 === t4 || t4.call(e4.callback, { resultCode: this.bindAliasResultData.errorCode, message: this.bindAliasResultData.errorMsg });
          n2.default.set({ key: n2.default.KEY_BIND_ALIAS_TIME, data: (/* @__PURE__ */ new Date()).getTime() });
        }
      }
      class u {
        constructor() {
          this.errorCode = -1;
          this.errorMsg = "";
        }
        static parse(t4) {
          let e4 = new u();
          let r3 = JSON.parse(t4);
          e4.errorCode = r3.errorCode;
          e4.errorMsg = r3.errorMsg;
          return e4;
        }
      }
      e3["default"] = o2;
    }, 9479: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = r2(4198);
      const s2 = i2(r2(9586));
      class a extends s2.default {
        constructor() {
          super(...arguments);
          this.feedbackResultData = new o2();
        }
        static parse(t4) {
          let e4 = new a();
          super.parseActionMsg(e4, t4);
          e4.feedbackResultData = o2.parse(e4.actionMsgData.msgData);
          return e4;
        }
        receive() {
          var t4;
          this.feedbackResultData;
          let e4 = s2.default.removeWaitingResponseMessage(this.actionMsgData.msgId);
          if (e4)
            null === (t4 = e4.callback) || void 0 === t4 || t4.call(e4.callback, { resultCode: n2.ErrorCode.SUCCESS, message: "received" });
        }
      }
      class o2 {
        constructor() {
          this.actionId = "";
          this.taskId = "";
          this.result = "";
        }
        static parse(t4) {
          let e4 = new o2();
          let r3 = JSON.parse(t4);
          e4.actionId = r3.actionId;
          e4.taskId = r3.taskId;
          e4.result = r3.result;
          return e4;
        }
      }
      e3["default"] = a;
    }, 6755: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      var n2;
      Object.defineProperty(e3, "__esModule", { value: true });
      const s2 = i2(r2(6379));
      const a = i2(r2(9586));
      const o2 = i2(r2(8723));
      class u extends a.default {
        constructor() {
          super(...arguments);
          this.pushMessageData = new c();
        }
        static parse(t4) {
          let e4 = new u();
          super.parseActionMsg(e4, t4);
          e4.pushMessageData = c.parse(e4.actionMsgData.msgData);
          return e4;
        }
        receive() {
          var t4;
          this.pushMessageData;
          if (this.pushMessageData.appId != s2.default.appid || !this.pushMessageData.messageid || !this.pushMessageData.taskId)
            this.stringify();
          o2.default.create(this, o2.default.ActionId.RECEIVE).send();
          o2.default.create(this, o2.default.ActionId.MP_RECEIVE).send();
          if (this.actionMsgData.msgExtraData && s2.default.onPushMsg)
            null === (t4 = s2.default.onPushMsg) || void 0 === t4 || t4.call(s2.default.onPushMsg, { message: this.actionMsgData.msgExtraData });
        }
      }
      class c {
        constructor() {
          this.id = "";
          this.appKey = "";
          this.appId = "";
          this.messageid = "";
          this.taskId = "";
          this.actionChain = [];
          this.cdnType = "";
        }
        static parse(t4) {
          let e4 = new c();
          let r3 = JSON.parse(t4);
          e4.id = r3.id;
          e4.appKey = r3.appKey;
          e4.appId = r3.appId;
          e4.messageid = r3.messageid;
          e4.taskId = r3.taskId;
          e4.actionChain = r3.actionChain;
          e4.cdnType = r3.cdnType;
          return e4;
        }
      }
      n2 = class {
      }, n2.GO_TO = "goto", n2.TRANSMIT = "transmit";
      e3["default"] = u;
    }, 9510: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(9586));
      class s2 extends n2.default {
        constructor() {
          super(...arguments);
          this.setModeResultData = new a();
        }
        static parse(t4) {
          let e4 = new s2();
          super.parseActionMsg(e4, t4);
          e4.setModeResultData = a.parse(e4.actionMsgData.msgData);
          return e4;
        }
        receive() {
          var t4;
          this.setModeResultData;
          let e4 = n2.default.removeWaitingResponseMessage(this.actionMsgData.msgId);
          if (e4)
            null === (t4 = e4.callback) || void 0 === t4 || t4.call(e4.callback, { resultCode: this.setModeResultData.errorCode, message: this.setModeResultData.errorMsg });
        }
      }
      class a {
        constructor() {
          this.errorCode = -1;
          this.errorMsg = "";
        }
        static parse(t4) {
          let e4 = new a();
          let r3 = JSON.parse(t4);
          e4.errorCode = r3.errorCode;
          e4.errorMsg = r3.errorMsg;
          return e4;
        }
      }
      e3["default"] = s2;
    }, 4626: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(8506));
      const s2 = i2(r2(529));
      const a = i2(r2(9586));
      class o2 extends a.default {
        constructor() {
          super(...arguments);
          this.setTagResultData = new u();
        }
        static parse(t4) {
          let e4 = new o2();
          super.parseActionMsg(e4, t4);
          e4.setTagResultData = u.parse(e4.actionMsgData.msgData);
          return e4;
        }
        receive() {
          var t4;
          s2.default.info(`set tag result`, this.setTagResultData);
          let e4 = a.default.removeWaitingResponseMessage(this.actionMsgData.msgId);
          if (e4)
            null === (t4 = e4.callback) || void 0 === t4 || t4.call(e4.callback, { resultCode: this.setTagResultData.errorCode, message: this.setTagResultData.errorMsg });
          n2.default.set({ key: n2.default.KEY_SET_TAG_TIME, data: (/* @__PURE__ */ new Date()).getTime() });
        }
      }
      class u {
        constructor() {
          this.errorCode = 0;
          this.errorMsg = "";
        }
        static parse(t4) {
          let e4 = new u();
          let r3 = JSON.parse(t4);
          e4.errorCode = r3.errorCode;
          e4.errorMsg = r3.errorMsg;
          return e4;
        }
      }
      e3["default"] = o2;
    }, 7562: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(8506));
      const s2 = i2(r2(529));
      const a = i2(r2(9586));
      class o2 extends a.default {
        constructor() {
          super(...arguments);
          this.unbindAliasResultData = new u();
        }
        static parse(t4) {
          let e4 = new o2();
          super.parseActionMsg(e4, t4);
          e4.unbindAliasResultData = u.parse(e4.actionMsgData.msgData);
          return e4;
        }
        receive() {
          var t4;
          s2.default.info(`unbind alias result`, this.unbindAliasResultData);
          let e4 = a.default.removeWaitingResponseMessage(this.actionMsgData.msgId);
          if (e4)
            null === (t4 = e4.callback) || void 0 === t4 || t4.call(e4.callback, { resultCode: this.unbindAliasResultData.errorCode, message: this.unbindAliasResultData.errorMsg });
          n2.default.set({ key: n2.default.KEY_BIND_ALIAS_TIME, data: (/* @__PURE__ */ new Date()).getTime() });
        }
      }
      class u {
        constructor() {
          this.errorCode = -1;
          this.errorMsg = "";
        }
        static parse(t4) {
          let e4 = new u();
          let r3 = JSON.parse(t4);
          e4.errorCode = r3.errorCode;
          e4.errorMsg = r3.errorMsg;
          return e4;
        }
      }
      e3["default"] = o2;
    }, 8227: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        constructor(t4) {
          this.delay = 10;
          this.delay = t4;
        }
        start() {
          this.cancel();
          let t4 = this;
          this.timer = setInterval(function() {
            t4.run();
          }, this.delay);
        }
        cancel() {
          if (this.timer)
            clearInterval(this.timer);
        }
      }
      e3["default"] = r2;
    }, 7167: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      var n2;
      Object.defineProperty(e3, "__esModule", { value: true });
      const s2 = i2(r2(6362));
      const a = i2(r2(8227));
      class o2 extends a.default {
        static getInstance() {
          return o2.InstanceHolder.instance;
        }
        run() {
          s2.default.create().send();
        }
        refresh() {
          this.delay = 60 * 1e3;
          this.start();
        }
      }
      o2.INTERVAL = 60 * 1e3;
      o2.InstanceHolder = (n2 = class {
      }, n2.instance = new o2(o2.INTERVAL), n2);
      e3["default"] = o2;
    }, 2323: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(4736));
      const s2 = i2(r2(6667));
      var a;
      (function(t4) {
        let e4 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        let r3 = (0, n2.default)("9223372036854775808");
        function i3(t5) {
          let e5 = a2(t5);
          let r4 = o2(e5);
          let i4 = r4[1];
          let n3 = r4[0];
          return u(i4) + u(n3);
        }
        t4.to_getui = i3;
        function a2(t5) {
          let e5 = s2.default.md5Hex(t5);
          let r4 = c(e5);
          r4[6] &= 15;
          r4[6] |= 48;
          r4[8] &= 63;
          r4[8] |= 128;
          return r4;
        }
        function o2(t5) {
          let e5 = (0, n2.default)(0);
          let r4 = (0, n2.default)(0);
          for (let r5 = 0; r5 < 8; r5++)
            e5 = e5.multiply(256).plus((0, n2.default)(255 & t5[r5]));
          for (let e6 = 8; e6 < 16; e6++)
            r4 = r4.multiply(256).plus((0, n2.default)(255 & t5[e6]));
          return [e5, r4];
        }
        function u(t5) {
          if (t5 >= r3)
            t5 = r3.multiply(2).minus(t5);
          let i4 = "";
          for (; t5 > (0, n2.default)(0); t5 = t5.divide(62))
            i4 += e4.charAt(Number(t5.divmod(62).remainder));
          return i4;
        }
        function c(t5) {
          let e5 = t5.length;
          if (e5 % 2 != 0)
            return [];
          let r4 = new Array();
          for (let i4 = 0; i4 < e5; i4 += 2)
            r4.push(parseInt(t5.substring(i4, i4 + 2), 16));
          return r4;
        }
      })(a || (a = {}));
      e3["default"] = a;
    }, 6667: function(t3, e3, r2) {
      var i2 = this && this.__importDefault || function(t4) {
        return t4 && t4.__esModule ? t4 : { default: t4 };
      };
      Object.defineProperty(e3, "__esModule", { value: true });
      const n2 = i2(r2(2620));
      const s2 = i2(r2(1354));
      const a = i2(r2(6379));
      var o2;
      (function(t4) {
        let e4;
        let r3;
        let i3;
        let o3;
        let u = new n2.default();
        let c = s2.default.mode.CBC;
        let l = s2.default.pad.Pkcs7;
        let f2 = s2.default.AES;
        t4.algorithmMap = /* @__PURE__ */ new Map([["aes", s2.default.AES]]);
        t4.modeMap = /* @__PURE__ */ new Map([["cbc", s2.default.mode.CBC], ["cfb", s2.default.mode.CFB], ["cfb128", s2.default.mode.CFB], ["ecb", s2.default.mode.ECB], ["ofb", s2.default.mode.OFB]]);
        t4.paddingMap = /* @__PURE__ */ new Map([["nopadding", s2.default.pad.NoPadding], ["pkcs7", s2.default.pad.Pkcs7]]);
        function h() {
          e4 = s2.default.MD5((/* @__PURE__ */ new Date()).getTime().toString());
          r3 = s2.default.MD5(e4);
          u.setPublicKey(a.default.publicKey);
          e4.toString(s2.default.enc.Hex);
          r3.toString(s2.default.enc.Hex);
          i3 = u.encrypt(e4.toString(s2.default.enc.Hex));
          o3 = u.encrypt(r3.toString(s2.default.enc.Hex));
        }
        t4.resetKey = h;
        function d2(e5, r4, i4) {
          f2 = t4.algorithmMap.get(e5);
          c = t4.modeMap.get(r4);
          l = t4.paddingMap.get(i4);
        }
        t4.setEncryptParams = d2;
        function v(t5) {
          return f2.encrypt(t5, e4, { iv: r3, mode: c, padding: l }).toString();
        }
        t4.encrypt = v;
        function p2(t5) {
          return f2.decrypt(t5, e4, { iv: r3, mode: c, padding: l }).toString(s2.default.enc.Utf8);
        }
        t4.decrypt = p2;
        function g(t5) {
          return s2.default.SHA256(t5).toString(s2.default.enc.Base64);
        }
        t4.sha256 = g;
        function y(t5) {
          return s2.default.MD5(t5).toString(s2.default.enc.Hex);
        }
        t4.md5Hex = y;
        function m() {
          return i3 ? i3 : "";
        }
        t4.getEncryptedSecretKey = m;
        function w() {
          return o3 ? o3 : "";
        }
        t4.getEncryptedIV = w;
      })(o2 || (o2 = {}));
      e3["default"] = o2;
    }, 529: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        static info(...t4) {
          if (this.debugMode)
            console.info(`[GtPush]`, t4);
        }
        static warn(...t4) {
          console.warn(`[GtPush]`, t4);
        }
        static error(...t4) {
          console.error(`[GtPush]`, t4);
        }
      }
      r2.debugMode = false;
      e3["default"] = r2;
    }, 3854: (t3, e3) => {
      Object.defineProperty(e3, "__esModule", { value: true });
      class r2 {
        static getStr(t4, e4) {
          try {
            if (!t4 || void 0 === t4[e4])
              return "";
            return t4[e4];
          } catch (t5) {
          }
          return "";
        }
      }
      e3["default"] = r2;
    }, 2620: (t3, e3, r2) => {
      r2.r(e3);
      r2.d(e3, { JSEncrypt: () => wt, default: () => St });
      var i2 = "0123456789abcdefghijklmnopqrstuvwxyz";
      function n2(t4) {
        return i2.charAt(t4);
      }
      function s2(t4, e4) {
        return t4 & e4;
      }
      function a(t4, e4) {
        return t4 | e4;
      }
      function o2(t4, e4) {
        return t4 ^ e4;
      }
      function u(t4, e4) {
        return t4 & ~e4;
      }
      function c(t4) {
        if (0 == t4)
          return -1;
        var e4 = 0;
        if (0 == (65535 & t4)) {
          t4 >>= 16;
          e4 += 16;
        }
        if (0 == (255 & t4)) {
          t4 >>= 8;
          e4 += 8;
        }
        if (0 == (15 & t4)) {
          t4 >>= 4;
          e4 += 4;
        }
        if (0 == (3 & t4)) {
          t4 >>= 2;
          e4 += 2;
        }
        if (0 == (1 & t4))
          ++e4;
        return e4;
      }
      function l(t4) {
        var e4 = 0;
        while (0 != t4) {
          t4 &= t4 - 1;
          ++e4;
        }
        return e4;
      }
      var f2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
      var h = "=";
      function d2(t4) {
        var e4;
        var r3;
        var i3 = "";
        for (e4 = 0; e4 + 3 <= t4.length; e4 += 3) {
          r3 = parseInt(t4.substring(e4, e4 + 3), 16);
          i3 += f2.charAt(r3 >> 6) + f2.charAt(63 & r3);
        }
        if (e4 + 1 == t4.length) {
          r3 = parseInt(t4.substring(e4, e4 + 1), 16);
          i3 += f2.charAt(r3 << 2);
        } else if (e4 + 2 == t4.length) {
          r3 = parseInt(t4.substring(e4, e4 + 2), 16);
          i3 += f2.charAt(r3 >> 2) + f2.charAt((3 & r3) << 4);
        }
        while ((3 & i3.length) > 0)
          i3 += h;
        return i3;
      }
      function v(t4) {
        var e4 = "";
        var r3;
        var i3 = 0;
        var s3 = 0;
        for (r3 = 0; r3 < t4.length; ++r3) {
          if (t4.charAt(r3) == h)
            break;
          var a2 = f2.indexOf(t4.charAt(r3));
          if (a2 < 0)
            continue;
          if (0 == i3) {
            e4 += n2(a2 >> 2);
            s3 = 3 & a2;
            i3 = 1;
          } else if (1 == i3) {
            e4 += n2(s3 << 2 | a2 >> 4);
            s3 = 15 & a2;
            i3 = 2;
          } else if (2 == i3) {
            e4 += n2(s3);
            e4 += n2(a2 >> 2);
            s3 = 3 & a2;
            i3 = 3;
          } else {
            e4 += n2(s3 << 2 | a2 >> 4);
            e4 += n2(15 & a2);
            i3 = 0;
          }
        }
        if (1 == i3)
          e4 += n2(s3 << 2);
        return e4;
      }
      var g;
      var y = { decode: function(t4) {
        var e4;
        if (void 0 === g) {
          var r3 = "0123456789ABCDEF";
          var i3 = " \f\n\r	 \u2028\u2029";
          g = {};
          for (e4 = 0; e4 < 16; ++e4)
            g[r3.charAt(e4)] = e4;
          r3 = r3.toLowerCase();
          for (e4 = 10; e4 < 16; ++e4)
            g[r3.charAt(e4)] = e4;
          for (e4 = 0; e4 < i3.length; ++e4)
            g[i3.charAt(e4)] = -1;
        }
        var n3 = [];
        var s3 = 0;
        var a2 = 0;
        for (e4 = 0; e4 < t4.length; ++e4) {
          var o3 = t4.charAt(e4);
          if ("=" == o3)
            break;
          o3 = g[o3];
          if (-1 == o3)
            continue;
          if (void 0 === o3)
            throw new Error("Illegal character at offset " + e4);
          s3 |= o3;
          if (++a2 >= 2) {
            n3[n3.length] = s3;
            s3 = 0;
            a2 = 0;
          } else
            s3 <<= 4;
        }
        if (a2)
          throw new Error("Hex encoding incomplete: 4 bits missing");
        return n3;
      } };
      var m;
      var w = { decode: function(t4) {
        var e4;
        if (void 0 === m) {
          var r3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
          var i3 = "= \f\n\r	 \u2028\u2029";
          m = /* @__PURE__ */ Object.create(null);
          for (e4 = 0; e4 < 64; ++e4)
            m[r3.charAt(e4)] = e4;
          m["-"] = 62;
          m["_"] = 63;
          for (e4 = 0; e4 < i3.length; ++e4)
            m[i3.charAt(e4)] = -1;
        }
        var n3 = [];
        var s3 = 0;
        var a2 = 0;
        for (e4 = 0; e4 < t4.length; ++e4) {
          var o3 = t4.charAt(e4);
          if ("=" == o3)
            break;
          o3 = m[o3];
          if (-1 == o3)
            continue;
          if (void 0 === o3)
            throw new Error("Illegal character at offset " + e4);
          s3 |= o3;
          if (++a2 >= 4) {
            n3[n3.length] = s3 >> 16;
            n3[n3.length] = s3 >> 8 & 255;
            n3[n3.length] = 255 & s3;
            s3 = 0;
            a2 = 0;
          } else
            s3 <<= 6;
        }
        switch (a2) {
          case 1:
            throw new Error("Base64 encoding incomplete: at least 2 bits missing");
          case 2:
            n3[n3.length] = s3 >> 10;
            break;
          case 3:
            n3[n3.length] = s3 >> 16;
            n3[n3.length] = s3 >> 8 & 255;
            break;
        }
        return n3;
      }, re: /-----BEGIN [^-]+-----([A-Za-z0-9+\/=\s]+)-----END [^-]+-----|begin-base64[^\n]+\n([A-Za-z0-9+\/=\s]+)====/, unarmor: function(t4) {
        var e4 = w.re.exec(t4);
        if (e4)
          if (e4[1])
            t4 = e4[1];
          else if (e4[2])
            t4 = e4[2];
          else
            throw new Error("RegExp out of sync");
        return w.decode(t4);
      } };
      var S = 1e13;
      var _ = function() {
        function t4(t5) {
          this.buf = [+t5 || 0];
        }
        t4.prototype.mulAdd = function(t5, e4) {
          var r3 = this.buf;
          var i3 = r3.length;
          var n3;
          var s3;
          for (n3 = 0; n3 < i3; ++n3) {
            s3 = r3[n3] * t5 + e4;
            if (s3 < S)
              e4 = 0;
            else {
              e4 = 0 | s3 / S;
              s3 -= e4 * S;
            }
            r3[n3] = s3;
          }
          if (e4 > 0)
            r3[n3] = e4;
        };
        t4.prototype.sub = function(t5) {
          var e4 = this.buf;
          var r3 = e4.length;
          var i3;
          var n3;
          for (i3 = 0; i3 < r3; ++i3) {
            n3 = e4[i3] - t5;
            if (n3 < 0) {
              n3 += S;
              t5 = 1;
            } else
              t5 = 0;
            e4[i3] = n3;
          }
          while (0 === e4[e4.length - 1])
            e4.pop();
        };
        t4.prototype.toString = function(t5) {
          if (10 != (t5 || 10))
            throw new Error("only base 10 is supported");
          var e4 = this.buf;
          var r3 = e4[e4.length - 1].toString();
          for (var i3 = e4.length - 2; i3 >= 0; --i3)
            r3 += (S + e4[i3]).toString().substring(1);
          return r3;
        };
        t4.prototype.valueOf = function() {
          var t5 = this.buf;
          var e4 = 0;
          for (var r3 = t5.length - 1; r3 >= 0; --r3)
            e4 = e4 * S + t5[r3];
          return e4;
        };
        t4.prototype.simplify = function() {
          var t5 = this.buf;
          return 1 == t5.length ? t5[0] : this;
        };
        return t4;
      }();
      var b = "…";
      var E2 = /^(\d\d)(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01])([01]\d|2[0-3])(?:([0-5]\d)(?:([0-5]\d)(?:[.,](\d{1,3}))?)?)?(Z|[-+](?:[0]\d|1[0-2])([0-5]\d)?)?$/;
      var D = /^(\d\d\d\d)(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01])([01]\d|2[0-3])(?:([0-5]\d)(?:([0-5]\d)(?:[.,](\d{1,3}))?)?)?(Z|[-+](?:[0]\d|1[0-2])([0-5]\d)?)?$/;
      function M(t4, e4) {
        if (t4.length > e4)
          t4 = t4.substring(0, e4) + b;
        return t4;
      }
      var T = function() {
        function t4(e4, r3) {
          this.hexDigits = "0123456789ABCDEF";
          if (e4 instanceof t4) {
            this.enc = e4.enc;
            this.pos = e4.pos;
          } else {
            this.enc = e4;
            this.pos = r3;
          }
        }
        t4.prototype.get = function(t5) {
          if (void 0 === t5)
            t5 = this.pos++;
          if (t5 >= this.enc.length)
            throw new Error("Requesting byte offset " + t5 + " on a stream of length " + this.enc.length);
          return "string" === typeof this.enc ? this.enc.charCodeAt(t5) : this.enc[t5];
        };
        t4.prototype.hexByte = function(t5) {
          return this.hexDigits.charAt(t5 >> 4 & 15) + this.hexDigits.charAt(15 & t5);
        };
        t4.prototype.hexDump = function(t5, e4, r3) {
          var i3 = "";
          for (var n3 = t5; n3 < e4; ++n3) {
            i3 += this.hexByte(this.get(n3));
            if (true !== r3)
              switch (15 & n3) {
                case 7:
                  i3 += "  ";
                  break;
                case 15:
                  i3 += "\n";
                  break;
                default:
                  i3 += " ";
              }
          }
          return i3;
        };
        t4.prototype.isASCII = function(t5, e4) {
          for (var r3 = t5; r3 < e4; ++r3) {
            var i3 = this.get(r3);
            if (i3 < 32 || i3 > 176)
              return false;
          }
          return true;
        };
        t4.prototype.parseStringISO = function(t5, e4) {
          var r3 = "";
          for (var i3 = t5; i3 < e4; ++i3)
            r3 += String.fromCharCode(this.get(i3));
          return r3;
        };
        t4.prototype.parseStringUTF = function(t5, e4) {
          var r3 = "";
          for (var i3 = t5; i3 < e4; ) {
            var n3 = this.get(i3++);
            if (n3 < 128)
              r3 += String.fromCharCode(n3);
            else if (n3 > 191 && n3 < 224)
              r3 += String.fromCharCode((31 & n3) << 6 | 63 & this.get(i3++));
            else
              r3 += String.fromCharCode((15 & n3) << 12 | (63 & this.get(i3++)) << 6 | 63 & this.get(i3++));
          }
          return r3;
        };
        t4.prototype.parseStringBMP = function(t5, e4) {
          var r3 = "";
          var i3;
          var n3;
          for (var s3 = t5; s3 < e4; ) {
            i3 = this.get(s3++);
            n3 = this.get(s3++);
            r3 += String.fromCharCode(i3 << 8 | n3);
          }
          return r3;
        };
        t4.prototype.parseTime = function(t5, e4, r3) {
          var i3 = this.parseStringISO(t5, e4);
          var n3 = (r3 ? E2 : D).exec(i3);
          if (!n3)
            return "Unrecognized time: " + i3;
          if (r3) {
            n3[1] = +n3[1];
            n3[1] += +n3[1] < 70 ? 2e3 : 1900;
          }
          i3 = n3[1] + "-" + n3[2] + "-" + n3[3] + " " + n3[4];
          if (n3[5]) {
            i3 += ":" + n3[5];
            if (n3[6]) {
              i3 += ":" + n3[6];
              if (n3[7])
                i3 += "." + n3[7];
            }
          }
          if (n3[8]) {
            i3 += " UTC";
            if ("Z" != n3[8]) {
              i3 += n3[8];
              if (n3[9])
                i3 += ":" + n3[9];
            }
          }
          return i3;
        };
        t4.prototype.parseInteger = function(t5, e4) {
          var r3 = this.get(t5);
          var i3 = r3 > 127;
          var n3 = i3 ? 255 : 0;
          var s3;
          var a2 = "";
          while (r3 == n3 && ++t5 < e4)
            r3 = this.get(t5);
          s3 = e4 - t5;
          if (0 === s3)
            return i3 ? -1 : 0;
          if (s3 > 4) {
            a2 = r3;
            s3 <<= 3;
            while (0 == (128 & (+a2 ^ n3))) {
              a2 = +a2 << 1;
              --s3;
            }
            a2 = "(" + s3 + " bit)\n";
          }
          if (i3)
            r3 -= 256;
          var o3 = new _(r3);
          for (var u2 = t5 + 1; u2 < e4; ++u2)
            o3.mulAdd(256, this.get(u2));
          return a2 + o3.toString();
        };
        t4.prototype.parseBitString = function(t5, e4, r3) {
          var i3 = this.get(t5);
          var n3 = (e4 - t5 - 1 << 3) - i3;
          var s3 = "(" + n3 + " bit)\n";
          var a2 = "";
          for (var o3 = t5 + 1; o3 < e4; ++o3) {
            var u2 = this.get(o3);
            var c2 = o3 == e4 - 1 ? i3 : 0;
            for (var l2 = 7; l2 >= c2; --l2)
              a2 += u2 >> l2 & 1 ? "1" : "0";
            if (a2.length > r3)
              return s3 + M(a2, r3);
          }
          return s3 + a2;
        };
        t4.prototype.parseOctetString = function(t5, e4, r3) {
          if (this.isASCII(t5, e4))
            return M(this.parseStringISO(t5, e4), r3);
          var i3 = e4 - t5;
          var n3 = "(" + i3 + " byte)\n";
          r3 /= 2;
          if (i3 > r3)
            e4 = t5 + r3;
          for (var s3 = t5; s3 < e4; ++s3)
            n3 += this.hexByte(this.get(s3));
          if (i3 > r3)
            n3 += b;
          return n3;
        };
        t4.prototype.parseOID = function(t5, e4, r3) {
          var i3 = "";
          var n3 = new _();
          var s3 = 0;
          for (var a2 = t5; a2 < e4; ++a2) {
            var o3 = this.get(a2);
            n3.mulAdd(128, 127 & o3);
            s3 += 7;
            if (!(128 & o3)) {
              if ("" === i3) {
                n3 = n3.simplify();
                if (n3 instanceof _) {
                  n3.sub(80);
                  i3 = "2." + n3.toString();
                } else {
                  var u2 = n3 < 80 ? n3 < 40 ? 0 : 1 : 2;
                  i3 = u2 + "." + (n3 - 40 * u2);
                }
              } else
                i3 += "." + n3.toString();
              if (i3.length > r3)
                return M(i3, r3);
              n3 = new _();
              s3 = 0;
            }
          }
          if (s3 > 0)
            i3 += ".incomplete";
          return i3;
        };
        return t4;
      }();
      var I = function() {
        function t4(t5, e4, r3, i3, n3) {
          if (!(i3 instanceof A))
            throw new Error("Invalid tag value.");
          this.stream = t5;
          this.header = e4;
          this.length = r3;
          this.tag = i3;
          this.sub = n3;
        }
        t4.prototype.typeName = function() {
          switch (this.tag.tagClass) {
            case 0:
              switch (this.tag.tagNumber) {
                case 0:
                  return "EOC";
                case 1:
                  return "BOOLEAN";
                case 2:
                  return "INTEGER";
                case 3:
                  return "BIT_STRING";
                case 4:
                  return "OCTET_STRING";
                case 5:
                  return "NULL";
                case 6:
                  return "OBJECT_IDENTIFIER";
                case 7:
                  return "ObjectDescriptor";
                case 8:
                  return "EXTERNAL";
                case 9:
                  return "REAL";
                case 10:
                  return "ENUMERATED";
                case 11:
                  return "EMBEDDED_PDV";
                case 12:
                  return "UTF8String";
                case 16:
                  return "SEQUENCE";
                case 17:
                  return "SET";
                case 18:
                  return "NumericString";
                case 19:
                  return "PrintableString";
                case 20:
                  return "TeletexString";
                case 21:
                  return "VideotexString";
                case 22:
                  return "IA5String";
                case 23:
                  return "UTCTime";
                case 24:
                  return "GeneralizedTime";
                case 25:
                  return "GraphicString";
                case 26:
                  return "VisibleString";
                case 27:
                  return "GeneralString";
                case 28:
                  return "UniversalString";
                case 30:
                  return "BMPString";
              }
              return "Universal_" + this.tag.tagNumber.toString();
            case 1:
              return "Application_" + this.tag.tagNumber.toString();
            case 2:
              return "[" + this.tag.tagNumber.toString() + "]";
            case 3:
              return "Private_" + this.tag.tagNumber.toString();
          }
        };
        t4.prototype.content = function(t5) {
          if (void 0 === this.tag)
            return null;
          if (void 0 === t5)
            t5 = 1 / 0;
          var e4 = this.posContent();
          var r3 = Math.abs(this.length);
          if (!this.tag.isUniversal()) {
            if (null !== this.sub)
              return "(" + this.sub.length + " elem)";
            return this.stream.parseOctetString(e4, e4 + r3, t5);
          }
          switch (this.tag.tagNumber) {
            case 1:
              return 0 === this.stream.get(e4) ? "false" : "true";
            case 2:
              return this.stream.parseInteger(e4, e4 + r3);
            case 3:
              return this.sub ? "(" + this.sub.length + " elem)" : this.stream.parseBitString(e4, e4 + r3, t5);
            case 4:
              return this.sub ? "(" + this.sub.length + " elem)" : this.stream.parseOctetString(e4, e4 + r3, t5);
            case 6:
              return this.stream.parseOID(e4, e4 + r3, t5);
            case 16:
            case 17:
              if (null !== this.sub)
                return "(" + this.sub.length + " elem)";
              else
                return "(no elem)";
            case 12:
              return M(this.stream.parseStringUTF(e4, e4 + r3), t5);
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 26:
              return M(this.stream.parseStringISO(e4, e4 + r3), t5);
            case 30:
              return M(this.stream.parseStringBMP(e4, e4 + r3), t5);
            case 23:
            case 24:
              return this.stream.parseTime(e4, e4 + r3, 23 == this.tag.tagNumber);
          }
          return null;
        };
        t4.prototype.toString = function() {
          return this.typeName() + "@" + this.stream.pos + "[header:" + this.header + ",length:" + this.length + ",sub:" + (null === this.sub ? "null" : this.sub.length) + "]";
        };
        t4.prototype.toPrettyString = function(t5) {
          if (void 0 === t5)
            t5 = "";
          var e4 = t5 + this.typeName() + " @" + this.stream.pos;
          if (this.length >= 0)
            e4 += "+";
          e4 += this.length;
          if (this.tag.tagConstructed)
            e4 += " (constructed)";
          else if (this.tag.isUniversal() && (3 == this.tag.tagNumber || 4 == this.tag.tagNumber) && null !== this.sub)
            e4 += " (encapsulates)";
          e4 += "\n";
          if (null !== this.sub) {
            t5 += "  ";
            for (var r3 = 0, i3 = this.sub.length; r3 < i3; ++r3)
              e4 += this.sub[r3].toPrettyString(t5);
          }
          return e4;
        };
        t4.prototype.posStart = function() {
          return this.stream.pos;
        };
        t4.prototype.posContent = function() {
          return this.stream.pos + this.header;
        };
        t4.prototype.posEnd = function() {
          return this.stream.pos + this.header + Math.abs(this.length);
        };
        t4.prototype.toHexString = function() {
          return this.stream.hexDump(this.posStart(), this.posEnd(), true);
        };
        t4.decodeLength = function(t5) {
          var e4 = t5.get();
          var r3 = 127 & e4;
          if (r3 == e4)
            return r3;
          if (r3 > 6)
            throw new Error("Length over 48 bits not supported at position " + (t5.pos - 1));
          if (0 === r3)
            return null;
          e4 = 0;
          for (var i3 = 0; i3 < r3; ++i3)
            e4 = 256 * e4 + t5.get();
          return e4;
        };
        t4.prototype.getHexStringValue = function() {
          var t5 = this.toHexString();
          var e4 = 2 * this.header;
          var r3 = 2 * this.length;
          return t5.substr(e4, r3);
        };
        t4.decode = function(e4) {
          var r3;
          if (!(e4 instanceof T))
            r3 = new T(e4, 0);
          else
            r3 = e4;
          var i3 = new T(r3);
          var n3 = new A(r3);
          var s3 = t4.decodeLength(r3);
          var a2 = r3.pos;
          var o3 = a2 - i3.pos;
          var u2 = null;
          var c2 = function() {
            var e5 = [];
            if (null !== s3) {
              var i4 = a2 + s3;
              while (r3.pos < i4)
                e5[e5.length] = t4.decode(r3);
              if (r3.pos != i4)
                throw new Error("Content size is not correct for container starting at offset " + a2);
            } else
              try {
                for (; ; ) {
                  var n4 = t4.decode(r3);
                  if (n4.tag.isEOC())
                    break;
                  e5[e5.length] = n4;
                }
                s3 = a2 - r3.pos;
              } catch (t5) {
                throw new Error("Exception while decoding undefined length content: " + t5);
              }
            return e5;
          };
          if (n3.tagConstructed)
            u2 = c2();
          else if (n3.isUniversal() && (3 == n3.tagNumber || 4 == n3.tagNumber))
            try {
              if (3 == n3.tagNumber) {
                if (0 != r3.get())
                  throw new Error("BIT STRINGs with unused bits cannot encapsulate.");
              }
              u2 = c2();
              for (var l2 = 0; l2 < u2.length; ++l2)
                if (u2[l2].tag.isEOC())
                  throw new Error("EOC is not supposed to be actual content.");
            } catch (t5) {
              u2 = null;
            }
          if (null === u2) {
            if (null === s3)
              throw new Error("We can't skip over an invalid tag with undefined length at offset " + a2);
            r3.pos = a2 + Math.abs(s3);
          }
          return new t4(i3, o3, s3, n3, u2);
        };
        return t4;
      }();
      var A = function() {
        function t4(t5) {
          var e4 = t5.get();
          this.tagClass = e4 >> 6;
          this.tagConstructed = 0 !== (32 & e4);
          this.tagNumber = 31 & e4;
          if (31 == this.tagNumber) {
            var r3 = new _();
            do {
              e4 = t5.get();
              r3.mulAdd(128, 127 & e4);
            } while (128 & e4);
            this.tagNumber = r3.simplify();
          }
        }
        t4.prototype.isUniversal = function() {
          return 0 === this.tagClass;
        };
        t4.prototype.isEOC = function() {
          return 0 === this.tagClass && 0 === this.tagNumber;
        };
        return t4;
      }();
      var x;
      var R = 244837814094590;
      var B = 15715070 == (16777215 & R);
      var O = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997];
      var k = (1 << 26) / O[O.length - 1];
      var C = function() {
        function t4(t5, e4, r3) {
          if (null != t5)
            if ("number" == typeof t5)
              this.fromNumber(t5, e4, r3);
            else if (null == e4 && "string" != typeof t5)
              this.fromString(t5, 256);
            else
              this.fromString(t5, e4);
        }
        t4.prototype.toString = function(t5) {
          if (this.s < 0)
            return "-" + this.negate().toString(t5);
          var e4;
          if (16 == t5)
            e4 = 4;
          else if (8 == t5)
            e4 = 3;
          else if (2 == t5)
            e4 = 1;
          else if (32 == t5)
            e4 = 5;
          else if (4 == t5)
            e4 = 2;
          else
            return this.toRadix(t5);
          var r3 = (1 << e4) - 1;
          var i3;
          var s3 = false;
          var a2 = "";
          var o3 = this.t;
          var u2 = this.DB - o3 * this.DB % e4;
          if (o3-- > 0) {
            if (u2 < this.DB && (i3 = this[o3] >> u2) > 0) {
              s3 = true;
              a2 = n2(i3);
            }
            while (o3 >= 0) {
              if (u2 < e4) {
                i3 = (this[o3] & (1 << u2) - 1) << e4 - u2;
                i3 |= this[--o3] >> (u2 += this.DB - e4);
              } else {
                i3 = this[o3] >> (u2 -= e4) & r3;
                if (u2 <= 0) {
                  u2 += this.DB;
                  --o3;
                }
              }
              if (i3 > 0)
                s3 = true;
              if (s3)
                a2 += n2(i3);
            }
          }
          return s3 ? a2 : "0";
        };
        t4.prototype.negate = function() {
          var e4 = H();
          t4.ZERO.subTo(this, e4);
          return e4;
        };
        t4.prototype.abs = function() {
          return this.s < 0 ? this.negate() : this;
        };
        t4.prototype.compareTo = function(t5) {
          var e4 = this.s - t5.s;
          if (0 != e4)
            return e4;
          var r3 = this.t;
          e4 = r3 - t5.t;
          if (0 != e4)
            return this.s < 0 ? -e4 : e4;
          while (--r3 >= 0)
            if (0 != (e4 = this[r3] - t5[r3]))
              return e4;
          return 0;
        };
        t4.prototype.bitLength = function() {
          if (this.t <= 0)
            return 0;
          return this.DB * (this.t - 1) + W(this[this.t - 1] ^ this.s & this.DM);
        };
        t4.prototype.mod = function(e4) {
          var r3 = H();
          this.abs().divRemTo(e4, null, r3);
          if (this.s < 0 && r3.compareTo(t4.ZERO) > 0)
            e4.subTo(r3, r3);
          return r3;
        };
        t4.prototype.modPowInt = function(t5, e4) {
          var r3;
          if (t5 < 256 || e4.isEven())
            r3 = new P(e4);
          else
            r3 = new V(e4);
          return this.exp(t5, r3);
        };
        t4.prototype.clone = function() {
          var t5 = H();
          this.copyTo(t5);
          return t5;
        };
        t4.prototype.intValue = function() {
          if (this.s < 0) {
            if (1 == this.t)
              return this[0] - this.DV;
            else if (0 == this.t)
              return -1;
          } else if (1 == this.t)
            return this[0];
          else if (0 == this.t)
            return 0;
          return (this[1] & (1 << 32 - this.DB) - 1) << this.DB | this[0];
        };
        t4.prototype.byteValue = function() {
          return 0 == this.t ? this.s : this[0] << 24 >> 24;
        };
        t4.prototype.shortValue = function() {
          return 0 == this.t ? this.s : this[0] << 16 >> 16;
        };
        t4.prototype.signum = function() {
          if (this.s < 0)
            return -1;
          else if (this.t <= 0 || 1 == this.t && this[0] <= 0)
            return 0;
          else
            return 1;
        };
        t4.prototype.toByteArray = function() {
          var t5 = this.t;
          var e4 = [];
          e4[0] = this.s;
          var r3 = this.DB - t5 * this.DB % 8;
          var i3;
          var n3 = 0;
          if (t5-- > 0) {
            if (r3 < this.DB && (i3 = this[t5] >> r3) != (this.s & this.DM) >> r3)
              e4[n3++] = i3 | this.s << this.DB - r3;
            while (t5 >= 0) {
              if (r3 < 8) {
                i3 = (this[t5] & (1 << r3) - 1) << 8 - r3;
                i3 |= this[--t5] >> (r3 += this.DB - 8);
              } else {
                i3 = this[t5] >> (r3 -= 8) & 255;
                if (r3 <= 0) {
                  r3 += this.DB;
                  --t5;
                }
              }
              if (0 != (128 & i3))
                i3 |= -256;
              if (0 == n3 && (128 & this.s) != (128 & i3))
                ++n3;
              if (n3 > 0 || i3 != this.s)
                e4[n3++] = i3;
            }
          }
          return e4;
        };
        t4.prototype.equals = function(t5) {
          return 0 == this.compareTo(t5);
        };
        t4.prototype.min = function(t5) {
          return this.compareTo(t5) < 0 ? this : t5;
        };
        t4.prototype.max = function(t5) {
          return this.compareTo(t5) > 0 ? this : t5;
        };
        t4.prototype.and = function(t5) {
          var e4 = H();
          this.bitwiseTo(t5, s2, e4);
          return e4;
        };
        t4.prototype.or = function(t5) {
          var e4 = H();
          this.bitwiseTo(t5, a, e4);
          return e4;
        };
        t4.prototype.xor = function(t5) {
          var e4 = H();
          this.bitwiseTo(t5, o2, e4);
          return e4;
        };
        t4.prototype.andNot = function(t5) {
          var e4 = H();
          this.bitwiseTo(t5, u, e4);
          return e4;
        };
        t4.prototype.not = function() {
          var t5 = H();
          for (var e4 = 0; e4 < this.t; ++e4)
            t5[e4] = this.DM & ~this[e4];
          t5.t = this.t;
          t5.s = ~this.s;
          return t5;
        };
        t4.prototype.shiftLeft = function(t5) {
          var e4 = H();
          if (t5 < 0)
            this.rShiftTo(-t5, e4);
          else
            this.lShiftTo(t5, e4);
          return e4;
        };
        t4.prototype.shiftRight = function(t5) {
          var e4 = H();
          if (t5 < 0)
            this.lShiftTo(-t5, e4);
          else
            this.rShiftTo(t5, e4);
          return e4;
        };
        t4.prototype.getLowestSetBit = function() {
          for (var t5 = 0; t5 < this.t; ++t5)
            if (0 != this[t5])
              return t5 * this.DB + c(this[t5]);
          if (this.s < 0)
            return this.t * this.DB;
          return -1;
        };
        t4.prototype.bitCount = function() {
          var t5 = 0;
          var e4 = this.s & this.DM;
          for (var r3 = 0; r3 < this.t; ++r3)
            t5 += l(this[r3] ^ e4);
          return t5;
        };
        t4.prototype.testBit = function(t5) {
          var e4 = Math.floor(t5 / this.DB);
          if (e4 >= this.t)
            return 0 != this.s;
          return 0 != (this[e4] & 1 << t5 % this.DB);
        };
        t4.prototype.setBit = function(t5) {
          return this.changeBit(t5, a);
        };
        t4.prototype.clearBit = function(t5) {
          return this.changeBit(t5, u);
        };
        t4.prototype.flipBit = function(t5) {
          return this.changeBit(t5, o2);
        };
        t4.prototype.add = function(t5) {
          var e4 = H();
          this.addTo(t5, e4);
          return e4;
        };
        t4.prototype.subtract = function(t5) {
          var e4 = H();
          this.subTo(t5, e4);
          return e4;
        };
        t4.prototype.multiply = function(t5) {
          var e4 = H();
          this.multiplyTo(t5, e4);
          return e4;
        };
        t4.prototype.divide = function(t5) {
          var e4 = H();
          this.divRemTo(t5, e4, null);
          return e4;
        };
        t4.prototype.remainder = function(t5) {
          var e4 = H();
          this.divRemTo(t5, null, e4);
          return e4;
        };
        t4.prototype.divideAndRemainder = function(t5) {
          var e4 = H();
          var r3 = H();
          this.divRemTo(t5, e4, r3);
          return [e4, r3];
        };
        t4.prototype.modPow = function(t5, e4) {
          var r3 = t5.bitLength();
          var i3;
          var n3 = Y(1);
          var s3;
          if (r3 <= 0)
            return n3;
          else if (r3 < 18)
            i3 = 1;
          else if (r3 < 48)
            i3 = 3;
          else if (r3 < 144)
            i3 = 4;
          else if (r3 < 768)
            i3 = 5;
          else
            i3 = 6;
          if (r3 < 8)
            s3 = new P(e4);
          else if (e4.isEven())
            s3 = new L(e4);
          else
            s3 = new V(e4);
          var a2 = [];
          var o3 = 3;
          var u2 = i3 - 1;
          var c2 = (1 << i3) - 1;
          a2[1] = s3.convert(this);
          if (i3 > 1) {
            var l2 = H();
            s3.sqrTo(a2[1], l2);
            while (o3 <= c2) {
              a2[o3] = H();
              s3.mulTo(l2, a2[o3 - 2], a2[o3]);
              o3 += 2;
            }
          }
          var f3 = t5.t - 1;
          var h2;
          var d3 = true;
          var v2 = H();
          var p2;
          r3 = W(t5[f3]) - 1;
          while (f3 >= 0) {
            if (r3 >= u2)
              h2 = t5[f3] >> r3 - u2 & c2;
            else {
              h2 = (t5[f3] & (1 << r3 + 1) - 1) << u2 - r3;
              if (f3 > 0)
                h2 |= t5[f3 - 1] >> this.DB + r3 - u2;
            }
            o3 = i3;
            while (0 == (1 & h2)) {
              h2 >>= 1;
              --o3;
            }
            if ((r3 -= o3) < 0) {
              r3 += this.DB;
              --f3;
            }
            if (d3) {
              a2[h2].copyTo(n3);
              d3 = false;
            } else {
              while (o3 > 1) {
                s3.sqrTo(n3, v2);
                s3.sqrTo(v2, n3);
                o3 -= 2;
              }
              if (o3 > 0)
                s3.sqrTo(n3, v2);
              else {
                p2 = n3;
                n3 = v2;
                v2 = p2;
              }
              s3.mulTo(v2, a2[h2], n3);
            }
            while (f3 >= 0 && 0 == (t5[f3] & 1 << r3)) {
              s3.sqrTo(n3, v2);
              p2 = n3;
              n3 = v2;
              v2 = p2;
              if (--r3 < 0) {
                r3 = this.DB - 1;
                --f3;
              }
            }
          }
          return s3.revert(n3);
        };
        t4.prototype.modInverse = function(e4) {
          var r3 = e4.isEven();
          if (this.isEven() && r3 || 0 == e4.signum())
            return t4.ZERO;
          var i3 = e4.clone();
          var n3 = this.clone();
          var s3 = Y(1);
          var a2 = Y(0);
          var o3 = Y(0);
          var u2 = Y(1);
          while (0 != i3.signum()) {
            while (i3.isEven()) {
              i3.rShiftTo(1, i3);
              if (r3) {
                if (!s3.isEven() || !a2.isEven()) {
                  s3.addTo(this, s3);
                  a2.subTo(e4, a2);
                }
                s3.rShiftTo(1, s3);
              } else if (!a2.isEven())
                a2.subTo(e4, a2);
              a2.rShiftTo(1, a2);
            }
            while (n3.isEven()) {
              n3.rShiftTo(1, n3);
              if (r3) {
                if (!o3.isEven() || !u2.isEven()) {
                  o3.addTo(this, o3);
                  u2.subTo(e4, u2);
                }
                o3.rShiftTo(1, o3);
              } else if (!u2.isEven())
                u2.subTo(e4, u2);
              u2.rShiftTo(1, u2);
            }
            if (i3.compareTo(n3) >= 0) {
              i3.subTo(n3, i3);
              if (r3)
                s3.subTo(o3, s3);
              a2.subTo(u2, a2);
            } else {
              n3.subTo(i3, n3);
              if (r3)
                o3.subTo(s3, o3);
              u2.subTo(a2, u2);
            }
          }
          if (0 != n3.compareTo(t4.ONE))
            return t4.ZERO;
          if (u2.compareTo(e4) >= 0)
            return u2.subtract(e4);
          if (u2.signum() < 0)
            u2.addTo(e4, u2);
          else
            return u2;
          if (u2.signum() < 0)
            return u2.add(e4);
          else
            return u2;
        };
        t4.prototype.pow = function(t5) {
          return this.exp(t5, new N());
        };
        t4.prototype.gcd = function(t5) {
          var e4 = this.s < 0 ? this.negate() : this.clone();
          var r3 = t5.s < 0 ? t5.negate() : t5.clone();
          if (e4.compareTo(r3) < 0) {
            var i3 = e4;
            e4 = r3;
            r3 = i3;
          }
          var n3 = e4.getLowestSetBit();
          var s3 = r3.getLowestSetBit();
          if (s3 < 0)
            return e4;
          if (n3 < s3)
            s3 = n3;
          if (s3 > 0) {
            e4.rShiftTo(s3, e4);
            r3.rShiftTo(s3, r3);
          }
          while (e4.signum() > 0) {
            if ((n3 = e4.getLowestSetBit()) > 0)
              e4.rShiftTo(n3, e4);
            if ((n3 = r3.getLowestSetBit()) > 0)
              r3.rShiftTo(n3, r3);
            if (e4.compareTo(r3) >= 0) {
              e4.subTo(r3, e4);
              e4.rShiftTo(1, e4);
            } else {
              r3.subTo(e4, r3);
              r3.rShiftTo(1, r3);
            }
          }
          if (s3 > 0)
            r3.lShiftTo(s3, r3);
          return r3;
        };
        t4.prototype.isProbablePrime = function(t5) {
          var e4;
          var r3 = this.abs();
          if (1 == r3.t && r3[0] <= O[O.length - 1]) {
            for (e4 = 0; e4 < O.length; ++e4)
              if (r3[0] == O[e4])
                return true;
            return false;
          }
          if (r3.isEven())
            return false;
          e4 = 1;
          while (e4 < O.length) {
            var i3 = O[e4];
            var n3 = e4 + 1;
            while (n3 < O.length && i3 < k)
              i3 *= O[n3++];
            i3 = r3.modInt(i3);
            while (e4 < n3)
              if (i3 % O[e4++] == 0)
                return false;
          }
          return r3.millerRabin(t5);
        };
        t4.prototype.copyTo = function(t5) {
          for (var e4 = this.t - 1; e4 >= 0; --e4)
            t5[e4] = this[e4];
          t5.t = this.t;
          t5.s = this.s;
        };
        t4.prototype.fromInt = function(t5) {
          this.t = 1;
          this.s = t5 < 0 ? -1 : 0;
          if (t5 > 0)
            this[0] = t5;
          else if (t5 < -1)
            this[0] = t5 + this.DV;
          else
            this.t = 0;
        };
        t4.prototype.fromString = function(e4, r3) {
          var i3;
          if (16 == r3)
            i3 = 4;
          else if (8 == r3)
            i3 = 3;
          else if (256 == r3)
            i3 = 8;
          else if (2 == r3)
            i3 = 1;
          else if (32 == r3)
            i3 = 5;
          else if (4 == r3)
            i3 = 2;
          else {
            this.fromRadix(e4, r3);
            return;
          }
          this.t = 0;
          this.s = 0;
          var n3 = e4.length;
          var s3 = false;
          var a2 = 0;
          while (--n3 >= 0) {
            var o3 = 8 == i3 ? 255 & +e4[n3] : G(e4, n3);
            if (o3 < 0) {
              if ("-" == e4.charAt(n3))
                s3 = true;
              continue;
            }
            s3 = false;
            if (0 == a2)
              this[this.t++] = o3;
            else if (a2 + i3 > this.DB) {
              this[this.t - 1] |= (o3 & (1 << this.DB - a2) - 1) << a2;
              this[this.t++] = o3 >> this.DB - a2;
            } else
              this[this.t - 1] |= o3 << a2;
            a2 += i3;
            if (a2 >= this.DB)
              a2 -= this.DB;
          }
          if (8 == i3 && 0 != (128 & +e4[0])) {
            this.s = -1;
            if (a2 > 0)
              this[this.t - 1] |= (1 << this.DB - a2) - 1 << a2;
          }
          this.clamp();
          if (s3)
            t4.ZERO.subTo(this, this);
        };
        t4.prototype.clamp = function() {
          var t5 = this.s & this.DM;
          while (this.t > 0 && this[this.t - 1] == t5)
            --this.t;
        };
        t4.prototype.dlShiftTo = function(t5, e4) {
          var r3;
          for (r3 = this.t - 1; r3 >= 0; --r3)
            e4[r3 + t5] = this[r3];
          for (r3 = t5 - 1; r3 >= 0; --r3)
            e4[r3] = 0;
          e4.t = this.t + t5;
          e4.s = this.s;
        };
        t4.prototype.drShiftTo = function(t5, e4) {
          for (var r3 = t5; r3 < this.t; ++r3)
            e4[r3 - t5] = this[r3];
          e4.t = Math.max(this.t - t5, 0);
          e4.s = this.s;
        };
        t4.prototype.lShiftTo = function(t5, e4) {
          var r3 = t5 % this.DB;
          var i3 = this.DB - r3;
          var n3 = (1 << i3) - 1;
          var s3 = Math.floor(t5 / this.DB);
          var a2 = this.s << r3 & this.DM;
          for (var o3 = this.t - 1; o3 >= 0; --o3) {
            e4[o3 + s3 + 1] = this[o3] >> i3 | a2;
            a2 = (this[o3] & n3) << r3;
          }
          for (var o3 = s3 - 1; o3 >= 0; --o3)
            e4[o3] = 0;
          e4[s3] = a2;
          e4.t = this.t + s3 + 1;
          e4.s = this.s;
          e4.clamp();
        };
        t4.prototype.rShiftTo = function(t5, e4) {
          e4.s = this.s;
          var r3 = Math.floor(t5 / this.DB);
          if (r3 >= this.t) {
            e4.t = 0;
            return;
          }
          var i3 = t5 % this.DB;
          var n3 = this.DB - i3;
          var s3 = (1 << i3) - 1;
          e4[0] = this[r3] >> i3;
          for (var a2 = r3 + 1; a2 < this.t; ++a2) {
            e4[a2 - r3 - 1] |= (this[a2] & s3) << n3;
            e4[a2 - r3] = this[a2] >> i3;
          }
          if (i3 > 0)
            e4[this.t - r3 - 1] |= (this.s & s3) << n3;
          e4.t = this.t - r3;
          e4.clamp();
        };
        t4.prototype.subTo = function(t5, e4) {
          var r3 = 0;
          var i3 = 0;
          var n3 = Math.min(t5.t, this.t);
          while (r3 < n3) {
            i3 += this[r3] - t5[r3];
            e4[r3++] = i3 & this.DM;
            i3 >>= this.DB;
          }
          if (t5.t < this.t) {
            i3 -= t5.s;
            while (r3 < this.t) {
              i3 += this[r3];
              e4[r3++] = i3 & this.DM;
              i3 >>= this.DB;
            }
            i3 += this.s;
          } else {
            i3 += this.s;
            while (r3 < t5.t) {
              i3 -= t5[r3];
              e4[r3++] = i3 & this.DM;
              i3 >>= this.DB;
            }
            i3 -= t5.s;
          }
          e4.s = i3 < 0 ? -1 : 0;
          if (i3 < -1)
            e4[r3++] = this.DV + i3;
          else if (i3 > 0)
            e4[r3++] = i3;
          e4.t = r3;
          e4.clamp();
        };
        t4.prototype.multiplyTo = function(e4, r3) {
          var i3 = this.abs();
          var n3 = e4.abs();
          var s3 = i3.t;
          r3.t = s3 + n3.t;
          while (--s3 >= 0)
            r3[s3] = 0;
          for (s3 = 0; s3 < n3.t; ++s3)
            r3[s3 + i3.t] = i3.am(0, n3[s3], r3, s3, 0, i3.t);
          r3.s = 0;
          r3.clamp();
          if (this.s != e4.s)
            t4.ZERO.subTo(r3, r3);
        };
        t4.prototype.squareTo = function(t5) {
          var e4 = this.abs();
          var r3 = t5.t = 2 * e4.t;
          while (--r3 >= 0)
            t5[r3] = 0;
          for (r3 = 0; r3 < e4.t - 1; ++r3) {
            var i3 = e4.am(r3, e4[r3], t5, 2 * r3, 0, 1);
            if ((t5[r3 + e4.t] += e4.am(r3 + 1, 2 * e4[r3], t5, 2 * r3 + 1, i3, e4.t - r3 - 1)) >= e4.DV) {
              t5[r3 + e4.t] -= e4.DV;
              t5[r3 + e4.t + 1] = 1;
            }
          }
          if (t5.t > 0)
            t5[t5.t - 1] += e4.am(r3, e4[r3], t5, 2 * r3, 0, 1);
          t5.s = 0;
          t5.clamp();
        };
        t4.prototype.divRemTo = function(e4, r3, i3) {
          var n3 = e4.abs();
          if (n3.t <= 0)
            return;
          var s3 = this.abs();
          if (s3.t < n3.t) {
            if (null != r3)
              r3.fromInt(0);
            if (null != i3)
              this.copyTo(i3);
            return;
          }
          if (null == i3)
            i3 = H();
          var a2 = H();
          var o3 = this.s;
          var u2 = e4.s;
          var c2 = this.DB - W(n3[n3.t - 1]);
          if (c2 > 0) {
            n3.lShiftTo(c2, a2);
            s3.lShiftTo(c2, i3);
          } else {
            n3.copyTo(a2);
            s3.copyTo(i3);
          }
          var l2 = a2.t;
          var f3 = a2[l2 - 1];
          if (0 == f3)
            return;
          var h2 = f3 * (1 << this.F1) + (l2 > 1 ? a2[l2 - 2] >> this.F2 : 0);
          var d3 = this.FV / h2;
          var v2 = (1 << this.F1) / h2;
          var p2 = 1 << this.F2;
          var g2 = i3.t;
          var y2 = g2 - l2;
          var m2 = null == r3 ? H() : r3;
          a2.dlShiftTo(y2, m2);
          if (i3.compareTo(m2) >= 0) {
            i3[i3.t++] = 1;
            i3.subTo(m2, i3);
          }
          t4.ONE.dlShiftTo(l2, m2);
          m2.subTo(a2, a2);
          while (a2.t < l2)
            a2[a2.t++] = 0;
          while (--y2 >= 0) {
            var w2 = i3[--g2] == f3 ? this.DM : Math.floor(i3[g2] * d3 + (i3[g2 - 1] + p2) * v2);
            if ((i3[g2] += a2.am(0, w2, i3, y2, 0, l2)) < w2) {
              a2.dlShiftTo(y2, m2);
              i3.subTo(m2, i3);
              while (i3[g2] < --w2)
                i3.subTo(m2, i3);
            }
          }
          if (null != r3) {
            i3.drShiftTo(l2, r3);
            if (o3 != u2)
              t4.ZERO.subTo(r3, r3);
          }
          i3.t = l2;
          i3.clamp();
          if (c2 > 0)
            i3.rShiftTo(c2, i3);
          if (o3 < 0)
            t4.ZERO.subTo(i3, i3);
        };
        t4.prototype.invDigit = function() {
          if (this.t < 1)
            return 0;
          var t5 = this[0];
          if (0 == (1 & t5))
            return 0;
          var e4 = 3 & t5;
          e4 = e4 * (2 - (15 & t5) * e4) & 15;
          e4 = e4 * (2 - (255 & t5) * e4) & 255;
          e4 = e4 * (2 - ((65535 & t5) * e4 & 65535)) & 65535;
          e4 = e4 * (2 - t5 * e4 % this.DV) % this.DV;
          return e4 > 0 ? this.DV - e4 : -e4;
        };
        t4.prototype.isEven = function() {
          return 0 == (this.t > 0 ? 1 & this[0] : this.s);
        };
        t4.prototype.exp = function(e4, r3) {
          if (e4 > 4294967295 || e4 < 1)
            return t4.ONE;
          var i3 = H();
          var n3 = H();
          var s3 = r3.convert(this);
          var a2 = W(e4) - 1;
          s3.copyTo(i3);
          while (--a2 >= 0) {
            r3.sqrTo(i3, n3);
            if ((e4 & 1 << a2) > 0)
              r3.mulTo(n3, s3, i3);
            else {
              var o3 = i3;
              i3 = n3;
              n3 = o3;
            }
          }
          return r3.revert(i3);
        };
        t4.prototype.chunkSize = function(t5) {
          return Math.floor(Math.LN2 * this.DB / Math.log(t5));
        };
        t4.prototype.toRadix = function(t5) {
          if (null == t5)
            t5 = 10;
          if (0 == this.signum() || t5 < 2 || t5 > 36)
            return "0";
          var e4 = this.chunkSize(t5);
          var r3 = Math.pow(t5, e4);
          var i3 = Y(r3);
          var n3 = H();
          var s3 = H();
          var a2 = "";
          this.divRemTo(i3, n3, s3);
          while (n3.signum() > 0) {
            a2 = (r3 + s3.intValue()).toString(t5).substr(1) + a2;
            n3.divRemTo(i3, n3, s3);
          }
          return s3.intValue().toString(t5) + a2;
        };
        t4.prototype.fromRadix = function(e4, r3) {
          this.fromInt(0);
          if (null == r3)
            r3 = 10;
          var i3 = this.chunkSize(r3);
          var n3 = Math.pow(r3, i3);
          var s3 = false;
          var a2 = 0;
          var o3 = 0;
          for (var u2 = 0; u2 < e4.length; ++u2) {
            var c2 = G(e4, u2);
            if (c2 < 0) {
              if ("-" == e4.charAt(u2) && 0 == this.signum())
                s3 = true;
              continue;
            }
            o3 = r3 * o3 + c2;
            if (++a2 >= i3) {
              this.dMultiply(n3);
              this.dAddOffset(o3, 0);
              a2 = 0;
              o3 = 0;
            }
          }
          if (a2 > 0) {
            this.dMultiply(Math.pow(r3, a2));
            this.dAddOffset(o3, 0);
          }
          if (s3)
            t4.ZERO.subTo(this, this);
        };
        t4.prototype.fromNumber = function(e4, r3, i3) {
          if ("number" == typeof r3)
            if (e4 < 2)
              this.fromInt(1);
            else {
              this.fromNumber(e4, i3);
              if (!this.testBit(e4 - 1))
                this.bitwiseTo(t4.ONE.shiftLeft(e4 - 1), a, this);
              if (this.isEven())
                this.dAddOffset(1, 0);
              while (!this.isProbablePrime(r3)) {
                this.dAddOffset(2, 0);
                if (this.bitLength() > e4)
                  this.subTo(t4.ONE.shiftLeft(e4 - 1), this);
              }
            }
          else {
            var n3 = [];
            var s3 = 7 & e4;
            n3.length = (e4 >> 3) + 1;
            r3.nextBytes(n3);
            if (s3 > 0)
              n3[0] &= (1 << s3) - 1;
            else
              n3[0] = 0;
            this.fromString(n3, 256);
          }
        };
        t4.prototype.bitwiseTo = function(t5, e4, r3) {
          var i3;
          var n3;
          var s3 = Math.min(t5.t, this.t);
          for (i3 = 0; i3 < s3; ++i3)
            r3[i3] = e4(this[i3], t5[i3]);
          if (t5.t < this.t) {
            n3 = t5.s & this.DM;
            for (i3 = s3; i3 < this.t; ++i3)
              r3[i3] = e4(this[i3], n3);
            r3.t = this.t;
          } else {
            n3 = this.s & this.DM;
            for (i3 = s3; i3 < t5.t; ++i3)
              r3[i3] = e4(n3, t5[i3]);
            r3.t = t5.t;
          }
          r3.s = e4(this.s, t5.s);
          r3.clamp();
        };
        t4.prototype.changeBit = function(e4, r3) {
          var i3 = t4.ONE.shiftLeft(e4);
          this.bitwiseTo(i3, r3, i3);
          return i3;
        };
        t4.prototype.addTo = function(t5, e4) {
          var r3 = 0;
          var i3 = 0;
          var n3 = Math.min(t5.t, this.t);
          while (r3 < n3) {
            i3 += this[r3] + t5[r3];
            e4[r3++] = i3 & this.DM;
            i3 >>= this.DB;
          }
          if (t5.t < this.t) {
            i3 += t5.s;
            while (r3 < this.t) {
              i3 += this[r3];
              e4[r3++] = i3 & this.DM;
              i3 >>= this.DB;
            }
            i3 += this.s;
          } else {
            i3 += this.s;
            while (r3 < t5.t) {
              i3 += t5[r3];
              e4[r3++] = i3 & this.DM;
              i3 >>= this.DB;
            }
            i3 += t5.s;
          }
          e4.s = i3 < 0 ? -1 : 0;
          if (i3 > 0)
            e4[r3++] = i3;
          else if (i3 < -1)
            e4[r3++] = this.DV + i3;
          e4.t = r3;
          e4.clamp();
        };
        t4.prototype.dMultiply = function(t5) {
          this[this.t] = this.am(0, t5 - 1, this, 0, 0, this.t);
          ++this.t;
          this.clamp();
        };
        t4.prototype.dAddOffset = function(t5, e4) {
          if (0 == t5)
            return;
          while (this.t <= e4)
            this[this.t++] = 0;
          this[e4] += t5;
          while (this[e4] >= this.DV) {
            this[e4] -= this.DV;
            if (++e4 >= this.t)
              this[this.t++] = 0;
            ++this[e4];
          }
        };
        t4.prototype.multiplyLowerTo = function(t5, e4, r3) {
          var i3 = Math.min(this.t + t5.t, e4);
          r3.s = 0;
          r3.t = i3;
          while (i3 > 0)
            r3[--i3] = 0;
          for (var n3 = r3.t - this.t; i3 < n3; ++i3)
            r3[i3 + this.t] = this.am(0, t5[i3], r3, i3, 0, this.t);
          for (var n3 = Math.min(t5.t, e4); i3 < n3; ++i3)
            this.am(0, t5[i3], r3, i3, 0, e4 - i3);
          r3.clamp();
        };
        t4.prototype.multiplyUpperTo = function(t5, e4, r3) {
          --e4;
          var i3 = r3.t = this.t + t5.t - e4;
          r3.s = 0;
          while (--i3 >= 0)
            r3[i3] = 0;
          for (i3 = Math.max(e4 - this.t, 0); i3 < t5.t; ++i3)
            r3[this.t + i3 - e4] = this.am(e4 - i3, t5[i3], r3, 0, 0, this.t + i3 - e4);
          r3.clamp();
          r3.drShiftTo(1, r3);
        };
        t4.prototype.modInt = function(t5) {
          if (t5 <= 0)
            return 0;
          var e4 = this.DV % t5;
          var r3 = this.s < 0 ? t5 - 1 : 0;
          if (this.t > 0)
            if (0 == e4)
              r3 = this[0] % t5;
            else
              for (var i3 = this.t - 1; i3 >= 0; --i3)
                r3 = (e4 * r3 + this[i3]) % t5;
          return r3;
        };
        t4.prototype.millerRabin = function(e4) {
          var r3 = this.subtract(t4.ONE);
          var i3 = r3.getLowestSetBit();
          if (i3 <= 0)
            return false;
          var n3 = r3.shiftRight(i3);
          e4 = e4 + 1 >> 1;
          if (e4 > O.length)
            e4 = O.length;
          var s3 = H();
          for (var a2 = 0; a2 < e4; ++a2) {
            s3.fromInt(O[Math.floor(Math.random() * O.length)]);
            var o3 = s3.modPow(n3, this);
            if (0 != o3.compareTo(t4.ONE) && 0 != o3.compareTo(r3)) {
              var u2 = 1;
              while (u2++ < i3 && 0 != o3.compareTo(r3)) {
                o3 = o3.modPowInt(2, this);
                if (0 == o3.compareTo(t4.ONE))
                  return false;
              }
              if (0 != o3.compareTo(r3))
                return false;
            }
          }
          return true;
        };
        t4.prototype.square = function() {
          var t5 = H();
          this.squareTo(t5);
          return t5;
        };
        t4.prototype.gcda = function(t5, e4) {
          var r3 = this.s < 0 ? this.negate() : this.clone();
          var i3 = t5.s < 0 ? t5.negate() : t5.clone();
          if (r3.compareTo(i3) < 0) {
            var n3 = r3;
            r3 = i3;
            i3 = n3;
          }
          var s3 = r3.getLowestSetBit();
          var a2 = i3.getLowestSetBit();
          if (a2 < 0) {
            e4(r3);
            return;
          }
          if (s3 < a2)
            a2 = s3;
          if (a2 > 0) {
            r3.rShiftTo(a2, r3);
            i3.rShiftTo(a2, i3);
          }
          var o3 = function() {
            if ((s3 = r3.getLowestSetBit()) > 0)
              r3.rShiftTo(s3, r3);
            if ((s3 = i3.getLowestSetBit()) > 0)
              i3.rShiftTo(s3, i3);
            if (r3.compareTo(i3) >= 0) {
              r3.subTo(i3, r3);
              r3.rShiftTo(1, r3);
            } else {
              i3.subTo(r3, i3);
              i3.rShiftTo(1, i3);
            }
            if (!(r3.signum() > 0)) {
              if (a2 > 0)
                i3.lShiftTo(a2, i3);
              setTimeout(function() {
                e4(i3);
              }, 0);
            } else
              setTimeout(o3, 0);
          };
          setTimeout(o3, 10);
        };
        t4.prototype.fromNumberAsync = function(e4, r3, i3, n3) {
          if ("number" == typeof r3)
            if (e4 < 2)
              this.fromInt(1);
            else {
              this.fromNumber(e4, i3);
              if (!this.testBit(e4 - 1))
                this.bitwiseTo(t4.ONE.shiftLeft(e4 - 1), a, this);
              if (this.isEven())
                this.dAddOffset(1, 0);
              var s3 = this;
              var o3 = function() {
                s3.dAddOffset(2, 0);
                if (s3.bitLength() > e4)
                  s3.subTo(t4.ONE.shiftLeft(e4 - 1), s3);
                if (s3.isProbablePrime(r3))
                  setTimeout(function() {
                    n3();
                  }, 0);
                else
                  setTimeout(o3, 0);
              };
              setTimeout(o3, 0);
            }
          else {
            var u2 = [];
            var c2 = 7 & e4;
            u2.length = (e4 >> 3) + 1;
            r3.nextBytes(u2);
            if (c2 > 0)
              u2[0] &= (1 << c2) - 1;
            else
              u2[0] = 0;
            this.fromString(u2, 256);
          }
        };
        return t4;
      }();
      var N = function() {
        function t4() {
        }
        t4.prototype.convert = function(t5) {
          return t5;
        };
        t4.prototype.revert = function(t5) {
          return t5;
        };
        t4.prototype.mulTo = function(t5, e4, r3) {
          t5.multiplyTo(e4, r3);
        };
        t4.prototype.sqrTo = function(t5, e4) {
          t5.squareTo(e4);
        };
        return t4;
      }();
      var P = function() {
        function t4(t5) {
          this.m = t5;
        }
        t4.prototype.convert = function(t5) {
          if (t5.s < 0 || t5.compareTo(this.m) >= 0)
            return t5.mod(this.m);
          else
            return t5;
        };
        t4.prototype.revert = function(t5) {
          return t5;
        };
        t4.prototype.reduce = function(t5) {
          t5.divRemTo(this.m, null, t5);
        };
        t4.prototype.mulTo = function(t5, e4, r3) {
          t5.multiplyTo(e4, r3);
          this.reduce(r3);
        };
        t4.prototype.sqrTo = function(t5, e4) {
          t5.squareTo(e4);
          this.reduce(e4);
        };
        return t4;
      }();
      var V = function() {
        function t4(t5) {
          this.m = t5;
          this.mp = t5.invDigit();
          this.mpl = 32767 & this.mp;
          this.mph = this.mp >> 15;
          this.um = (1 << t5.DB - 15) - 1;
          this.mt2 = 2 * t5.t;
        }
        t4.prototype.convert = function(t5) {
          var e4 = H();
          t5.abs().dlShiftTo(this.m.t, e4);
          e4.divRemTo(this.m, null, e4);
          if (t5.s < 0 && e4.compareTo(C.ZERO) > 0)
            this.m.subTo(e4, e4);
          return e4;
        };
        t4.prototype.revert = function(t5) {
          var e4 = H();
          t5.copyTo(e4);
          this.reduce(e4);
          return e4;
        };
        t4.prototype.reduce = function(t5) {
          while (t5.t <= this.mt2)
            t5[t5.t++] = 0;
          for (var e4 = 0; e4 < this.m.t; ++e4) {
            var r3 = 32767 & t5[e4];
            var i3 = r3 * this.mpl + ((r3 * this.mph + (t5[e4] >> 15) * this.mpl & this.um) << 15) & t5.DM;
            r3 = e4 + this.m.t;
            t5[r3] += this.m.am(0, i3, t5, e4, 0, this.m.t);
            while (t5[r3] >= t5.DV) {
              t5[r3] -= t5.DV;
              t5[++r3]++;
            }
          }
          t5.clamp();
          t5.drShiftTo(this.m.t, t5);
          if (t5.compareTo(this.m) >= 0)
            t5.subTo(this.m, t5);
        };
        t4.prototype.mulTo = function(t5, e4, r3) {
          t5.multiplyTo(e4, r3);
          this.reduce(r3);
        };
        t4.prototype.sqrTo = function(t5, e4) {
          t5.squareTo(e4);
          this.reduce(e4);
        };
        return t4;
      }();
      var L = function() {
        function t4(t5) {
          this.m = t5;
          this.r2 = H();
          this.q3 = H();
          C.ONE.dlShiftTo(2 * t5.t, this.r2);
          this.mu = this.r2.divide(t5);
        }
        t4.prototype.convert = function(t5) {
          if (t5.s < 0 || t5.t > 2 * this.m.t)
            return t5.mod(this.m);
          else if (t5.compareTo(this.m) < 0)
            return t5;
          else {
            var e4 = H();
            t5.copyTo(e4);
            this.reduce(e4);
            return e4;
          }
        };
        t4.prototype.revert = function(t5) {
          return t5;
        };
        t4.prototype.reduce = function(t5) {
          t5.drShiftTo(this.m.t - 1, this.r2);
          if (t5.t > this.m.t + 1) {
            t5.t = this.m.t + 1;
            t5.clamp();
          }
          this.mu.multiplyUpperTo(this.r2, this.m.t + 1, this.q3);
          this.m.multiplyLowerTo(this.q3, this.m.t + 1, this.r2);
          while (t5.compareTo(this.r2) < 0)
            t5.dAddOffset(1, this.m.t + 1);
          t5.subTo(this.r2, t5);
          while (t5.compareTo(this.m) >= 0)
            t5.subTo(this.m, t5);
        };
        t4.prototype.mulTo = function(t5, e4, r3) {
          t5.multiplyTo(e4, r3);
          this.reduce(r3);
        };
        t4.prototype.sqrTo = function(t5, e4) {
          t5.squareTo(e4);
          this.reduce(e4);
        };
        return t4;
      }();
      function H() {
        return new C(null);
      }
      function U(t4, e4) {
        return new C(t4, e4);
      }
      var K = "undefined" !== typeof navigator;
      if (K && B && "Microsoft Internet Explorer" == navigator.appName) {
        C.prototype.am = function t4(e4, r3, i3, n3, s3, a2) {
          var o3 = 32767 & r3;
          var u2 = r3 >> 15;
          while (--a2 >= 0) {
            var c2 = 32767 & this[e4];
            var l2 = this[e4++] >> 15;
            var f3 = u2 * c2 + l2 * o3;
            c2 = o3 * c2 + ((32767 & f3) << 15) + i3[n3] + (1073741823 & s3);
            s3 = (c2 >>> 30) + (f3 >>> 15) + u2 * l2 + (s3 >>> 30);
            i3[n3++] = 1073741823 & c2;
          }
          return s3;
        };
        x = 30;
      } else if (K && B && "Netscape" != navigator.appName) {
        C.prototype.am = function t4(e4, r3, i3, n3, s3, a2) {
          while (--a2 >= 0) {
            var o3 = r3 * this[e4++] + i3[n3] + s3;
            s3 = Math.floor(o3 / 67108864);
            i3[n3++] = 67108863 & o3;
          }
          return s3;
        };
        x = 26;
      } else {
        C.prototype.am = function t4(e4, r3, i3, n3, s3, a2) {
          var o3 = 16383 & r3;
          var u2 = r3 >> 14;
          while (--a2 >= 0) {
            var c2 = 16383 & this[e4];
            var l2 = this[e4++] >> 14;
            var f3 = u2 * c2 + l2 * o3;
            c2 = o3 * c2 + ((16383 & f3) << 14) + i3[n3] + s3;
            s3 = (c2 >> 28) + (f3 >> 14) + u2 * l2;
            i3[n3++] = 268435455 & c2;
          }
          return s3;
        };
        x = 28;
      }
      C.prototype.DB = x;
      C.prototype.DM = (1 << x) - 1;
      C.prototype.DV = 1 << x;
      var j = 52;
      C.prototype.FV = Math.pow(2, j);
      C.prototype.F1 = j - x;
      C.prototype.F2 = 2 * x - j;
      var q = [];
      var F;
      var z;
      F = "0".charCodeAt(0);
      for (z = 0; z <= 9; ++z)
        q[F++] = z;
      F = "a".charCodeAt(0);
      for (z = 10; z < 36; ++z)
        q[F++] = z;
      F = "A".charCodeAt(0);
      for (z = 10; z < 36; ++z)
        q[F++] = z;
      function G(t4, e4) {
        var r3 = q[t4.charCodeAt(e4)];
        return null == r3 ? -1 : r3;
      }
      function Y(t4) {
        var e4 = H();
        e4.fromInt(t4);
        return e4;
      }
      function W(t4) {
        var e4 = 1;
        var r3;
        if (0 != (r3 = t4 >>> 16)) {
          t4 = r3;
          e4 += 16;
        }
        if (0 != (r3 = t4 >> 8)) {
          t4 = r3;
          e4 += 8;
        }
        if (0 != (r3 = t4 >> 4)) {
          t4 = r3;
          e4 += 4;
        }
        if (0 != (r3 = t4 >> 2)) {
          t4 = r3;
          e4 += 2;
        }
        if (0 != (r3 = t4 >> 1)) {
          t4 = r3;
          e4 += 1;
        }
        return e4;
      }
      C.ZERO = Y(0);
      C.ONE = Y(1);
      var J = function() {
        function t4() {
          this.i = 0;
          this.j = 0;
          this.S = [];
        }
        t4.prototype.init = function(t5) {
          var e4;
          var r3;
          var i3;
          for (e4 = 0; e4 < 256; ++e4)
            this.S[e4] = e4;
          r3 = 0;
          for (e4 = 0; e4 < 256; ++e4) {
            r3 = r3 + this.S[e4] + t5[e4 % t5.length] & 255;
            i3 = this.S[e4];
            this.S[e4] = this.S[r3];
            this.S[r3] = i3;
          }
          this.i = 0;
          this.j = 0;
        };
        t4.prototype.next = function() {
          var t5;
          this.i = this.i + 1 & 255;
          this.j = this.j + this.S[this.i] & 255;
          t5 = this.S[this.i];
          this.S[this.i] = this.S[this.j];
          this.S[this.j] = t5;
          return this.S[t5 + this.S[this.i] & 255];
        };
        return t4;
      }();
      function Z() {
        return new J();
      }
      var $ = 256;
      var X;
      var Q = null;
      var tt2;
      if (null == Q) {
        Q = [];
        tt2 = 0;
      }
      function nt() {
        if (null == X) {
          X = Z();
          while (tt2 < $) {
            var t4 = Math.floor(65536 * Math.random());
            Q[tt2++] = 255 & t4;
          }
          X.init(Q);
          for (tt2 = 0; tt2 < Q.length; ++tt2)
            Q[tt2] = 0;
          tt2 = 0;
        }
        return X.next();
      }
      var st = function() {
        function t4() {
        }
        t4.prototype.nextBytes = function(t5) {
          for (var e4 = 0; e4 < t5.length; ++e4)
            t5[e4] = nt();
        };
        return t4;
      }();
      function at(t4, e4) {
        if (e4 < t4.length + 22) {
          console.error("Message too long for RSA");
          return null;
        }
        var r3 = e4 - t4.length - 6;
        var i3 = "";
        for (var n3 = 0; n3 < r3; n3 += 2)
          i3 += "ff";
        var s3 = "0001" + i3 + "00" + t4;
        return U(s3, 16);
      }
      function ot(t4, e4) {
        if (e4 < t4.length + 11) {
          console.error("Message too long for RSA");
          return null;
        }
        var r3 = [];
        var i3 = t4.length - 1;
        while (i3 >= 0 && e4 > 0) {
          var n3 = t4.charCodeAt(i3--);
          if (n3 < 128)
            r3[--e4] = n3;
          else if (n3 > 127 && n3 < 2048) {
            r3[--e4] = 63 & n3 | 128;
            r3[--e4] = n3 >> 6 | 192;
          } else {
            r3[--e4] = 63 & n3 | 128;
            r3[--e4] = n3 >> 6 & 63 | 128;
            r3[--e4] = n3 >> 12 | 224;
          }
        }
        r3[--e4] = 0;
        var s3 = new st();
        var a2 = [];
        while (e4 > 2) {
          a2[0] = 0;
          while (0 == a2[0])
            s3.nextBytes(a2);
          r3[--e4] = a2[0];
        }
        r3[--e4] = 2;
        r3[--e4] = 0;
        return new C(r3);
      }
      var ut = function() {
        function t4() {
          this.n = null;
          this.e = 0;
          this.d = null;
          this.p = null;
          this.q = null;
          this.dmp1 = null;
          this.dmq1 = null;
          this.coeff = null;
        }
        t4.prototype.doPublic = function(t5) {
          return t5.modPowInt(this.e, this.n);
        };
        t4.prototype.doPrivate = function(t5) {
          if (null == this.p || null == this.q)
            return t5.modPow(this.d, this.n);
          var e4 = t5.mod(this.p).modPow(this.dmp1, this.p);
          var r3 = t5.mod(this.q).modPow(this.dmq1, this.q);
          while (e4.compareTo(r3) < 0)
            e4 = e4.add(this.p);
          return e4.subtract(r3).multiply(this.coeff).mod(this.p).multiply(this.q).add(r3);
        };
        t4.prototype.setPublic = function(t5, e4) {
          if (null != t5 && null != e4 && t5.length > 0 && e4.length > 0) {
            this.n = U(t5, 16);
            this.e = parseInt(e4, 16);
          } else
            console.error("Invalid RSA public key");
        };
        t4.prototype.encrypt = function(t5) {
          var e4 = this.n.bitLength() + 7 >> 3;
          var r3 = ot(t5, e4);
          if (null == r3)
            return null;
          var i3 = this.doPublic(r3);
          if (null == i3)
            return null;
          var n3 = i3.toString(16);
          var s3 = n3.length;
          for (var a2 = 0; a2 < 2 * e4 - s3; a2++)
            n3 = "0" + n3;
          return n3;
        };
        t4.prototype.setPrivate = function(t5, e4, r3) {
          if (null != t5 && null != e4 && t5.length > 0 && e4.length > 0) {
            this.n = U(t5, 16);
            this.e = parseInt(e4, 16);
            this.d = U(r3, 16);
          } else
            console.error("Invalid RSA private key");
        };
        t4.prototype.setPrivateEx = function(t5, e4, r3, i3, n3, s3, a2, o3) {
          if (null != t5 && null != e4 && t5.length > 0 && e4.length > 0) {
            this.n = U(t5, 16);
            this.e = parseInt(e4, 16);
            this.d = U(r3, 16);
            this.p = U(i3, 16);
            this.q = U(n3, 16);
            this.dmp1 = U(s3, 16);
            this.dmq1 = U(a2, 16);
            this.coeff = U(o3, 16);
          } else
            console.error("Invalid RSA private key");
        };
        t4.prototype.generate = function(t5, e4) {
          var r3 = new st();
          var i3 = t5 >> 1;
          this.e = parseInt(e4, 16);
          var n3 = new C(e4, 16);
          for (; ; ) {
            for (; ; ) {
              this.p = new C(t5 - i3, 1, r3);
              if (0 == this.p.subtract(C.ONE).gcd(n3).compareTo(C.ONE) && this.p.isProbablePrime(10))
                break;
            }
            for (; ; ) {
              this.q = new C(i3, 1, r3);
              if (0 == this.q.subtract(C.ONE).gcd(n3).compareTo(C.ONE) && this.q.isProbablePrime(10))
                break;
            }
            if (this.p.compareTo(this.q) <= 0) {
              var s3 = this.p;
              this.p = this.q;
              this.q = s3;
            }
            var a2 = this.p.subtract(C.ONE);
            var o3 = this.q.subtract(C.ONE);
            var u2 = a2.multiply(o3);
            if (0 == u2.gcd(n3).compareTo(C.ONE)) {
              this.n = this.p.multiply(this.q);
              this.d = n3.modInverse(u2);
              this.dmp1 = this.d.mod(a2);
              this.dmq1 = this.d.mod(o3);
              this.coeff = this.q.modInverse(this.p);
              break;
            }
          }
        };
        t4.prototype.decrypt = function(t5) {
          var e4 = U(t5, 16);
          var r3 = this.doPrivate(e4);
          if (null == r3)
            return null;
          return ct(r3, this.n.bitLength() + 7 >> 3);
        };
        t4.prototype.generateAsync = function(t5, e4, r3) {
          var i3 = new st();
          var n3 = t5 >> 1;
          this.e = parseInt(e4, 16);
          var s3 = new C(e4, 16);
          var a2 = this;
          var o3 = function() {
            var e5 = function() {
              if (a2.p.compareTo(a2.q) <= 0) {
                var t6 = a2.p;
                a2.p = a2.q;
                a2.q = t6;
              }
              var e6 = a2.p.subtract(C.ONE);
              var i4 = a2.q.subtract(C.ONE);
              var n4 = e6.multiply(i4);
              if (0 == n4.gcd(s3).compareTo(C.ONE)) {
                a2.n = a2.p.multiply(a2.q);
                a2.d = s3.modInverse(n4);
                a2.dmp1 = a2.d.mod(e6);
                a2.dmq1 = a2.d.mod(i4);
                a2.coeff = a2.q.modInverse(a2.p);
                setTimeout(function() {
                  r3();
                }, 0);
              } else
                setTimeout(o3, 0);
            };
            var u2 = function() {
              a2.q = H();
              a2.q.fromNumberAsync(n3, 1, i3, function() {
                a2.q.subtract(C.ONE).gcda(s3, function(t6) {
                  if (0 == t6.compareTo(C.ONE) && a2.q.isProbablePrime(10))
                    setTimeout(e5, 0);
                  else
                    setTimeout(u2, 0);
                });
              });
            };
            var c2 = function() {
              a2.p = H();
              a2.p.fromNumberAsync(t5 - n3, 1, i3, function() {
                a2.p.subtract(C.ONE).gcda(s3, function(t6) {
                  if (0 == t6.compareTo(C.ONE) && a2.p.isProbablePrime(10))
                    setTimeout(u2, 0);
                  else
                    setTimeout(c2, 0);
                });
              });
            };
            setTimeout(c2, 0);
          };
          setTimeout(o3, 0);
        };
        t4.prototype.sign = function(t5, e4, r3) {
          var i3 = ht(r3);
          var n3 = i3 + e4(t5).toString();
          var s3 = at(n3, this.n.bitLength() / 4);
          if (null == s3)
            return null;
          var a2 = this.doPrivate(s3);
          if (null == a2)
            return null;
          var o3 = a2.toString(16);
          if (0 == (1 & o3.length))
            return o3;
          else
            return "0" + o3;
        };
        t4.prototype.verify = function(t5, e4, r3) {
          var i3 = U(e4, 16);
          var n3 = this.doPublic(i3);
          if (null == n3)
            return null;
          var s3 = n3.toString(16).replace(/^1f+00/, "");
          var a2 = dt(s3);
          return a2 == r3(t5).toString();
        };
        t4.prototype.encryptLong = function(t5) {
          var e4 = this;
          var r3 = "";
          var i3 = (this.n.bitLength() + 7 >> 3) - 11;
          var n3 = this.setSplitChn(t5, i3);
          n3.forEach(function(t6) {
            r3 += e4.encrypt(t6);
          });
          return r3;
        };
        t4.prototype.decryptLong = function(t5) {
          var e4 = "";
          var r3 = this.n.bitLength() + 7 >> 3;
          var i3 = 2 * r3;
          if (t5.length > i3) {
            var n3 = t5.match(new RegExp(".{1," + i3 + "}", "g")) || [];
            var s3 = [];
            for (var a2 = 0; a2 < n3.length; a2++) {
              var o3 = U(n3[a2], 16);
              var u2 = this.doPrivate(o3);
              if (null == u2)
                return null;
              s3.push(u2);
            }
            e4 = lt(s3, r3);
          } else
            e4 = this.decrypt(t5);
          return e4;
        };
        t4.prototype.setSplitChn = function(t5, e4, r3) {
          if (void 0 === r3)
            r3 = [];
          var i3 = t5.split("");
          var n3 = 0;
          for (var s3 = 0; s3 < i3.length; s3++) {
            var a2 = i3[s3].charCodeAt(0);
            if (a2 <= 127)
              n3 += 1;
            else if (a2 <= 2047)
              n3 += 2;
            else if (a2 <= 65535)
              n3 += 3;
            else
              n3 += 4;
            if (n3 > e4) {
              var o3 = t5.substring(0, s3);
              r3.push(o3);
              return this.setSplitChn(t5.substring(s3), e4, r3);
            }
          }
          r3.push(t5);
          return r3;
        };
        return t4;
      }();
      function ct(t4, e4) {
        var r3 = t4.toByteArray();
        var i3 = 0;
        while (i3 < r3.length && 0 == r3[i3])
          ++i3;
        if (r3.length - i3 != e4 - 1 || 2 != r3[i3])
          return null;
        ++i3;
        while (0 != r3[i3])
          if (++i3 >= r3.length)
            return null;
        var n3 = "";
        while (++i3 < r3.length) {
          var s3 = 255 & r3[i3];
          if (s3 < 128)
            n3 += String.fromCharCode(s3);
          else if (s3 > 191 && s3 < 224) {
            n3 += String.fromCharCode((31 & s3) << 6 | 63 & r3[i3 + 1]);
            ++i3;
          } else {
            n3 += String.fromCharCode((15 & s3) << 12 | (63 & r3[i3 + 1]) << 6 | 63 & r3[i3 + 2]);
            i3 += 2;
          }
        }
        return n3;
      }
      function lt(t4, e4) {
        var r3 = [];
        for (var i3 = 0; i3 < t4.length; i3++) {
          var n3 = t4[i3];
          var s3 = n3.toByteArray();
          var a2 = 0;
          while (a2 < s3.length && 0 == s3[a2])
            ++a2;
          if (s3.length - a2 != e4 - 1 || 2 != s3[a2])
            return null;
          ++a2;
          while (0 != s3[a2])
            if (++a2 >= s3.length)
              return null;
          r3 = r3.concat(s3.slice(a2 + 1));
        }
        var o3 = r3;
        var u2 = -1;
        var c2 = "";
        while (++u2 < o3.length) {
          var l2 = 255 & o3[u2];
          if (l2 < 128)
            c2 += String.fromCharCode(l2);
          else if (l2 > 191 && l2 < 224) {
            c2 += String.fromCharCode((31 & l2) << 6 | 63 & o3[u2 + 1]);
            ++u2;
          } else {
            c2 += String.fromCharCode((15 & l2) << 12 | (63 & o3[u2 + 1]) << 6 | 63 & o3[u2 + 2]);
            u2 += 2;
          }
        }
        return c2;
      }
      var ft = { md2: "3020300c06082a864886f70d020205000410", md5: "3020300c06082a864886f70d020505000410", sha1: "3021300906052b0e03021a05000414", sha224: "302d300d06096086480165030402040500041c", sha256: "3031300d060960864801650304020105000420", sha384: "3041300d060960864801650304020205000430", sha512: "3051300d060960864801650304020305000440", ripemd160: "3021300906052b2403020105000414" };
      function ht(t4) {
        return ft[t4] || "";
      }
      function dt(t4) {
        for (var e4 in ft)
          if (ft.hasOwnProperty(e4)) {
            var r3 = ft[e4];
            var i3 = r3.length;
            if (t4.substr(0, i3) == r3)
              return t4.substr(i3);
          }
        return t4;
      }
      var vt = {};
      vt.lang = { extend: function(t4, e4, r3) {
        if (!e4 || !t4)
          throw new Error("YAHOO.lang.extend failed, please check that all dependencies are included.");
        var i3 = function() {
        };
        i3.prototype = e4.prototype;
        t4.prototype = new i3();
        t4.prototype.constructor = t4;
        t4.superclass = e4.prototype;
        if (e4.prototype.constructor == Object.prototype.constructor)
          e4.prototype.constructor = e4;
        if (r3) {
          var n3;
          for (n3 in r3)
            t4.prototype[n3] = r3[n3];
          var s3 = function() {
          }, a2 = ["toString", "valueOf"];
          try {
            if (/MSIE/.test(navigator.userAgent))
              s3 = function(t5, e5) {
                for (n3 = 0; n3 < a2.length; n3 += 1) {
                  var r4 = a2[n3], i4 = e5[r4];
                  if ("function" === typeof i4 && i4 != Object.prototype[r4])
                    t5[r4] = i4;
                }
              };
          } catch (t5) {
          }
          s3(t4.prototype, r3);
        }
      } };
      var pt = {};
      if ("undefined" == typeof pt.asn1 || !pt.asn1)
        pt.asn1 = {};
      pt.asn1.ASN1Util = new function() {
        this.integerToByteHex = function(t4) {
          var e4 = t4.toString(16);
          if (e4.length % 2 == 1)
            e4 = "0" + e4;
          return e4;
        };
        this.bigIntToMinTwosComplementsHex = function(t4) {
          var e4 = t4.toString(16);
          if ("-" != e4.substr(0, 1)) {
            if (e4.length % 2 == 1)
              e4 = "0" + e4;
            else if (!e4.match(/^[0-7]/))
              e4 = "00" + e4;
          } else {
            var r3 = e4.substr(1);
            var i3 = r3.length;
            if (i3 % 2 == 1)
              i3 += 1;
            else if (!e4.match(/^[0-7]/))
              i3 += 2;
            var n3 = "";
            for (var s3 = 0; s3 < i3; s3++)
              n3 += "f";
            var a2 = new C(n3, 16);
            var o3 = a2.xor(t4).add(C.ONE);
            e4 = o3.toString(16).replace(/^-/, "");
          }
          return e4;
        };
        this.getPEMStringFromHex = function(t4, e4) {
          return hextopem(t4, e4);
        };
        this.newObject = function(t4) {
          var e4 = pt, r3 = e4.asn1, i3 = r3.DERBoolean, n3 = r3.DERInteger, s3 = r3.DERBitString, a2 = r3.DEROctetString, o3 = r3.DERNull, u2 = r3.DERObjectIdentifier, c2 = r3.DEREnumerated, l2 = r3.DERUTF8String, f3 = r3.DERNumericString, h2 = r3.DERPrintableString, d3 = r3.DERTeletexString, v2 = r3.DERIA5String, p2 = r3.DERUTCTime, g2 = r3.DERGeneralizedTime, y2 = r3.DERSequence, m2 = r3.DERSet, w2 = r3.DERTaggedObject, S2 = r3.ASN1Util.newObject;
          var _2 = Object.keys(t4);
          if (1 != _2.length)
            throw "key of param shall be only one.";
          var b2 = _2[0];
          if (-1 == ":bool:int:bitstr:octstr:null:oid:enum:utf8str:numstr:prnstr:telstr:ia5str:utctime:gentime:seq:set:tag:".indexOf(":" + b2 + ":"))
            throw "undefined key: " + b2;
          if ("bool" == b2)
            return new i3(t4[b2]);
          if ("int" == b2)
            return new n3(t4[b2]);
          if ("bitstr" == b2)
            return new s3(t4[b2]);
          if ("octstr" == b2)
            return new a2(t4[b2]);
          if ("null" == b2)
            return new o3(t4[b2]);
          if ("oid" == b2)
            return new u2(t4[b2]);
          if ("enum" == b2)
            return new c2(t4[b2]);
          if ("utf8str" == b2)
            return new l2(t4[b2]);
          if ("numstr" == b2)
            return new f3(t4[b2]);
          if ("prnstr" == b2)
            return new h2(t4[b2]);
          if ("telstr" == b2)
            return new d3(t4[b2]);
          if ("ia5str" == b2)
            return new v2(t4[b2]);
          if ("utctime" == b2)
            return new p2(t4[b2]);
          if ("gentime" == b2)
            return new g2(t4[b2]);
          if ("seq" == b2) {
            var E3 = t4[b2];
            var D2 = [];
            for (var M2 = 0; M2 < E3.length; M2++) {
              var T2 = S2(E3[M2]);
              D2.push(T2);
            }
            return new y2({ array: D2 });
          }
          if ("set" == b2) {
            var E3 = t4[b2];
            var D2 = [];
            for (var M2 = 0; M2 < E3.length; M2++) {
              var T2 = S2(E3[M2]);
              D2.push(T2);
            }
            return new m2({ array: D2 });
          }
          if ("tag" == b2) {
            var I2 = t4[b2];
            if ("[object Array]" === Object.prototype.toString.call(I2) && 3 == I2.length) {
              var A2 = S2(I2[2]);
              return new w2({ tag: I2[0], explicit: I2[1], obj: A2 });
            } else {
              var x2 = {};
              if (void 0 !== I2.explicit)
                x2.explicit = I2.explicit;
              if (void 0 !== I2.tag)
                x2.tag = I2.tag;
              if (void 0 === I2.obj)
                throw "obj shall be specified for 'tag'.";
              x2.obj = S2(I2.obj);
              return new w2(x2);
            }
          }
        };
        this.jsonToASN1HEX = function(t4) {
          var e4 = this.newObject(t4);
          return e4.getEncodedHex();
        };
      }();
      pt.asn1.ASN1Util.oidHexToInt = function(t4) {
        var e4 = "";
        var r3 = parseInt(t4.substr(0, 2), 16);
        var i3 = Math.floor(r3 / 40);
        var n3 = r3 % 40;
        var e4 = i3 + "." + n3;
        var s3 = "";
        for (var a2 = 2; a2 < t4.length; a2 += 2) {
          var o3 = parseInt(t4.substr(a2, 2), 16);
          var u2 = ("00000000" + o3.toString(2)).slice(-8);
          s3 += u2.substr(1, 7);
          if ("0" == u2.substr(0, 1)) {
            var c2 = new C(s3, 2);
            e4 = e4 + "." + c2.toString(10);
            s3 = "";
          }
        }
        return e4;
      };
      pt.asn1.ASN1Util.oidIntToHex = function(t4) {
        var e4 = function(t5) {
          var e5 = t5.toString(16);
          if (1 == e5.length)
            e5 = "0" + e5;
          return e5;
        };
        var r3 = function(t5) {
          var r4 = "";
          var i4 = new C(t5, 10);
          var n4 = i4.toString(2);
          var s4 = 7 - n4.length % 7;
          if (7 == s4)
            s4 = 0;
          var a3 = "";
          for (var o3 = 0; o3 < s4; o3++)
            a3 += "0";
          n4 = a3 + n4;
          for (var o3 = 0; o3 < n4.length - 1; o3 += 7) {
            var u2 = n4.substr(o3, 7);
            if (o3 != n4.length - 7)
              u2 = "1" + u2;
            r4 += e4(parseInt(u2, 2));
          }
          return r4;
        };
        if (!t4.match(/^[0-9.]+$/))
          throw "malformed oid string: " + t4;
        var i3 = "";
        var n3 = t4.split(".");
        var s3 = 40 * parseInt(n3[0]) + parseInt(n3[1]);
        i3 += e4(s3);
        n3.splice(0, 2);
        for (var a2 = 0; a2 < n3.length; a2++)
          i3 += r3(n3[a2]);
        return i3;
      };
      pt.asn1.ASN1Object = function() {
        var n3 = "";
        this.getLengthHexFromValue = function() {
          if ("undefined" == typeof this.hV || null == this.hV)
            throw "this.hV is null or undefined.";
          if (this.hV.length % 2 == 1)
            throw "value hex must be even length: n=" + n3.length + ",v=" + this.hV;
          var t4 = this.hV.length / 2;
          var e4 = t4.toString(16);
          if (e4.length % 2 == 1)
            e4 = "0" + e4;
          if (t4 < 128)
            return e4;
          else {
            var r3 = e4.length / 2;
            if (r3 > 15)
              throw "ASN.1 length too long to represent by 8x: n = " + t4.toString(16);
            var i3 = 128 + r3;
            return i3.toString(16) + e4;
          }
        };
        this.getEncodedHex = function() {
          if (null == this.hTLV || this.isModified) {
            this.hV = this.getFreshValueHex();
            this.hL = this.getLengthHexFromValue();
            this.hTLV = this.hT + this.hL + this.hV;
            this.isModified = false;
          }
          return this.hTLV;
        };
        this.getValueHex = function() {
          this.getEncodedHex();
          return this.hV;
        };
        this.getFreshValueHex = function() {
          return "";
        };
      };
      pt.asn1.DERAbstractString = function(t4) {
        pt.asn1.DERAbstractString.superclass.constructor.call(this);
        this.getString = function() {
          return this.s;
        };
        this.setString = function(t5) {
          this.hTLV = null;
          this.isModified = true;
          this.s = t5;
          this.hV = stohex(this.s);
        };
        this.setStringHex = function(t5) {
          this.hTLV = null;
          this.isModified = true;
          this.s = null;
          this.hV = t5;
        };
        this.getFreshValueHex = function() {
          return this.hV;
        };
        if ("undefined" != typeof t4) {
          if ("string" == typeof t4)
            this.setString(t4);
          else if ("undefined" != typeof t4["str"])
            this.setString(t4["str"]);
          else if ("undefined" != typeof t4["hex"])
            this.setStringHex(t4["hex"]);
        }
      };
      vt.lang.extend(pt.asn1.DERAbstractString, pt.asn1.ASN1Object);
      pt.asn1.DERAbstractTime = function(t4) {
        pt.asn1.DERAbstractTime.superclass.constructor.call(this);
        this.localDateToUTC = function(t5) {
          utc = t5.getTime() + 6e4 * t5.getTimezoneOffset();
          var e4 = new Date(utc);
          return e4;
        };
        this.formatDate = function(t5, e4, r3) {
          var i3 = this.zeroPadding;
          var n3 = this.localDateToUTC(t5);
          var s3 = String(n3.getFullYear());
          if ("utc" == e4)
            s3 = s3.substr(2, 2);
          var a2 = i3(String(n3.getMonth() + 1), 2);
          var o3 = i3(String(n3.getDate()), 2);
          var u2 = i3(String(n3.getHours()), 2);
          var c2 = i3(String(n3.getMinutes()), 2);
          var l2 = i3(String(n3.getSeconds()), 2);
          var f3 = s3 + a2 + o3 + u2 + c2 + l2;
          if (true === r3) {
            var h2 = n3.getMilliseconds();
            if (0 != h2) {
              var d3 = i3(String(h2), 3);
              d3 = d3.replace(/[0]+$/, "");
              f3 = f3 + "." + d3;
            }
          }
          return f3 + "Z";
        };
        this.zeroPadding = function(t5, e4) {
          if (t5.length >= e4)
            return t5;
          return new Array(e4 - t5.length + 1).join("0") + t5;
        };
        this.getString = function() {
          return this.s;
        };
        this.setString = function(t5) {
          this.hTLV = null;
          this.isModified = true;
          this.s = t5;
          this.hV = stohex(t5);
        };
        this.setByDateValue = function(t5, e4, r3, i3, n3, s3) {
          var a2 = new Date(Date.UTC(t5, e4 - 1, r3, i3, n3, s3, 0));
          this.setByDate(a2);
        };
        this.getFreshValueHex = function() {
          return this.hV;
        };
      };
      vt.lang.extend(pt.asn1.DERAbstractTime, pt.asn1.ASN1Object);
      pt.asn1.DERAbstractStructured = function(t4) {
        pt.asn1.DERAbstractString.superclass.constructor.call(this);
        this.setByASN1ObjectArray = function(t5) {
          this.hTLV = null;
          this.isModified = true;
          this.asn1Array = t5;
        };
        this.appendASN1Object = function(t5) {
          this.hTLV = null;
          this.isModified = true;
          this.asn1Array.push(t5);
        };
        this.asn1Array = new Array();
        if ("undefined" != typeof t4) {
          if ("undefined" != typeof t4["array"])
            this.asn1Array = t4["array"];
        }
      };
      vt.lang.extend(pt.asn1.DERAbstractStructured, pt.asn1.ASN1Object);
      pt.asn1.DERBoolean = function() {
        pt.asn1.DERBoolean.superclass.constructor.call(this);
        this.hT = "01";
        this.hTLV = "0101ff";
      };
      vt.lang.extend(pt.asn1.DERBoolean, pt.asn1.ASN1Object);
      pt.asn1.DERInteger = function(t4) {
        pt.asn1.DERInteger.superclass.constructor.call(this);
        this.hT = "02";
        this.setByBigInteger = function(t5) {
          this.hTLV = null;
          this.isModified = true;
          this.hV = pt.asn1.ASN1Util.bigIntToMinTwosComplementsHex(t5);
        };
        this.setByInteger = function(t5) {
          var e4 = new C(String(t5), 10);
          this.setByBigInteger(e4);
        };
        this.setValueHex = function(t5) {
          this.hV = t5;
        };
        this.getFreshValueHex = function() {
          return this.hV;
        };
        if ("undefined" != typeof t4) {
          if ("undefined" != typeof t4["bigint"])
            this.setByBigInteger(t4["bigint"]);
          else if ("undefined" != typeof t4["int"])
            this.setByInteger(t4["int"]);
          else if ("number" == typeof t4)
            this.setByInteger(t4);
          else if ("undefined" != typeof t4["hex"])
            this.setValueHex(t4["hex"]);
        }
      };
      vt.lang.extend(pt.asn1.DERInteger, pt.asn1.ASN1Object);
      pt.asn1.DERBitString = function(t4) {
        if (void 0 !== t4 && "undefined" !== typeof t4.obj) {
          var e4 = pt.asn1.ASN1Util.newObject(t4.obj);
          t4.hex = "00" + e4.getEncodedHex();
        }
        pt.asn1.DERBitString.superclass.constructor.call(this);
        this.hT = "03";
        this.setHexValueIncludingUnusedBits = function(t5) {
          this.hTLV = null;
          this.isModified = true;
          this.hV = t5;
        };
        this.setUnusedBitsAndHexValue = function(t5, e5) {
          if (t5 < 0 || 7 < t5)
            throw "unused bits shall be from 0 to 7: u = " + t5;
          var r3 = "0" + t5;
          this.hTLV = null;
          this.isModified = true;
          this.hV = r3 + e5;
        };
        this.setByBinaryString = function(t5) {
          t5 = t5.replace(/0+$/, "");
          var e5 = 8 - t5.length % 8;
          if (8 == e5)
            e5 = 0;
          for (var r3 = 0; r3 <= e5; r3++)
            t5 += "0";
          var i3 = "";
          for (var r3 = 0; r3 < t5.length - 1; r3 += 8) {
            var n3 = t5.substr(r3, 8);
            var s3 = parseInt(n3, 2).toString(16);
            if (1 == s3.length)
              s3 = "0" + s3;
            i3 += s3;
          }
          this.hTLV = null;
          this.isModified = true;
          this.hV = "0" + e5 + i3;
        };
        this.setByBooleanArray = function(t5) {
          var e5 = "";
          for (var r3 = 0; r3 < t5.length; r3++)
            if (true == t5[r3])
              e5 += "1";
            else
              e5 += "0";
          this.setByBinaryString(e5);
        };
        this.newFalseArray = function(t5) {
          var e5 = new Array(t5);
          for (var r3 = 0; r3 < t5; r3++)
            e5[r3] = false;
          return e5;
        };
        this.getFreshValueHex = function() {
          return this.hV;
        };
        if ("undefined" != typeof t4) {
          if ("string" == typeof t4 && t4.toLowerCase().match(/^[0-9a-f]+$/))
            this.setHexValueIncludingUnusedBits(t4);
          else if ("undefined" != typeof t4["hex"])
            this.setHexValueIncludingUnusedBits(t4["hex"]);
          else if ("undefined" != typeof t4["bin"])
            this.setByBinaryString(t4["bin"]);
          else if ("undefined" != typeof t4["array"])
            this.setByBooleanArray(t4["array"]);
        }
      };
      vt.lang.extend(pt.asn1.DERBitString, pt.asn1.ASN1Object);
      pt.asn1.DEROctetString = function(t4) {
        if (void 0 !== t4 && "undefined" !== typeof t4.obj) {
          var e4 = pt.asn1.ASN1Util.newObject(t4.obj);
          t4.hex = e4.getEncodedHex();
        }
        pt.asn1.DEROctetString.superclass.constructor.call(this, t4);
        this.hT = "04";
      };
      vt.lang.extend(pt.asn1.DEROctetString, pt.asn1.DERAbstractString);
      pt.asn1.DERNull = function() {
        pt.asn1.DERNull.superclass.constructor.call(this);
        this.hT = "05";
        this.hTLV = "0500";
      };
      vt.lang.extend(pt.asn1.DERNull, pt.asn1.ASN1Object);
      pt.asn1.DERObjectIdentifier = function(t4) {
        var e4 = function(t5) {
          var e5 = t5.toString(16);
          if (1 == e5.length)
            e5 = "0" + e5;
          return e5;
        };
        var r3 = function(t5) {
          var r4 = "";
          var i3 = new C(t5, 10);
          var n3 = i3.toString(2);
          var s3 = 7 - n3.length % 7;
          if (7 == s3)
            s3 = 0;
          var a2 = "";
          for (var o3 = 0; o3 < s3; o3++)
            a2 += "0";
          n3 = a2 + n3;
          for (var o3 = 0; o3 < n3.length - 1; o3 += 7) {
            var u2 = n3.substr(o3, 7);
            if (o3 != n3.length - 7)
              u2 = "1" + u2;
            r4 += e4(parseInt(u2, 2));
          }
          return r4;
        };
        pt.asn1.DERObjectIdentifier.superclass.constructor.call(this);
        this.hT = "06";
        this.setValueHex = function(t5) {
          this.hTLV = null;
          this.isModified = true;
          this.s = null;
          this.hV = t5;
        };
        this.setValueOidString = function(t5) {
          if (!t5.match(/^[0-9.]+$/))
            throw "malformed oid string: " + t5;
          var i3 = "";
          var n3 = t5.split(".");
          var s3 = 40 * parseInt(n3[0]) + parseInt(n3[1]);
          i3 += e4(s3);
          n3.splice(0, 2);
          for (var a2 = 0; a2 < n3.length; a2++)
            i3 += r3(n3[a2]);
          this.hTLV = null;
          this.isModified = true;
          this.s = null;
          this.hV = i3;
        };
        this.setValueName = function(t5) {
          var e5 = pt.asn1.x509.OID.name2oid(t5);
          if ("" !== e5)
            this.setValueOidString(e5);
          else
            throw "DERObjectIdentifier oidName undefined: " + t5;
        };
        this.getFreshValueHex = function() {
          return this.hV;
        };
        if (void 0 !== t4) {
          if ("string" === typeof t4)
            if (t4.match(/^[0-2].[0-9.]+$/))
              this.setValueOidString(t4);
            else
              this.setValueName(t4);
          else if (void 0 !== t4.oid)
            this.setValueOidString(t4.oid);
          else if (void 0 !== t4.hex)
            this.setValueHex(t4.hex);
          else if (void 0 !== t4.name)
            this.setValueName(t4.name);
        }
      };
      vt.lang.extend(pt.asn1.DERObjectIdentifier, pt.asn1.ASN1Object);
      pt.asn1.DEREnumerated = function(t4) {
        pt.asn1.DEREnumerated.superclass.constructor.call(this);
        this.hT = "0a";
        this.setByBigInteger = function(t5) {
          this.hTLV = null;
          this.isModified = true;
          this.hV = pt.asn1.ASN1Util.bigIntToMinTwosComplementsHex(t5);
        };
        this.setByInteger = function(t5) {
          var e4 = new C(String(t5), 10);
          this.setByBigInteger(e4);
        };
        this.setValueHex = function(t5) {
          this.hV = t5;
        };
        this.getFreshValueHex = function() {
          return this.hV;
        };
        if ("undefined" != typeof t4) {
          if ("undefined" != typeof t4["int"])
            this.setByInteger(t4["int"]);
          else if ("number" == typeof t4)
            this.setByInteger(t4);
          else if ("undefined" != typeof t4["hex"])
            this.setValueHex(t4["hex"]);
        }
      };
      vt.lang.extend(pt.asn1.DEREnumerated, pt.asn1.ASN1Object);
      pt.asn1.DERUTF8String = function(t4) {
        pt.asn1.DERUTF8String.superclass.constructor.call(this, t4);
        this.hT = "0c";
      };
      vt.lang.extend(pt.asn1.DERUTF8String, pt.asn1.DERAbstractString);
      pt.asn1.DERNumericString = function(t4) {
        pt.asn1.DERNumericString.superclass.constructor.call(this, t4);
        this.hT = "12";
      };
      vt.lang.extend(pt.asn1.DERNumericString, pt.asn1.DERAbstractString);
      pt.asn1.DERPrintableString = function(t4) {
        pt.asn1.DERPrintableString.superclass.constructor.call(this, t4);
        this.hT = "13";
      };
      vt.lang.extend(pt.asn1.DERPrintableString, pt.asn1.DERAbstractString);
      pt.asn1.DERTeletexString = function(t4) {
        pt.asn1.DERTeletexString.superclass.constructor.call(this, t4);
        this.hT = "14";
      };
      vt.lang.extend(pt.asn1.DERTeletexString, pt.asn1.DERAbstractString);
      pt.asn1.DERIA5String = function(t4) {
        pt.asn1.DERIA5String.superclass.constructor.call(this, t4);
        this.hT = "16";
      };
      vt.lang.extend(pt.asn1.DERIA5String, pt.asn1.DERAbstractString);
      pt.asn1.DERUTCTime = function(t4) {
        pt.asn1.DERUTCTime.superclass.constructor.call(this, t4);
        this.hT = "17";
        this.setByDate = function(t5) {
          this.hTLV = null;
          this.isModified = true;
          this.date = t5;
          this.s = this.formatDate(this.date, "utc");
          this.hV = stohex(this.s);
        };
        this.getFreshValueHex = function() {
          if ("undefined" == typeof this.date && "undefined" == typeof this.s) {
            this.date = /* @__PURE__ */ new Date();
            this.s = this.formatDate(this.date, "utc");
            this.hV = stohex(this.s);
          }
          return this.hV;
        };
        if (void 0 !== t4) {
          if (void 0 !== t4.str)
            this.setString(t4.str);
          else if ("string" == typeof t4 && t4.match(/^[0-9]{12}Z$/))
            this.setString(t4);
          else if (void 0 !== t4.hex)
            this.setStringHex(t4.hex);
          else if (void 0 !== t4.date)
            this.setByDate(t4.date);
        }
      };
      vt.lang.extend(pt.asn1.DERUTCTime, pt.asn1.DERAbstractTime);
      pt.asn1.DERGeneralizedTime = function(t4) {
        pt.asn1.DERGeneralizedTime.superclass.constructor.call(this, t4);
        this.hT = "18";
        this.withMillis = false;
        this.setByDate = function(t5) {
          this.hTLV = null;
          this.isModified = true;
          this.date = t5;
          this.s = this.formatDate(this.date, "gen", this.withMillis);
          this.hV = stohex(this.s);
        };
        this.getFreshValueHex = function() {
          if (void 0 === this.date && void 0 === this.s) {
            this.date = /* @__PURE__ */ new Date();
            this.s = this.formatDate(this.date, "gen", this.withMillis);
            this.hV = stohex(this.s);
          }
          return this.hV;
        };
        if (void 0 !== t4) {
          if (void 0 !== t4.str)
            this.setString(t4.str);
          else if ("string" == typeof t4 && t4.match(/^[0-9]{14}Z$/))
            this.setString(t4);
          else if (void 0 !== t4.hex)
            this.setStringHex(t4.hex);
          else if (void 0 !== t4.date)
            this.setByDate(t4.date);
          if (true === t4.millis)
            this.withMillis = true;
        }
      };
      vt.lang.extend(pt.asn1.DERGeneralizedTime, pt.asn1.DERAbstractTime);
      pt.asn1.DERSequence = function(t4) {
        pt.asn1.DERSequence.superclass.constructor.call(this, t4);
        this.hT = "30";
        this.getFreshValueHex = function() {
          var t5 = "";
          for (var e4 = 0; e4 < this.asn1Array.length; e4++) {
            var r3 = this.asn1Array[e4];
            t5 += r3.getEncodedHex();
          }
          this.hV = t5;
          return this.hV;
        };
      };
      vt.lang.extend(pt.asn1.DERSequence, pt.asn1.DERAbstractStructured);
      pt.asn1.DERSet = function(t4) {
        pt.asn1.DERSet.superclass.constructor.call(this, t4);
        this.hT = "31";
        this.sortFlag = true;
        this.getFreshValueHex = function() {
          var t5 = new Array();
          for (var e4 = 0; e4 < this.asn1Array.length; e4++) {
            var r3 = this.asn1Array[e4];
            t5.push(r3.getEncodedHex());
          }
          if (true == this.sortFlag)
            t5.sort();
          this.hV = t5.join("");
          return this.hV;
        };
        if ("undefined" != typeof t4) {
          if ("undefined" != typeof t4.sortflag && false == t4.sortflag)
            this.sortFlag = false;
        }
      };
      vt.lang.extend(pt.asn1.DERSet, pt.asn1.DERAbstractStructured);
      pt.asn1.DERTaggedObject = function(t4) {
        pt.asn1.DERTaggedObject.superclass.constructor.call(this);
        this.hT = "a0";
        this.hV = "";
        this.isExplicit = true;
        this.asn1Object = null;
        this.setASN1Object = function(t5, e4, r3) {
          this.hT = e4;
          this.isExplicit = t5;
          this.asn1Object = r3;
          if (this.isExplicit) {
            this.hV = this.asn1Object.getEncodedHex();
            this.hTLV = null;
            this.isModified = true;
          } else {
            this.hV = null;
            this.hTLV = r3.getEncodedHex();
            this.hTLV = this.hTLV.replace(/^../, e4);
            this.isModified = false;
          }
        };
        this.getFreshValueHex = function() {
          return this.hV;
        };
        if ("undefined" != typeof t4) {
          if ("undefined" != typeof t4["tag"])
            this.hT = t4["tag"];
          if ("undefined" != typeof t4["explicit"])
            this.isExplicit = t4["explicit"];
          if ("undefined" != typeof t4["obj"]) {
            this.asn1Object = t4["obj"];
            this.setASN1Object(this.isExplicit, this.hT, this.asn1Object);
          }
        }
      };
      vt.lang.extend(pt.asn1.DERTaggedObject, pt.asn1.ASN1Object);
      var gt = function() {
        var t4 = function(e4, r3) {
          t4 = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(t5, e5) {
            t5.__proto__ = e5;
          } || function(t5, e5) {
            for (var r4 in e5)
              if (Object.prototype.hasOwnProperty.call(e5, r4))
                t5[r4] = e5[r4];
          };
          return t4(e4, r3);
        };
        return function(e4, r3) {
          if ("function" !== typeof r3 && null !== r3)
            throw new TypeError("Class extends value " + String(r3) + " is not a constructor or null");
          t4(e4, r3);
          function i3() {
            this.constructor = e4;
          }
          e4.prototype = null === r3 ? Object.create(r3) : (i3.prototype = r3.prototype, new i3());
        };
      }();
      var yt = function(t4) {
        gt(e4, t4);
        function e4(r3) {
          var i3 = t4.call(this) || this;
          if (r3) {
            if ("string" === typeof r3)
              i3.parseKey(r3);
            else if (e4.hasPrivateKeyProperty(r3) || e4.hasPublicKeyProperty(r3))
              i3.parsePropertiesFrom(r3);
          }
          return i3;
        }
        e4.prototype.parseKey = function(t5) {
          try {
            var e5 = 0;
            var r3 = 0;
            var i3 = /^\s*(?:[0-9A-Fa-f][0-9A-Fa-f]\s*)+$/;
            var n3 = i3.test(t5) ? y.decode(t5) : w.unarmor(t5);
            var s3 = I.decode(n3);
            if (3 === s3.sub.length)
              s3 = s3.sub[2].sub[0];
            if (9 === s3.sub.length) {
              e5 = s3.sub[1].getHexStringValue();
              this.n = U(e5, 16);
              r3 = s3.sub[2].getHexStringValue();
              this.e = parseInt(r3, 16);
              var a2 = s3.sub[3].getHexStringValue();
              this.d = U(a2, 16);
              var o3 = s3.sub[4].getHexStringValue();
              this.p = U(o3, 16);
              var u2 = s3.sub[5].getHexStringValue();
              this.q = U(u2, 16);
              var c2 = s3.sub[6].getHexStringValue();
              this.dmp1 = U(c2, 16);
              var l2 = s3.sub[7].getHexStringValue();
              this.dmq1 = U(l2, 16);
              var f3 = s3.sub[8].getHexStringValue();
              this.coeff = U(f3, 16);
            } else if (2 === s3.sub.length) {
              var h2 = s3.sub[1];
              var d3 = h2.sub[0];
              e5 = d3.sub[0].getHexStringValue();
              this.n = U(e5, 16);
              r3 = d3.sub[1].getHexStringValue();
              this.e = parseInt(r3, 16);
            } else
              return false;
            return true;
          } catch (t6) {
            return false;
          }
        };
        e4.prototype.getPrivateBaseKey = function() {
          var t5 = { array: [new pt.asn1.DERInteger({ int: 0 }), new pt.asn1.DERInteger({ bigint: this.n }), new pt.asn1.DERInteger({ int: this.e }), new pt.asn1.DERInteger({ bigint: this.d }), new pt.asn1.DERInteger({ bigint: this.p }), new pt.asn1.DERInteger({ bigint: this.q }), new pt.asn1.DERInteger({ bigint: this.dmp1 }), new pt.asn1.DERInteger({ bigint: this.dmq1 }), new pt.asn1.DERInteger({ bigint: this.coeff })] };
          var e5 = new pt.asn1.DERSequence(t5);
          return e5.getEncodedHex();
        };
        e4.prototype.getPrivateBaseKeyB64 = function() {
          return d2(this.getPrivateBaseKey());
        };
        e4.prototype.getPublicBaseKey = function() {
          var t5 = new pt.asn1.DERSequence({ array: [new pt.asn1.DERObjectIdentifier({ oid: "1.2.840.113549.1.1.1" }), new pt.asn1.DERNull()] });
          var e5 = new pt.asn1.DERSequence({ array: [new pt.asn1.DERInteger({ bigint: this.n }), new pt.asn1.DERInteger({ int: this.e })] });
          var r3 = new pt.asn1.DERBitString({ hex: "00" + e5.getEncodedHex() });
          var i3 = new pt.asn1.DERSequence({ array: [t5, r3] });
          return i3.getEncodedHex();
        };
        e4.prototype.getPublicBaseKeyB64 = function() {
          return d2(this.getPublicBaseKey());
        };
        e4.wordwrap = function(t5, e5) {
          e5 = e5 || 64;
          if (!t5)
            return t5;
          var r3 = "(.{1," + e5 + "})( +|$\n?)|(.{1," + e5 + "})";
          return t5.match(RegExp(r3, "g")).join("\n");
        };
        e4.prototype.getPrivateKey = function() {
          var t5 = "-----BEGIN RSA PRIVATE KEY-----\n";
          t5 += e4.wordwrap(this.getPrivateBaseKeyB64()) + "\n";
          t5 += "-----END RSA PRIVATE KEY-----";
          return t5;
        };
        e4.prototype.getPublicKey = function() {
          var t5 = "-----BEGIN PUBLIC KEY-----\n";
          t5 += e4.wordwrap(this.getPublicBaseKeyB64()) + "\n";
          t5 += "-----END PUBLIC KEY-----";
          return t5;
        };
        e4.hasPublicKeyProperty = function(t5) {
          t5 = t5 || {};
          return t5.hasOwnProperty("n") && t5.hasOwnProperty("e");
        };
        e4.hasPrivateKeyProperty = function(t5) {
          t5 = t5 || {};
          return t5.hasOwnProperty("n") && t5.hasOwnProperty("e") && t5.hasOwnProperty("d") && t5.hasOwnProperty("p") && t5.hasOwnProperty("q") && t5.hasOwnProperty("dmp1") && t5.hasOwnProperty("dmq1") && t5.hasOwnProperty("coeff");
        };
        e4.prototype.parsePropertiesFrom = function(t5) {
          this.n = t5.n;
          this.e = t5.e;
          if (t5.hasOwnProperty("d")) {
            this.d = t5.d;
            this.p = t5.p;
            this.q = t5.q;
            this.dmp1 = t5.dmp1;
            this.dmq1 = t5.dmq1;
            this.coeff = t5.coeff;
          }
        };
        return e4;
      }(ut);
      const mt = { i: "3.2.1" };
      var wt = function() {
        function t4(t5) {
          if (void 0 === t5)
            t5 = {};
          t5 = t5 || {};
          this.default_key_size = t5.default_key_size ? parseInt(t5.default_key_size, 10) : 1024;
          this.default_public_exponent = t5.default_public_exponent || "010001";
          this.log = t5.log || false;
          this.key = null;
        }
        t4.prototype.setKey = function(t5) {
          if (this.log && this.key)
            console.warn("A key was already set, overriding existing.");
          this.key = new yt(t5);
        };
        t4.prototype.setPrivateKey = function(t5) {
          this.setKey(t5);
        };
        t4.prototype.setPublicKey = function(t5) {
          this.setKey(t5);
        };
        t4.prototype.decrypt = function(t5) {
          try {
            return this.getKey().decrypt(t5);
          } catch (t6) {
            return false;
          }
        };
        t4.prototype.encrypt = function(t5) {
          try {
            return this.getKey().encrypt(t5);
          } catch (t6) {
            return false;
          }
        };
        t4.prototype.encryptLong = function(t5) {
          try {
            return d2(this.getKey().encryptLong(t5));
          } catch (t6) {
            return false;
          }
        };
        t4.prototype.decryptLong = function(t5) {
          try {
            return this.getKey().decryptLong(t5);
          } catch (t6) {
            return false;
          }
        };
        t4.prototype.sign = function(t5, e4, r3) {
          try {
            return d2(this.getKey().sign(t5, e4, r3));
          } catch (t6) {
            return false;
          }
        };
        t4.prototype.verify = function(t5, e4, r3) {
          try {
            return this.getKey().verify(t5, v(e4), r3);
          } catch (t6) {
            return false;
          }
        };
        t4.prototype.getKey = function(t5) {
          if (!this.key) {
            this.key = new yt();
            if (t5 && "[object Function]" === {}.toString.call(t5)) {
              this.key.generateAsync(this.default_key_size, this.default_public_exponent, t5);
              return;
            }
            this.key.generate(this.default_key_size, this.default_public_exponent);
          }
          return this.key;
        };
        t4.prototype.getPrivateKey = function() {
          return this.getKey().getPrivateKey();
        };
        t4.prototype.getPrivateKeyB64 = function() {
          return this.getKey().getPrivateBaseKeyB64();
        };
        t4.prototype.getPublicKey = function() {
          return this.getKey().getPublicKey();
        };
        t4.prototype.getPublicKeyB64 = function() {
          return this.getKey().getPublicBaseKeyB64();
        };
        t4.version = mt.i;
        return t4;
      }();
      const St = wt;
    }, 2480: () => {
    } };
    var e2 = {};
    function r(i2) {
      var n2 = e2[i2];
      if (void 0 !== n2)
        return n2.exports;
      var s2 = e2[i2] = { id: i2, loaded: false, exports: {} };
      t2[i2].call(s2.exports, s2, s2.exports, r);
      s2.loaded = true;
      return s2.exports;
    }
    (() => {
      r.d = (t3, e3) => {
        for (var i2 in e3)
          if (r.o(e3, i2) && !r.o(t3, i2))
            Object.defineProperty(t3, i2, { enumerable: true, get: e3[i2] });
      };
    })();
    (() => {
      r.g = function() {
        if ("object" === typeof globalThis)
          return globalThis;
        try {
          return this || new Function("return this")();
        } catch (t3) {
          if ("object" === typeof window)
            return window;
        }
      }();
    })();
    (() => {
      r.o = (t3, e3) => Object.prototype.hasOwnProperty.call(t3, e3);
    })();
    (() => {
      r.r = (t3) => {
        if ("undefined" !== typeof Symbol && Symbol.toStringTag)
          Object.defineProperty(t3, Symbol.toStringTag, { value: "Module" });
        Object.defineProperty(t3, "__esModule", { value: true });
      };
    })();
    (() => {
      r.nmd = (t3) => {
        t3.paths = [];
        if (!t3.children)
          t3.children = [];
        return t3;
      };
    })();
    var i = r(9021);
    return i;
  })());
})(gtpushMin);
var GtPush = /* @__PURE__ */ getDefaultExportFromCjs(gtpushMinExports);
index.invokePushCallback({
  type: "enabled"
});
const appid = "__UNI__54737E5";
{
  if (typeof index.onAppShow === "function") {
    index.onAppShow(() => {
      GtPush.enableSocket(true);
    });
  }
  GtPush.init({
    appid,
    onError: (res) => {
      console.error(res.error);
      const data = {
        type: "clientId",
        cid: "",
        errMsg: res.error
      };
      index.invokePushCallback(data);
    },
    onClientId: (res) => {
      const data = {
        type: "clientId",
        cid: res.cid
      };
      index.invokePushCallback(data);
    },
    onlineState: (res) => {
      const data = {
        type: "lineState",
        online: res.online
      };
      index.invokePushCallback(data);
    },
    onPushMsg: (res) => {
      const data = {
        type: "pushMsg",
        message: res.message
      };
      index.invokePushCallback(data);
    }
  });
}
var isVue2 = false;
function set$2(target, key, val) {
  if (Array.isArray(target)) {
    target.length = Math.max(target.length, key);
    target.splice(key, 1, val);
    return val;
  }
  target[key] = val;
  return val;
}
function del(target, key) {
  if (Array.isArray(target)) {
    target.splice(key, 1);
    return;
  }
  delete target[key];
}
/*!
  * pinia v2.0.33
  * (c) 2023 Eduardo San Martin Morote
  * @license MIT
  */
let activePinia;
const setActivePinia = (pinia) => activePinia = pinia;
const getActivePinia = () => getCurrentInstance() && inject(piniaSymbol) || activePinia;
const piniaSymbol = Symbol("pinia");
function isPlainObject(o2) {
  return o2 && typeof o2 === "object" && Object.prototype.toString.call(o2) === "[object Object]" && typeof o2.toJSON !== "function";
}
var MutationType;
(function(MutationType2) {
  MutationType2["direct"] = "direct";
  MutationType2["patchObject"] = "patch object";
  MutationType2["patchFunction"] = "patch function";
})(MutationType || (MutationType = {}));
const IS_CLIENT = typeof window !== "undefined";
const USE_DEVTOOLS = IS_CLIENT;
const componentStateTypes = [];
const getStoreType = (id) => "🍍 " + id;
function registerPiniaDevtools(app, pinia) {
}
function addStoreToDevtools(app, store) {
  if (!componentStateTypes.includes(getStoreType(store.$id))) {
    componentStateTypes.push(getStoreType(store.$id));
  }
}
function patchActionForGrouping(store, actionNames) {
  const actions = actionNames.reduce((storeActions, actionName) => {
    storeActions[actionName] = toRaw(store)[actionName];
    return storeActions;
  }, {});
  for (const actionName in actions) {
    store[actionName] = function() {
      const trackedStore = new Proxy(store, {
        get(...args) {
          return Reflect.get(...args);
        },
        set(...args) {
          return Reflect.set(...args);
        }
      });
      return actions[actionName].apply(trackedStore, arguments);
    };
  }
}
function devtoolsPlugin({ app, store, options }) {
  if (store.$id.startsWith("__hot:")) {
    return;
  }
  if (options.state) {
    store._isOptionsAPI = true;
  }
  if (typeof options.state === "function") {
    patchActionForGrouping(
      // @ts-expect-error: can cast the store...
      store,
      Object.keys(options.actions)
    );
    const originalHotUpdate = store._hotUpdate;
    toRaw(store)._hotUpdate = function(newStore) {
      originalHotUpdate.apply(this, arguments);
      patchActionForGrouping(store, Object.keys(newStore._hmrPayload.actions));
    };
  }
  addStoreToDevtools(
    app,
    // FIXME: is there a way to allow the assignment from Store<Id, S, G, A> to StoreGeneric?
    store
  );
}
function createPinia() {
  const scope = effectScope(true);
  const state = scope.run(() => ref({}));
  let _p = [];
  let toBeInstalled = [];
  const pinia = markRaw({
    install(app) {
      setActivePinia(pinia);
      {
        pinia._a = app;
        app.provide(piniaSymbol, pinia);
        app.config.globalProperties.$pinia = pinia;
        toBeInstalled.forEach((plugin2) => _p.push(plugin2));
        toBeInstalled = [];
      }
    },
    use(plugin2) {
      if (!this._a && !isVue2) {
        toBeInstalled.push(plugin2);
      } else {
        _p.push(plugin2);
      }
      return this;
    },
    _p,
    // it's actually undefined here
    // @ts-expect-error
    _a: null,
    _e: scope,
    _s: /* @__PURE__ */ new Map(),
    state
  });
  if (USE_DEVTOOLS && typeof Proxy !== "undefined") {
    pinia.use(devtoolsPlugin);
  }
  return pinia;
}
const isUseStore = (fn) => {
  return typeof fn === "function" && typeof fn.$id === "string";
};
function patchObject(newState, oldState) {
  for (const key in oldState) {
    const subPatch = oldState[key];
    if (!(key in newState)) {
      continue;
    }
    const targetValue = newState[key];
    if (isPlainObject(targetValue) && isPlainObject(subPatch) && !isRef(subPatch) && !isReactive(subPatch)) {
      newState[key] = patchObject(targetValue, subPatch);
    } else {
      {
        newState[key] = subPatch;
      }
    }
  }
  return newState;
}
function acceptHMRUpdate(initialUseStore, hot) {
  return (newModule) => {
    const pinia = hot.data.pinia || initialUseStore._pinia;
    if (!pinia) {
      return;
    }
    hot.data.pinia = pinia;
    for (const exportName in newModule) {
      const useStore = newModule[exportName];
      if (isUseStore(useStore) && pinia._s.has(useStore.$id)) {
        const id = useStore.$id;
        if (id !== initialUseStore.$id) {
          console.warn(`The id of the store changed from "${initialUseStore.$id}" to "${id}". Reloading.`);
          return hot.invalidate();
        }
        const existingStore = pinia._s.get(id);
        if (!existingStore) {
          console.log(`[Pinia]: skipping hmr because store doesn't exist yet`);
          return;
        }
        useStore(pinia, existingStore);
      }
    }
  };
}
const noop = () => {
};
function addSubscription(subscriptions, callback, detached, onCleanup = noop) {
  subscriptions.push(callback);
  const removeSubscription = () => {
    const idx = subscriptions.indexOf(callback);
    if (idx > -1) {
      subscriptions.splice(idx, 1);
      onCleanup();
    }
  };
  if (!detached && getCurrentScope()) {
    onScopeDispose(removeSubscription);
  }
  return removeSubscription;
}
function triggerSubscriptions(subscriptions, ...args) {
  subscriptions.slice().forEach((callback) => {
    callback(...args);
  });
}
function mergeReactiveObjects(target, patchToApply) {
  if (target instanceof Map && patchToApply instanceof Map) {
    patchToApply.forEach((value, key) => target.set(key, value));
  }
  if (target instanceof Set && patchToApply instanceof Set) {
    patchToApply.forEach(target.add, target);
  }
  for (const key in patchToApply) {
    if (!patchToApply.hasOwnProperty(key))
      continue;
    const subPatch = patchToApply[key];
    const targetValue = target[key];
    if (isPlainObject(targetValue) && isPlainObject(subPatch) && target.hasOwnProperty(key) && !isRef(subPatch) && !isReactive(subPatch)) {
      target[key] = mergeReactiveObjects(targetValue, subPatch);
    } else {
      target[key] = subPatch;
    }
  }
  return target;
}
const skipHydrateSymbol = Symbol("pinia:skipHydration");
function skipHydrate(obj) {
  return Object.defineProperty(obj, skipHydrateSymbol, {});
}
function shouldHydrate(obj) {
  return !isPlainObject(obj) || !obj.hasOwnProperty(skipHydrateSymbol);
}
const { assign } = Object;
function isComputed(o2) {
  return !!(isRef(o2) && o2.effect);
}
function createOptionsStore(id, options, pinia, hot) {
  const { state, actions, getters } = options;
  const initialState = pinia.state.value[id];
  let store;
  function setup() {
    if (!initialState && !hot) {
      {
        pinia.state.value[id] = state ? state() : {};
      }
    }
    const localState = hot ? (
      // use ref() to unwrap refs inside state TODO: check if this is still necessary
      toRefs(ref(state ? state() : {}).value)
    ) : toRefs(pinia.state.value[id]);
    return assign(localState, actions, Object.keys(getters || {}).reduce((computedGetters, name) => {
      if (name in localState) {
        console.warn(`[🍍]: A getter cannot have the same name as another state property. Rename one of them. Found with "${name}" in store "${id}".`);
      }
      computedGetters[name] = markRaw(computed(() => {
        setActivePinia(pinia);
        const store2 = pinia._s.get(id);
        return getters[name].call(store2, store2);
      }));
      return computedGetters;
    }, {}));
  }
  store = createSetupStore(id, setup, options, pinia, hot, true);
  return store;
}
function createSetupStore($id, setup, options = {}, pinia, hot, isOptionsStore) {
  let scope;
  const optionsForPlugin = assign({ actions: {} }, options);
  if (!pinia._e.active) {
    throw new Error("Pinia destroyed");
  }
  const $subscribeOptions = {
    deep: true
    // flush: 'post',
  };
  {
    $subscribeOptions.onTrigger = (event) => {
      if (isListening) {
        debuggerEvents = event;
      } else if (isListening == false && !store._hotUpdating) {
        if (Array.isArray(debuggerEvents)) {
          debuggerEvents.push(event);
        } else {
          console.error("🍍 debuggerEvents should be an array. This is most likely an internal Pinia bug.");
        }
      }
    };
  }
  let isListening;
  let isSyncListening;
  let subscriptions = markRaw([]);
  let actionSubscriptions = markRaw([]);
  let debuggerEvents;
  const initialState = pinia.state.value[$id];
  if (!isOptionsStore && !initialState && !hot) {
    {
      pinia.state.value[$id] = {};
    }
  }
  const hotState = ref({});
  let activeListener;
  function $patch(partialStateOrMutator) {
    let subscriptionMutation;
    isListening = isSyncListening = false;
    {
      debuggerEvents = [];
    }
    if (typeof partialStateOrMutator === "function") {
      partialStateOrMutator(pinia.state.value[$id]);
      subscriptionMutation = {
        type: MutationType.patchFunction,
        storeId: $id,
        events: debuggerEvents
      };
    } else {
      mergeReactiveObjects(pinia.state.value[$id], partialStateOrMutator);
      subscriptionMutation = {
        type: MutationType.patchObject,
        payload: partialStateOrMutator,
        storeId: $id,
        events: debuggerEvents
      };
    }
    const myListenerId = activeListener = Symbol();
    nextTick$1().then(() => {
      if (activeListener === myListenerId) {
        isListening = true;
      }
    });
    isSyncListening = true;
    triggerSubscriptions(subscriptions, subscriptionMutation, pinia.state.value[$id]);
  }
  const $reset = isOptionsStore ? function $reset2() {
    const { state } = options;
    const newState = state ? state() : {};
    this.$patch(($state) => {
      assign($state, newState);
    });
  } : (
    /* istanbul ignore next */
    () => {
      throw new Error(`🍍: Store "${$id}" is built using the setup syntax and does not implement $reset().`);
    }
  );
  function $dispose() {
    scope.stop();
    subscriptions = [];
    actionSubscriptions = [];
    pinia._s.delete($id);
  }
  function wrapAction(name, action) {
    return function() {
      setActivePinia(pinia);
      const args = Array.from(arguments);
      const afterCallbackList = [];
      const onErrorCallbackList = [];
      function after(callback) {
        afterCallbackList.push(callback);
      }
      function onError(callback) {
        onErrorCallbackList.push(callback);
      }
      triggerSubscriptions(actionSubscriptions, {
        args,
        name,
        store,
        after,
        onError
      });
      let ret;
      try {
        ret = action.apply(this && this.$id === $id ? this : store, args);
      } catch (error) {
        triggerSubscriptions(onErrorCallbackList, error);
        throw error;
      }
      if (ret instanceof Promise) {
        return ret.then((value) => {
          triggerSubscriptions(afterCallbackList, value);
          return value;
        }).catch((error) => {
          triggerSubscriptions(onErrorCallbackList, error);
          return Promise.reject(error);
        });
      }
      triggerSubscriptions(afterCallbackList, ret);
      return ret;
    };
  }
  const _hmrPayload = /* @__PURE__ */ markRaw({
    actions: {},
    getters: {},
    state: [],
    hotState
  });
  const partialStore = {
    _p: pinia,
    // _s: scope,
    $id,
    $onAction: addSubscription.bind(null, actionSubscriptions),
    $patch,
    $reset,
    $subscribe(callback, options2 = {}) {
      const removeSubscription = addSubscription(subscriptions, callback, options2.detached, () => stopWatcher());
      const stopWatcher = scope.run(() => watch(() => pinia.state.value[$id], (state) => {
        if (options2.flush === "sync" ? isSyncListening : isListening) {
          callback({
            storeId: $id,
            type: MutationType.direct,
            events: debuggerEvents
          }, state);
        }
      }, assign({}, $subscribeOptions, options2)));
      return removeSubscription;
    },
    $dispose
  };
  const store = reactive(
    assign(
      {
        _hmrPayload,
        _customProperties: markRaw(/* @__PURE__ */ new Set())
        // devtools custom properties
      },
      partialStore
      // must be added later
      // setupStore
    )
  );
  pinia._s.set($id, store);
  const setupStore = pinia._e.run(() => {
    scope = effectScope();
    return scope.run(() => setup());
  });
  for (const key in setupStore) {
    const prop = setupStore[key];
    if (isRef(prop) && !isComputed(prop) || isReactive(prop)) {
      if (hot) {
        set$2(hotState.value, key, toRef(setupStore, key));
      } else if (!isOptionsStore) {
        if (initialState && shouldHydrate(prop)) {
          if (isRef(prop)) {
            prop.value = initialState[key];
          } else {
            mergeReactiveObjects(prop, initialState[key]);
          }
        }
        {
          pinia.state.value[$id][key] = prop;
        }
      }
      {
        _hmrPayload.state.push(key);
      }
    } else if (typeof prop === "function") {
      const actionValue = hot ? prop : wrapAction(key, prop);
      {
        setupStore[key] = actionValue;
      }
      {
        _hmrPayload.actions[key] = prop;
      }
      optionsForPlugin.actions[key] = prop;
    } else {
      if (isComputed(prop)) {
        _hmrPayload.getters[key] = isOptionsStore ? (
          // @ts-expect-error
          options.getters[key]
        ) : prop;
        if (IS_CLIENT) {
          const getters = setupStore._getters || // @ts-expect-error: same
          (setupStore._getters = markRaw([]));
          getters.push(key);
        }
      }
    }
  }
  {
    assign(store, setupStore);
    assign(toRaw(store), setupStore);
  }
  Object.defineProperty(store, "$state", {
    get: () => hot ? hotState.value : pinia.state.value[$id],
    set: (state) => {
      if (hot) {
        throw new Error("cannot set hotState");
      }
      $patch(($state) => {
        assign($state, state);
      });
    }
  });
  {
    store._hotUpdate = markRaw((newStore) => {
      store._hotUpdating = true;
      newStore._hmrPayload.state.forEach((stateKey) => {
        if (stateKey in store.$state) {
          const newStateTarget = newStore.$state[stateKey];
          const oldStateSource = store.$state[stateKey];
          if (typeof newStateTarget === "object" && isPlainObject(newStateTarget) && isPlainObject(oldStateSource)) {
            patchObject(newStateTarget, oldStateSource);
          } else {
            newStore.$state[stateKey] = oldStateSource;
          }
        }
        set$2(store, stateKey, toRef(newStore.$state, stateKey));
      });
      Object.keys(store.$state).forEach((stateKey) => {
        if (!(stateKey in newStore.$state)) {
          del(store, stateKey);
        }
      });
      isListening = false;
      isSyncListening = false;
      pinia.state.value[$id] = toRef(newStore._hmrPayload, "hotState");
      isSyncListening = true;
      nextTick$1().then(() => {
        isListening = true;
      });
      for (const actionName in newStore._hmrPayload.actions) {
        const action = newStore[actionName];
        set$2(store, actionName, wrapAction(actionName, action));
      }
      for (const getterName in newStore._hmrPayload.getters) {
        const getter = newStore._hmrPayload.getters[getterName];
        const getterValue = isOptionsStore ? (
          // special handling of options api
          computed(() => {
            setActivePinia(pinia);
            return getter.call(store, store);
          })
        ) : getter;
        set$2(store, getterName, getterValue);
      }
      Object.keys(store._hmrPayload.getters).forEach((key) => {
        if (!(key in newStore._hmrPayload.getters)) {
          del(store, key);
        }
      });
      Object.keys(store._hmrPayload.actions).forEach((key) => {
        if (!(key in newStore._hmrPayload.actions)) {
          del(store, key);
        }
      });
      store._hmrPayload = newStore._hmrPayload;
      store._getters = newStore._getters;
      store._hotUpdating = false;
    });
  }
  if (USE_DEVTOOLS) {
    const nonEnumerable = {
      writable: true,
      configurable: true,
      // avoid warning on devtools trying to display this property
      enumerable: false
    };
    ["_p", "_hmrPayload", "_getters", "_customProperties"].forEach((p2) => {
      Object.defineProperty(store, p2, assign({ value: store[p2] }, nonEnumerable));
    });
  }
  pinia._p.forEach((extender) => {
    if (USE_DEVTOOLS) {
      const extensions = scope.run(() => extender({
        store,
        app: pinia._a,
        pinia,
        options: optionsForPlugin
      }));
      Object.keys(extensions || {}).forEach((key) => store._customProperties.add(key));
      assign(store, extensions);
    } else {
      assign(store, scope.run(() => extender({
        store,
        app: pinia._a,
        pinia,
        options: optionsForPlugin
      })));
    }
  });
  if (store.$state && typeof store.$state === "object" && typeof store.$state.constructor === "function" && !store.$state.constructor.toString().includes("[native code]")) {
    console.warn(`[🍍]: The "state" must be a plain object. It cannot be
	state: () => new MyClass()
Found in store "${store.$id}".`);
  }
  if (initialState && isOptionsStore && options.hydrate) {
    options.hydrate(store.$state, initialState);
  }
  isListening = true;
  isSyncListening = true;
  return store;
}
function defineStore(idOrOptions, setup, setupOptions) {
  let id;
  let options;
  const isSetupStore = typeof setup === "function";
  if (typeof idOrOptions === "string") {
    id = idOrOptions;
    options = isSetupStore ? setupOptions : setup;
  } else {
    options = idOrOptions;
    id = idOrOptions.id;
  }
  function useStore(pinia, hot) {
    const currentInstance2 = getCurrentInstance();
    pinia = // in test mode, ignore the argument provided as we can always retrieve a
    // pinia instance with getActivePinia()
    pinia || currentInstance2 && inject(piniaSymbol, null);
    if (pinia)
      setActivePinia(pinia);
    if (!activePinia) {
      throw new Error(`[🍍]: getActivePinia was called with no active Pinia. Did you forget to install pinia?
	const pinia = createPinia()
	app.use(pinia)
This will fail in production.`);
    }
    pinia = activePinia;
    if (!pinia._s.has(id)) {
      if (isSetupStore) {
        createSetupStore(id, setup, options, pinia);
      } else {
        createOptionsStore(id, options, pinia);
      }
      {
        useStore._pinia = pinia;
      }
    }
    const store = pinia._s.get(id);
    if (hot) {
      const hotId = "__hot:" + id;
      const newStore = isSetupStore ? createSetupStore(hotId, setup, options, pinia, true) : createOptionsStore(hotId, assign({}, options), pinia, true);
      hot._hotUpdate(newStore);
      delete pinia.state.value[hotId];
      pinia._s.delete(hotId);
    }
    if (IS_CLIENT && currentInstance2 && currentInstance2.proxy && // avoid adding stores that are just built for hot module replacement
    !hot) {
      const vm = currentInstance2.proxy;
      const cache = "_pStores" in vm ? vm._pStores : vm._pStores = {};
      cache[id] = store;
    }
    return store;
  }
  useStore.$id = id;
  return useStore;
}
let mapStoreSuffix = "Store";
function setMapStoreSuffix(suffix) {
  mapStoreSuffix = suffix;
}
function mapStores(...stores) {
  if (Array.isArray(stores[0])) {
    console.warn(`[🍍]: Directly pass all stores to "mapStores()" without putting them in an array:
Replace
	mapStores([useAuthStore, useCartStore])
with
	mapStores(useAuthStore, useCartStore)
This will fail in production if not fixed.`);
    stores = stores[0];
  }
  return stores.reduce((reduced, useStore) => {
    reduced[useStore.$id + mapStoreSuffix] = function() {
      return useStore(this.$pinia);
    };
    return reduced;
  }, {});
}
function mapState(useStore, keysOrMapper) {
  return Array.isArray(keysOrMapper) ? keysOrMapper.reduce((reduced, key) => {
    reduced[key] = function() {
      return useStore(this.$pinia)[key];
    };
    return reduced;
  }, {}) : Object.keys(keysOrMapper).reduce((reduced, key) => {
    reduced[key] = function() {
      const store = useStore(this.$pinia);
      const storeKey = keysOrMapper[key];
      return typeof storeKey === "function" ? storeKey.call(this, store) : store[storeKey];
    };
    return reduced;
  }, {});
}
const mapGetters = mapState;
function mapActions(useStore, keysOrMapper) {
  return Array.isArray(keysOrMapper) ? keysOrMapper.reduce((reduced, key) => {
    reduced[key] = function(...args) {
      return useStore(this.$pinia)[key](...args);
    };
    return reduced;
  }, {}) : Object.keys(keysOrMapper).reduce((reduced, key) => {
    reduced[key] = function(...args) {
      return useStore(this.$pinia)[keysOrMapper[key]](...args);
    };
    return reduced;
  }, {});
}
function mapWritableState(useStore, keysOrMapper) {
  return Array.isArray(keysOrMapper) ? keysOrMapper.reduce((reduced, key) => {
    reduced[key] = {
      get() {
        return useStore(this.$pinia)[key];
      },
      set(value) {
        return useStore(this.$pinia)[key] = value;
      }
    };
    return reduced;
  }, {}) : Object.keys(keysOrMapper).reduce((reduced, key) => {
    reduced[key] = {
      get() {
        return useStore(this.$pinia)[keysOrMapper[key]];
      },
      set(value) {
        return useStore(this.$pinia)[keysOrMapper[key]] = value;
      }
    };
    return reduced;
  }, {});
}
function storeToRefs(store) {
  {
    store = toRaw(store);
    const refs = {};
    for (const key in store) {
      const value = store[key];
      if (isRef(value) || isReactive(value)) {
        refs[key] = // ---
        toRef(store, key);
      }
    }
    return refs;
  }
}
const PiniaVuePlugin = function(_Vue) {
  _Vue.mixin({
    beforeCreate() {
      const options = this.$options;
      if (options.pinia) {
        const pinia = options.pinia;
        if (!this._provided) {
          const provideCache = {};
          Object.defineProperty(this, "_provided", {
            get: () => provideCache,
            set: (v) => Object.assign(provideCache, v)
          });
        }
        this._provided[piniaSymbol] = pinia;
        if (!this.$pinia) {
          this.$pinia = pinia;
        }
        pinia._a = this;
        if (IS_CLIENT) {
          setActivePinia(pinia);
        }
        if (USE_DEVTOOLS) {
          registerPiniaDevtools(pinia._a);
        }
      } else if (!this.$pinia && options.parent && options.parent.$pinia) {
        this.$pinia = options.parent.$pinia;
      }
    },
    destroyed() {
      delete this._pStores;
    }
  });
};
const Pinia = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  get MutationType() {
    return MutationType;
  },
  PiniaVuePlugin,
  acceptHMRUpdate,
  createPinia,
  defineStore,
  getActivePinia,
  mapActions,
  mapGetters,
  mapState,
  mapStores,
  mapWritableState,
  setActivePinia,
  setMapStoreSuffix,
  skipHydrate,
  storeToRefs
}, Symbol.toStringTag, { value: "Module" }));
const createHook = (lifecycle) => (hook, target = getCurrentInstance()) => {
  !isInSSRComponentSetup && injectHook(lifecycle, hook, target);
};
const onShow = /* @__PURE__ */ createHook(ON_SHOW);
const onHide = /* @__PURE__ */ createHook(ON_HIDE);
const onLoad = /* @__PURE__ */ createHook(ON_LOAD);
const onReady = /* @__PURE__ */ createHook(ON_READY);
const onUnload = /* @__PURE__ */ createHook(ON_UNLOAD);
const onShareAppMessage = /* @__PURE__ */ createHook(ON_SHARE_APP_MESSAGE);
//! moment.js
//! version : 2.29.4
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com
var hookCallback;
function hooks() {
  return hookCallback.apply(null, arguments);
}
function setHookCallback(callback) {
  hookCallback = callback;
}
function isArray$1(input) {
  return input instanceof Array || Object.prototype.toString.call(input) === "[object Array]";
}
function isObject(input) {
  return input != null && Object.prototype.toString.call(input) === "[object Object]";
}
function hasOwnProp(a, b) {
  return Object.prototype.hasOwnProperty.call(a, b);
}
function isObjectEmpty(obj) {
  if (Object.getOwnPropertyNames) {
    return Object.getOwnPropertyNames(obj).length === 0;
  } else {
    var k;
    for (k in obj) {
      if (hasOwnProp(obj, k)) {
        return false;
      }
    }
    return true;
  }
}
function isUndefined(input) {
  return input === void 0;
}
function isNumber(input) {
  return typeof input === "number" || Object.prototype.toString.call(input) === "[object Number]";
}
function isDate(input) {
  return input instanceof Date || Object.prototype.toString.call(input) === "[object Date]";
}
function map(arr, fn) {
  var res = [], i, arrLen = arr.length;
  for (i = 0; i < arrLen; ++i) {
    res.push(fn(arr[i], i));
  }
  return res;
}
function extend(a, b) {
  for (var i in b) {
    if (hasOwnProp(b, i)) {
      a[i] = b[i];
    }
  }
  if (hasOwnProp(b, "toString")) {
    a.toString = b.toString;
  }
  if (hasOwnProp(b, "valueOf")) {
    a.valueOf = b.valueOf;
  }
  return a;
}
function createUTC(input, format2, locale2, strict) {
  return createLocalOrUTC(input, format2, locale2, strict, true).utc();
}
function defaultParsingFlags() {
  return {
    empty: false,
    unusedTokens: [],
    unusedInput: [],
    overflow: -2,
    charsLeftOver: 0,
    nullInput: false,
    invalidEra: null,
    invalidMonth: null,
    invalidFormat: false,
    userInvalidated: false,
    iso: false,
    parsedDateParts: [],
    era: null,
    meridiem: null,
    rfc2822: false,
    weekdayMismatch: false
  };
}
function getParsingFlags(m) {
  if (m._pf == null) {
    m._pf = defaultParsingFlags();
  }
  return m._pf;
}
var some;
if (Array.prototype.some) {
  some = Array.prototype.some;
} else {
  some = function(fun) {
    var t2 = Object(this), len = t2.length >>> 0, i;
    for (i = 0; i < len; i++) {
      if (i in t2 && fun.call(this, t2[i], i, t2)) {
        return true;
      }
    }
    return false;
  };
}
function isValid(m) {
  if (m._isValid == null) {
    var flags = getParsingFlags(m), parsedParts = some.call(flags.parsedDateParts, function(i) {
      return i != null;
    }), isNowValid = !isNaN(m._d.getTime()) && flags.overflow < 0 && !flags.empty && !flags.invalidEra && !flags.invalidMonth && !flags.invalidWeekday && !flags.weekdayMismatch && !flags.nullInput && !flags.invalidFormat && !flags.userInvalidated && (!flags.meridiem || flags.meridiem && parsedParts);
    if (m._strict) {
      isNowValid = isNowValid && flags.charsLeftOver === 0 && flags.unusedTokens.length === 0 && flags.bigHour === void 0;
    }
    if (Object.isFrozen == null || !Object.isFrozen(m)) {
      m._isValid = isNowValid;
    } else {
      return isNowValid;
    }
  }
  return m._isValid;
}
function createInvalid(flags) {
  var m = createUTC(NaN);
  if (flags != null) {
    extend(getParsingFlags(m), flags);
  } else {
    getParsingFlags(m).userInvalidated = true;
  }
  return m;
}
var momentProperties = hooks.momentProperties = [], updateInProgress = false;
function copyConfig(to2, from2) {
  var i, prop, val, momentPropertiesLen = momentProperties.length;
  if (!isUndefined(from2._isAMomentObject)) {
    to2._isAMomentObject = from2._isAMomentObject;
  }
  if (!isUndefined(from2._i)) {
    to2._i = from2._i;
  }
  if (!isUndefined(from2._f)) {
    to2._f = from2._f;
  }
  if (!isUndefined(from2._l)) {
    to2._l = from2._l;
  }
  if (!isUndefined(from2._strict)) {
    to2._strict = from2._strict;
  }
  if (!isUndefined(from2._tzm)) {
    to2._tzm = from2._tzm;
  }
  if (!isUndefined(from2._isUTC)) {
    to2._isUTC = from2._isUTC;
  }
  if (!isUndefined(from2._offset)) {
    to2._offset = from2._offset;
  }
  if (!isUndefined(from2._pf)) {
    to2._pf = getParsingFlags(from2);
  }
  if (!isUndefined(from2._locale)) {
    to2._locale = from2._locale;
  }
  if (momentPropertiesLen > 0) {
    for (i = 0; i < momentPropertiesLen; i++) {
      prop = momentProperties[i];
      val = from2[prop];
      if (!isUndefined(val)) {
        to2[prop] = val;
      }
    }
  }
  return to2;
}
function Moment(config) {
  copyConfig(this, config);
  this._d = new Date(config._d != null ? config._d.getTime() : NaN);
  if (!this.isValid()) {
    this._d = /* @__PURE__ */ new Date(NaN);
  }
  if (updateInProgress === false) {
    updateInProgress = true;
    hooks.updateOffset(this);
    updateInProgress = false;
  }
}
function isMoment(obj) {
  return obj instanceof Moment || obj != null && obj._isAMomentObject != null;
}
function warn(msg) {
  if (hooks.suppressDeprecationWarnings === false && typeof console !== "undefined" && console.warn) {
    console.warn("Deprecation warning: " + msg);
  }
}
function deprecate(msg, fn) {
  var firstTime = true;
  return extend(function() {
    if (hooks.deprecationHandler != null) {
      hooks.deprecationHandler(null, msg);
    }
    if (firstTime) {
      var args = [], arg, i, key, argLen = arguments.length;
      for (i = 0; i < argLen; i++) {
        arg = "";
        if (typeof arguments[i] === "object") {
          arg += "\n[" + i + "] ";
          for (key in arguments[0]) {
            if (hasOwnProp(arguments[0], key)) {
              arg += key + ": " + arguments[0][key] + ", ";
            }
          }
          arg = arg.slice(0, -2);
        } else {
          arg = arguments[i];
        }
        args.push(arg);
      }
      warn(
        msg + "\nArguments: " + Array.prototype.slice.call(args).join("") + "\n" + new Error().stack
      );
      firstTime = false;
    }
    return fn.apply(this, arguments);
  }, fn);
}
var deprecations = {};
function deprecateSimple(name, msg) {
  if (hooks.deprecationHandler != null) {
    hooks.deprecationHandler(name, msg);
  }
  if (!deprecations[name]) {
    warn(msg);
    deprecations[name] = true;
  }
}
hooks.suppressDeprecationWarnings = false;
hooks.deprecationHandler = null;
function isFunction(input) {
  return typeof Function !== "undefined" && input instanceof Function || Object.prototype.toString.call(input) === "[object Function]";
}
function set(config) {
  var prop, i;
  for (i in config) {
    if (hasOwnProp(config, i)) {
      prop = config[i];
      if (isFunction(prop)) {
        this[i] = prop;
      } else {
        this["_" + i] = prop;
      }
    }
  }
  this._config = config;
  this._dayOfMonthOrdinalParseLenient = new RegExp(
    (this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) + "|" + /\d{1,2}/.source
  );
}
function mergeConfigs(parentConfig, childConfig) {
  var res = extend({}, parentConfig), prop;
  for (prop in childConfig) {
    if (hasOwnProp(childConfig, prop)) {
      if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
        res[prop] = {};
        extend(res[prop], parentConfig[prop]);
        extend(res[prop], childConfig[prop]);
      } else if (childConfig[prop] != null) {
        res[prop] = childConfig[prop];
      } else {
        delete res[prop];
      }
    }
  }
  for (prop in parentConfig) {
    if (hasOwnProp(parentConfig, prop) && !hasOwnProp(childConfig, prop) && isObject(parentConfig[prop])) {
      res[prop] = extend({}, res[prop]);
    }
  }
  return res;
}
function Locale(config) {
  if (config != null) {
    this.set(config);
  }
}
var keys;
if (Object.keys) {
  keys = Object.keys;
} else {
  keys = function(obj) {
    var i, res = [];
    for (i in obj) {
      if (hasOwnProp(obj, i)) {
        res.push(i);
      }
    }
    return res;
  };
}
var defaultCalendar = {
  sameDay: "[Today at] LT",
  nextDay: "[Tomorrow at] LT",
  nextWeek: "dddd [at] LT",
  lastDay: "[Yesterday at] LT",
  lastWeek: "[Last] dddd [at] LT",
  sameElse: "L"
};
function calendar(key, mom, now2) {
  var output = this._calendar[key] || this._calendar["sameElse"];
  return isFunction(output) ? output.call(mom, now2) : output;
}
function zeroFill(number, targetLength, forceSign) {
  var absNumber = "" + Math.abs(number), zerosToFill = targetLength - absNumber.length, sign2 = number >= 0;
  return (sign2 ? forceSign ? "+" : "" : "-") + Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
}
var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|N{1,5}|YYYYYY|YYYYY|YYYY|YY|y{2,4}|yo?|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g, localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, formatFunctions = {}, formatTokenFunctions = {};
function addFormatToken(token2, padded, ordinal2, callback) {
  var func = callback;
  if (typeof callback === "string") {
    func = function() {
      return this[callback]();
    };
  }
  if (token2) {
    formatTokenFunctions[token2] = func;
  }
  if (padded) {
    formatTokenFunctions[padded[0]] = function() {
      return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
    };
  }
  if (ordinal2) {
    formatTokenFunctions[ordinal2] = function() {
      return this.localeData().ordinal(
        func.apply(this, arguments),
        token2
      );
    };
  }
}
function removeFormattingTokens(input) {
  if (input.match(/\[[\s\S]/)) {
    return input.replace(/^\[|\]$/g, "");
  }
  return input.replace(/\\/g, "");
}
function makeFormatFunction(format2) {
  var array = format2.match(formattingTokens), i, length;
  for (i = 0, length = array.length; i < length; i++) {
    if (formatTokenFunctions[array[i]]) {
      array[i] = formatTokenFunctions[array[i]];
    } else {
      array[i] = removeFormattingTokens(array[i]);
    }
  }
  return function(mom) {
    var output = "", i2;
    for (i2 = 0; i2 < length; i2++) {
      output += isFunction(array[i2]) ? array[i2].call(mom, format2) : array[i2];
    }
    return output;
  };
}
function formatMoment(m, format2) {
  if (!m.isValid()) {
    return m.localeData().invalidDate();
  }
  format2 = expandFormat(format2, m.localeData());
  formatFunctions[format2] = formatFunctions[format2] || makeFormatFunction(format2);
  return formatFunctions[format2](m);
}
function expandFormat(format2, locale2) {
  var i = 5;
  function replaceLongDateFormatTokens(input) {
    return locale2.longDateFormat(input) || input;
  }
  localFormattingTokens.lastIndex = 0;
  while (i >= 0 && localFormattingTokens.test(format2)) {
    format2 = format2.replace(
      localFormattingTokens,
      replaceLongDateFormatTokens
    );
    localFormattingTokens.lastIndex = 0;
    i -= 1;
  }
  return format2;
}
var defaultLongDateFormat = {
  LTS: "h:mm:ss A",
  LT: "h:mm A",
  L: "MM/DD/YYYY",
  LL: "MMMM D, YYYY",
  LLL: "MMMM D, YYYY h:mm A",
  LLLL: "dddd, MMMM D, YYYY h:mm A"
};
function longDateFormat(key) {
  var format2 = this._longDateFormat[key], formatUpper = this._longDateFormat[key.toUpperCase()];
  if (format2 || !formatUpper) {
    return format2;
  }
  this._longDateFormat[key] = formatUpper.match(formattingTokens).map(function(tok) {
    if (tok === "MMMM" || tok === "MM" || tok === "DD" || tok === "dddd") {
      return tok.slice(1);
    }
    return tok;
  }).join("");
  return this._longDateFormat[key];
}
var defaultInvalidDate = "Invalid date";
function invalidDate() {
  return this._invalidDate;
}
var defaultOrdinal = "%d", defaultDayOfMonthOrdinalParse = /\d{1,2}/;
function ordinal(number) {
  return this._ordinal.replace("%d", number);
}
var defaultRelativeTime = {
  future: "in %s",
  past: "%s ago",
  s: "a few seconds",
  ss: "%d seconds",
  m: "a minute",
  mm: "%d minutes",
  h: "an hour",
  hh: "%d hours",
  d: "a day",
  dd: "%d days",
  w: "a week",
  ww: "%d weeks",
  M: "a month",
  MM: "%d months",
  y: "a year",
  yy: "%d years"
};
function relativeTime(number, withoutSuffix, string, isFuture) {
  var output = this._relativeTime[string];
  return isFunction(output) ? output(number, withoutSuffix, string, isFuture) : output.replace(/%d/i, number);
}
function pastFuture(diff2, output) {
  var format2 = this._relativeTime[diff2 > 0 ? "future" : "past"];
  return isFunction(format2) ? format2(output) : format2.replace(/%s/i, output);
}
var aliases = {};
function addUnitAlias(unit, shorthand) {
  var lowerCase = unit.toLowerCase();
  aliases[lowerCase] = aliases[lowerCase + "s"] = aliases[shorthand] = unit;
}
function normalizeUnits(units) {
  return typeof units === "string" ? aliases[units] || aliases[units.toLowerCase()] : void 0;
}
function normalizeObjectUnits(inputObject) {
  var normalizedInput = {}, normalizedProp, prop;
  for (prop in inputObject) {
    if (hasOwnProp(inputObject, prop)) {
      normalizedProp = normalizeUnits(prop);
      if (normalizedProp) {
        normalizedInput[normalizedProp] = inputObject[prop];
      }
    }
  }
  return normalizedInput;
}
var priorities = {};
function addUnitPriority(unit, priority) {
  priorities[unit] = priority;
}
function getPrioritizedUnits(unitsObj) {
  var units = [], u;
  for (u in unitsObj) {
    if (hasOwnProp(unitsObj, u)) {
      units.push({ unit: u, priority: priorities[u] });
    }
  }
  units.sort(function(a, b) {
    return a.priority - b.priority;
  });
  return units;
}
function isLeapYear(year) {
  return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
}
function absFloor(number) {
  if (number < 0) {
    return Math.ceil(number) || 0;
  } else {
    return Math.floor(number);
  }
}
function toInt(argumentForCoercion) {
  var coercedNumber = +argumentForCoercion, value = 0;
  if (coercedNumber !== 0 && isFinite(coercedNumber)) {
    value = absFloor(coercedNumber);
  }
  return value;
}
function makeGetSet(unit, keepTime) {
  return function(value) {
    if (value != null) {
      set$1(this, unit, value);
      hooks.updateOffset(this, keepTime);
      return this;
    } else {
      return get(this, unit);
    }
  };
}
function get(mom, unit) {
  return mom.isValid() ? mom._d["get" + (mom._isUTC ? "UTC" : "") + unit]() : NaN;
}
function set$1(mom, unit, value) {
  if (mom.isValid() && !isNaN(value)) {
    if (unit === "FullYear" && isLeapYear(mom.year()) && mom.month() === 1 && mom.date() === 29) {
      value = toInt(value);
      mom._d["set" + (mom._isUTC ? "UTC" : "") + unit](
        value,
        mom.month(),
        daysInMonth(value, mom.month())
      );
    } else {
      mom._d["set" + (mom._isUTC ? "UTC" : "") + unit](value);
    }
  }
}
function stringGet(units) {
  units = normalizeUnits(units);
  if (isFunction(this[units])) {
    return this[units]();
  }
  return this;
}
function stringSet(units, value) {
  if (typeof units === "object") {
    units = normalizeObjectUnits(units);
    var prioritized = getPrioritizedUnits(units), i, prioritizedLen = prioritized.length;
    for (i = 0; i < prioritizedLen; i++) {
      this[prioritized[i].unit](units[prioritized[i].unit]);
    }
  } else {
    units = normalizeUnits(units);
    if (isFunction(this[units])) {
      return this[units](value);
    }
  }
  return this;
}
var match1 = /\d/, match2 = /\d\d/, match3 = /\d{3}/, match4 = /\d{4}/, match6 = /[+-]?\d{6}/, match1to2 = /\d\d?/, match3to4 = /\d\d\d\d?/, match5to6 = /\d\d\d\d\d\d?/, match1to3 = /\d{1,3}/, match1to4 = /\d{1,4}/, match1to6 = /[+-]?\d{1,6}/, matchUnsigned = /\d+/, matchSigned = /[+-]?\d+/, matchOffset = /Z|[+-]\d\d:?\d\d/gi, matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi, matchTimestamp = /[+-]?\d+(\.\d{1,3})?/, matchWord = /[0-9]{0,256}['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFF07\uFF10-\uFFEF]{1,256}|[\u0600-\u06FF\/]{1,256}(\s*?[\u0600-\u06FF]{1,256}){1,2}/i, regexes;
regexes = {};
function addRegexToken(token2, regex, strictRegex) {
  regexes[token2] = isFunction(regex) ? regex : function(isStrict, localeData2) {
    return isStrict && strictRegex ? strictRegex : regex;
  };
}
function getParseRegexForToken(token2, config) {
  if (!hasOwnProp(regexes, token2)) {
    return new RegExp(unescapeFormat(token2));
  }
  return regexes[token2](config._strict, config._locale);
}
function unescapeFormat(s2) {
  return regexEscape(
    s2.replace("\\", "").replace(
      /\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,
      function(matched, p1, p2, p3, p4) {
        return p1 || p2 || p3 || p4;
      }
    )
  );
}
function regexEscape(s2) {
  return s2.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
}
var tokens = {};
function addParseToken(token2, callback) {
  var i, func = callback, tokenLen;
  if (typeof token2 === "string") {
    token2 = [token2];
  }
  if (isNumber(callback)) {
    func = function(input, array) {
      array[callback] = toInt(input);
    };
  }
  tokenLen = token2.length;
  for (i = 0; i < tokenLen; i++) {
    tokens[token2[i]] = func;
  }
}
function addWeekParseToken(token2, callback) {
  addParseToken(token2, function(input, array, config, token3) {
    config._w = config._w || {};
    callback(input, config._w, config, token3);
  });
}
function addTimeToArrayFromToken(token2, input, config) {
  if (input != null && hasOwnProp(tokens, token2)) {
    tokens[token2](input, config._a, config, token2);
  }
}
var YEAR = 0, MONTH = 1, DATE = 2, HOUR = 3, MINUTE = 4, SECOND = 5, MILLISECOND = 6, WEEK = 7, WEEKDAY = 8;
function mod(n2, x) {
  return (n2 % x + x) % x;
}
var indexOf;
if (Array.prototype.indexOf) {
  indexOf = Array.prototype.indexOf;
} else {
  indexOf = function(o2) {
    var i;
    for (i = 0; i < this.length; ++i) {
      if (this[i] === o2) {
        return i;
      }
    }
    return -1;
  };
}
function daysInMonth(year, month) {
  if (isNaN(year) || isNaN(month)) {
    return NaN;
  }
  var modMonth = mod(month, 12);
  year += (month - modMonth) / 12;
  return modMonth === 1 ? isLeapYear(year) ? 29 : 28 : 31 - modMonth % 7 % 2;
}
addFormatToken("M", ["MM", 2], "Mo", function() {
  return this.month() + 1;
});
addFormatToken("MMM", 0, 0, function(format2) {
  return this.localeData().monthsShort(this, format2);
});
addFormatToken("MMMM", 0, 0, function(format2) {
  return this.localeData().months(this, format2);
});
addUnitAlias("month", "M");
addUnitPriority("month", 8);
addRegexToken("M", match1to2);
addRegexToken("MM", match1to2, match2);
addRegexToken("MMM", function(isStrict, locale2) {
  return locale2.monthsShortRegex(isStrict);
});
addRegexToken("MMMM", function(isStrict, locale2) {
  return locale2.monthsRegex(isStrict);
});
addParseToken(["M", "MM"], function(input, array) {
  array[MONTH] = toInt(input) - 1;
});
addParseToken(["MMM", "MMMM"], function(input, array, config, token2) {
  var month = config._locale.monthsParse(input, token2, config._strict);
  if (month != null) {
    array[MONTH] = month;
  } else {
    getParsingFlags(config).invalidMonth = input;
  }
});
var defaultLocaleMonths = "January_February_March_April_May_June_July_August_September_October_November_December".split(
  "_"
), defaultLocaleMonthsShort = "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"), MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/, defaultMonthsShortRegex = matchWord, defaultMonthsRegex = matchWord;
function localeMonths(m, format2) {
  if (!m) {
    return isArray$1(this._months) ? this._months : this._months["standalone"];
  }
  return isArray$1(this._months) ? this._months[m.month()] : this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format2) ? "format" : "standalone"][m.month()];
}
function localeMonthsShort(m, format2) {
  if (!m) {
    return isArray$1(this._monthsShort) ? this._monthsShort : this._monthsShort["standalone"];
  }
  return isArray$1(this._monthsShort) ? this._monthsShort[m.month()] : this._monthsShort[MONTHS_IN_FORMAT.test(format2) ? "format" : "standalone"][m.month()];
}
function handleStrictParse(monthName, format2, strict) {
  var i, ii, mom, llc = monthName.toLocaleLowerCase();
  if (!this._monthsParse) {
    this._monthsParse = [];
    this._longMonthsParse = [];
    this._shortMonthsParse = [];
    for (i = 0; i < 12; ++i) {
      mom = createUTC([2e3, i]);
      this._shortMonthsParse[i] = this.monthsShort(
        mom,
        ""
      ).toLocaleLowerCase();
      this._longMonthsParse[i] = this.months(mom, "").toLocaleLowerCase();
    }
  }
  if (strict) {
    if (format2 === "MMM") {
      ii = indexOf.call(this._shortMonthsParse, llc);
      return ii !== -1 ? ii : null;
    } else {
      ii = indexOf.call(this._longMonthsParse, llc);
      return ii !== -1 ? ii : null;
    }
  } else {
    if (format2 === "MMM") {
      ii = indexOf.call(this._shortMonthsParse, llc);
      if (ii !== -1) {
        return ii;
      }
      ii = indexOf.call(this._longMonthsParse, llc);
      return ii !== -1 ? ii : null;
    } else {
      ii = indexOf.call(this._longMonthsParse, llc);
      if (ii !== -1) {
        return ii;
      }
      ii = indexOf.call(this._shortMonthsParse, llc);
      return ii !== -1 ? ii : null;
    }
  }
}
function localeMonthsParse(monthName, format2, strict) {
  var i, mom, regex;
  if (this._monthsParseExact) {
    return handleStrictParse.call(this, monthName, format2, strict);
  }
  if (!this._monthsParse) {
    this._monthsParse = [];
    this._longMonthsParse = [];
    this._shortMonthsParse = [];
  }
  for (i = 0; i < 12; i++) {
    mom = createUTC([2e3, i]);
    if (strict && !this._longMonthsParse[i]) {
      this._longMonthsParse[i] = new RegExp(
        "^" + this.months(mom, "").replace(".", "") + "$",
        "i"
      );
      this._shortMonthsParse[i] = new RegExp(
        "^" + this.monthsShort(mom, "").replace(".", "") + "$",
        "i"
      );
    }
    if (!strict && !this._monthsParse[i]) {
      regex = "^" + this.months(mom, "") + "|^" + this.monthsShort(mom, "");
      this._monthsParse[i] = new RegExp(regex.replace(".", ""), "i");
    }
    if (strict && format2 === "MMMM" && this._longMonthsParse[i].test(monthName)) {
      return i;
    } else if (strict && format2 === "MMM" && this._shortMonthsParse[i].test(monthName)) {
      return i;
    } else if (!strict && this._monthsParse[i].test(monthName)) {
      return i;
    }
  }
}
function setMonth(mom, value) {
  var dayOfMonth;
  if (!mom.isValid()) {
    return mom;
  }
  if (typeof value === "string") {
    if (/^\d+$/.test(value)) {
      value = toInt(value);
    } else {
      value = mom.localeData().monthsParse(value);
      if (!isNumber(value)) {
        return mom;
      }
    }
  }
  dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
  mom._d["set" + (mom._isUTC ? "UTC" : "") + "Month"](value, dayOfMonth);
  return mom;
}
function getSetMonth(value) {
  if (value != null) {
    setMonth(this, value);
    hooks.updateOffset(this, true);
    return this;
  } else {
    return get(this, "Month");
  }
}
function getDaysInMonth() {
  return daysInMonth(this.year(), this.month());
}
function monthsShortRegex(isStrict) {
  if (this._monthsParseExact) {
    if (!hasOwnProp(this, "_monthsRegex")) {
      computeMonthsParse.call(this);
    }
    if (isStrict) {
      return this._monthsShortStrictRegex;
    } else {
      return this._monthsShortRegex;
    }
  } else {
    if (!hasOwnProp(this, "_monthsShortRegex")) {
      this._monthsShortRegex = defaultMonthsShortRegex;
    }
    return this._monthsShortStrictRegex && isStrict ? this._monthsShortStrictRegex : this._monthsShortRegex;
  }
}
function monthsRegex(isStrict) {
  if (this._monthsParseExact) {
    if (!hasOwnProp(this, "_monthsRegex")) {
      computeMonthsParse.call(this);
    }
    if (isStrict) {
      return this._monthsStrictRegex;
    } else {
      return this._monthsRegex;
    }
  } else {
    if (!hasOwnProp(this, "_monthsRegex")) {
      this._monthsRegex = defaultMonthsRegex;
    }
    return this._monthsStrictRegex && isStrict ? this._monthsStrictRegex : this._monthsRegex;
  }
}
function computeMonthsParse() {
  function cmpLenRev(a, b) {
    return b.length - a.length;
  }
  var shortPieces = [], longPieces = [], mixedPieces = [], i, mom;
  for (i = 0; i < 12; i++) {
    mom = createUTC([2e3, i]);
    shortPieces.push(this.monthsShort(mom, ""));
    longPieces.push(this.months(mom, ""));
    mixedPieces.push(this.months(mom, ""));
    mixedPieces.push(this.monthsShort(mom, ""));
  }
  shortPieces.sort(cmpLenRev);
  longPieces.sort(cmpLenRev);
  mixedPieces.sort(cmpLenRev);
  for (i = 0; i < 12; i++) {
    shortPieces[i] = regexEscape(shortPieces[i]);
    longPieces[i] = regexEscape(longPieces[i]);
  }
  for (i = 0; i < 24; i++) {
    mixedPieces[i] = regexEscape(mixedPieces[i]);
  }
  this._monthsRegex = new RegExp("^(" + mixedPieces.join("|") + ")", "i");
  this._monthsShortRegex = this._monthsRegex;
  this._monthsStrictRegex = new RegExp(
    "^(" + longPieces.join("|") + ")",
    "i"
  );
  this._monthsShortStrictRegex = new RegExp(
    "^(" + shortPieces.join("|") + ")",
    "i"
  );
}
addFormatToken("Y", 0, 0, function() {
  var y = this.year();
  return y <= 9999 ? zeroFill(y, 4) : "+" + y;
});
addFormatToken(0, ["YY", 2], 0, function() {
  return this.year() % 100;
});
addFormatToken(0, ["YYYY", 4], 0, "year");
addFormatToken(0, ["YYYYY", 5], 0, "year");
addFormatToken(0, ["YYYYYY", 6, true], 0, "year");
addUnitAlias("year", "y");
addUnitPriority("year", 1);
addRegexToken("Y", matchSigned);
addRegexToken("YY", match1to2, match2);
addRegexToken("YYYY", match1to4, match4);
addRegexToken("YYYYY", match1to6, match6);
addRegexToken("YYYYYY", match1to6, match6);
addParseToken(["YYYYY", "YYYYYY"], YEAR);
addParseToken("YYYY", function(input, array) {
  array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
});
addParseToken("YY", function(input, array) {
  array[YEAR] = hooks.parseTwoDigitYear(input);
});
addParseToken("Y", function(input, array) {
  array[YEAR] = parseInt(input, 10);
});
function daysInYear(year) {
  return isLeapYear(year) ? 366 : 365;
}
hooks.parseTwoDigitYear = function(input) {
  return toInt(input) + (toInt(input) > 68 ? 1900 : 2e3);
};
var getSetYear = makeGetSet("FullYear", true);
function getIsLeapYear() {
  return isLeapYear(this.year());
}
function createDate(y, m, d2, h, M, s2, ms) {
  var date;
  if (y < 100 && y >= 0) {
    date = new Date(y + 400, m, d2, h, M, s2, ms);
    if (isFinite(date.getFullYear())) {
      date.setFullYear(y);
    }
  } else {
    date = new Date(y, m, d2, h, M, s2, ms);
  }
  return date;
}
function createUTCDate(y) {
  var date, args;
  if (y < 100 && y >= 0) {
    args = Array.prototype.slice.call(arguments);
    args[0] = y + 400;
    date = new Date(Date.UTC.apply(null, args));
    if (isFinite(date.getUTCFullYear())) {
      date.setUTCFullYear(y);
    }
  } else {
    date = new Date(Date.UTC.apply(null, arguments));
  }
  return date;
}
function firstWeekOffset(year, dow, doy) {
  var fwd = 7 + dow - doy, fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;
  return -fwdlw + fwd - 1;
}
function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
  var localWeekday = (7 + weekday - dow) % 7, weekOffset = firstWeekOffset(year, dow, doy), dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset, resYear, resDayOfYear;
  if (dayOfYear <= 0) {
    resYear = year - 1;
    resDayOfYear = daysInYear(resYear) + dayOfYear;
  } else if (dayOfYear > daysInYear(year)) {
    resYear = year + 1;
    resDayOfYear = dayOfYear - daysInYear(year);
  } else {
    resYear = year;
    resDayOfYear = dayOfYear;
  }
  return {
    year: resYear,
    dayOfYear: resDayOfYear
  };
}
function weekOfYear(mom, dow, doy) {
  var weekOffset = firstWeekOffset(mom.year(), dow, doy), week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1, resWeek, resYear;
  if (week < 1) {
    resYear = mom.year() - 1;
    resWeek = week + weeksInYear(resYear, dow, doy);
  } else if (week > weeksInYear(mom.year(), dow, doy)) {
    resWeek = week - weeksInYear(mom.year(), dow, doy);
    resYear = mom.year() + 1;
  } else {
    resYear = mom.year();
    resWeek = week;
  }
  return {
    week: resWeek,
    year: resYear
  };
}
function weeksInYear(year, dow, doy) {
  var weekOffset = firstWeekOffset(year, dow, doy), weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
  return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
}
addFormatToken("w", ["ww", 2], "wo", "week");
addFormatToken("W", ["WW", 2], "Wo", "isoWeek");
addUnitAlias("week", "w");
addUnitAlias("isoWeek", "W");
addUnitPriority("week", 5);
addUnitPriority("isoWeek", 5);
addRegexToken("w", match1to2);
addRegexToken("ww", match1to2, match2);
addRegexToken("W", match1to2);
addRegexToken("WW", match1to2, match2);
addWeekParseToken(
  ["w", "ww", "W", "WW"],
  function(input, week, config, token2) {
    week[token2.substr(0, 1)] = toInt(input);
  }
);
function localeWeek(mom) {
  return weekOfYear(mom, this._week.dow, this._week.doy).week;
}
var defaultLocaleWeek = {
  dow: 0,
  // Sunday is the first day of the week.
  doy: 6
  // The week that contains Jan 6th is the first week of the year.
};
function localeFirstDayOfWeek() {
  return this._week.dow;
}
function localeFirstDayOfYear() {
  return this._week.doy;
}
function getSetWeek(input) {
  var week = this.localeData().week(this);
  return input == null ? week : this.add((input - week) * 7, "d");
}
function getSetISOWeek(input) {
  var week = weekOfYear(this, 1, 4).week;
  return input == null ? week : this.add((input - week) * 7, "d");
}
addFormatToken("d", 0, "do", "day");
addFormatToken("dd", 0, 0, function(format2) {
  return this.localeData().weekdaysMin(this, format2);
});
addFormatToken("ddd", 0, 0, function(format2) {
  return this.localeData().weekdaysShort(this, format2);
});
addFormatToken("dddd", 0, 0, function(format2) {
  return this.localeData().weekdays(this, format2);
});
addFormatToken("e", 0, 0, "weekday");
addFormatToken("E", 0, 0, "isoWeekday");
addUnitAlias("day", "d");
addUnitAlias("weekday", "e");
addUnitAlias("isoWeekday", "E");
addUnitPriority("day", 11);
addUnitPriority("weekday", 11);
addUnitPriority("isoWeekday", 11);
addRegexToken("d", match1to2);
addRegexToken("e", match1to2);
addRegexToken("E", match1to2);
addRegexToken("dd", function(isStrict, locale2) {
  return locale2.weekdaysMinRegex(isStrict);
});
addRegexToken("ddd", function(isStrict, locale2) {
  return locale2.weekdaysShortRegex(isStrict);
});
addRegexToken("dddd", function(isStrict, locale2) {
  return locale2.weekdaysRegex(isStrict);
});
addWeekParseToken(["dd", "ddd", "dddd"], function(input, week, config, token2) {
  var weekday = config._locale.weekdaysParse(input, token2, config._strict);
  if (weekday != null) {
    week.d = weekday;
  } else {
    getParsingFlags(config).invalidWeekday = input;
  }
});
addWeekParseToken(["d", "e", "E"], function(input, week, config, token2) {
  week[token2] = toInt(input);
});
function parseWeekday(input, locale2) {
  if (typeof input !== "string") {
    return input;
  }
  if (!isNaN(input)) {
    return parseInt(input, 10);
  }
  input = locale2.weekdaysParse(input);
  if (typeof input === "number") {
    return input;
  }
  return null;
}
function parseIsoWeekday(input, locale2) {
  if (typeof input === "string") {
    return locale2.weekdaysParse(input) % 7 || 7;
  }
  return isNaN(input) ? null : input;
}
function shiftWeekdays(ws, n2) {
  return ws.slice(n2, 7).concat(ws.slice(0, n2));
}
var defaultLocaleWeekdays = "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"), defaultLocaleWeekdaysShort = "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"), defaultLocaleWeekdaysMin = "Su_Mo_Tu_We_Th_Fr_Sa".split("_"), defaultWeekdaysRegex = matchWord, defaultWeekdaysShortRegex = matchWord, defaultWeekdaysMinRegex = matchWord;
function localeWeekdays(m, format2) {
  var weekdays = isArray$1(this._weekdays) ? this._weekdays : this._weekdays[m && m !== true && this._weekdays.isFormat.test(format2) ? "format" : "standalone"];
  return m === true ? shiftWeekdays(weekdays, this._week.dow) : m ? weekdays[m.day()] : weekdays;
}
function localeWeekdaysShort(m) {
  return m === true ? shiftWeekdays(this._weekdaysShort, this._week.dow) : m ? this._weekdaysShort[m.day()] : this._weekdaysShort;
}
function localeWeekdaysMin(m) {
  return m === true ? shiftWeekdays(this._weekdaysMin, this._week.dow) : m ? this._weekdaysMin[m.day()] : this._weekdaysMin;
}
function handleStrictParse$1(weekdayName, format2, strict) {
  var i, ii, mom, llc = weekdayName.toLocaleLowerCase();
  if (!this._weekdaysParse) {
    this._weekdaysParse = [];
    this._shortWeekdaysParse = [];
    this._minWeekdaysParse = [];
    for (i = 0; i < 7; ++i) {
      mom = createUTC([2e3, 1]).day(i);
      this._minWeekdaysParse[i] = this.weekdaysMin(
        mom,
        ""
      ).toLocaleLowerCase();
      this._shortWeekdaysParse[i] = this.weekdaysShort(
        mom,
        ""
      ).toLocaleLowerCase();
      this._weekdaysParse[i] = this.weekdays(mom, "").toLocaleLowerCase();
    }
  }
  if (strict) {
    if (format2 === "dddd") {
      ii = indexOf.call(this._weekdaysParse, llc);
      return ii !== -1 ? ii : null;
    } else if (format2 === "ddd") {
      ii = indexOf.call(this._shortWeekdaysParse, llc);
      return ii !== -1 ? ii : null;
    } else {
      ii = indexOf.call(this._minWeekdaysParse, llc);
      return ii !== -1 ? ii : null;
    }
  } else {
    if (format2 === "dddd") {
      ii = indexOf.call(this._weekdaysParse, llc);
      if (ii !== -1) {
        return ii;
      }
      ii = indexOf.call(this._shortWeekdaysParse, llc);
      if (ii !== -1) {
        return ii;
      }
      ii = indexOf.call(this._minWeekdaysParse, llc);
      return ii !== -1 ? ii : null;
    } else if (format2 === "ddd") {
      ii = indexOf.call(this._shortWeekdaysParse, llc);
      if (ii !== -1) {
        return ii;
      }
      ii = indexOf.call(this._weekdaysParse, llc);
      if (ii !== -1) {
        return ii;
      }
      ii = indexOf.call(this._minWeekdaysParse, llc);
      return ii !== -1 ? ii : null;
    } else {
      ii = indexOf.call(this._minWeekdaysParse, llc);
      if (ii !== -1) {
        return ii;
      }
      ii = indexOf.call(this._weekdaysParse, llc);
      if (ii !== -1) {
        return ii;
      }
      ii = indexOf.call(this._shortWeekdaysParse, llc);
      return ii !== -1 ? ii : null;
    }
  }
}
function localeWeekdaysParse(weekdayName, format2, strict) {
  var i, mom, regex;
  if (this._weekdaysParseExact) {
    return handleStrictParse$1.call(this, weekdayName, format2, strict);
  }
  if (!this._weekdaysParse) {
    this._weekdaysParse = [];
    this._minWeekdaysParse = [];
    this._shortWeekdaysParse = [];
    this._fullWeekdaysParse = [];
  }
  for (i = 0; i < 7; i++) {
    mom = createUTC([2e3, 1]).day(i);
    if (strict && !this._fullWeekdaysParse[i]) {
      this._fullWeekdaysParse[i] = new RegExp(
        "^" + this.weekdays(mom, "").replace(".", "\\.?") + "$",
        "i"
      );
      this._shortWeekdaysParse[i] = new RegExp(
        "^" + this.weekdaysShort(mom, "").replace(".", "\\.?") + "$",
        "i"
      );
      this._minWeekdaysParse[i] = new RegExp(
        "^" + this.weekdaysMin(mom, "").replace(".", "\\.?") + "$",
        "i"
      );
    }
    if (!this._weekdaysParse[i]) {
      regex = "^" + this.weekdays(mom, "") + "|^" + this.weekdaysShort(mom, "") + "|^" + this.weekdaysMin(mom, "");
      this._weekdaysParse[i] = new RegExp(regex.replace(".", ""), "i");
    }
    if (strict && format2 === "dddd" && this._fullWeekdaysParse[i].test(weekdayName)) {
      return i;
    } else if (strict && format2 === "ddd" && this._shortWeekdaysParse[i].test(weekdayName)) {
      return i;
    } else if (strict && format2 === "dd" && this._minWeekdaysParse[i].test(weekdayName)) {
      return i;
    } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
      return i;
    }
  }
}
function getSetDayOfWeek(input) {
  if (!this.isValid()) {
    return input != null ? this : NaN;
  }
  var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
  if (input != null) {
    input = parseWeekday(input, this.localeData());
    return this.add(input - day, "d");
  } else {
    return day;
  }
}
function getSetLocaleDayOfWeek(input) {
  if (!this.isValid()) {
    return input != null ? this : NaN;
  }
  var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
  return input == null ? weekday : this.add(input - weekday, "d");
}
function getSetISODayOfWeek(input) {
  if (!this.isValid()) {
    return input != null ? this : NaN;
  }
  if (input != null) {
    var weekday = parseIsoWeekday(input, this.localeData());
    return this.day(this.day() % 7 ? weekday : weekday - 7);
  } else {
    return this.day() || 7;
  }
}
function weekdaysRegex(isStrict) {
  if (this._weekdaysParseExact) {
    if (!hasOwnProp(this, "_weekdaysRegex")) {
      computeWeekdaysParse.call(this);
    }
    if (isStrict) {
      return this._weekdaysStrictRegex;
    } else {
      return this._weekdaysRegex;
    }
  } else {
    if (!hasOwnProp(this, "_weekdaysRegex")) {
      this._weekdaysRegex = defaultWeekdaysRegex;
    }
    return this._weekdaysStrictRegex && isStrict ? this._weekdaysStrictRegex : this._weekdaysRegex;
  }
}
function weekdaysShortRegex(isStrict) {
  if (this._weekdaysParseExact) {
    if (!hasOwnProp(this, "_weekdaysRegex")) {
      computeWeekdaysParse.call(this);
    }
    if (isStrict) {
      return this._weekdaysShortStrictRegex;
    } else {
      return this._weekdaysShortRegex;
    }
  } else {
    if (!hasOwnProp(this, "_weekdaysShortRegex")) {
      this._weekdaysShortRegex = defaultWeekdaysShortRegex;
    }
    return this._weekdaysShortStrictRegex && isStrict ? this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
  }
}
function weekdaysMinRegex(isStrict) {
  if (this._weekdaysParseExact) {
    if (!hasOwnProp(this, "_weekdaysRegex")) {
      computeWeekdaysParse.call(this);
    }
    if (isStrict) {
      return this._weekdaysMinStrictRegex;
    } else {
      return this._weekdaysMinRegex;
    }
  } else {
    if (!hasOwnProp(this, "_weekdaysMinRegex")) {
      this._weekdaysMinRegex = defaultWeekdaysMinRegex;
    }
    return this._weekdaysMinStrictRegex && isStrict ? this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
  }
}
function computeWeekdaysParse() {
  function cmpLenRev(a, b) {
    return b.length - a.length;
  }
  var minPieces = [], shortPieces = [], longPieces = [], mixedPieces = [], i, mom, minp, shortp, longp;
  for (i = 0; i < 7; i++) {
    mom = createUTC([2e3, 1]).day(i);
    minp = regexEscape(this.weekdaysMin(mom, ""));
    shortp = regexEscape(this.weekdaysShort(mom, ""));
    longp = regexEscape(this.weekdays(mom, ""));
    minPieces.push(minp);
    shortPieces.push(shortp);
    longPieces.push(longp);
    mixedPieces.push(minp);
    mixedPieces.push(shortp);
    mixedPieces.push(longp);
  }
  minPieces.sort(cmpLenRev);
  shortPieces.sort(cmpLenRev);
  longPieces.sort(cmpLenRev);
  mixedPieces.sort(cmpLenRev);
  this._weekdaysRegex = new RegExp("^(" + mixedPieces.join("|") + ")", "i");
  this._weekdaysShortRegex = this._weekdaysRegex;
  this._weekdaysMinRegex = this._weekdaysRegex;
  this._weekdaysStrictRegex = new RegExp(
    "^(" + longPieces.join("|") + ")",
    "i"
  );
  this._weekdaysShortStrictRegex = new RegExp(
    "^(" + shortPieces.join("|") + ")",
    "i"
  );
  this._weekdaysMinStrictRegex = new RegExp(
    "^(" + minPieces.join("|") + ")",
    "i"
  );
}
function hFormat() {
  return this.hours() % 12 || 12;
}
function kFormat() {
  return this.hours() || 24;
}
addFormatToken("H", ["HH", 2], 0, "hour");
addFormatToken("h", ["hh", 2], 0, hFormat);
addFormatToken("k", ["kk", 2], 0, kFormat);
addFormatToken("hmm", 0, 0, function() {
  return "" + hFormat.apply(this) + zeroFill(this.minutes(), 2);
});
addFormatToken("hmmss", 0, 0, function() {
  return "" + hFormat.apply(this) + zeroFill(this.minutes(), 2) + zeroFill(this.seconds(), 2);
});
addFormatToken("Hmm", 0, 0, function() {
  return "" + this.hours() + zeroFill(this.minutes(), 2);
});
addFormatToken("Hmmss", 0, 0, function() {
  return "" + this.hours() + zeroFill(this.minutes(), 2) + zeroFill(this.seconds(), 2);
});
function meridiem(token2, lowercase) {
  addFormatToken(token2, 0, 0, function() {
    return this.localeData().meridiem(
      this.hours(),
      this.minutes(),
      lowercase
    );
  });
}
meridiem("a", true);
meridiem("A", false);
addUnitAlias("hour", "h");
addUnitPriority("hour", 13);
function matchMeridiem(isStrict, locale2) {
  return locale2._meridiemParse;
}
addRegexToken("a", matchMeridiem);
addRegexToken("A", matchMeridiem);
addRegexToken("H", match1to2);
addRegexToken("h", match1to2);
addRegexToken("k", match1to2);
addRegexToken("HH", match1to2, match2);
addRegexToken("hh", match1to2, match2);
addRegexToken("kk", match1to2, match2);
addRegexToken("hmm", match3to4);
addRegexToken("hmmss", match5to6);
addRegexToken("Hmm", match3to4);
addRegexToken("Hmmss", match5to6);
addParseToken(["H", "HH"], HOUR);
addParseToken(["k", "kk"], function(input, array, config) {
  var kInput = toInt(input);
  array[HOUR] = kInput === 24 ? 0 : kInput;
});
addParseToken(["a", "A"], function(input, array, config) {
  config._isPm = config._locale.isPM(input);
  config._meridiem = input;
});
addParseToken(["h", "hh"], function(input, array, config) {
  array[HOUR] = toInt(input);
  getParsingFlags(config).bigHour = true;
});
addParseToken("hmm", function(input, array, config) {
  var pos = input.length - 2;
  array[HOUR] = toInt(input.substr(0, pos));
  array[MINUTE] = toInt(input.substr(pos));
  getParsingFlags(config).bigHour = true;
});
addParseToken("hmmss", function(input, array, config) {
  var pos1 = input.length - 4, pos2 = input.length - 2;
  array[HOUR] = toInt(input.substr(0, pos1));
  array[MINUTE] = toInt(input.substr(pos1, 2));
  array[SECOND] = toInt(input.substr(pos2));
  getParsingFlags(config).bigHour = true;
});
addParseToken("Hmm", function(input, array, config) {
  var pos = input.length - 2;
  array[HOUR] = toInt(input.substr(0, pos));
  array[MINUTE] = toInt(input.substr(pos));
});
addParseToken("Hmmss", function(input, array, config) {
  var pos1 = input.length - 4, pos2 = input.length - 2;
  array[HOUR] = toInt(input.substr(0, pos1));
  array[MINUTE] = toInt(input.substr(pos1, 2));
  array[SECOND] = toInt(input.substr(pos2));
});
function localeIsPM(input) {
  return (input + "").toLowerCase().charAt(0) === "p";
}
var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i, getSetHour = makeGetSet("Hours", true);
function localeMeridiem(hours2, minutes2, isLower) {
  if (hours2 > 11) {
    return isLower ? "pm" : "PM";
  } else {
    return isLower ? "am" : "AM";
  }
}
var baseConfig = {
  calendar: defaultCalendar,
  longDateFormat: defaultLongDateFormat,
  invalidDate: defaultInvalidDate,
  ordinal: defaultOrdinal,
  dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
  relativeTime: defaultRelativeTime,
  months: defaultLocaleMonths,
  monthsShort: defaultLocaleMonthsShort,
  week: defaultLocaleWeek,
  weekdays: defaultLocaleWeekdays,
  weekdaysMin: defaultLocaleWeekdaysMin,
  weekdaysShort: defaultLocaleWeekdaysShort,
  meridiemParse: defaultLocaleMeridiemParse
};
var locales = {}, localeFamilies = {}, globalLocale;
function commonPrefix(arr1, arr2) {
  var i, minl = Math.min(arr1.length, arr2.length);
  for (i = 0; i < minl; i += 1) {
    if (arr1[i] !== arr2[i]) {
      return i;
    }
  }
  return minl;
}
function normalizeLocale(key) {
  return key ? key.toLowerCase().replace("_", "-") : key;
}
function chooseLocale(names) {
  var i = 0, j, next, locale2, split;
  while (i < names.length) {
    split = normalizeLocale(names[i]).split("-");
    j = split.length;
    next = normalizeLocale(names[i + 1]);
    next = next ? next.split("-") : null;
    while (j > 0) {
      locale2 = loadLocale(split.slice(0, j).join("-"));
      if (locale2) {
        return locale2;
      }
      if (next && next.length >= j && commonPrefix(split, next) >= j - 1) {
        break;
      }
      j--;
    }
    i++;
  }
  return globalLocale;
}
function isLocaleNameSane(name) {
  return name.match("^[^/\\\\]*$") != null;
}
function loadLocale(name) {
  var oldLocale = null, aliasedRequire;
  if (locales[name] === void 0 && typeof module !== "undefined" && module && module.exports && isLocaleNameSane(name)) {
    try {
      oldLocale = globalLocale._abbr;
      aliasedRequire = require;
      aliasedRequire("./locale/" + name);
      getSetGlobalLocale(oldLocale);
    } catch (e2) {
      locales[name] = null;
    }
  }
  return locales[name];
}
function getSetGlobalLocale(key, values) {
  var data;
  if (key) {
    if (isUndefined(values)) {
      data = getLocale(key);
    } else {
      data = defineLocale(key, values);
    }
    if (data) {
      globalLocale = data;
    } else {
      if (typeof console !== "undefined" && console.warn) {
        console.warn(
          "Locale " + key + " not found. Did you forget to load it?"
        );
      }
    }
  }
  return globalLocale._abbr;
}
function defineLocale(name, config) {
  if (config !== null) {
    var locale2, parentConfig = baseConfig;
    config.abbr = name;
    if (locales[name] != null) {
      deprecateSimple(
        "defineLocaleOverride",
        "use moment.updateLocale(localeName, config) to change an existing locale. moment.defineLocale(localeName, config) should only be used for creating a new locale See http://momentjs.com/guides/#/warnings/define-locale/ for more info."
      );
      parentConfig = locales[name]._config;
    } else if (config.parentLocale != null) {
      if (locales[config.parentLocale] != null) {
        parentConfig = locales[config.parentLocale]._config;
      } else {
        locale2 = loadLocale(config.parentLocale);
        if (locale2 != null) {
          parentConfig = locale2._config;
        } else {
          if (!localeFamilies[config.parentLocale]) {
            localeFamilies[config.parentLocale] = [];
          }
          localeFamilies[config.parentLocale].push({
            name,
            config
          });
          return null;
        }
      }
    }
    locales[name] = new Locale(mergeConfigs(parentConfig, config));
    if (localeFamilies[name]) {
      localeFamilies[name].forEach(function(x) {
        defineLocale(x.name, x.config);
      });
    }
    getSetGlobalLocale(name);
    return locales[name];
  } else {
    delete locales[name];
    return null;
  }
}
function updateLocale(name, config) {
  if (config != null) {
    var locale2, tmpLocale, parentConfig = baseConfig;
    if (locales[name] != null && locales[name].parentLocale != null) {
      locales[name].set(mergeConfigs(locales[name]._config, config));
    } else {
      tmpLocale = loadLocale(name);
      if (tmpLocale != null) {
        parentConfig = tmpLocale._config;
      }
      config = mergeConfigs(parentConfig, config);
      if (tmpLocale == null) {
        config.abbr = name;
      }
      locale2 = new Locale(config);
      locale2.parentLocale = locales[name];
      locales[name] = locale2;
    }
    getSetGlobalLocale(name);
  } else {
    if (locales[name] != null) {
      if (locales[name].parentLocale != null) {
        locales[name] = locales[name].parentLocale;
        if (name === getSetGlobalLocale()) {
          getSetGlobalLocale(name);
        }
      } else if (locales[name] != null) {
        delete locales[name];
      }
    }
  }
  return locales[name];
}
function getLocale(key) {
  var locale2;
  if (key && key._locale && key._locale._abbr) {
    key = key._locale._abbr;
  }
  if (!key) {
    return globalLocale;
  }
  if (!isArray$1(key)) {
    locale2 = loadLocale(key);
    if (locale2) {
      return locale2;
    }
    key = [key];
  }
  return chooseLocale(key);
}
function listLocales() {
  return keys(locales);
}
function checkOverflow(m) {
  var overflow, a = m._a;
  if (a && getParsingFlags(m).overflow === -2) {
    overflow = a[MONTH] < 0 || a[MONTH] > 11 ? MONTH : a[DATE] < 1 || a[DATE] > daysInMonth(a[YEAR], a[MONTH]) ? DATE : a[HOUR] < 0 || a[HOUR] > 24 || a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0) ? HOUR : a[MINUTE] < 0 || a[MINUTE] > 59 ? MINUTE : a[SECOND] < 0 || a[SECOND] > 59 ? SECOND : a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND : -1;
    if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
      overflow = DATE;
    }
    if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
      overflow = WEEK;
    }
    if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
      overflow = WEEKDAY;
    }
    getParsingFlags(m).overflow = overflow;
  }
  return m;
}
var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([+-]\d\d(?::?\d\d)?|\s*Z)?)?$/, basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d|))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([+-]\d\d(?::?\d\d)?|\s*Z)?)?$/, tzRegex = /Z|[+-]\d\d(?::?\d\d)?/, isoDates = [
  ["YYYYYY-MM-DD", /[+-]\d{6}-\d\d-\d\d/],
  ["YYYY-MM-DD", /\d{4}-\d\d-\d\d/],
  ["GGGG-[W]WW-E", /\d{4}-W\d\d-\d/],
  ["GGGG-[W]WW", /\d{4}-W\d\d/, false],
  ["YYYY-DDD", /\d{4}-\d{3}/],
  ["YYYY-MM", /\d{4}-\d\d/, false],
  ["YYYYYYMMDD", /[+-]\d{10}/],
  ["YYYYMMDD", /\d{8}/],
  ["GGGG[W]WWE", /\d{4}W\d{3}/],
  ["GGGG[W]WW", /\d{4}W\d{2}/, false],
  ["YYYYDDD", /\d{7}/],
  ["YYYYMM", /\d{6}/, false],
  ["YYYY", /\d{4}/, false]
], isoTimes = [
  ["HH:mm:ss.SSSS", /\d\d:\d\d:\d\d\.\d+/],
  ["HH:mm:ss,SSSS", /\d\d:\d\d:\d\d,\d+/],
  ["HH:mm:ss", /\d\d:\d\d:\d\d/],
  ["HH:mm", /\d\d:\d\d/],
  ["HHmmss.SSSS", /\d\d\d\d\d\d\.\d+/],
  ["HHmmss,SSSS", /\d\d\d\d\d\d,\d+/],
  ["HHmmss", /\d\d\d\d\d\d/],
  ["HHmm", /\d\d\d\d/],
  ["HH", /\d\d/]
], aspNetJsonRegex = /^\/?Date\((-?\d+)/i, rfc2822 = /^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/, obsOffsets = {
  UT: 0,
  GMT: 0,
  EDT: -4 * 60,
  EST: -5 * 60,
  CDT: -5 * 60,
  CST: -6 * 60,
  MDT: -6 * 60,
  MST: -7 * 60,
  PDT: -7 * 60,
  PST: -8 * 60
};
function configFromISO(config) {
  var i, l, string = config._i, match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string), allowTime, dateFormat, timeFormat, tzFormat, isoDatesLen = isoDates.length, isoTimesLen = isoTimes.length;
  if (match) {
    getParsingFlags(config).iso = true;
    for (i = 0, l = isoDatesLen; i < l; i++) {
      if (isoDates[i][1].exec(match[1])) {
        dateFormat = isoDates[i][0];
        allowTime = isoDates[i][2] !== false;
        break;
      }
    }
    if (dateFormat == null) {
      config._isValid = false;
      return;
    }
    if (match[3]) {
      for (i = 0, l = isoTimesLen; i < l; i++) {
        if (isoTimes[i][1].exec(match[3])) {
          timeFormat = (match[2] || " ") + isoTimes[i][0];
          break;
        }
      }
      if (timeFormat == null) {
        config._isValid = false;
        return;
      }
    }
    if (!allowTime && timeFormat != null) {
      config._isValid = false;
      return;
    }
    if (match[4]) {
      if (tzRegex.exec(match[4])) {
        tzFormat = "Z";
      } else {
        config._isValid = false;
        return;
      }
    }
    config._f = dateFormat + (timeFormat || "") + (tzFormat || "");
    configFromStringAndFormat(config);
  } else {
    config._isValid = false;
  }
}
function extractFromRFC2822Strings(yearStr, monthStr, dayStr, hourStr, minuteStr, secondStr) {
  var result = [
    untruncateYear(yearStr),
    defaultLocaleMonthsShort.indexOf(monthStr),
    parseInt(dayStr, 10),
    parseInt(hourStr, 10),
    parseInt(minuteStr, 10)
  ];
  if (secondStr) {
    result.push(parseInt(secondStr, 10));
  }
  return result;
}
function untruncateYear(yearStr) {
  var year = parseInt(yearStr, 10);
  if (year <= 49) {
    return 2e3 + year;
  } else if (year <= 999) {
    return 1900 + year;
  }
  return year;
}
function preprocessRFC2822(s2) {
  return s2.replace(/\([^()]*\)|[\n\t]/g, " ").replace(/(\s\s+)/g, " ").replace(/^\s\s*/, "").replace(/\s\s*$/, "");
}
function checkWeekday(weekdayStr, parsedInput, config) {
  if (weekdayStr) {
    var weekdayProvided = defaultLocaleWeekdaysShort.indexOf(weekdayStr), weekdayActual = new Date(
      parsedInput[0],
      parsedInput[1],
      parsedInput[2]
    ).getDay();
    if (weekdayProvided !== weekdayActual) {
      getParsingFlags(config).weekdayMismatch = true;
      config._isValid = false;
      return false;
    }
  }
  return true;
}
function calculateOffset(obsOffset, militaryOffset, numOffset) {
  if (obsOffset) {
    return obsOffsets[obsOffset];
  } else if (militaryOffset) {
    return 0;
  } else {
    var hm = parseInt(numOffset, 10), m = hm % 100, h = (hm - m) / 100;
    return h * 60 + m;
  }
}
function configFromRFC2822(config) {
  var match = rfc2822.exec(preprocessRFC2822(config._i)), parsedArray;
  if (match) {
    parsedArray = extractFromRFC2822Strings(
      match[4],
      match[3],
      match[2],
      match[5],
      match[6],
      match[7]
    );
    if (!checkWeekday(match[1], parsedArray, config)) {
      return;
    }
    config._a = parsedArray;
    config._tzm = calculateOffset(match[8], match[9], match[10]);
    config._d = createUTCDate.apply(null, config._a);
    config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
    getParsingFlags(config).rfc2822 = true;
  } else {
    config._isValid = false;
  }
}
function configFromString(config) {
  var matched = aspNetJsonRegex.exec(config._i);
  if (matched !== null) {
    config._d = /* @__PURE__ */ new Date(+matched[1]);
    return;
  }
  configFromISO(config);
  if (config._isValid === false) {
    delete config._isValid;
  } else {
    return;
  }
  configFromRFC2822(config);
  if (config._isValid === false) {
    delete config._isValid;
  } else {
    return;
  }
  if (config._strict) {
    config._isValid = false;
  } else {
    hooks.createFromInputFallback(config);
  }
}
hooks.createFromInputFallback = deprecate(
  "value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are discouraged. Please refer to http://momentjs.com/guides/#/warnings/js-date/ for more info.",
  function(config) {
    config._d = /* @__PURE__ */ new Date(config._i + (config._useUTC ? " UTC" : ""));
  }
);
function defaults(a, b, c) {
  if (a != null) {
    return a;
  }
  if (b != null) {
    return b;
  }
  return c;
}
function currentDateArray(config) {
  var nowValue = new Date(hooks.now());
  if (config._useUTC) {
    return [
      nowValue.getUTCFullYear(),
      nowValue.getUTCMonth(),
      nowValue.getUTCDate()
    ];
  }
  return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
}
function configFromArray(config) {
  var i, date, input = [], currentDate, expectedWeekday, yearToUse;
  if (config._d) {
    return;
  }
  currentDate = currentDateArray(config);
  if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
    dayOfYearFromWeekInfo(config);
  }
  if (config._dayOfYear != null) {
    yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);
    if (config._dayOfYear > daysInYear(yearToUse) || config._dayOfYear === 0) {
      getParsingFlags(config)._overflowDayOfYear = true;
    }
    date = createUTCDate(yearToUse, 0, config._dayOfYear);
    config._a[MONTH] = date.getUTCMonth();
    config._a[DATE] = date.getUTCDate();
  }
  for (i = 0; i < 3 && config._a[i] == null; ++i) {
    config._a[i] = input[i] = currentDate[i];
  }
  for (; i < 7; i++) {
    config._a[i] = input[i] = config._a[i] == null ? i === 2 ? 1 : 0 : config._a[i];
  }
  if (config._a[HOUR] === 24 && config._a[MINUTE] === 0 && config._a[SECOND] === 0 && config._a[MILLISECOND] === 0) {
    config._nextDay = true;
    config._a[HOUR] = 0;
  }
  config._d = (config._useUTC ? createUTCDate : createDate).apply(
    null,
    input
  );
  expectedWeekday = config._useUTC ? config._d.getUTCDay() : config._d.getDay();
  if (config._tzm != null) {
    config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
  }
  if (config._nextDay) {
    config._a[HOUR] = 24;
  }
  if (config._w && typeof config._w.d !== "undefined" && config._w.d !== expectedWeekday) {
    getParsingFlags(config).weekdayMismatch = true;
  }
}
function dayOfYearFromWeekInfo(config) {
  var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow, curWeek;
  w = config._w;
  if (w.GG != null || w.W != null || w.E != null) {
    dow = 1;
    doy = 4;
    weekYear = defaults(
      w.GG,
      config._a[YEAR],
      weekOfYear(createLocal(), 1, 4).year
    );
    week = defaults(w.W, 1);
    weekday = defaults(w.E, 1);
    if (weekday < 1 || weekday > 7) {
      weekdayOverflow = true;
    }
  } else {
    dow = config._locale._week.dow;
    doy = config._locale._week.doy;
    curWeek = weekOfYear(createLocal(), dow, doy);
    weekYear = defaults(w.gg, config._a[YEAR], curWeek.year);
    week = defaults(w.w, curWeek.week);
    if (w.d != null) {
      weekday = w.d;
      if (weekday < 0 || weekday > 6) {
        weekdayOverflow = true;
      }
    } else if (w.e != null) {
      weekday = w.e + dow;
      if (w.e < 0 || w.e > 6) {
        weekdayOverflow = true;
      }
    } else {
      weekday = dow;
    }
  }
  if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
    getParsingFlags(config)._overflowWeeks = true;
  } else if (weekdayOverflow != null) {
    getParsingFlags(config)._overflowWeekday = true;
  } else {
    temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
    config._a[YEAR] = temp.year;
    config._dayOfYear = temp.dayOfYear;
  }
}
hooks.ISO_8601 = function() {
};
hooks.RFC_2822 = function() {
};
function configFromStringAndFormat(config) {
  if (config._f === hooks.ISO_8601) {
    configFromISO(config);
    return;
  }
  if (config._f === hooks.RFC_2822) {
    configFromRFC2822(config);
    return;
  }
  config._a = [];
  getParsingFlags(config).empty = true;
  var string = "" + config._i, i, parsedInput, tokens2, token2, skipped, stringLength = string.length, totalParsedInputLength = 0, era, tokenLen;
  tokens2 = expandFormat(config._f, config._locale).match(formattingTokens) || [];
  tokenLen = tokens2.length;
  for (i = 0; i < tokenLen; i++) {
    token2 = tokens2[i];
    parsedInput = (string.match(getParseRegexForToken(token2, config)) || [])[0];
    if (parsedInput) {
      skipped = string.substr(0, string.indexOf(parsedInput));
      if (skipped.length > 0) {
        getParsingFlags(config).unusedInput.push(skipped);
      }
      string = string.slice(
        string.indexOf(parsedInput) + parsedInput.length
      );
      totalParsedInputLength += parsedInput.length;
    }
    if (formatTokenFunctions[token2]) {
      if (parsedInput) {
        getParsingFlags(config).empty = false;
      } else {
        getParsingFlags(config).unusedTokens.push(token2);
      }
      addTimeToArrayFromToken(token2, parsedInput, config);
    } else if (config._strict && !parsedInput) {
      getParsingFlags(config).unusedTokens.push(token2);
    }
  }
  getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
  if (string.length > 0) {
    getParsingFlags(config).unusedInput.push(string);
  }
  if (config._a[HOUR] <= 12 && getParsingFlags(config).bigHour === true && config._a[HOUR] > 0) {
    getParsingFlags(config).bigHour = void 0;
  }
  getParsingFlags(config).parsedDateParts = config._a.slice(0);
  getParsingFlags(config).meridiem = config._meridiem;
  config._a[HOUR] = meridiemFixWrap(
    config._locale,
    config._a[HOUR],
    config._meridiem
  );
  era = getParsingFlags(config).era;
  if (era !== null) {
    config._a[YEAR] = config._locale.erasConvertYear(era, config._a[YEAR]);
  }
  configFromArray(config);
  checkOverflow(config);
}
function meridiemFixWrap(locale2, hour, meridiem2) {
  var isPm;
  if (meridiem2 == null) {
    return hour;
  }
  if (locale2.meridiemHour != null) {
    return locale2.meridiemHour(hour, meridiem2);
  } else if (locale2.isPM != null) {
    isPm = locale2.isPM(meridiem2);
    if (isPm && hour < 12) {
      hour += 12;
    }
    if (!isPm && hour === 12) {
      hour = 0;
    }
    return hour;
  } else {
    return hour;
  }
}
function configFromStringAndArray(config) {
  var tempConfig, bestMoment, scoreToBeat, i, currentScore, validFormatFound, bestFormatIsValid = false, configfLen = config._f.length;
  if (configfLen === 0) {
    getParsingFlags(config).invalidFormat = true;
    config._d = /* @__PURE__ */ new Date(NaN);
    return;
  }
  for (i = 0; i < configfLen; i++) {
    currentScore = 0;
    validFormatFound = false;
    tempConfig = copyConfig({}, config);
    if (config._useUTC != null) {
      tempConfig._useUTC = config._useUTC;
    }
    tempConfig._f = config._f[i];
    configFromStringAndFormat(tempConfig);
    if (isValid(tempConfig)) {
      validFormatFound = true;
    }
    currentScore += getParsingFlags(tempConfig).charsLeftOver;
    currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;
    getParsingFlags(tempConfig).score = currentScore;
    if (!bestFormatIsValid) {
      if (scoreToBeat == null || currentScore < scoreToBeat || validFormatFound) {
        scoreToBeat = currentScore;
        bestMoment = tempConfig;
        if (validFormatFound) {
          bestFormatIsValid = true;
        }
      }
    } else {
      if (currentScore < scoreToBeat) {
        scoreToBeat = currentScore;
        bestMoment = tempConfig;
      }
    }
  }
  extend(config, bestMoment || tempConfig);
}
function configFromObject(config) {
  if (config._d) {
    return;
  }
  var i = normalizeObjectUnits(config._i), dayOrDate = i.day === void 0 ? i.date : i.day;
  config._a = map(
    [i.year, i.month, dayOrDate, i.hour, i.minute, i.second, i.millisecond],
    function(obj) {
      return obj && parseInt(obj, 10);
    }
  );
  configFromArray(config);
}
function createFromConfig(config) {
  var res = new Moment(checkOverflow(prepareConfig(config)));
  if (res._nextDay) {
    res.add(1, "d");
    res._nextDay = void 0;
  }
  return res;
}
function prepareConfig(config) {
  var input = config._i, format2 = config._f;
  config._locale = config._locale || getLocale(config._l);
  if (input === null || format2 === void 0 && input === "") {
    return createInvalid({ nullInput: true });
  }
  if (typeof input === "string") {
    config._i = input = config._locale.preparse(input);
  }
  if (isMoment(input)) {
    return new Moment(checkOverflow(input));
  } else if (isDate(input)) {
    config._d = input;
  } else if (isArray$1(format2)) {
    configFromStringAndArray(config);
  } else if (format2) {
    configFromStringAndFormat(config);
  } else {
    configFromInput(config);
  }
  if (!isValid(config)) {
    config._d = null;
  }
  return config;
}
function configFromInput(config) {
  var input = config._i;
  if (isUndefined(input)) {
    config._d = new Date(hooks.now());
  } else if (isDate(input)) {
    config._d = new Date(input.valueOf());
  } else if (typeof input === "string") {
    configFromString(config);
  } else if (isArray$1(input)) {
    config._a = map(input.slice(0), function(obj) {
      return parseInt(obj, 10);
    });
    configFromArray(config);
  } else if (isObject(input)) {
    configFromObject(config);
  } else if (isNumber(input)) {
    config._d = new Date(input);
  } else {
    hooks.createFromInputFallback(config);
  }
}
function createLocalOrUTC(input, format2, locale2, strict, isUTC) {
  var c = {};
  if (format2 === true || format2 === false) {
    strict = format2;
    format2 = void 0;
  }
  if (locale2 === true || locale2 === false) {
    strict = locale2;
    locale2 = void 0;
  }
  if (isObject(input) && isObjectEmpty(input) || isArray$1(input) && input.length === 0) {
    input = void 0;
  }
  c._isAMomentObject = true;
  c._useUTC = c._isUTC = isUTC;
  c._l = locale2;
  c._i = input;
  c._f = format2;
  c._strict = strict;
  return createFromConfig(c);
}
function createLocal(input, format2, locale2, strict) {
  return createLocalOrUTC(input, format2, locale2, strict, false);
}
var prototypeMin = deprecate(
  "moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/",
  function() {
    var other = createLocal.apply(null, arguments);
    if (this.isValid() && other.isValid()) {
      return other < this ? this : other;
    } else {
      return createInvalid();
    }
  }
), prototypeMax = deprecate(
  "moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/",
  function() {
    var other = createLocal.apply(null, arguments);
    if (this.isValid() && other.isValid()) {
      return other > this ? this : other;
    } else {
      return createInvalid();
    }
  }
);
function pickBy(fn, moments) {
  var res, i;
  if (moments.length === 1 && isArray$1(moments[0])) {
    moments = moments[0];
  }
  if (!moments.length) {
    return createLocal();
  }
  res = moments[0];
  for (i = 1; i < moments.length; ++i) {
    if (!moments[i].isValid() || moments[i][fn](res)) {
      res = moments[i];
    }
  }
  return res;
}
function min() {
  var args = [].slice.call(arguments, 0);
  return pickBy("isBefore", args);
}
function max() {
  var args = [].slice.call(arguments, 0);
  return pickBy("isAfter", args);
}
var now = function() {
  return Date.now ? Date.now() : +/* @__PURE__ */ new Date();
};
var ordering = [
  "year",
  "quarter",
  "month",
  "week",
  "day",
  "hour",
  "minute",
  "second",
  "millisecond"
];
function isDurationValid(m) {
  var key, unitHasDecimal = false, i, orderLen = ordering.length;
  for (key in m) {
    if (hasOwnProp(m, key) && !(indexOf.call(ordering, key) !== -1 && (m[key] == null || !isNaN(m[key])))) {
      return false;
    }
  }
  for (i = 0; i < orderLen; ++i) {
    if (m[ordering[i]]) {
      if (unitHasDecimal) {
        return false;
      }
      if (parseFloat(m[ordering[i]]) !== toInt(m[ordering[i]])) {
        unitHasDecimal = true;
      }
    }
  }
  return true;
}
function isValid$1() {
  return this._isValid;
}
function createInvalid$1() {
  return createDuration(NaN);
}
function Duration(duration) {
  var normalizedInput = normalizeObjectUnits(duration), years2 = normalizedInput.year || 0, quarters = normalizedInput.quarter || 0, months2 = normalizedInput.month || 0, weeks2 = normalizedInput.week || normalizedInput.isoWeek || 0, days2 = normalizedInput.day || 0, hours2 = normalizedInput.hour || 0, minutes2 = normalizedInput.minute || 0, seconds2 = normalizedInput.second || 0, milliseconds2 = normalizedInput.millisecond || 0;
  this._isValid = isDurationValid(normalizedInput);
  this._milliseconds = +milliseconds2 + seconds2 * 1e3 + // 1000
  minutes2 * 6e4 + // 1000 * 60
  hours2 * 1e3 * 60 * 60;
  this._days = +days2 + weeks2 * 7;
  this._months = +months2 + quarters * 3 + years2 * 12;
  this._data = {};
  this._locale = getLocale();
  this._bubble();
}
function isDuration(obj) {
  return obj instanceof Duration;
}
function absRound(number) {
  if (number < 0) {
    return Math.round(-1 * number) * -1;
  } else {
    return Math.round(number);
  }
}
function compareArrays(array1, array2, dontConvert) {
  var len = Math.min(array1.length, array2.length), lengthDiff = Math.abs(array1.length - array2.length), diffs = 0, i;
  for (i = 0; i < len; i++) {
    if (dontConvert && array1[i] !== array2[i] || !dontConvert && toInt(array1[i]) !== toInt(array2[i])) {
      diffs++;
    }
  }
  return diffs + lengthDiff;
}
function offset(token2, separator) {
  addFormatToken(token2, 0, 0, function() {
    var offset2 = this.utcOffset(), sign2 = "+";
    if (offset2 < 0) {
      offset2 = -offset2;
      sign2 = "-";
    }
    return sign2 + zeroFill(~~(offset2 / 60), 2) + separator + zeroFill(~~offset2 % 60, 2);
  });
}
offset("Z", ":");
offset("ZZ", "");
addRegexToken("Z", matchShortOffset);
addRegexToken("ZZ", matchShortOffset);
addParseToken(["Z", "ZZ"], function(input, array, config) {
  config._useUTC = true;
  config._tzm = offsetFromString(matchShortOffset, input);
});
var chunkOffset = /([\+\-]|\d\d)/gi;
function offsetFromString(matcher, string) {
  var matches = (string || "").match(matcher), chunk, parts, minutes2;
  if (matches === null) {
    return null;
  }
  chunk = matches[matches.length - 1] || [];
  parts = (chunk + "").match(chunkOffset) || ["-", 0, 0];
  minutes2 = +(parts[1] * 60) + toInt(parts[2]);
  return minutes2 === 0 ? 0 : parts[0] === "+" ? minutes2 : -minutes2;
}
function cloneWithOffset(input, model) {
  var res, diff2;
  if (model._isUTC) {
    res = model.clone();
    diff2 = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf();
    res._d.setTime(res._d.valueOf() + diff2);
    hooks.updateOffset(res, false);
    return res;
  } else {
    return createLocal(input).local();
  }
}
function getDateOffset(m) {
  return -Math.round(m._d.getTimezoneOffset());
}
hooks.updateOffset = function() {
};
function getSetOffset(input, keepLocalTime, keepMinutes) {
  var offset2 = this._offset || 0, localAdjust;
  if (!this.isValid()) {
    return input != null ? this : NaN;
  }
  if (input != null) {
    if (typeof input === "string") {
      input = offsetFromString(matchShortOffset, input);
      if (input === null) {
        return this;
      }
    } else if (Math.abs(input) < 16 && !keepMinutes) {
      input = input * 60;
    }
    if (!this._isUTC && keepLocalTime) {
      localAdjust = getDateOffset(this);
    }
    this._offset = input;
    this._isUTC = true;
    if (localAdjust != null) {
      this.add(localAdjust, "m");
    }
    if (offset2 !== input) {
      if (!keepLocalTime || this._changeInProgress) {
        addSubtract(
          this,
          createDuration(input - offset2, "m"),
          1,
          false
        );
      } else if (!this._changeInProgress) {
        this._changeInProgress = true;
        hooks.updateOffset(this, true);
        this._changeInProgress = null;
      }
    }
    return this;
  } else {
    return this._isUTC ? offset2 : getDateOffset(this);
  }
}
function getSetZone(input, keepLocalTime) {
  if (input != null) {
    if (typeof input !== "string") {
      input = -input;
    }
    this.utcOffset(input, keepLocalTime);
    return this;
  } else {
    return -this.utcOffset();
  }
}
function setOffsetToUTC(keepLocalTime) {
  return this.utcOffset(0, keepLocalTime);
}
function setOffsetToLocal(keepLocalTime) {
  if (this._isUTC) {
    this.utcOffset(0, keepLocalTime);
    this._isUTC = false;
    if (keepLocalTime) {
      this.subtract(getDateOffset(this), "m");
    }
  }
  return this;
}
function setOffsetToParsedOffset() {
  if (this._tzm != null) {
    this.utcOffset(this._tzm, false, true);
  } else if (typeof this._i === "string") {
    var tZone = offsetFromString(matchOffset, this._i);
    if (tZone != null) {
      this.utcOffset(tZone);
    } else {
      this.utcOffset(0, true);
    }
  }
  return this;
}
function hasAlignedHourOffset(input) {
  if (!this.isValid()) {
    return false;
  }
  input = input ? createLocal(input).utcOffset() : 0;
  return (this.utcOffset() - input) % 60 === 0;
}
function isDaylightSavingTime() {
  return this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset();
}
function isDaylightSavingTimeShifted() {
  if (!isUndefined(this._isDSTShifted)) {
    return this._isDSTShifted;
  }
  var c = {}, other;
  copyConfig(c, this);
  c = prepareConfig(c);
  if (c._a) {
    other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
    this._isDSTShifted = this.isValid() && compareArrays(c._a, other.toArray()) > 0;
  } else {
    this._isDSTShifted = false;
  }
  return this._isDSTShifted;
}
function isLocal() {
  return this.isValid() ? !this._isUTC : false;
}
function isUtcOffset() {
  return this.isValid() ? this._isUTC : false;
}
function isUtc() {
  return this.isValid() ? this._isUTC && this._offset === 0 : false;
}
var aspNetRegex = /^(-|\+)?(?:(\d*)[. ])?(\d+):(\d+)(?::(\d+)(\.\d*)?)?$/, isoRegex = /^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;
function createDuration(input, key) {
  var duration = input, match = null, sign2, ret, diffRes;
  if (isDuration(input)) {
    duration = {
      ms: input._milliseconds,
      d: input._days,
      M: input._months
    };
  } else if (isNumber(input) || !isNaN(+input)) {
    duration = {};
    if (key) {
      duration[key] = +input;
    } else {
      duration.milliseconds = +input;
    }
  } else if (match = aspNetRegex.exec(input)) {
    sign2 = match[1] === "-" ? -1 : 1;
    duration = {
      y: 0,
      d: toInt(match[DATE]) * sign2,
      h: toInt(match[HOUR]) * sign2,
      m: toInt(match[MINUTE]) * sign2,
      s: toInt(match[SECOND]) * sign2,
      ms: toInt(absRound(match[MILLISECOND] * 1e3)) * sign2
      // the millisecond decimal point is included in the match
    };
  } else if (match = isoRegex.exec(input)) {
    sign2 = match[1] === "-" ? -1 : 1;
    duration = {
      y: parseIso(match[2], sign2),
      M: parseIso(match[3], sign2),
      w: parseIso(match[4], sign2),
      d: parseIso(match[5], sign2),
      h: parseIso(match[6], sign2),
      m: parseIso(match[7], sign2),
      s: parseIso(match[8], sign2)
    };
  } else if (duration == null) {
    duration = {};
  } else if (typeof duration === "object" && ("from" in duration || "to" in duration)) {
    diffRes = momentsDifference(
      createLocal(duration.from),
      createLocal(duration.to)
    );
    duration = {};
    duration.ms = diffRes.milliseconds;
    duration.M = diffRes.months;
  }
  ret = new Duration(duration);
  if (isDuration(input) && hasOwnProp(input, "_locale")) {
    ret._locale = input._locale;
  }
  if (isDuration(input) && hasOwnProp(input, "_isValid")) {
    ret._isValid = input._isValid;
  }
  return ret;
}
createDuration.fn = Duration.prototype;
createDuration.invalid = createInvalid$1;
function parseIso(inp, sign2) {
  var res = inp && parseFloat(inp.replace(",", "."));
  return (isNaN(res) ? 0 : res) * sign2;
}
function positiveMomentsDifference(base, other) {
  var res = {};
  res.months = other.month() - base.month() + (other.year() - base.year()) * 12;
  if (base.clone().add(res.months, "M").isAfter(other)) {
    --res.months;
  }
  res.milliseconds = +other - +base.clone().add(res.months, "M");
  return res;
}
function momentsDifference(base, other) {
  var res;
  if (!(base.isValid() && other.isValid())) {
    return { milliseconds: 0, months: 0 };
  }
  other = cloneWithOffset(other, base);
  if (base.isBefore(other)) {
    res = positiveMomentsDifference(base, other);
  } else {
    res = positiveMomentsDifference(other, base);
    res.milliseconds = -res.milliseconds;
    res.months = -res.months;
  }
  return res;
}
function createAdder(direction, name) {
  return function(val, period) {
    var dur, tmp;
    if (period !== null && !isNaN(+period)) {
      deprecateSimple(
        name,
        "moment()." + name + "(period, number) is deprecated. Please use moment()." + name + "(number, period). See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info."
      );
      tmp = val;
      val = period;
      period = tmp;
    }
    dur = createDuration(val, period);
    addSubtract(this, dur, direction);
    return this;
  };
}
function addSubtract(mom, duration, isAdding, updateOffset) {
  var milliseconds2 = duration._milliseconds, days2 = absRound(duration._days), months2 = absRound(duration._months);
  if (!mom.isValid()) {
    return;
  }
  updateOffset = updateOffset == null ? true : updateOffset;
  if (months2) {
    setMonth(mom, get(mom, "Month") + months2 * isAdding);
  }
  if (days2) {
    set$1(mom, "Date", get(mom, "Date") + days2 * isAdding);
  }
  if (milliseconds2) {
    mom._d.setTime(mom._d.valueOf() + milliseconds2 * isAdding);
  }
  if (updateOffset) {
    hooks.updateOffset(mom, days2 || months2);
  }
}
var add = createAdder(1, "add"), subtract = createAdder(-1, "subtract");
function isString(input) {
  return typeof input === "string" || input instanceof String;
}
function isMomentInput(input) {
  return isMoment(input) || isDate(input) || isString(input) || isNumber(input) || isNumberOrStringArray(input) || isMomentInputObject(input) || input === null || input === void 0;
}
function isMomentInputObject(input) {
  var objectTest = isObject(input) && !isObjectEmpty(input), propertyTest = false, properties = [
    "years",
    "year",
    "y",
    "months",
    "month",
    "M",
    "days",
    "day",
    "d",
    "dates",
    "date",
    "D",
    "hours",
    "hour",
    "h",
    "minutes",
    "minute",
    "m",
    "seconds",
    "second",
    "s",
    "milliseconds",
    "millisecond",
    "ms"
  ], i, property, propertyLen = properties.length;
  for (i = 0; i < propertyLen; i += 1) {
    property = properties[i];
    propertyTest = propertyTest || hasOwnProp(input, property);
  }
  return objectTest && propertyTest;
}
function isNumberOrStringArray(input) {
  var arrayTest = isArray$1(input), dataTypeTest = false;
  if (arrayTest) {
    dataTypeTest = input.filter(function(item) {
      return !isNumber(item) && isString(input);
    }).length === 0;
  }
  return arrayTest && dataTypeTest;
}
function isCalendarSpec(input) {
  var objectTest = isObject(input) && !isObjectEmpty(input), propertyTest = false, properties = [
    "sameDay",
    "nextDay",
    "lastDay",
    "nextWeek",
    "lastWeek",
    "sameElse"
  ], i, property;
  for (i = 0; i < properties.length; i += 1) {
    property = properties[i];
    propertyTest = propertyTest || hasOwnProp(input, property);
  }
  return objectTest && propertyTest;
}
function getCalendarFormat(myMoment, now2) {
  var diff2 = myMoment.diff(now2, "days", true);
  return diff2 < -6 ? "sameElse" : diff2 < -1 ? "lastWeek" : diff2 < 0 ? "lastDay" : diff2 < 1 ? "sameDay" : diff2 < 2 ? "nextDay" : diff2 < 7 ? "nextWeek" : "sameElse";
}
function calendar$1(time, formats) {
  if (arguments.length === 1) {
    if (!arguments[0]) {
      time = void 0;
      formats = void 0;
    } else if (isMomentInput(arguments[0])) {
      time = arguments[0];
      formats = void 0;
    } else if (isCalendarSpec(arguments[0])) {
      formats = arguments[0];
      time = void 0;
    }
  }
  var now2 = time || createLocal(), sod = cloneWithOffset(now2, this).startOf("day"), format2 = hooks.calendarFormat(this, sod) || "sameElse", output = formats && (isFunction(formats[format2]) ? formats[format2].call(this, now2) : formats[format2]);
  return this.format(
    output || this.localeData().calendar(format2, this, createLocal(now2))
  );
}
function clone() {
  return new Moment(this);
}
function isAfter(input, units) {
  var localInput = isMoment(input) ? input : createLocal(input);
  if (!(this.isValid() && localInput.isValid())) {
    return false;
  }
  units = normalizeUnits(units) || "millisecond";
  if (units === "millisecond") {
    return this.valueOf() > localInput.valueOf();
  } else {
    return localInput.valueOf() < this.clone().startOf(units).valueOf();
  }
}
function isBefore(input, units) {
  var localInput = isMoment(input) ? input : createLocal(input);
  if (!(this.isValid() && localInput.isValid())) {
    return false;
  }
  units = normalizeUnits(units) || "millisecond";
  if (units === "millisecond") {
    return this.valueOf() < localInput.valueOf();
  } else {
    return this.clone().endOf(units).valueOf() < localInput.valueOf();
  }
}
function isBetween(from2, to2, units, inclusivity) {
  var localFrom = isMoment(from2) ? from2 : createLocal(from2), localTo = isMoment(to2) ? to2 : createLocal(to2);
  if (!(this.isValid() && localFrom.isValid() && localTo.isValid())) {
    return false;
  }
  inclusivity = inclusivity || "()";
  return (inclusivity[0] === "(" ? this.isAfter(localFrom, units) : !this.isBefore(localFrom, units)) && (inclusivity[1] === ")" ? this.isBefore(localTo, units) : !this.isAfter(localTo, units));
}
function isSame(input, units) {
  var localInput = isMoment(input) ? input : createLocal(input), inputMs;
  if (!(this.isValid() && localInput.isValid())) {
    return false;
  }
  units = normalizeUnits(units) || "millisecond";
  if (units === "millisecond") {
    return this.valueOf() === localInput.valueOf();
  } else {
    inputMs = localInput.valueOf();
    return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
  }
}
function isSameOrAfter(input, units) {
  return this.isSame(input, units) || this.isAfter(input, units);
}
function isSameOrBefore(input, units) {
  return this.isSame(input, units) || this.isBefore(input, units);
}
function diff(input, units, asFloat) {
  var that, zoneDelta, output;
  if (!this.isValid()) {
    return NaN;
  }
  that = cloneWithOffset(input, this);
  if (!that.isValid()) {
    return NaN;
  }
  zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;
  units = normalizeUnits(units);
  switch (units) {
    case "year":
      output = monthDiff(this, that) / 12;
      break;
    case "month":
      output = monthDiff(this, that);
      break;
    case "quarter":
      output = monthDiff(this, that) / 3;
      break;
    case "second":
      output = (this - that) / 1e3;
      break;
    case "minute":
      output = (this - that) / 6e4;
      break;
    case "hour":
      output = (this - that) / 36e5;
      break;
    case "day":
      output = (this - that - zoneDelta) / 864e5;
      break;
    case "week":
      output = (this - that - zoneDelta) / 6048e5;
      break;
    default:
      output = this - that;
  }
  return asFloat ? output : absFloor(output);
}
function monthDiff(a, b) {
  if (a.date() < b.date()) {
    return -monthDiff(b, a);
  }
  var wholeMonthDiff = (b.year() - a.year()) * 12 + (b.month() - a.month()), anchor = a.clone().add(wholeMonthDiff, "months"), anchor2, adjust;
  if (b - anchor < 0) {
    anchor2 = a.clone().add(wholeMonthDiff - 1, "months");
    adjust = (b - anchor) / (anchor - anchor2);
  } else {
    anchor2 = a.clone().add(wholeMonthDiff + 1, "months");
    adjust = (b - anchor) / (anchor2 - anchor);
  }
  return -(wholeMonthDiff + adjust) || 0;
}
hooks.defaultFormat = "YYYY-MM-DDTHH:mm:ssZ";
hooks.defaultFormatUtc = "YYYY-MM-DDTHH:mm:ss[Z]";
function toString() {
  return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ");
}
function toISOString(keepOffset) {
  if (!this.isValid()) {
    return null;
  }
  var utc2 = keepOffset !== true, m = utc2 ? this.clone().utc() : this;
  if (m.year() < 0 || m.year() > 9999) {
    return formatMoment(
      m,
      utc2 ? "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]" : "YYYYYY-MM-DD[T]HH:mm:ss.SSSZ"
    );
  }
  if (isFunction(Date.prototype.toISOString)) {
    if (utc2) {
      return this.toDate().toISOString();
    } else {
      return new Date(this.valueOf() + this.utcOffset() * 60 * 1e3).toISOString().replace("Z", formatMoment(m, "Z"));
    }
  }
  return formatMoment(
    m,
    utc2 ? "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]" : "YYYY-MM-DD[T]HH:mm:ss.SSSZ"
  );
}
function inspect() {
  if (!this.isValid()) {
    return "moment.invalid(/* " + this._i + " */)";
  }
  var func = "moment", zone = "", prefix, year, datetime, suffix;
  if (!this.isLocal()) {
    func = this.utcOffset() === 0 ? "moment.utc" : "moment.parseZone";
    zone = "Z";
  }
  prefix = "[" + func + '("]';
  year = 0 <= this.year() && this.year() <= 9999 ? "YYYY" : "YYYYYY";
  datetime = "-MM-DD[T]HH:mm:ss.SSS";
  suffix = zone + '[")]';
  return this.format(prefix + year + datetime + suffix);
}
function format(inputString) {
  if (!inputString) {
    inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
  }
  var output = formatMoment(this, inputString);
  return this.localeData().postformat(output);
}
function from(time, withoutSuffix) {
  if (this.isValid() && (isMoment(time) && time.isValid() || createLocal(time).isValid())) {
    return createDuration({ to: this, from: time }).locale(this.locale()).humanize(!withoutSuffix);
  } else {
    return this.localeData().invalidDate();
  }
}
function fromNow(withoutSuffix) {
  return this.from(createLocal(), withoutSuffix);
}
function to(time, withoutSuffix) {
  if (this.isValid() && (isMoment(time) && time.isValid() || createLocal(time).isValid())) {
    return createDuration({ from: this, to: time }).locale(this.locale()).humanize(!withoutSuffix);
  } else {
    return this.localeData().invalidDate();
  }
}
function toNow(withoutSuffix) {
  return this.to(createLocal(), withoutSuffix);
}
function locale(key) {
  var newLocaleData;
  if (key === void 0) {
    return this._locale._abbr;
  } else {
    newLocaleData = getLocale(key);
    if (newLocaleData != null) {
      this._locale = newLocaleData;
    }
    return this;
  }
}
var lang = deprecate(
  "moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.",
  function(key) {
    if (key === void 0) {
      return this.localeData();
    } else {
      return this.locale(key);
    }
  }
);
function localeData() {
  return this._locale;
}
var MS_PER_SECOND = 1e3, MS_PER_MINUTE = 60 * MS_PER_SECOND, MS_PER_HOUR = 60 * MS_PER_MINUTE, MS_PER_400_YEARS = (365 * 400 + 97) * 24 * MS_PER_HOUR;
function mod$1(dividend, divisor) {
  return (dividend % divisor + divisor) % divisor;
}
function localStartOfDate(y, m, d2) {
  if (y < 100 && y >= 0) {
    return new Date(y + 400, m, d2) - MS_PER_400_YEARS;
  } else {
    return new Date(y, m, d2).valueOf();
  }
}
function utcStartOfDate(y, m, d2) {
  if (y < 100 && y >= 0) {
    return Date.UTC(y + 400, m, d2) - MS_PER_400_YEARS;
  } else {
    return Date.UTC(y, m, d2);
  }
}
function startOf(units) {
  var time, startOfDate;
  units = normalizeUnits(units);
  if (units === void 0 || units === "millisecond" || !this.isValid()) {
    return this;
  }
  startOfDate = this._isUTC ? utcStartOfDate : localStartOfDate;
  switch (units) {
    case "year":
      time = startOfDate(this.year(), 0, 1);
      break;
    case "quarter":
      time = startOfDate(
        this.year(),
        this.month() - this.month() % 3,
        1
      );
      break;
    case "month":
      time = startOfDate(this.year(), this.month(), 1);
      break;
    case "week":
      time = startOfDate(
        this.year(),
        this.month(),
        this.date() - this.weekday()
      );
      break;
    case "isoWeek":
      time = startOfDate(
        this.year(),
        this.month(),
        this.date() - (this.isoWeekday() - 1)
      );
      break;
    case "day":
    case "date":
      time = startOfDate(this.year(), this.month(), this.date());
      break;
    case "hour":
      time = this._d.valueOf();
      time -= mod$1(
        time + (this._isUTC ? 0 : this.utcOffset() * MS_PER_MINUTE),
        MS_PER_HOUR
      );
      break;
    case "minute":
      time = this._d.valueOf();
      time -= mod$1(time, MS_PER_MINUTE);
      break;
    case "second":
      time = this._d.valueOf();
      time -= mod$1(time, MS_PER_SECOND);
      break;
  }
  this._d.setTime(time);
  hooks.updateOffset(this, true);
  return this;
}
function endOf(units) {
  var time, startOfDate;
  units = normalizeUnits(units);
  if (units === void 0 || units === "millisecond" || !this.isValid()) {
    return this;
  }
  startOfDate = this._isUTC ? utcStartOfDate : localStartOfDate;
  switch (units) {
    case "year":
      time = startOfDate(this.year() + 1, 0, 1) - 1;
      break;
    case "quarter":
      time = startOfDate(
        this.year(),
        this.month() - this.month() % 3 + 3,
        1
      ) - 1;
      break;
    case "month":
      time = startOfDate(this.year(), this.month() + 1, 1) - 1;
      break;
    case "week":
      time = startOfDate(
        this.year(),
        this.month(),
        this.date() - this.weekday() + 7
      ) - 1;
      break;
    case "isoWeek":
      time = startOfDate(
        this.year(),
        this.month(),
        this.date() - (this.isoWeekday() - 1) + 7
      ) - 1;
      break;
    case "day":
    case "date":
      time = startOfDate(this.year(), this.month(), this.date() + 1) - 1;
      break;
    case "hour":
      time = this._d.valueOf();
      time += MS_PER_HOUR - mod$1(
        time + (this._isUTC ? 0 : this.utcOffset() * MS_PER_MINUTE),
        MS_PER_HOUR
      ) - 1;
      break;
    case "minute":
      time = this._d.valueOf();
      time += MS_PER_MINUTE - mod$1(time, MS_PER_MINUTE) - 1;
      break;
    case "second":
      time = this._d.valueOf();
      time += MS_PER_SECOND - mod$1(time, MS_PER_SECOND) - 1;
      break;
  }
  this._d.setTime(time);
  hooks.updateOffset(this, true);
  return this;
}
function valueOf() {
  return this._d.valueOf() - (this._offset || 0) * 6e4;
}
function unix() {
  return Math.floor(this.valueOf() / 1e3);
}
function toDate() {
  return new Date(this.valueOf());
}
function toArray() {
  var m = this;
  return [
    m.year(),
    m.month(),
    m.date(),
    m.hour(),
    m.minute(),
    m.second(),
    m.millisecond()
  ];
}
function toObject() {
  var m = this;
  return {
    years: m.year(),
    months: m.month(),
    date: m.date(),
    hours: m.hours(),
    minutes: m.minutes(),
    seconds: m.seconds(),
    milliseconds: m.milliseconds()
  };
}
function toJSON() {
  return this.isValid() ? this.toISOString() : null;
}
function isValid$2() {
  return isValid(this);
}
function parsingFlags() {
  return extend({}, getParsingFlags(this));
}
function invalidAt() {
  return getParsingFlags(this).overflow;
}
function creationData() {
  return {
    input: this._i,
    format: this._f,
    locale: this._locale,
    isUTC: this._isUTC,
    strict: this._strict
  };
}
addFormatToken("N", 0, 0, "eraAbbr");
addFormatToken("NN", 0, 0, "eraAbbr");
addFormatToken("NNN", 0, 0, "eraAbbr");
addFormatToken("NNNN", 0, 0, "eraName");
addFormatToken("NNNNN", 0, 0, "eraNarrow");
addFormatToken("y", ["y", 1], "yo", "eraYear");
addFormatToken("y", ["yy", 2], 0, "eraYear");
addFormatToken("y", ["yyy", 3], 0, "eraYear");
addFormatToken("y", ["yyyy", 4], 0, "eraYear");
addRegexToken("N", matchEraAbbr);
addRegexToken("NN", matchEraAbbr);
addRegexToken("NNN", matchEraAbbr);
addRegexToken("NNNN", matchEraName);
addRegexToken("NNNNN", matchEraNarrow);
addParseToken(
  ["N", "NN", "NNN", "NNNN", "NNNNN"],
  function(input, array, config, token2) {
    var era = config._locale.erasParse(input, token2, config._strict);
    if (era) {
      getParsingFlags(config).era = era;
    } else {
      getParsingFlags(config).invalidEra = input;
    }
  }
);
addRegexToken("y", matchUnsigned);
addRegexToken("yy", matchUnsigned);
addRegexToken("yyy", matchUnsigned);
addRegexToken("yyyy", matchUnsigned);
addRegexToken("yo", matchEraYearOrdinal);
addParseToken(["y", "yy", "yyy", "yyyy"], YEAR);
addParseToken(["yo"], function(input, array, config, token2) {
  var match;
  if (config._locale._eraYearOrdinalRegex) {
    match = input.match(config._locale._eraYearOrdinalRegex);
  }
  if (config._locale.eraYearOrdinalParse) {
    array[YEAR] = config._locale.eraYearOrdinalParse(input, match);
  } else {
    array[YEAR] = parseInt(input, 10);
  }
});
function localeEras(m, format2) {
  var i, l, date, eras = this._eras || getLocale("en")._eras;
  for (i = 0, l = eras.length; i < l; ++i) {
    switch (typeof eras[i].since) {
      case "string":
        date = hooks(eras[i].since).startOf("day");
        eras[i].since = date.valueOf();
        break;
    }
    switch (typeof eras[i].until) {
      case "undefined":
        eras[i].until = Infinity;
        break;
      case "string":
        date = hooks(eras[i].until).startOf("day").valueOf();
        eras[i].until = date.valueOf();
        break;
    }
  }
  return eras;
}
function localeErasParse(eraName, format2, strict) {
  var i, l, eras = this.eras(), name, abbr, narrow;
  eraName = eraName.toUpperCase();
  for (i = 0, l = eras.length; i < l; ++i) {
    name = eras[i].name.toUpperCase();
    abbr = eras[i].abbr.toUpperCase();
    narrow = eras[i].narrow.toUpperCase();
    if (strict) {
      switch (format2) {
        case "N":
        case "NN":
        case "NNN":
          if (abbr === eraName) {
            return eras[i];
          }
          break;
        case "NNNN":
          if (name === eraName) {
            return eras[i];
          }
          break;
        case "NNNNN":
          if (narrow === eraName) {
            return eras[i];
          }
          break;
      }
    } else if ([name, abbr, narrow].indexOf(eraName) >= 0) {
      return eras[i];
    }
  }
}
function localeErasConvertYear(era, year) {
  var dir = era.since <= era.until ? 1 : -1;
  if (year === void 0) {
    return hooks(era.since).year();
  } else {
    return hooks(era.since).year() + (year - era.offset) * dir;
  }
}
function getEraName() {
  var i, l, val, eras = this.localeData().eras();
  for (i = 0, l = eras.length; i < l; ++i) {
    val = this.clone().startOf("day").valueOf();
    if (eras[i].since <= val && val <= eras[i].until) {
      return eras[i].name;
    }
    if (eras[i].until <= val && val <= eras[i].since) {
      return eras[i].name;
    }
  }
  return "";
}
function getEraNarrow() {
  var i, l, val, eras = this.localeData().eras();
  for (i = 0, l = eras.length; i < l; ++i) {
    val = this.clone().startOf("day").valueOf();
    if (eras[i].since <= val && val <= eras[i].until) {
      return eras[i].narrow;
    }
    if (eras[i].until <= val && val <= eras[i].since) {
      return eras[i].narrow;
    }
  }
  return "";
}
function getEraAbbr() {
  var i, l, val, eras = this.localeData().eras();
  for (i = 0, l = eras.length; i < l; ++i) {
    val = this.clone().startOf("day").valueOf();
    if (eras[i].since <= val && val <= eras[i].until) {
      return eras[i].abbr;
    }
    if (eras[i].until <= val && val <= eras[i].since) {
      return eras[i].abbr;
    }
  }
  return "";
}
function getEraYear() {
  var i, l, dir, val, eras = this.localeData().eras();
  for (i = 0, l = eras.length; i < l; ++i) {
    dir = eras[i].since <= eras[i].until ? 1 : -1;
    val = this.clone().startOf("day").valueOf();
    if (eras[i].since <= val && val <= eras[i].until || eras[i].until <= val && val <= eras[i].since) {
      return (this.year() - hooks(eras[i].since).year()) * dir + eras[i].offset;
    }
  }
  return this.year();
}
function erasNameRegex(isStrict) {
  if (!hasOwnProp(this, "_erasNameRegex")) {
    computeErasParse.call(this);
  }
  return isStrict ? this._erasNameRegex : this._erasRegex;
}
function erasAbbrRegex(isStrict) {
  if (!hasOwnProp(this, "_erasAbbrRegex")) {
    computeErasParse.call(this);
  }
  return isStrict ? this._erasAbbrRegex : this._erasRegex;
}
function erasNarrowRegex(isStrict) {
  if (!hasOwnProp(this, "_erasNarrowRegex")) {
    computeErasParse.call(this);
  }
  return isStrict ? this._erasNarrowRegex : this._erasRegex;
}
function matchEraAbbr(isStrict, locale2) {
  return locale2.erasAbbrRegex(isStrict);
}
function matchEraName(isStrict, locale2) {
  return locale2.erasNameRegex(isStrict);
}
function matchEraNarrow(isStrict, locale2) {
  return locale2.erasNarrowRegex(isStrict);
}
function matchEraYearOrdinal(isStrict, locale2) {
  return locale2._eraYearOrdinalRegex || matchUnsigned;
}
function computeErasParse() {
  var abbrPieces = [], namePieces = [], narrowPieces = [], mixedPieces = [], i, l, eras = this.eras();
  for (i = 0, l = eras.length; i < l; ++i) {
    namePieces.push(regexEscape(eras[i].name));
    abbrPieces.push(regexEscape(eras[i].abbr));
    narrowPieces.push(regexEscape(eras[i].narrow));
    mixedPieces.push(regexEscape(eras[i].name));
    mixedPieces.push(regexEscape(eras[i].abbr));
    mixedPieces.push(regexEscape(eras[i].narrow));
  }
  this._erasRegex = new RegExp("^(" + mixedPieces.join("|") + ")", "i");
  this._erasNameRegex = new RegExp("^(" + namePieces.join("|") + ")", "i");
  this._erasAbbrRegex = new RegExp("^(" + abbrPieces.join("|") + ")", "i");
  this._erasNarrowRegex = new RegExp(
    "^(" + narrowPieces.join("|") + ")",
    "i"
  );
}
addFormatToken(0, ["gg", 2], 0, function() {
  return this.weekYear() % 100;
});
addFormatToken(0, ["GG", 2], 0, function() {
  return this.isoWeekYear() % 100;
});
function addWeekYearFormatToken(token2, getter) {
  addFormatToken(0, [token2, token2.length], 0, getter);
}
addWeekYearFormatToken("gggg", "weekYear");
addWeekYearFormatToken("ggggg", "weekYear");
addWeekYearFormatToken("GGGG", "isoWeekYear");
addWeekYearFormatToken("GGGGG", "isoWeekYear");
addUnitAlias("weekYear", "gg");
addUnitAlias("isoWeekYear", "GG");
addUnitPriority("weekYear", 1);
addUnitPriority("isoWeekYear", 1);
addRegexToken("G", matchSigned);
addRegexToken("g", matchSigned);
addRegexToken("GG", match1to2, match2);
addRegexToken("gg", match1to2, match2);
addRegexToken("GGGG", match1to4, match4);
addRegexToken("gggg", match1to4, match4);
addRegexToken("GGGGG", match1to6, match6);
addRegexToken("ggggg", match1to6, match6);
addWeekParseToken(
  ["gggg", "ggggg", "GGGG", "GGGGG"],
  function(input, week, config, token2) {
    week[token2.substr(0, 2)] = toInt(input);
  }
);
addWeekParseToken(["gg", "GG"], function(input, week, config, token2) {
  week[token2] = hooks.parseTwoDigitYear(input);
});
function getSetWeekYear(input) {
  return getSetWeekYearHelper.call(
    this,
    input,
    this.week(),
    this.weekday(),
    this.localeData()._week.dow,
    this.localeData()._week.doy
  );
}
function getSetISOWeekYear(input) {
  return getSetWeekYearHelper.call(
    this,
    input,
    this.isoWeek(),
    this.isoWeekday(),
    1,
    4
  );
}
function getISOWeeksInYear() {
  return weeksInYear(this.year(), 1, 4);
}
function getISOWeeksInISOWeekYear() {
  return weeksInYear(this.isoWeekYear(), 1, 4);
}
function getWeeksInYear() {
  var weekInfo = this.localeData()._week;
  return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
}
function getWeeksInWeekYear() {
  var weekInfo = this.localeData()._week;
  return weeksInYear(this.weekYear(), weekInfo.dow, weekInfo.doy);
}
function getSetWeekYearHelper(input, week, weekday, dow, doy) {
  var weeksTarget;
  if (input == null) {
    return weekOfYear(this, dow, doy).year;
  } else {
    weeksTarget = weeksInYear(input, dow, doy);
    if (week > weeksTarget) {
      week = weeksTarget;
    }
    return setWeekAll.call(this, input, week, weekday, dow, doy);
  }
}
function setWeekAll(weekYear, week, weekday, dow, doy) {
  var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy), date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);
  this.year(date.getUTCFullYear());
  this.month(date.getUTCMonth());
  this.date(date.getUTCDate());
  return this;
}
addFormatToken("Q", 0, "Qo", "quarter");
addUnitAlias("quarter", "Q");
addUnitPriority("quarter", 7);
addRegexToken("Q", match1);
addParseToken("Q", function(input, array) {
  array[MONTH] = (toInt(input) - 1) * 3;
});
function getSetQuarter(input) {
  return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
}
addFormatToken("D", ["DD", 2], "Do", "date");
addUnitAlias("date", "D");
addUnitPriority("date", 9);
addRegexToken("D", match1to2);
addRegexToken("DD", match1to2, match2);
addRegexToken("Do", function(isStrict, locale2) {
  return isStrict ? locale2._dayOfMonthOrdinalParse || locale2._ordinalParse : locale2._dayOfMonthOrdinalParseLenient;
});
addParseToken(["D", "DD"], DATE);
addParseToken("Do", function(input, array) {
  array[DATE] = toInt(input.match(match1to2)[0]);
});
var getSetDayOfMonth = makeGetSet("Date", true);
addFormatToken("DDD", ["DDDD", 3], "DDDo", "dayOfYear");
addUnitAlias("dayOfYear", "DDD");
addUnitPriority("dayOfYear", 4);
addRegexToken("DDD", match1to3);
addRegexToken("DDDD", match3);
addParseToken(["DDD", "DDDD"], function(input, array, config) {
  config._dayOfYear = toInt(input);
});
function getSetDayOfYear(input) {
  var dayOfYear = Math.round(
    (this.clone().startOf("day") - this.clone().startOf("year")) / 864e5
  ) + 1;
  return input == null ? dayOfYear : this.add(input - dayOfYear, "d");
}
addFormatToken("m", ["mm", 2], 0, "minute");
addUnitAlias("minute", "m");
addUnitPriority("minute", 14);
addRegexToken("m", match1to2);
addRegexToken("mm", match1to2, match2);
addParseToken(["m", "mm"], MINUTE);
var getSetMinute = makeGetSet("Minutes", false);
addFormatToken("s", ["ss", 2], 0, "second");
addUnitAlias("second", "s");
addUnitPriority("second", 15);
addRegexToken("s", match1to2);
addRegexToken("ss", match1to2, match2);
addParseToken(["s", "ss"], SECOND);
var getSetSecond = makeGetSet("Seconds", false);
addFormatToken("S", 0, 0, function() {
  return ~~(this.millisecond() / 100);
});
addFormatToken(0, ["SS", 2], 0, function() {
  return ~~(this.millisecond() / 10);
});
addFormatToken(0, ["SSS", 3], 0, "millisecond");
addFormatToken(0, ["SSSS", 4], 0, function() {
  return this.millisecond() * 10;
});
addFormatToken(0, ["SSSSS", 5], 0, function() {
  return this.millisecond() * 100;
});
addFormatToken(0, ["SSSSSS", 6], 0, function() {
  return this.millisecond() * 1e3;
});
addFormatToken(0, ["SSSSSSS", 7], 0, function() {
  return this.millisecond() * 1e4;
});
addFormatToken(0, ["SSSSSSSS", 8], 0, function() {
  return this.millisecond() * 1e5;
});
addFormatToken(0, ["SSSSSSSSS", 9], 0, function() {
  return this.millisecond() * 1e6;
});
addUnitAlias("millisecond", "ms");
addUnitPriority("millisecond", 16);
addRegexToken("S", match1to3, match1);
addRegexToken("SS", match1to3, match2);
addRegexToken("SSS", match1to3, match3);
var token, getSetMillisecond;
for (token = "SSSS"; token.length <= 9; token += "S") {
  addRegexToken(token, matchUnsigned);
}
function parseMs(input, array) {
  array[MILLISECOND] = toInt(("0." + input) * 1e3);
}
for (token = "S"; token.length <= 9; token += "S") {
  addParseToken(token, parseMs);
}
getSetMillisecond = makeGetSet("Milliseconds", false);
addFormatToken("z", 0, 0, "zoneAbbr");
addFormatToken("zz", 0, 0, "zoneName");
function getZoneAbbr() {
  return this._isUTC ? "UTC" : "";
}
function getZoneName() {
  return this._isUTC ? "Coordinated Universal Time" : "";
}
var proto = Moment.prototype;
proto.add = add;
proto.calendar = calendar$1;
proto.clone = clone;
proto.diff = diff;
proto.endOf = endOf;
proto.format = format;
proto.from = from;
proto.fromNow = fromNow;
proto.to = to;
proto.toNow = toNow;
proto.get = stringGet;
proto.invalidAt = invalidAt;
proto.isAfter = isAfter;
proto.isBefore = isBefore;
proto.isBetween = isBetween;
proto.isSame = isSame;
proto.isSameOrAfter = isSameOrAfter;
proto.isSameOrBefore = isSameOrBefore;
proto.isValid = isValid$2;
proto.lang = lang;
proto.locale = locale;
proto.localeData = localeData;
proto.max = prototypeMax;
proto.min = prototypeMin;
proto.parsingFlags = parsingFlags;
proto.set = stringSet;
proto.startOf = startOf;
proto.subtract = subtract;
proto.toArray = toArray;
proto.toObject = toObject;
proto.toDate = toDate;
proto.toISOString = toISOString;
proto.inspect = inspect;
if (typeof Symbol !== "undefined" && Symbol.for != null) {
  proto[Symbol.for("nodejs.util.inspect.custom")] = function() {
    return "Moment<" + this.format() + ">";
  };
}
proto.toJSON = toJSON;
proto.toString = toString;
proto.unix = unix;
proto.valueOf = valueOf;
proto.creationData = creationData;
proto.eraName = getEraName;
proto.eraNarrow = getEraNarrow;
proto.eraAbbr = getEraAbbr;
proto.eraYear = getEraYear;
proto.year = getSetYear;
proto.isLeapYear = getIsLeapYear;
proto.weekYear = getSetWeekYear;
proto.isoWeekYear = getSetISOWeekYear;
proto.quarter = proto.quarters = getSetQuarter;
proto.month = getSetMonth;
proto.daysInMonth = getDaysInMonth;
proto.week = proto.weeks = getSetWeek;
proto.isoWeek = proto.isoWeeks = getSetISOWeek;
proto.weeksInYear = getWeeksInYear;
proto.weeksInWeekYear = getWeeksInWeekYear;
proto.isoWeeksInYear = getISOWeeksInYear;
proto.isoWeeksInISOWeekYear = getISOWeeksInISOWeekYear;
proto.date = getSetDayOfMonth;
proto.day = proto.days = getSetDayOfWeek;
proto.weekday = getSetLocaleDayOfWeek;
proto.isoWeekday = getSetISODayOfWeek;
proto.dayOfYear = getSetDayOfYear;
proto.hour = proto.hours = getSetHour;
proto.minute = proto.minutes = getSetMinute;
proto.second = proto.seconds = getSetSecond;
proto.millisecond = proto.milliseconds = getSetMillisecond;
proto.utcOffset = getSetOffset;
proto.utc = setOffsetToUTC;
proto.local = setOffsetToLocal;
proto.parseZone = setOffsetToParsedOffset;
proto.hasAlignedHourOffset = hasAlignedHourOffset;
proto.isDST = isDaylightSavingTime;
proto.isLocal = isLocal;
proto.isUtcOffset = isUtcOffset;
proto.isUtc = isUtc;
proto.isUTC = isUtc;
proto.zoneAbbr = getZoneAbbr;
proto.zoneName = getZoneName;
proto.dates = deprecate(
  "dates accessor is deprecated. Use date instead.",
  getSetDayOfMonth
);
proto.months = deprecate(
  "months accessor is deprecated. Use month instead",
  getSetMonth
);
proto.years = deprecate(
  "years accessor is deprecated. Use year instead",
  getSetYear
);
proto.zone = deprecate(
  "moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/",
  getSetZone
);
proto.isDSTShifted = deprecate(
  "isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information",
  isDaylightSavingTimeShifted
);
function createUnix(input) {
  return createLocal(input * 1e3);
}
function createInZone() {
  return createLocal.apply(null, arguments).parseZone();
}
function preParsePostFormat(string) {
  return string;
}
var proto$1 = Locale.prototype;
proto$1.calendar = calendar;
proto$1.longDateFormat = longDateFormat;
proto$1.invalidDate = invalidDate;
proto$1.ordinal = ordinal;
proto$1.preparse = preParsePostFormat;
proto$1.postformat = preParsePostFormat;
proto$1.relativeTime = relativeTime;
proto$1.pastFuture = pastFuture;
proto$1.set = set;
proto$1.eras = localeEras;
proto$1.erasParse = localeErasParse;
proto$1.erasConvertYear = localeErasConvertYear;
proto$1.erasAbbrRegex = erasAbbrRegex;
proto$1.erasNameRegex = erasNameRegex;
proto$1.erasNarrowRegex = erasNarrowRegex;
proto$1.months = localeMonths;
proto$1.monthsShort = localeMonthsShort;
proto$1.monthsParse = localeMonthsParse;
proto$1.monthsRegex = monthsRegex;
proto$1.monthsShortRegex = monthsShortRegex;
proto$1.week = localeWeek;
proto$1.firstDayOfYear = localeFirstDayOfYear;
proto$1.firstDayOfWeek = localeFirstDayOfWeek;
proto$1.weekdays = localeWeekdays;
proto$1.weekdaysMin = localeWeekdaysMin;
proto$1.weekdaysShort = localeWeekdaysShort;
proto$1.weekdaysParse = localeWeekdaysParse;
proto$1.weekdaysRegex = weekdaysRegex;
proto$1.weekdaysShortRegex = weekdaysShortRegex;
proto$1.weekdaysMinRegex = weekdaysMinRegex;
proto$1.isPM = localeIsPM;
proto$1.meridiem = localeMeridiem;
function get$1(format2, index2, field, setter) {
  var locale2 = getLocale(), utc2 = createUTC().set(setter, index2);
  return locale2[field](utc2, format2);
}
function listMonthsImpl(format2, index2, field) {
  if (isNumber(format2)) {
    index2 = format2;
    format2 = void 0;
  }
  format2 = format2 || "";
  if (index2 != null) {
    return get$1(format2, index2, field, "month");
  }
  var i, out = [];
  for (i = 0; i < 12; i++) {
    out[i] = get$1(format2, i, field, "month");
  }
  return out;
}
function listWeekdaysImpl(localeSorted, format2, index2, field) {
  if (typeof localeSorted === "boolean") {
    if (isNumber(format2)) {
      index2 = format2;
      format2 = void 0;
    }
    format2 = format2 || "";
  } else {
    format2 = localeSorted;
    index2 = format2;
    localeSorted = false;
    if (isNumber(format2)) {
      index2 = format2;
      format2 = void 0;
    }
    format2 = format2 || "";
  }
  var locale2 = getLocale(), shift = localeSorted ? locale2._week.dow : 0, i, out = [];
  if (index2 != null) {
    return get$1(format2, (index2 + shift) % 7, field, "day");
  }
  for (i = 0; i < 7; i++) {
    out[i] = get$1(format2, (i + shift) % 7, field, "day");
  }
  return out;
}
function listMonths(format2, index2) {
  return listMonthsImpl(format2, index2, "months");
}
function listMonthsShort(format2, index2) {
  return listMonthsImpl(format2, index2, "monthsShort");
}
function listWeekdays(localeSorted, format2, index2) {
  return listWeekdaysImpl(localeSorted, format2, index2, "weekdays");
}
function listWeekdaysShort(localeSorted, format2, index2) {
  return listWeekdaysImpl(localeSorted, format2, index2, "weekdaysShort");
}
function listWeekdaysMin(localeSorted, format2, index2) {
  return listWeekdaysImpl(localeSorted, format2, index2, "weekdaysMin");
}
getSetGlobalLocale("en", {
  eras: [
    {
      since: "0001-01-01",
      until: Infinity,
      offset: 1,
      name: "Anno Domini",
      narrow: "AD",
      abbr: "AD"
    },
    {
      since: "0000-12-31",
      until: -Infinity,
      offset: 1,
      name: "Before Christ",
      narrow: "BC",
      abbr: "BC"
    }
  ],
  dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
  ordinal: function(number) {
    var b = number % 10, output = toInt(number % 100 / 10) === 1 ? "th" : b === 1 ? "st" : b === 2 ? "nd" : b === 3 ? "rd" : "th";
    return number + output;
  }
});
hooks.lang = deprecate(
  "moment.lang is deprecated. Use moment.locale instead.",
  getSetGlobalLocale
);
hooks.langData = deprecate(
  "moment.langData is deprecated. Use moment.localeData instead.",
  getLocale
);
var mathAbs = Math.abs;
function abs() {
  var data = this._data;
  this._milliseconds = mathAbs(this._milliseconds);
  this._days = mathAbs(this._days);
  this._months = mathAbs(this._months);
  data.milliseconds = mathAbs(data.milliseconds);
  data.seconds = mathAbs(data.seconds);
  data.minutes = mathAbs(data.minutes);
  data.hours = mathAbs(data.hours);
  data.months = mathAbs(data.months);
  data.years = mathAbs(data.years);
  return this;
}
function addSubtract$1(duration, input, value, direction) {
  var other = createDuration(input, value);
  duration._milliseconds += direction * other._milliseconds;
  duration._days += direction * other._days;
  duration._months += direction * other._months;
  return duration._bubble();
}
function add$1(input, value) {
  return addSubtract$1(this, input, value, 1);
}
function subtract$1(input, value) {
  return addSubtract$1(this, input, value, -1);
}
function absCeil(number) {
  if (number < 0) {
    return Math.floor(number);
  } else {
    return Math.ceil(number);
  }
}
function bubble() {
  var milliseconds2 = this._milliseconds, days2 = this._days, months2 = this._months, data = this._data, seconds2, minutes2, hours2, years2, monthsFromDays;
  if (!(milliseconds2 >= 0 && days2 >= 0 && months2 >= 0 || milliseconds2 <= 0 && days2 <= 0 && months2 <= 0)) {
    milliseconds2 += absCeil(monthsToDays(months2) + days2) * 864e5;
    days2 = 0;
    months2 = 0;
  }
  data.milliseconds = milliseconds2 % 1e3;
  seconds2 = absFloor(milliseconds2 / 1e3);
  data.seconds = seconds2 % 60;
  minutes2 = absFloor(seconds2 / 60);
  data.minutes = minutes2 % 60;
  hours2 = absFloor(minutes2 / 60);
  data.hours = hours2 % 24;
  days2 += absFloor(hours2 / 24);
  monthsFromDays = absFloor(daysToMonths(days2));
  months2 += monthsFromDays;
  days2 -= absCeil(monthsToDays(monthsFromDays));
  years2 = absFloor(months2 / 12);
  months2 %= 12;
  data.days = days2;
  data.months = months2;
  data.years = years2;
  return this;
}
function daysToMonths(days2) {
  return days2 * 4800 / 146097;
}
function monthsToDays(months2) {
  return months2 * 146097 / 4800;
}
function as(units) {
  if (!this.isValid()) {
    return NaN;
  }
  var days2, months2, milliseconds2 = this._milliseconds;
  units = normalizeUnits(units);
  if (units === "month" || units === "quarter" || units === "year") {
    days2 = this._days + milliseconds2 / 864e5;
    months2 = this._months + daysToMonths(days2);
    switch (units) {
      case "month":
        return months2;
      case "quarter":
        return months2 / 3;
      case "year":
        return months2 / 12;
    }
  } else {
    days2 = this._days + Math.round(monthsToDays(this._months));
    switch (units) {
      case "week":
        return days2 / 7 + milliseconds2 / 6048e5;
      case "day":
        return days2 + milliseconds2 / 864e5;
      case "hour":
        return days2 * 24 + milliseconds2 / 36e5;
      case "minute":
        return days2 * 1440 + milliseconds2 / 6e4;
      case "second":
        return days2 * 86400 + milliseconds2 / 1e3;
      case "millisecond":
        return Math.floor(days2 * 864e5) + milliseconds2;
      default:
        throw new Error("Unknown unit " + units);
    }
  }
}
function valueOf$1() {
  if (!this.isValid()) {
    return NaN;
  }
  return this._milliseconds + this._days * 864e5 + this._months % 12 * 2592e6 + toInt(this._months / 12) * 31536e6;
}
function makeAs(alias) {
  return function() {
    return this.as(alias);
  };
}
var asMilliseconds = makeAs("ms"), asSeconds = makeAs("s"), asMinutes = makeAs("m"), asHours = makeAs("h"), asDays = makeAs("d"), asWeeks = makeAs("w"), asMonths = makeAs("M"), asQuarters = makeAs("Q"), asYears = makeAs("y");
function clone$1() {
  return createDuration(this);
}
function get$2(units) {
  units = normalizeUnits(units);
  return this.isValid() ? this[units + "s"]() : NaN;
}
function makeGetter(name) {
  return function() {
    return this.isValid() ? this._data[name] : NaN;
  };
}
var milliseconds = makeGetter("milliseconds"), seconds = makeGetter("seconds"), minutes = makeGetter("minutes"), hours = makeGetter("hours"), days = makeGetter("days"), months = makeGetter("months"), years = makeGetter("years");
function weeks() {
  return absFloor(this.days() / 7);
}
var round = Math.round, thresholds = {
  ss: 44,
  // a few seconds to seconds
  s: 45,
  // seconds to minute
  m: 45,
  // minutes to hour
  h: 22,
  // hours to day
  d: 26,
  // days to month/week
  w: null,
  // weeks to month
  M: 11
  // months to year
};
function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale2) {
  return locale2.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
}
function relativeTime$1(posNegDuration, withoutSuffix, thresholds2, locale2) {
  var duration = createDuration(posNegDuration).abs(), seconds2 = round(duration.as("s")), minutes2 = round(duration.as("m")), hours2 = round(duration.as("h")), days2 = round(duration.as("d")), months2 = round(duration.as("M")), weeks2 = round(duration.as("w")), years2 = round(duration.as("y")), a = seconds2 <= thresholds2.ss && ["s", seconds2] || seconds2 < thresholds2.s && ["ss", seconds2] || minutes2 <= 1 && ["m"] || minutes2 < thresholds2.m && ["mm", minutes2] || hours2 <= 1 && ["h"] || hours2 < thresholds2.h && ["hh", hours2] || days2 <= 1 && ["d"] || days2 < thresholds2.d && ["dd", days2];
  if (thresholds2.w != null) {
    a = a || weeks2 <= 1 && ["w"] || weeks2 < thresholds2.w && ["ww", weeks2];
  }
  a = a || months2 <= 1 && ["M"] || months2 < thresholds2.M && ["MM", months2] || years2 <= 1 && ["y"] || ["yy", years2];
  a[2] = withoutSuffix;
  a[3] = +posNegDuration > 0;
  a[4] = locale2;
  return substituteTimeAgo.apply(null, a);
}
function getSetRelativeTimeRounding(roundingFunction) {
  if (roundingFunction === void 0) {
    return round;
  }
  if (typeof roundingFunction === "function") {
    round = roundingFunction;
    return true;
  }
  return false;
}
function getSetRelativeTimeThreshold(threshold, limit) {
  if (thresholds[threshold] === void 0) {
    return false;
  }
  if (limit === void 0) {
    return thresholds[threshold];
  }
  thresholds[threshold] = limit;
  if (threshold === "s") {
    thresholds.ss = limit - 1;
  }
  return true;
}
function humanize(argWithSuffix, argThresholds) {
  if (!this.isValid()) {
    return this.localeData().invalidDate();
  }
  var withSuffix = false, th = thresholds, locale2, output;
  if (typeof argWithSuffix === "object") {
    argThresholds = argWithSuffix;
    argWithSuffix = false;
  }
  if (typeof argWithSuffix === "boolean") {
    withSuffix = argWithSuffix;
  }
  if (typeof argThresholds === "object") {
    th = Object.assign({}, thresholds, argThresholds);
    if (argThresholds.s != null && argThresholds.ss == null) {
      th.ss = argThresholds.s - 1;
    }
  }
  locale2 = this.localeData();
  output = relativeTime$1(this, !withSuffix, th, locale2);
  if (withSuffix) {
    output = locale2.pastFuture(+this, output);
  }
  return locale2.postformat(output);
}
var abs$1 = Math.abs;
function sign(x) {
  return (x > 0) - (x < 0) || +x;
}
function toISOString$1() {
  if (!this.isValid()) {
    return this.localeData().invalidDate();
  }
  var seconds2 = abs$1(this._milliseconds) / 1e3, days2 = abs$1(this._days), months2 = abs$1(this._months), minutes2, hours2, years2, s2, total = this.asSeconds(), totalSign, ymSign, daysSign, hmsSign;
  if (!total) {
    return "P0D";
  }
  minutes2 = absFloor(seconds2 / 60);
  hours2 = absFloor(minutes2 / 60);
  seconds2 %= 60;
  minutes2 %= 60;
  years2 = absFloor(months2 / 12);
  months2 %= 12;
  s2 = seconds2 ? seconds2.toFixed(3).replace(/\.?0+$/, "") : "";
  totalSign = total < 0 ? "-" : "";
  ymSign = sign(this._months) !== sign(total) ? "-" : "";
  daysSign = sign(this._days) !== sign(total) ? "-" : "";
  hmsSign = sign(this._milliseconds) !== sign(total) ? "-" : "";
  return totalSign + "P" + (years2 ? ymSign + years2 + "Y" : "") + (months2 ? ymSign + months2 + "M" : "") + (days2 ? daysSign + days2 + "D" : "") + (hours2 || minutes2 || seconds2 ? "T" : "") + (hours2 ? hmsSign + hours2 + "H" : "") + (minutes2 ? hmsSign + minutes2 + "M" : "") + (seconds2 ? hmsSign + s2 + "S" : "");
}
var proto$2 = Duration.prototype;
proto$2.isValid = isValid$1;
proto$2.abs = abs;
proto$2.add = add$1;
proto$2.subtract = subtract$1;
proto$2.as = as;
proto$2.asMilliseconds = asMilliseconds;
proto$2.asSeconds = asSeconds;
proto$2.asMinutes = asMinutes;
proto$2.asHours = asHours;
proto$2.asDays = asDays;
proto$2.asWeeks = asWeeks;
proto$2.asMonths = asMonths;
proto$2.asQuarters = asQuarters;
proto$2.asYears = asYears;
proto$2.valueOf = valueOf$1;
proto$2._bubble = bubble;
proto$2.clone = clone$1;
proto$2.get = get$2;
proto$2.milliseconds = milliseconds;
proto$2.seconds = seconds;
proto$2.minutes = minutes;
proto$2.hours = hours;
proto$2.days = days;
proto$2.weeks = weeks;
proto$2.months = months;
proto$2.years = years;
proto$2.humanize = humanize;
proto$2.toISOString = toISOString$1;
proto$2.toString = toISOString$1;
proto$2.toJSON = toISOString$1;
proto$2.locale = locale;
proto$2.localeData = localeData;
proto$2.toIsoString = deprecate(
  "toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)",
  toISOString$1
);
proto$2.lang = lang;
addFormatToken("X", 0, 0, "unix");
addFormatToken("x", 0, 0, "valueOf");
addRegexToken("x", matchSigned);
addRegexToken("X", matchTimestamp);
addParseToken("X", function(input, array, config) {
  config._d = new Date(parseFloat(input) * 1e3);
});
addParseToken("x", function(input, array, config) {
  config._d = new Date(toInt(input));
});
//! moment.js
hooks.version = "2.29.4";
setHookCallback(createLocal);
hooks.fn = proto;
hooks.min = min;
hooks.max = max;
hooks.now = now;
hooks.utc = createUTC;
hooks.unix = createUnix;
hooks.months = listMonths;
hooks.isDate = isDate;
hooks.locale = getSetGlobalLocale;
hooks.invalid = createInvalid;
hooks.duration = createDuration;
hooks.isMoment = isMoment;
hooks.weekdays = listWeekdays;
hooks.parseZone = createInZone;
hooks.localeData = getLocale;
hooks.isDuration = isDuration;
hooks.monthsShort = listMonthsShort;
hooks.weekdaysMin = listWeekdaysMin;
hooks.defineLocale = defineLocale;
hooks.updateLocale = updateLocale;
hooks.locales = listLocales;
hooks.weekdaysShort = listWeekdaysShort;
hooks.normalizeUnits = normalizeUnits;
hooks.relativeTimeRounding = getSetRelativeTimeRounding;
hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
hooks.calendarFormat = getCalendarFormat;
hooks.prototype = proto;
hooks.HTML5_FMT = {
  DATETIME_LOCAL: "YYYY-MM-DDTHH:mm",
  // <input type="datetime-local" />
  DATETIME_LOCAL_SECONDS: "YYYY-MM-DDTHH:mm:ss",
  // <input type="datetime-local" step="1" />
  DATETIME_LOCAL_MS: "YYYY-MM-DDTHH:mm:ss.SSS",
  // <input type="datetime-local" step="0.001" />
  DATE: "YYYY-MM-DD",
  // <input type="date" />
  TIME: "HH:mm",
  // <input type="time" />
  TIME_SECONDS: "HH:mm:ss",
  // <input type="time" step="1" />
  TIME_MS: "HH:mm:ss.SSS",
  // <input type="time" step="0.001" />
  WEEK: "GGGG-[W]WW",
  // <input type="week" />
  MONTH: "YYYY-MM"
  // <input type="month" />
};
var __assign = function() {
  return (__assign = Object.assign || function(t2) {
    for (var e2, i = 1, r = arguments.length; i < r; i++)
      for (var o2 in e2 = arguments[i])
        Object.prototype.hasOwnProperty.call(e2, o2) && (t2[o2] = e2[o2]);
    return t2;
  }).apply(this, arguments);
};
function __awaiter(t2, n2, h, a) {
  return new (h = h || Promise)(function(i, e2) {
    function r(t3) {
      try {
        s2(a.next(t3));
      } catch (t4) {
        e2(t4);
      }
    }
    function o2(t3) {
      try {
        s2(a.throw(t3));
      } catch (t4) {
        e2(t4);
      }
    }
    function s2(t3) {
      var e3;
      t3.done ? i(t3.value) : ((e3 = t3.value) instanceof h ? e3 : new h(function(t4) {
        t4(e3);
      })).then(r, o2);
    }
    s2((a = a.apply(t2, n2 || [])).next());
  });
}
function __generator(i, r) {
  var o2, s2, n2, h = { label: 0, sent: function() {
    if (1 & n2[0])
      throw n2[1];
    return n2[1];
  }, trys: [], ops: [] }, t2 = { next: e2(0), throw: e2(1), return: e2(2) };
  return "function" == typeof Symbol && (t2[Symbol.iterator] = function() {
    return this;
  }), t2;
  function e2(e3) {
    return function(t3) {
      return function(e4) {
        if (o2)
          throw new TypeError("Generator is already executing.");
        for (; h; )
          try {
            if (o2 = 1, s2 && (n2 = 2 & e4[0] ? s2.return : e4[0] ? s2.throw || ((n2 = s2.return) && n2.call(s2), 0) : s2.next) && !(n2 = n2.call(s2, e4[1])).done)
              return n2;
            switch (s2 = 0, (e4 = n2 ? [2 & e4[0], n2.value] : e4)[0]) {
              case 0:
              case 1:
                n2 = e4;
                break;
              case 4:
                return h.label++, { value: e4[1], done: false };
              case 5:
                h.label++, s2 = e4[1], e4 = [0];
                continue;
              case 7:
                e4 = h.ops.pop(), h.trys.pop();
                continue;
              default:
                if (!(n2 = 0 < (n2 = h.trys).length && n2[n2.length - 1]) && (6 === e4[0] || 2 === e4[0])) {
                  h = 0;
                  continue;
                }
                if (3 === e4[0] && (!n2 || e4[1] > n2[0] && e4[1] < n2[3])) {
                  h.label = e4[1];
                  break;
                }
                if (6 === e4[0] && h.label < n2[1]) {
                  h.label = n2[1], n2 = e4;
                  break;
                }
                if (n2 && h.label < n2[2]) {
                  h.label = n2[2], h.ops.push(e4);
                  break;
                }
                n2[2] && h.ops.pop(), h.trys.pop();
                continue;
            }
            e4 = r.call(i, h);
          } catch (t4) {
            e4 = [6, t4], s2 = 0;
          } finally {
            o2 = n2 = 0;
          }
        if (5 & e4[0])
          throw e4[1];
        return { value: e4[0] ? e4[1] : void 0, done: true };
      }([e3, t3]);
    };
  }
}
var ADELTA = [0, 11, 15, 19, 23, 27, 31, 16, 18, 20, 22, 24, 26, 28, 20, 22, 24, 24, 26, 28, 28, 22, 24, 24, 26, 26, 28, 28, 24, 24, 26, 26, 26, 28, 28, 24, 26, 26, 26, 28, 28], VPAT = [3220, 1468, 2713, 1235, 3062, 1890, 2119, 1549, 2344, 2936, 1117, 2583, 1330, 2470, 1667, 2249, 2028, 3780, 481, 4011, 142, 3098, 831, 3445, 592, 2517, 1776, 2234, 1951, 2827, 1070, 2660, 1345, 3177], fmtword = [30660, 29427, 32170, 30877, 26159, 25368, 27713, 26998, 21522, 20773, 24188, 23371, 17913, 16590, 20375, 19104, 13663, 12392, 16177, 14854, 9396, 8579, 11994, 11245, 5769, 5054, 7399, 6608, 1890, 597, 3340, 2107], ECBLOCKS = [1, 0, 19, 7, 1, 0, 16, 10, 1, 0, 13, 13, 1, 0, 9, 17, 1, 0, 34, 10, 1, 0, 28, 16, 1, 0, 22, 22, 1, 0, 16, 28, 1, 0, 55, 15, 1, 0, 44, 26, 2, 0, 17, 18, 2, 0, 13, 22, 1, 0, 80, 20, 2, 0, 32, 18, 2, 0, 24, 26, 4, 0, 9, 16, 1, 0, 108, 26, 2, 0, 43, 24, 2, 2, 15, 18, 2, 2, 11, 22, 2, 0, 68, 18, 4, 0, 27, 16, 4, 0, 19, 24, 4, 0, 15, 28, 2, 0, 78, 20, 4, 0, 31, 18, 2, 4, 14, 18, 4, 1, 13, 26, 2, 0, 97, 24, 2, 2, 38, 22, 4, 2, 18, 22, 4, 2, 14, 26, 2, 0, 116, 30, 3, 2, 36, 22, 4, 4, 16, 20, 4, 4, 12, 24, 2, 2, 68, 18, 4, 1, 43, 26, 6, 2, 19, 24, 6, 2, 15, 28, 4, 0, 81, 20, 1, 4, 50, 30, 4, 4, 22, 28, 3, 8, 12, 24, 2, 2, 92, 24, 6, 2, 36, 22, 4, 6, 20, 26, 7, 4, 14, 28, 4, 0, 107, 26, 8, 1, 37, 22, 8, 4, 20, 24, 12, 4, 11, 22, 3, 1, 115, 30, 4, 5, 40, 24, 11, 5, 16, 20, 11, 5, 12, 24, 5, 1, 87, 22, 5, 5, 41, 24, 5, 7, 24, 30, 11, 7, 12, 24, 5, 1, 98, 24, 7, 3, 45, 28, 15, 2, 19, 24, 3, 13, 15, 30, 1, 5, 107, 28, 10, 1, 46, 28, 1, 15, 22, 28, 2, 17, 14, 28, 5, 1, 120, 30, 9, 4, 43, 26, 17, 1, 22, 28, 2, 19, 14, 28, 3, 4, 113, 28, 3, 11, 44, 26, 17, 4, 21, 26, 9, 16, 13, 26, 3, 5, 107, 28, 3, 13, 41, 26, 15, 5, 24, 30, 15, 10, 15, 28, 4, 4, 116, 28, 17, 0, 42, 26, 17, 6, 22, 28, 19, 6, 16, 30, 2, 7, 111, 28, 17, 0, 46, 28, 7, 16, 24, 30, 34, 0, 13, 24, 4, 5, 121, 30, 4, 14, 47, 28, 11, 14, 24, 30, 16, 14, 15, 30, 6, 4, 117, 30, 6, 14, 45, 28, 11, 16, 24, 30, 30, 2, 16, 30, 8, 4, 106, 26, 8, 13, 47, 28, 7, 22, 24, 30, 22, 13, 15, 30, 10, 2, 114, 28, 19, 4, 46, 28, 28, 6, 22, 28, 33, 4, 16, 30, 8, 4, 122, 30, 22, 3, 45, 28, 8, 26, 23, 30, 12, 28, 15, 30, 3, 10, 117, 30, 3, 23, 45, 28, 4, 31, 24, 30, 11, 31, 15, 30, 7, 7, 116, 30, 21, 7, 45, 28, 1, 37, 23, 30, 19, 26, 15, 30, 5, 10, 115, 30, 19, 10, 47, 28, 15, 25, 24, 30, 23, 25, 15, 30, 13, 3, 115, 30, 2, 29, 46, 28, 42, 1, 24, 30, 23, 28, 15, 30, 17, 0, 115, 30, 10, 23, 46, 28, 10, 35, 24, 30, 19, 35, 15, 30, 17, 1, 115, 30, 14, 21, 46, 28, 29, 19, 24, 30, 11, 46, 15, 30, 13, 6, 115, 30, 14, 23, 46, 28, 44, 7, 24, 30, 59, 1, 16, 30, 12, 7, 121, 30, 12, 26, 47, 28, 39, 14, 24, 30, 22, 41, 15, 30, 6, 14, 121, 30, 6, 34, 47, 28, 46, 10, 24, 30, 2, 64, 15, 30, 17, 4, 122, 30, 29, 14, 46, 28, 49, 10, 24, 30, 24, 46, 15, 30, 4, 18, 122, 30, 13, 32, 46, 28, 48, 14, 24, 30, 42, 32, 15, 30, 20, 4, 117, 30, 40, 7, 47, 28, 43, 22, 24, 30, 10, 67, 15, 30, 19, 6, 118, 30, 18, 31, 47, 28, 34, 34, 24, 30, 20, 61, 15, 30], GLOG = [255, 0, 1, 25, 2, 50, 26, 198, 3, 223, 51, 238, 27, 104, 199, 75, 4, 100, 224, 14, 52, 141, 239, 129, 28, 193, 105, 248, 200, 8, 76, 113, 5, 138, 101, 47, 225, 36, 15, 33, 53, 147, 142, 218, 240, 18, 130, 69, 29, 181, 194, 125, 106, 39, 249, 185, 201, 154, 9, 120, 77, 228, 114, 166, 6, 191, 139, 98, 102, 221, 48, 253, 226, 152, 37, 179, 16, 145, 34, 136, 54, 208, 148, 206, 143, 150, 219, 189, 241, 210, 19, 92, 131, 56, 70, 64, 30, 66, 182, 163, 195, 72, 126, 110, 107, 58, 40, 84, 250, 133, 186, 61, 202, 94, 155, 159, 10, 21, 121, 43, 78, 212, 229, 172, 115, 243, 167, 87, 7, 112, 192, 247, 140, 128, 99, 13, 103, 74, 222, 237, 49, 197, 254, 24, 227, 165, 153, 119, 38, 184, 180, 124, 17, 68, 146, 217, 35, 32, 137, 46, 55, 63, 209, 91, 149, 188, 207, 205, 144, 135, 151, 178, 220, 252, 190, 97, 242, 86, 211, 171, 20, 42, 93, 158, 132, 60, 57, 83, 71, 109, 65, 162, 31, 45, 67, 216, 183, 123, 164, 118, 196, 23, 73, 236, 127, 12, 111, 246, 108, 161, 59, 82, 41, 157, 85, 170, 251, 96, 134, 177, 187, 204, 62, 90, 203, 89, 95, 176, 156, 169, 160, 81, 11, 245, 22, 235, 122, 117, 44, 215, 79, 174, 213, 233, 230, 231, 173, 232, 116, 214, 244, 234, 168, 80, 88, 175], GEXP = [1, 2, 4, 8, 16, 32, 64, 128, 29, 58, 116, 232, 205, 135, 19, 38, 76, 152, 45, 90, 180, 117, 234, 201, 143, 3, 6, 12, 24, 48, 96, 192, 157, 39, 78, 156, 37, 74, 148, 53, 106, 212, 181, 119, 238, 193, 159, 35, 70, 140, 5, 10, 20, 40, 80, 160, 93, 186, 105, 210, 185, 111, 222, 161, 95, 190, 97, 194, 153, 47, 94, 188, 101, 202, 137, 15, 30, 60, 120, 240, 253, 231, 211, 187, 107, 214, 177, 127, 254, 225, 223, 163, 91, 182, 113, 226, 217, 175, 67, 134, 17, 34, 68, 136, 13, 26, 52, 104, 208, 189, 103, 206, 129, 31, 62, 124, 248, 237, 199, 147, 59, 118, 236, 197, 151, 51, 102, 204, 133, 23, 46, 92, 184, 109, 218, 169, 79, 158, 33, 66, 132, 21, 42, 84, 168, 77, 154, 41, 82, 164, 85, 170, 73, 146, 57, 114, 228, 213, 183, 115, 230, 209, 191, 99, 198, 145, 63, 126, 252, 229, 215, 179, 123, 246, 241, 255, 227, 219, 171, 75, 150, 49, 98, 196, 149, 55, 110, 220, 165, 87, 174, 65, 130, 25, 50, 100, 200, 141, 7, 14, 28, 56, 112, 224, 221, 167, 83, 166, 81, 162, 89, 178, 121, 242, 249, 239, 195, 155, 43, 86, 172, 69, 138, 9, 18, 36, 72, 144, 61, 122, 244, 245, 247, 243, 251, 235, 203, 139, 11, 22, 44, 88, 176, 125, 250, 233, 207, 131, 27, 54, 108, 216, 173, 71, 142, 0], UNIT_CONVERSION = function(t2) {
  return index.upx2px(Number(t2));
}, getPixelRatio = function(t2) {
  return index.getSystemInfoSync()[t2 || "pixelRatio"];
}, getTimeDate = function() {
  var t2 = /* @__PURE__ */ new Date();
  return t2.toLocaleDateString().replace(/\//g, "-") + " " + t2.toTimeString().slice(0, 8);
}, GETSIZE = { "MP-ALIPAY": function(t2) {
  return UNIT_CONVERSION(t2) * getPixelRatio();
}, "MP-WEIXIN": function(t2) {
  return UNIT_CONVERSION(t2);
}, "MP-BAIDU": function(t2) {
  return UNIT_CONVERSION(t2);
}, "MP-TOUTIAO": function(t2) {
  return UNIT_CONVERSION(t2);
}, "MP-QQ": function(t2) {
  return UNIT_CONVERSION(t2);
}, "MP-LARK": function(t2) {
  return UNIT_CONVERSION(t2);
}, "MP-KUAISHOU": function(t2) {
  return UNIT_CONVERSION(t2);
}, "MP-360": function(t2) {
  return UNIT_CONVERSION(t2);
}, "QUICKAPP-WEBVIEW": function(t2) {
  return UNIT_CONVERSION(t2);
}, "QUICKAPP-WEBVIEW-UNION": function(t2) {
  return UNIT_CONVERSION(t2);
}, "QUICKAPP-WEBVIEW-HUAWEI": function(t2) {
  return UNIT_CONVERSION(t2);
}, MP: function(t2) {
  return UNIT_CONVERSION(t2);
}, "APP-PLUS": function(t2) {
  return UNIT_CONVERSION(t2);
}, NVUE: function(t2) {
  return UNIT_CONVERSION(t2);
}, H5: function(t2) {
  return UNIT_CONVERSION(t2);
}, none: function(t2) {
  return UNIT_CONVERSION(t2);
} }, UtF16TO8 = function(t2) {
  for (var e2, i = t2.toString(), r = "", o2 = 0; o2 < i.length; o2++)
    1 <= (e2 = i.charCodeAt(o2)) && e2 <= 127 ? r += i.charAt(o2) : (2047 < e2 ? (r += String.fromCharCode(224 | e2 >> 12 & 15), r += String.fromCharCode(128 | e2 >> 6 & 63)) : r += String.fromCharCode(192 | e2 >> 6 & 31), r += String.fromCharCode(128 | e2 >> 0 & 63));
  return r;
}, SaveCodeImg = function(t2) {
  var i = UNIT_CONVERSION(Number(t2.width)), r = UNIT_CONVERSION(Number(t2.height)), e2 = getPixelRatio("pixelRatio"), o2 = i * e2, s2 = r * e2;
  return "MP-ALIPAY" == t2.source && (i = o2, r = s2), console.log(i, r), new Promise(function(e3) {
    "[object String]" == Object.prototype.toString.call(t2.id) ? index.canvasToTempFilePath({ canvasId: t2.id, width: i, height: r, destWidth: o2, destHeight: s2, fileType: t2.type || "jpg", quality: t2.quality || 1, complete: function(t3) {
      e3(t3);
    } }, t2.ctx) : "[object Object]" == Object.prototype.toString.call(t2.id) && t2.id.toTempFilePath(0, 0, i, r, o2, s2, t2.type || "png", 1, function(t3) {
      e3(t3);
    });
  });
}, SetGradient = function(t2, e2, i, r) {
  i = t2.createLinearGradient(0, 0, e2, i);
  return 1 === r.length && (i.addColorStop(0, r[0]), i.addColorStop(1, r[0])), 2 === r.length && (i.addColorStop(0, r[0]), i.addColorStop(1, r[1])), 3 === r.length && (i.addColorStop(0, r[0]), i.addColorStop(0.5, r[1]), i.addColorStop(1, r[2])), 4 === r.length && (i.addColorStop(0, r[0]), i.addColorStop(0.35, r[1]), i.addColorStop(0.7, r[2]), i.addColorStop(1, r[3])), 5 === r.length && (i.addColorStop(0, r[0]), i.addColorStop(0.35, r[1]), i.addColorStop(0.6, r[2]), i.addColorStop(0.8, r[3]), i.addColorStop(1, r[4])), 6 === r.length && (i.addColorStop(0, r[0]), i.addColorStop(0.25, r[1]), i.addColorStop(0.45, r[2]), i.addColorStop(0.65, r[3]), i.addColorStop(0.85, r[4]), i.addColorStop(1, r[5])), 7 === r.length && (i.addColorStop(0, r[0]), i.addColorStop(0.15, r[1]), i.addColorStop(0.35, r[2]), i.addColorStop(0.45, r[3]), i.addColorStop(0.65, r[4]), i.addColorStop(0.85, r[5]), i.addColorStop(1, r[6])), 8 === r.length && (i.addColorStop(0, r[0]), i.addColorStop(0.1, r[1]), i.addColorStop(0.25, r[2]), i.addColorStop(0.45, r[3]), i.addColorStop(0.65, r[4]), i.addColorStop(0.85, r[5]), i.addColorStop(0.9, r[6]), i.addColorStop(1, r[7])), 9 === r.length && (i.addColorStop(0, r[0]), i.addColorStop(0.2, r[1]), i.addColorStop(0.3, r[2]), i.addColorStop(0.5, r[3]), i.addColorStop(0.6, r[4]), i.addColorStop(0.7, r[5]), i.addColorStop(0.8, r[6]), i.addColorStop(0.9, r[7]), i.addColorStop(1, r[8])), 10 <= r.length && (i.addColorStop(0, r[0]), i.addColorStop(0.1, r[1]), i.addColorStop(0.2, r[2]), i.addColorStop(0.3, r[3]), i.addColorStop(0.4, r[4]), i.addColorStop(0.5, r[5]), i.addColorStop(0.6, r[6]), i.addColorStop(0.7, r[7]), i.addColorStop(0.85, r[8]), i.addColorStop(1, r[9])), i;
}, QRCodeInit = function() {
  function t2(t3) {
    void 0 === t3 && (t3 = 2), this.strinbuf = [], this.eccbuf = [], this.qrframe = [], this.framask = [], this.rlens = [], this.genpoly = [], this.ecclevel = 2, this.N1 = 3, this.N2 = 3, this.N3 = 40, this.N4 = 10, this.neccblk2 = 0, this.width = 0, this.neccblk1 = 0, this.datablkw = 0, this.eccblkwid = 0, this.ecclevel = t3;
  }
  return t2.prototype.setmask = function(t3, e2) {
    var i = null;
    e2 < t3 && (i = t3, t3 = e2, e2 = i), i = e2, i *= e2, i += e2, i >>= 1, this.framask[i += t3] = 1;
  }, t2.prototype.getWidth = function() {
    return this.width;
  }, t2.prototype.putalign = function(t3, e2) {
    this.qrframe[t3 + this.width * e2] = 1;
    for (var i = -2; i < 2; i++)
      this.qrframe[t3 + i + this.width * (e2 - 2)] = 1, this.qrframe[t3 - 2 + this.width * (e2 + i + 1)] = 1, this.qrframe[t3 + 2 + this.width * (e2 + i)] = 1, this.qrframe[t3 + i + 1 + this.width * (e2 + 2)] = 1;
    for (i = 0; i < 2; i++)
      this.setmask(t3 - 1, e2 + i), this.setmask(t3 + 1, e2 - i), this.setmask(t3 - i, e2 - 1), this.setmask(t3 + i, e2 + 1);
  }, t2.prototype.modnn = function(t3) {
    for (; 255 <= t3; )
      t3 = ((t3 -= 255) >> 8) + (255 & t3);
    return t3;
  }, t2.prototype.appendrs = function(t3, e2, i, r) {
    for (var o2, s2 = 0; s2 < r; s2++)
      this.strinbuf[i + s2] = 0;
    for (s2 = 0; s2 < e2; s2++) {
      if (255 != (o2 = GLOG[this.strinbuf[t3 + s2] ^ this.strinbuf[i]]))
        for (var n2 = 1; n2 < r; n2++)
          this.strinbuf[i + n2 - 1] = this.strinbuf[i + n2] ^ GEXP[this.modnn(o2 + this.genpoly[r - n2])];
      else
        for (n2 = i; n2 < i + r; n2++)
          this.strinbuf[n2] = this.strinbuf[n2 + 1];
      this.strinbuf[i + r - 1] = 255 == o2 ? 0 : GEXP[this.modnn(o2 + this.genpoly[0])];
    }
  }, t2.prototype.ismasked = function(t3, e2) {
    var i;
    return e2 < t3 && (i = t3, t3 = e2, e2 = i), i = e2, i += e2 * e2, i >>= 1, this.framask[i += t3];
  }, t2.prototype.badruns = function(t3) {
    for (var e2 = 0, i = 0; i <= t3; i++)
      5 <= this.rlens[i] && (e2 += this.N1 + this.rlens[i] - 5);
    for (i = 3; i < t3 - 1; i += 2)
      this.rlens[i - 2] == this.rlens[i + 2] && this.rlens[i + 2] == this.rlens[i - 1] && this.rlens[i - 1] == this.rlens[i + 1] && 3 * this.rlens[i - 1] == this.rlens[i] && (0 == this.rlens[i - 3] || t3 < i + 3 || 3 * this.rlens[i - 3] >= 4 * this.rlens[i] || 3 * this.rlens[i + 3] >= 4 * this.rlens[i]) && (e2 += this.N3);
    return e2;
  }, t2.prototype.toNum = function(t3) {
    return 0 === t3 ? 1 : 0;
  }, t2.prototype.applymask = function(t3) {
    switch (t3) {
      case 0:
        for (var e2 = 0; e2 < this.width; e2++)
          for (var i = 0; i < this.width; i++)
            i + e2 & 1 || this.ismasked(i, e2) || (this.qrframe[i + e2 * this.width] ^= 1);
        break;
      case 1:
        for (e2 = 0; e2 < this.width; e2++)
          for (i = 0; i < this.width; i++)
            1 & e2 || this.ismasked(i, e2) || (this.qrframe[i + e2 * this.width] ^= 1);
        break;
      case 2:
        for (e2 = 0; e2 < this.width; e2++)
          for (var r = 0, i = 0; i < this.width; i++, r++)
            (r = 3 == r ? 0 : r) || this.ismasked(i, e2) || (this.qrframe[i + e2 * this.width] ^= 1);
        break;
      case 3:
        for (var o2 = 0, e2 = 0; e2 < this.width; e2++, o2++)
          for (r = o2 = 3 == o2 ? 0 : o2, i = 0; i < this.width; i++, r++)
            (r = 3 == r ? 0 : r) || this.ismasked(i, e2) || (this.qrframe[i + e2 * this.width] ^= 1);
        break;
      case 4:
        for (e2 = 0; e2 < this.width; e2++)
          for (r = 0, o2 = e2 >> 1 & 1, i = 0; i < this.width; i++, r++)
            (o2 = 3 == r ? (r = 0) < o2 ? 0 : 1 : o2) || this.ismasked(i, e2) || (this.qrframe[i + e2 * this.width] ^= 1);
        break;
      case 5:
        for (o2 = 0, e2 = 0; e2 < this.width; e2++, o2++) {
          3 == o2 && (o2 = 0);
          for (r = 0, i = 0; i < this.width; i++, r++)
            3 == r && (r = 0), (i & e2 & 1) + this.toNum(this.toNum(r) | this.toNum(o2)) || this.ismasked(i, e2) || (this.qrframe[i + e2 * this.width] ^= 1);
        }
        break;
      case 6:
        for (o2 = 0, e2 = 0; e2 < this.width; e2++, o2++) {
          3 == o2 && (o2 = 0);
          for (r = 0, i = 0; i < this.width; i++, r++)
            (i & e2 & 1) + ((r = 3 == r ? 0 : r) && (r == o2 ? 1 : 0)) & 1 || this.ismasked(i, e2) || (this.qrframe[i + e2 * this.width] ^= 1);
        }
        break;
      case 7:
        for (o2 = 0, e2 = 0; e2 < this.width; e2++, o2++) {
          3 == o2 && (o2 = 0);
          for (r = 0, i = 0; i < this.width; i++, r++)
            ((r = 3 == r ? 0 : r) && (r == o2 ? 1 : 0)) + (i + e2 & 1) & 1 || this.ismasked(i, e2) || (this.qrframe[i + e2 * this.width] ^= 1);
        }
    }
  }, t2.prototype.Genframe = function(t3) {
    var e2 = t3.length, i = t3.slice(0), r = 0, o2 = 0, s2 = 0, n2 = 0, h = 0, a = 0;
    do {
      if (o2 = 4 * (this.ecclevel - 1) + 16 * (++h - 1), this.neccblk1 = ECBLOCKS[o2++], this.neccblk2 = ECBLOCKS[o2++], this.datablkw = ECBLOCKS[o2++], this.eccblkwid = ECBLOCKS[o2], e2 <= (o2 = this.datablkw * (this.neccblk1 + this.neccblk2) + this.neccblk2 - 3 + (h <= 9 ? 1 : 0)))
        break;
    } while (h < 40);
    this.width = 17 + 4 * h;
    for (var n2 = this.datablkw + (this.datablkw + this.eccblkwid) * (this.neccblk1 + this.neccblk2) + this.neccblk2, l = 0; l < n2; l++)
      this.eccbuf[l] = 0;
    for (l = 0; l < this.width * this.width; l++)
      this.qrframe[l] = 0;
    for (l = 0; l < (this.width * (this.width + 1) + 1) / 2; l++)
      this.framask[l] = 0;
    for (l = 0; l < 3; l++) {
      s2 = o2 = 0, 1 == l && (o2 = this.width - 7), 2 == l && (s2 = this.width - 7), this.qrframe[s2 + 3 + this.width * (o2 + 3)] = 1;
      for (var c = 0; c < 6; c++)
        this.qrframe[s2 + c + this.width * o2] = 1, this.qrframe[s2 + this.width * (o2 + c + 1)] = 1, this.qrframe[s2 + 6 + this.width * (o2 + c)] = 1, this.qrframe[s2 + c + 1 + this.width * (o2 + 6)] = 1;
      for (c = 1; c < 5; c++)
        this.setmask(s2 + c, o2 + 1), this.setmask(s2 + 1, o2 + c + 1), this.setmask(s2 + 5, o2 + c), this.setmask(s2 + c + 1, o2 + 5);
      for (c = 2; c < 4; c++)
        this.qrframe[s2 + c + this.width * (o2 + 2)] = 1, this.qrframe[s2 + 2 + this.width * (o2 + c + 1)] = 1, this.qrframe[s2 + 4 + this.width * (o2 + c)] = 1, this.qrframe[s2 + c + 1 + this.width * (o2 + 4)] = 1;
    }
    if (1 < h)
      for (e2 = ADELTA[h], s2 = this.width - 7; ; ) {
        for (r = this.width - 7; e2 - 3 < r && (this.putalign(r, s2), !(r < e2)); )
          r -= e2;
        if (s2 <= e2 + 9)
          break;
        s2 -= e2, this.putalign(6, s2), this.putalign(s2, 6);
      }
    this.qrframe[8 + this.width * (this.width - 8)] = 1;
    for (var d2 = 0; d2 < 7; d2++)
      this.setmask(7, d2), this.setmask(this.width - 8, d2), this.setmask(7, d2 + this.width - 7);
    for (c = 0; c < 8; c++)
      this.setmask(c, 7), this.setmask(c + this.width - 8, 7), this.setmask(c, this.width - 8);
    for (c = 0; c < 9; c++)
      this.setmask(c, 8);
    for (c = 0; c < 8; c++)
      this.setmask(c + this.width - 8, 8), this.setmask(8, c);
    for (d2 = 0; d2 < 7; d2++)
      this.setmask(8, d2 + this.width - 7);
    for (c = 0; c < this.width - 14; c++)
      1 & c ? (this.setmask(8 + c, 6), this.setmask(6, 8 + c)) : (this.qrframe[8 + c + 6 * this.width] = 1, this.qrframe[6 + this.width * (8 + c)] = 1);
    if (6 < h)
      for (e2 = VPAT[h - 7], o2 = 17, c = 0; c < 6; c++)
        for (d2 = 0; d2 < 3; d2++, o2--)
          1 & (11 < o2 ? h >> o2 - 12 : e2 >> o2) ? (this.qrframe[5 - c + this.width * (2 - d2 + this.width - 11)] = 1, this.qrframe[2 - d2 + this.width - 11 + this.width * (5 - c)] = 1) : (this.setmask(5 - c, 2 - d2 + this.width - 11), this.setmask(2 - d2 + this.width - 11, 5 - c));
    for (d2 = 0; d2 < this.width; d2++)
      for (c = 0; c <= d2; c++)
        this.qrframe[c + this.width * d2] && this.setmask(c, d2);
    n2 = i.length;
    for (var f2 = 0; f2 < n2; f2++)
      this.eccbuf[f2] = i.charCodeAt(f2);
    if (this.strinbuf = this.eccbuf.slice(0), (r = this.datablkw * (this.neccblk1 + this.neccblk2) + this.neccblk2) - 2 <= n2 && (n2 = r - 2, 9 < h && n2--), a = n2, 9 < h) {
      for (this.strinbuf[a + 2] = 0, this.strinbuf[a + 3] = 0; a--; )
        e2 = this.strinbuf[a], this.strinbuf[a + 3] |= 255 & e2 << 4, this.strinbuf[a + 2] = e2 >> 4;
      this.strinbuf[2] |= 255 & n2 << 4, this.strinbuf[1] = n2 >> 4, this.strinbuf[0] = 64 | n2 >> 12;
    } else {
      for (this.strinbuf[a + 1] = 0, this.strinbuf[a + 2] = 0; a--; )
        e2 = this.strinbuf[a], this.strinbuf[a + 2] |= 255 & e2 << 4, this.strinbuf[a + 1] = e2 >> 4;
      this.strinbuf[1] |= 255 & n2 << 4, this.strinbuf[0] = 64 | n2 >> 4;
    }
    for (a = n2 + 3 - (h < 10 ? 1 : 0); a < r; )
      this.strinbuf[a++] = 236, this.strinbuf[a++] = 17;
    this.genpoly[0] = 1;
    for (f2 = 0; f2 < this.eccblkwid; f2++) {
      this.genpoly[f2 + 1] = 1;
      for (var u = f2; 0 < u; u--)
        this.genpoly[u] = this.genpoly[u] ? this.genpoly[u - 1] ^ GEXP[this.modnn(GLOG[this.genpoly[u]] + f2)] : this.genpoly[u - 1];
      this.genpoly[0] = GEXP[this.modnn(GLOG[this.genpoly[0]] + f2)];
    }
    for (f2 = 0; f2 <= this.eccblkwid; f2++)
      this.genpoly[f2] = GLOG[this.genpoly[f2]];
    o2 = r;
    for (s2 = 0, f2 = 0; f2 < this.neccblk1; f2++)
      this.appendrs(s2, this.datablkw, o2, this.eccblkwid), s2 += this.datablkw, o2 += this.eccblkwid;
    for (f2 = 0; f2 < this.neccblk2; f2++)
      this.appendrs(s2, this.datablkw + 1, o2, this.eccblkwid), s2 += this.datablkw + 1, o2 += this.eccblkwid;
    for (f2 = s2 = 0; f2 < this.datablkw; f2++) {
      for (u = 0; u < this.neccblk1; u++)
        this.eccbuf[s2++] = this.strinbuf[f2 + u * this.datablkw];
      for (u = 0; u < this.neccblk2; u++)
        this.eccbuf[s2++] = this.strinbuf[this.neccblk1 * this.datablkw + f2 + u * (this.datablkw + 1)];
    }
    for (u = 0; u < this.neccblk2; u++)
      this.eccbuf[s2++] = this.strinbuf[this.neccblk1 * this.datablkw + a + u * (this.datablkw + 1)];
    for (f2 = 0; f2 < this.eccblkwid; f2++)
      for (u = 0; u < this.neccblk1 + this.neccblk2; u++)
        this.eccbuf[s2++] = this.strinbuf[r + f2 + u * this.eccblkwid];
    this.strinbuf = this.eccbuf, r = s2 = this.width - 1, o2 = n2 = 1;
    for (var p2 = (this.datablkw + this.eccblkwid) * (this.neccblk1 + this.neccblk2) + this.neccblk2, f2 = 0; f2 < p2; f2++) {
      e2 = this.strinbuf[f2];
      for (u = 0; u < 8; u++, e2 <<= 1)
        for (128 & e2 && (this.qrframe[r + this.width * s2] = 1); n2 ? r-- : (r++, o2 ? 0 != s2 ? s2-- : (o2 = 0 === o2 ? 1 : 0, 6 == (r -= 2) && (r--, s2 = 9)) : s2 != this.width - 1 ? s2++ : (o2 = 0 === o2 ? 1 : 0, 6 == (r -= 2) && (r--, s2 -= 8))), n2 = 0 < n2 ? 0 : 1, this.ismasked(r, s2); )
          ;
    }
    for (this.strinbuf = this.qrframe.slice(0), s2 = 3e4, o2 = e2 = 0; o2 < 8 && (this.applymask(o2), (r = this.badcheck()) < s2 && (s2 = r, e2 = o2), 7 != e2); o2++)
      this.qrframe = this.strinbuf.slice(0);
    e2 != o2 && this.applymask(e2), s2 = fmtword[e2 + (this.ecclevel - 1 << 3)];
    for (var S = 0; S < 8; S++, s2 >>= 1)
      1 & s2 && (this.qrframe[this.width - 1 - S + 8 * this.width] = 1, S < 6 ? this.qrframe[8 + this.width * S] = 1 : this.qrframe[8 + this.width * (S + 1)] = 1);
    for (S = 0; S < 7; S++, s2 >>= 1)
      1 & s2 && (this.qrframe[8 + this.width * (this.width - 7 + S)] = 1, S ? this.qrframe[6 - S + 8 * this.width] = 1 : this.qrframe[7 + 8 * this.width] = 1);
    return this.qrframe;
  }, t2.prototype.badcheck = function() {
    for (var t3 = 0, e2 = 0, i = 0, r = 0, o2 = 0, s2 = 0, n2 = 0, h = 0; h < this.width - 1; h++)
      for (var a = 0; a < this.width - 1; a++)
        (this.qrframe[a + this.width * h] && this.qrframe[a + 1 + this.width * h] && this.qrframe[a + this.width * (h + 1)] && this.qrframe[a + 1 + this.width * (h + 1)] || !(this.qrframe[a + this.width * h] || this.qrframe[a + 1 + this.width * h] || this.qrframe[a + this.width * (h + 1)] || this.qrframe[a + 1 + this.width * (h + 1)])) && (t3 += this.N2);
    for (h = 0; h < this.width; h++) {
      for (i = r = s2 = this.rlens[0] = 0; s2 < this.width; s2++)
        (o2 = this.qrframe[s2 + this.width * h]) == r ? this.rlens[i]++ : this.rlens[++i] = 1, e2 += (r = o2) ? 1 : -1;
      t3 += this.badruns(i);
    }
    var l = e2 = e2 < 0 ? -e2 : e2, c = 0;
    for (l += l << 2, l <<= 1; l > this.width * this.width; )
      l -= this.width * this.width, c++;
    t3 += c * this.N4;
    for (a = 0; a < this.width; a++) {
      for (i = r = n2 = this.rlens[0] = 0; n2 < this.width; n2++)
        (o2 = this.qrframe[a + this.width * n2]) == r ? this.rlens[i]++ : this.rlens[++i] = 1, r = o2;
      t3 += this.badruns(i);
    }
    return t3;
  }, t2;
}(), WidgetCode = function(t2, e2) {
  var i, r, o2, s2;
  t2.code ? t2.id ? (r = (/* @__PURE__ */ new Date()).getTime(), s2 = new QRCodeInit(t2.level), (o2 = UtF16TO8(t2.code)) ? (o2 = s2.Genframe(o2), s2 = s2.getWidth(), "[object String]" == Object.prototype.toString.call(t2.id) ? (i = index.createCanvasContext(t2.id, t2.ctx || null), RepaintCanvas(r, t2, i, o2, s2, e2)) : "[object Object]" == Object.prototype.toString.call(t2.id) && (i = t2.id, RepaintCanvas(r, t2, i, o2, s2, e2))) : console.warn("二维码code转换错误")) : console.warn("没有找到二维码canvas id或者实列!") : console.warn("没有找到二维码code");
}, RepaintCanvas = function(t2, e2, i, r, o2, s2) {
  var n2 = GETSIZE[e2.source || "none"] ? GETSIZE[e2.source || "none"](e2.size) : UNIT_CONVERSION(e2.size), h = GETSIZE[e2.source || "none"] ? GETSIZE[e2.source || "none"](e2.size) : UNIT_CONVERSION(e2.size), a = GETSIZE[e2.source || "none"] ? GETSIZE[e2.source || "none"](e2.size) : UNIT_CONVERSION(e2.size), l = (UNIT_CONVERSION(e2.padding || 0) || 0) + (e2.border ? e2.border.lineWidth || 5 : 0), c = Math.round(n2 / (o2 + l)), d2 = Math.floor((n2 - c * o2) / 2);
  i.clearRect(0, 0, h, a), i.setFillStyle(e2.bgColor || "#FFFFFF"), i.fillRect(0, 0, h, a), e2.src && i.drawImage(e2.src, 0, 0, n2, n2), e2.color ? SetColorCode(i, h, a, e2.color) : i.setFillStyle("#000000");
  for (var f2 = 0; f2 < o2; f2++)
    for (var u = 0; u < o2; u++)
      r[u * o2 + f2] && (SetCodeType[e2.type || "none"], SetCodeType[e2.type || "none"](e2.bgColor, i, c * f2 + d2, c * u + d2, c, c, e2.source));
  e2.img && (SetImageType[(null === (l = e2.img) || void 0 === l ? void 0 : l.type) || "none"] ? SetImageType[(null === (l = e2.img) || void 0 === l ? void 0 : l.type) || "none"](i, n2, e2.img, e2.source || "none") : SetImageType.none(i, n2, e2.img, e2.source || "none")), e2.text && SetTextCode(i, h, a, e2.text, e2.source || "none"), e2.border && SetBorderCode(i, h, a, e2.border, e2.source || "none"), i.restore(), starDraw(i, e2, t2, s2);
}, starDraw = function(t2, s2, n2, h) {
  var e2 = this;
  setTimeout(function() {
    t2.draw(false, function(o2) {
      return __awaiter(e2, void 0, void 0, function() {
        var e3, i, r;
        return __generator(this, function(t3) {
          switch (t3.label) {
            case 0:
              return h ? (e3 = h, i = [__assign({}, o2)], r = { createTime: getTimeDate(), takeUpTime: (/* @__PURE__ */ new Date()).getTime() - n2 }, [4, SaveCodeImg({ width: s2.size, height: s2.size, id: s2.id, source: s2.source, ctx: s2.ctx || null })]) : [3, 2];
            case 1:
              return e3.apply(void 0, [__assign.apply(void 0, i.concat([(r.img = t3.sent(), r.source = s2.source, r.model = getPixelRatio("model"), r.system = getPixelRatio("system"), r.platform = getPixelRatio("platform"), r.code = s2.code, r.size = UNIT_CONVERSION(s2.size), r.id = "[object String]" == Object.prototype.toString.call(s2.id) ? s2.id : "nvue", r)]))]), [3, 3];
            case 2:
              t3.label = 3;
            case 3:
              return [2];
          }
        });
      });
    });
  }, 300);
}, SetCodeType = { none: function(t2, e2, i, r, o2, s2, n2) {
  e2.fillRect(i, r, o2, s2);
}, starry: function(t2, e2, i, r, o2, s2, n2) {
  e2.drawImage("", i, r, o2, s2);
}, dots: function(t2, e2, i, r, o2, s2, n2) {
  void 0 === t2 && (t2 = "#ffffff"), e2.save(), e2.beginPath(), e2.arc(i, r, o2 / 2, 0, 2 * Math.PI), e2.closePath(), e2.fill(), e2.setLineWidth(1), e2.setStrokeStyle(t2), e2.stroke(), e2.clip(), e2.restore();
}, square: function(t2, e2, i, r, o2, s2, n2) {
  void 0 === t2 && (t2 = "#ffffff"), "MP-BAIDU" != n2 ? (e2.save(), e2.beginPath(), e2.moveTo(i, r), e2.arcTo(i + o2, r, i + o2, r + s2, 0), e2.arcTo(i + o2, r + s2, i, r + s2, 0), e2.arcTo(i, r + s2, i, r, 0), e2.arcTo(i, r, i + o2, r, 0), e2.fill(), e2.closePath(), e2.setLineWidth(1), e2.setStrokeStyle(t2), e2.stroke(), e2.clip(), e2.restore()) : e2.fillRect(i, r, o2, s2);
}, custom: function(t2, e2, i, r, o2, s2, n2) {
  e2.drawImage("", i, r, o2, s2);
} }, SetColorCode = function(t2, e2, i, r) {
  r = SetGradient(t2, e2, i, r);
  t2.setFillStyle(r);
}, SetImageType = { none: function(t2, e2, i, r) {
  r = GETSIZE[r](i.size || 30), e2 = Number(((e2 - r) / 2).toFixed(2));
  t2.save(), t2.drawImage(i.src, e2, e2, r, r);
}, circle: function(t2, e2, i, r) {
  var o2 = GETSIZE[r](i.size || 30), s2 = 2 * o2, n2 = e2 / 2 - o2, h = e2 / 2 - o2, a = n2 + o2, e2 = h + o2;
  t2.save(), t2.beginPath(), t2.arc(a, e2, o2, 0, 2 * Math.PI), t2.closePath(), t2.setLineWidth(GETSIZE[r](i.width || 5)), t2.setStrokeStyle(i.color || "#FFFFFF"), t2.stroke(), t2.clip(), t2.drawImage(i.src, n2, h, s2, s2);
}, round: function(t2, e2, i, r) {
  if ("MP-BAIDU" == r) {
    var o2 = GETSIZE[r](i.size || 30), s2 = 2 * o2, n2 = e2 / 2 - o2, h = e2 / 2 - o2, a = n2 + o2, l = h + o2;
    return t2.save(), t2.beginPath(), t2.arc(a, l, o2, 0, 2 * Math.PI), t2.closePath(), t2.setLineWidth(GETSIZE[r](i.width || 5)), t2.setStrokeStyle(i.color || "#FFFFFF"), t2.stroke(), t2.clip(), void t2.drawImage(i.src, n2, h, s2, s2);
  }
  l = i.degree || 5, o2 = GETSIZE[r](i.size || 30), n2 = o2, h = o2, s2 = e2 / 2 - o2 / 2, o2 = e2 / 2 - o2 / 2;
  h < 2 * (l = n2 < 2 * l ? n2 / 2 : l) && (l = h / 2), t2.save(), t2.beginPath(), t2.moveTo(s2 + l, o2), t2.arcTo(s2 + n2, o2, s2 + n2, o2 + h, l), t2.arcTo(s2 + n2, o2 + h, s2, o2 + h, l), t2.arcTo(s2, o2 + h, s2, o2, l), t2.arcTo(s2, o2, s2 + n2, o2, l), t2.closePath(), t2.setLineWidth(GETSIZE[r](i.width || 5)), t2.setStrokeStyle(i.color || "#FFFFFF"), t2.stroke(), t2.clip(), t2.drawImage(i.src, s2, o2, n2, n2);
} }, SetBorderCode = function(t2, e2, i, r, o2) {
  var s2 = (null == r ? void 0 : r.color) || ["#000000"], n2 = (null == r ? void 0 : r.degree) || 5, s2 = SetGradient(t2, e2, i, s2);
  t2.restore(), t2.setGlobalAlpha((null == r ? void 0 : r.opacity) || 1), "MP-BAIDU" == o2 ? (t2.setLineWidth((null == r ? void 0 : r.lineWidth) || 5), t2.setStrokeStyle(s2), t2.strokeRect(0, 0, e2, i)) : (t2.beginPath(), t2.moveTo(0 + n2, 0), t2.arcTo(0 + e2, 0, 0 + e2, 0 + i, n2), t2.arcTo(0 + e2, 0 + i, 0, 0 + i, n2), t2.arcTo(0, 0 + i, 0, 0, n2), t2.arcTo(0, 0, 0 + e2, 0, n2), t2.closePath(), t2.setLineWidth((null == r ? void 0 : r.lineWidth) || 5), t2.setStrokeStyle(s2), t2.stroke(), t2.clip()), t2.setGlobalAlpha(1);
}, SetTextCode = function(t2, e2, i, r, o2) {
  var s2 = r.color || ["#FFFFFF"], i = SetGradient(t2, e2, i, s2);
  t2.restore(), t2.setGlobalAlpha((null == r ? void 0 : r.opacity) || 1), t2.setTextAlign("center"), t2.setTextBaseline("middle"), t2.font = (null == r ? void 0 : r.font) || "normal 20px system-ui", "H5" == o2 ? t2.setFillStyle(i) : t2.setFillStyle(s2[0]), t2.fillText(r.content, e2 / 2, e2 / 2), t2.setGlobalAlpha(1);
}, QRCode = WidgetCode, GetImg = SaveCodeImg, GetPixelRatio = getPixelRatio, GetPx = UNIT_CONVERSION;
function isArray(arr) {
  return Object.prototype.toString.call(arr) === "[object Array]";
}
function deepClone(obj) {
  if ([null, void 0, NaN, false].includes(obj))
    return obj;
  if (typeof obj !== "object" && typeof obj !== "function") {
    return obj;
  }
  var o2 = isArray(obj) ? [] : {};
  for (let i in obj) {
    if (obj.hasOwnProperty(i)) {
      o2[i] = typeof obj[i] === "object" ? deepClone(obj[i]) : obj[i];
    }
  }
  return o2;
}
function getUUid(len = 32, firstU = true, radix = null) {
  let chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split("");
  let uuid = [];
  radix = radix || chars.length;
  if (len) {
    for (let i = 0; i < len; i++)
      uuid[i] = chars[0 | Math.random() * radix];
  } else {
    let r;
    uuid[8] = uuid[13] = uuid[18] = uuid[23] = "-";
    uuid[14] = "4";
    for (let i = 0; i < 36; i++) {
      if (!uuid[i]) {
        r = 0 | Math.random() * 16;
        uuid[i] = chars[i == 19 ? r & 3 | 8 : r];
      }
    }
  }
  if (firstU) {
    uuid.shift();
    return "u" + uuid.join("");
  } else {
    return uuid.join("");
  }
}
function platform() {
  let val = null;
  val = "VUE3";
  val = "MP";
  val = "MP-WEIXIN";
  return val;
}
exports.GetImg = GetImg;
exports.GetPixelRatio = GetPixelRatio;
exports.GetPx = GetPx;
exports.Pinia = Pinia;
exports.QRCode = QRCode;
exports._export_sfc = _export_sfc;
exports.computed = computed;
exports.createPinia = createPinia;
exports.createSSRApp = createSSRApp;
exports.d = d;
exports.deepClone = deepClone;
exports.defineStore = defineStore;
exports.e = e;
exports.f = f;
exports.getCurrentInstance = getCurrentInstance;
exports.getUUid = getUUid;
exports.hooks = hooks;
exports.index = index;
exports.initVueI18n = initVueI18n;
exports.mapStores = mapStores;
exports.n = n;
exports.nextTick$1 = nextTick$1;
exports.o = o;
exports.onDeactivated = onDeactivated;
exports.onHide = onHide;
exports.onLoad = onLoad;
exports.onMounted = onMounted;
exports.onReady = onReady;
exports.onShareAppMessage = onShareAppMessage;
exports.onShow = onShow;
exports.onUnload = onUnload;
exports.onUnmounted = onUnmounted;
exports.p = p;
exports.platform = platform;
exports.reactive = reactive;
exports.ref = ref;
exports.resolveComponent = resolveComponent;
exports.s = s;
exports.sr = sr;
exports.t = t;
exports.unref = unref;
exports.useCssVars = useCssVars;
exports.watch = watch;
exports.watchEffect = watchEffect;
exports.wx$1 = wx$1;
