"use strict";
const common_vendor = require("../common/vendor.js");
const userStore = common_vendor.defineStore("user", {
  state: () => {
    return {
      count: 10,
      userInfo: {
        shortPushCount: 0
      },
      taskVideoAd: {}
    };
  },
  // 也可以这样定义
  // state: () => ({ count: 0 })
  actions: {
    increment() {
      this.count++;
    },
    setUserInfo(data) {
      this.userInfo = data;
    },
    setTaskVideoAd(data) {
      this.taskVideoAd = data || {};
    }
  }
});
exports.userStore = userStore;
