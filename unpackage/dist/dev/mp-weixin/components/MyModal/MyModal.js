"use strict";
const common_vendor = require("../../common/vendor.js");
const stores_dynamicStyle = require("../../stores/dynamicStyle.js");
require("../../utils/messageAuth.js");
require("../../enum/GlobalEnum.js");
require("../../stores/user.js");
if (!Array) {
  const _easycom_MyButton2 = common_vendor.resolveComponent("MyButton");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_easycom_MyButton2 + _easycom_uni_popup2)();
}
const _easycom_MyButton = () => "../MyButton/MyButton.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_MyButton + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "MyModal",
  props: {
    openModalFlag: {
      type: Number
    },
    okFun: {
      type: Function
    },
    cancelFun: {
      type: Function
    },
    isShowFooter: {
      type: Boolean,
      default: true
    },
    modalBgc: {
      type: String,
      default: "#FFFFFF"
    },
    maskClick: {
      type: Boolean,
      default: true
    }
  },
  setup(__props) {
    const props = __props;
    common_vendor.useCssVars((_ctx) => ({
      "4ed64291": __props.modalBgc,
      "21a977ee": common_vendor.unref(primary_text_color)
    }));
    const dynamicStyle_store = stores_dynamicStyle.dynamicStyleStore();
    const {
      primary_text_color,
      primary_modal_bgc,
      primary_border_color,
      primary_orther_color
    } = dynamicStyle_store.globalStyle;
    const modalRef = common_vendor.ref();
    common_vendor.watch(() => props.openModalFlag, () => {
      if (props.openModalFlag > 0) {
        modalRef.value.open("center");
      } else {
        modalRef.value.close();
      }
    });
    const cancelFun = () => {
      modalRef.value.close();
      if (props.cancelFun) {
        props.cancelFun();
      }
    };
    const okFun = () => {
      props.okFun();
      modalRef.value.close();
    };
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: __props.isShowFooter
      }, __props.isShowFooter ? {
        b: common_vendor.p({
          bgc: common_vendor.unref(primary_orther_color),
          clickFun: cancelFun
        }),
        c: common_vendor.p({
          clickFun: okFun
        })
      } : {}, {
        d: common_vendor.sr(modalRef, "509d8399-0", {
          "k": "modalRef"
        }),
        e: common_vendor.p({
          ["mask-click"]: __props.maskClick,
          type: "dialog",
          ["safe-area"]: true
        }),
        f: common_vendor.s(_ctx.__cssVars())
      });
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/components/MyModal/MyModal.vue"]]);
wx.createComponent(Component);
