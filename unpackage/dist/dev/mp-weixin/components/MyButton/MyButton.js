"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  __name: "MyButton",
  props: {
    bgc: {
      type: String,
      default: "#65BFC7"
    },
    color: {
      type: String,
      default: "#fff"
    },
    fontSize: {
      type: String,
      default: "18px"
    },
    width: {
      type: String,
      default: "100%"
    },
    height: {
      type: String,
      default: "96rpx"
    },
    borderColor: {
      type: String
    },
    clickFun: {
      type: Function
    },
    padding: {
      type: String,
      default: "0 60rpx"
    },
    borderRadius: {
      type: String,
      default: "12rpx"
    }
  },
  setup(__props) {
    const props = __props;
    common_vendor.useCssVars((_ctx) => ({
      "7574efea": __props.borderRadius,
      "7c28f085": __props.padding,
      "2b0bd8e4": common_vendor.unref(_borderColor),
      "2f0e8b32": __props.bgc,
      "b4ab1e52": __props.color,
      "1eaf0a08": __props.fontSize,
      "b27d218c": __props.width,
      "179d7e33": __props.height
    }));
    const _borderColor = props.borderColor || props.bgc;
    const clickFun = () => {
      if (!props.clickFun)
        return;
      props.clickFun();
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o(clickFun),
        b: common_vendor.s(_ctx.__cssVars())
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/components/MyButton/MyButton.vue"]]);
wx.createComponent(Component);
