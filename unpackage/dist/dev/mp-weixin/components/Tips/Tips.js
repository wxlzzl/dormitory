"use strict";
const common_vendor = require("../../common/vendor.js");
const stores_user = require("../../stores/user.js");
const stores_dynamicStyle = require("../../stores/dynamicStyle.js");
const service_model_user = require("../../service/model/user.js");
const utils_changeData = require("../../utils/changeData.js");
const enum_GlobalEnum = require("../../enum/GlobalEnum.js");
require("../../utils/messageAuth.js");
require("../../service/request/http.js");
require("../../service/request/config.js");
require("../../utils/joinParamsUrl.js");
if (!Array) {
  const _easycom_uni_popup_message2 = common_vendor.resolveComponent("uni-popup-message");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  const _easycom_MyModal2 = common_vendor.resolveComponent("MyModal");
  (_easycom_uni_popup_message2 + _easycom_uni_popup2 + _easycom_MyModal2)();
}
const _easycom_uni_popup_message = () => "../../uni_modules/uni-popup/components/uni-popup-message/uni-popup-message.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
const _easycom_MyModal = () => "../MyModal/MyModal.js";
if (!Math) {
  (_easycom_uni_popup_message + _easycom_uni_popup + _easycom_MyModal + MyNull)();
}
const MyNull = () => "../MyNull/MyNull.js";
const _sfc_main = {
  __name: "Tips",
  setup(__props) {
    common_vendor.useCssVars((_ctx) => ({
      "b1a35e26": common_vendor.unref(primary_cool_color),
      "637eebd1": common_vendor.unref(primary_text_color),
      "a887621e": common_vendor.unref(primary_hot_color)
    }));
    const user_store = stores_user.userStore();
    const dynamicStyle_store = stores_dynamicStyle.dynamicStyleStore();
    const userModel = new service_model_user.UserModel();
    const tipsRef = common_vendor.ref();
    const messageText = common_vendor.ref("");
    const duration = common_vendor.ref();
    const msgType = common_vendor.ref("");
    const modalWXLoginFlag = common_vendor.ref(0);
    const modalAuthMessageFlag = common_vendor.ref(0);
    const openPhonePopubFlag = common_vendor.ref(0);
    const bindPhoneCallback = common_vendor.ref(false);
    const tipConent = common_vendor.ref("");
    const taskVideoAd = common_vendor.ref(null);
    const pushScreenAd = common_vendor.ref(null);
    const modelIsShowViewAdFlag = common_vendor.ref(0);
    const taskOkCallback = common_vendor.ref(null);
    const showIsViewAdTaskName = common_vendor.ref("");
    common_vendor.ref(false);
    const nowPage = common_vendor.ref("");
    const protocolData = common_vendor.ref({
      title: "",
      needAuthorization: -1,
      callback: null
    });
    const noBindPhone = common_vendor.computed(() => user_store.userInfo.phone === enum_GlobalEnum.DefaultPhone);
    const {
      primary_light_color,
      primary_text_color,
      primary_orther_color,
      primary_hot_color,
      primary_cool_color
    } = dynamicStyle_store.dormitoryStyle;
    const tipsCallback = (msg, type = "success", durationNum = 1500) => {
      msgType.value = type;
      messageText.value = msg;
      duration.value = durationNum;
      common_vendor.nextTick$1(() => {
        var _a;
        ((_a = tipsRef.value) == null ? void 0 : _a.open) && tipsRef.value.open();
      });
    };
    const setUserStore = (data) => {
      user_store.setUserInfo(data);
    };
    const getPages = (isFromNormal) => {
      if (isFromNormal)
        return;
      const pages = getCurrentPages();
      const nowFullPath = pages[pages.length - 1].$page.fullPath;
      const tabPages = [
        "/pages/Dormitory/Dormitory",
        "/pages/Dormitorys/Dormitorys",
        "/pages/User/User"
      ];
      if (tabPages.indexOf(nowFullPath) !== -1) {
        return;
      } else {
        common_vendor.index.redirectTo({
          url: nowFullPath
        });
      }
    };
    const joinPublicityActice = async (publicity_id) => {
      const { data, success } = await userModel.postJoinPublicity({}, publicity_id);
      common_vendor.index.removeStorageSync("newUserActive");
      if (success) {
        common_vendor.index.$emit("tips", data, "success", 3e3);
      }
    };
    const unBindPhoneFun = async () => {
      const { data, success } = await userModel.patchUnBindPhone({});
      if (success) {
        common_vendor.index.$emit("tips", data, "success");
        openPhonePopubFlag.value = -Math.random();
      }
      getUserInfo();
    };
    const getUserInfo = async (isFromNormal = false) => {
      const { data, success } = await userModel.getUserInfo();
      if (success) {
        data.createAt = utils_changeData.changeDate(data.createAt);
        common_vendor.index.$emit("userStore", data);
        const newUserActiveId = common_vendor.index.getStorageSync("newUserActive");
        if (data.isJoinActiveByNewUser === enum_GlobalEnum.UserIsJoinActiveByNewUser.NO && (newUserActiveId == null ? void 0 : newUserActiveId.publicity_id)) {
          joinPublicityActice(newUserActiveId.publicity_id);
        } else if (newUserActiveId == null ? void 0 : newUserActiveId.publicity_id) {
          common_vendor.index.$emit("tips", "您已经不是新用户了", "info", 2e3);
          common_vendor.index.removeStorageSync("newUserActive");
        }
        getPages(isFromNormal);
      }
    };
    const handleCallback = common_vendor.ref();
    const viewedAuthMessageFun = () => {
      handleCallback.value();
    };
    const mpGetUserInfo = async () => {
      common_vendor.index.login({
        async success(response) {
          common_vendor.index.showLoading({
            title: "正在登录",
            mask: true
          });
          const {
            success: resultSuccess,
            data: resultData
          } = await userModel.postLoginByWX({
            code: response.code
          });
          if (resultSuccess) {
            if (resultData.isOldUser) {
              common_vendor.index.setStorageSync("token", resultData.data);
              common_vendor.index.$emit("tips", "登录成功", "success");
            } else {
              common_vendor.index.$emit("tips", "登录成功,建议您去修改个人信息", "success");
              common_vendor.index.setStorageSync("token", resultData.data);
            }
            common_vendor.index.hideLoading();
            getUserInfo();
          } else {
            common_vendor.index.$emit("tips", resultData, "error");
          }
        }
      });
    };
    const decryptPhoneNumber = async (v) => {
      if (!noBindPhone.value) {
        const format = "YYYY-MM-DD";
        const isTodayChangeBindPhone = common_vendor.index.getStorageSync("isTodayChangeBindPhone") || {
          date: common_vendor.hooks().format(format),
          count: 0
        };
        if (isTodayChangeBindPhone.date === common_vendor.hooks().format(format)) {
          if (isTodayChangeBindPhone.count >= 2) {
            common_vendor.index.$emit("tips", "每天只能换绑两次手机号", "warn", 2e3);
            openPhonePopubFlag.value = -Math.random();
            return;
          } else {
            isTodayChangeBindPhone.count++;
            common_vendor.index.setStorageSync("isTodayChangeBindPhone", isTodayChangeBindPhone);
          }
        } else {
          isTodayChangeBindPhone.date = common_vendor.hooks().format(format);
          isTodayChangeBindPhone.count = 1;
          common_vendor.index.setStorageSync("isTodayChangeBindPhone", isTodayChangeBindPhone);
        }
      }
      const { code } = v.detail;
      if (!code)
        ;
      else {
        const { data, success } = await userModel.getPhoneNum({ code });
        if (success) {
          common_vendor.index.$emit("tips", data, "success");
          openPhonePopubFlag.value = -Math.random();
          setTimeout(() => {
            getUserInfo(true);
            bindPhoneCallback.value && bindPhoneCallback.value();
            bindPhoneCallback.value = false;
          }, 1e3);
        }
      }
    };
    const openAuthMessageTipFun = (e) => {
      handleCallback.value = e;
      modalAuthMessageFlag.value = Math.random();
    };
    const openWxLoginModelFun = () => {
      modalWXLoginFlag.value = Math.random();
    };
    const openPhonePopubFlagFun = (fn = false, isOrtherNeedOpen = false, _tipConent = "及时收到值日提醒") => {
      tipConent.value = _tipConent;
      if (fn && !isOrtherNeedOpen && common_vendor.index.getStorageSync("openPhoneNum") !== void 0 && common_vendor.index.getStorageSync("openPhoneNum") <= 20) {
        common_vendor.index.setStorageSync("openPhoneNum", (common_vendor.index.getStorageSync("openPhoneNum") || 0) + 1);
        return;
      } else {
        common_vendor.index.setStorageSync("openPhoneNum", 0);
      }
      console.log("打卡打卡");
      bindPhoneCallback.value = fn;
      openPhonePopubFlag.value = Math.random();
    };
    const goGetPrivacyInfo = () => {
      common_vendor.wx$1.openPrivacyContract({});
    };
    const checkUserPrivacyProtocol = (callback) => {
      if (common_vendor.wx$1.getPrivacySetting) {
        common_vendor.wx$1.getPrivacySetting({
          success: (res) => {
            console.log("隐私", res);
            protocolData.value.needAuthorization = res.needAuthorization ? Math.random() : -Math.random();
            if (res.needAuthorization) {
              protocolData.value.callback = callback;
              protocolData.value.title = res.privacyContractName;
            } else {
              user_store.setUserInfo({
                ...user_store.userInfo,
                isPrivacy: true
              });
              callback && callback();
              protocolData.value.callback = null;
            }
          }
        });
      }
    };
    const handleAgreePrivacyAuthorization = () => {
      protocolData.value.needAuthorization = -Math.random();
      user_store.setUserInfo({
        ...user_store.userInfo,
        isPrivacy: true
      });
      common_vendor.nextTick$1(() => {
        protocolData.value.callback && protocolData.value.callback();
        protocolData.value.callback = null;
      });
    };
    const handleRefulePrivacy = () => {
      user_store.setUserInfo({
        ...user_store.userInfo,
        isPrivacy: false
      });
      protocolData.value.needAuthorization = -Math.random();
      common_vendor.index.$emit("tips", "拒绝后，平台将无法获取您的头像、微信昵称、选择的图片、手机号，可能会影响到您使用平台的相应功能", "info", 3e3);
    };
    const createPushScreenAd = () => {
      const pushShowLastTime = common_vendor.index.getStorageSync("pushShowLastTime");
      const formart = "YYYY-MM-DD";
      if (pushShowLastTime === common_vendor.hooks().format(formart)) {
        console.log("不允许创建插屏广告");
        return;
      }
      if (common_vendor.wx$1.createInterstitialAd) {
        pushScreenAd.value = common_vendor.wx$1.createInterstitialAd({ adUnitId: "adunit-ab4aedfd646df784" });
        pushScreenAd.value.onLoad(() => {
          console.log("插屏广告创建成功");
        });
        pushScreenAd.value.onError((err) => {
          console.log("插屏广告创建失败", err);
        });
        pushScreenAd.value.onClose((res) => {
          common_vendor.index.$emit("tips", "每天我(开发者)要付出40元的成本去维护这个平台，广告是我唯一的补贴来源哦", "info", 3e3);
          if (!pushShowLastTime) {
            common_vendor.index.setStorageSync("pushShowLastTime", common_vendor.hooks().format(formart));
          } else {
            if (pushShowLastTime === common_vendor.hooks().format(formart)) {
              return;
            } else {
              common_vendor.index.setStorageSync("pushShowLastTime", common_vendor.hooks().format(formart));
            }
          }
        });
      }
    };
    const showPushScreenAd = () => {
      if (pushScreenAd.value) {
        pushScreenAd.value.show().catch((err) => {
          console.log("弹出插屏广告失败", err);
        });
      }
    };
    const autoShowPushScreenAd = () => {
      setTimeout(() => {
        showPushScreenAd();
      }, 2e3);
    };
    const createTaskVideoAd = () => {
      taskVideoAd.value = null;
      if (common_vendor.wx$1.createRewardedVideoAd) {
        taskVideoAd.value = common_vendor.wx$1.createRewardedVideoAd({
          adUnitId: "adunit-5f7f25ffbe0a4329"
        });
        taskVideoAd.value.onLoad(() => {
        });
        taskVideoAd.value.onError((err) => {
          common_vendor.index.$emit("tips", "因第三方原因，请重试一下哦", "error", 1500);
          common_vendor.index.setStorageSync("taskVideoAdAndPageCatch", null);
        });
        taskVideoAd.value.onClose((res) => {
          const { isEnded } = res;
          if (res && isEnded) {
            if (taskOkCallback.value) {
              taskOkCallback.value();
              taskOkCallback.value = null;
              return;
            }
            common_vendor.index.$emit("tips", "任务完成,请重新" + showIsViewAdTaskName.value, "success");
            let taskVideoAdAndPageCatch = common_vendor.index.getStorageSync("taskVideoAdAndPageCatch");
            taskVideoAdAndPageCatch[nowPage.value].count = taskVideoAdAndPageCatch[nowPage.value].count + 1;
            common_vendor.index.setStorageSync("taskVideoAdAndPageCatch", taskVideoAdAndPageCatch);
          } else {
            taskOkCallback.value = null;
            common_vendor.index.$emit("tips", "没有看完哦，坚持下哦", "info");
          }
        });
      }
    };
    const handlePageByTaskVideoAd = () => {
      var _a, _b;
      const pages = getCurrentPages();
      nowPage.value = (_a = pages[pages.length - 1]) == null ? void 0 : _a.route;
      if (!nowPage.value) {
        return;
      }
      console.log("当前页面", nowPage.value);
      const nowTime = common_vendor.hooks().format("YYYY-MM-DD");
      let taskVideoAdAndPageCatch = common_vendor.index.getStorageSync("taskVideoAdAndPageCatch");
      if (taskVideoAdAndPageCatch) {
        if (!taskVideoAdAndPageCatch[nowPage.value]) {
          taskVideoAdAndPageCatch[nowPage.value] = {
            count: 0,
            time: nowTime
          };
        } else {
          const lastTime = (_b = taskVideoAdAndPageCatch[nowPage.value]) == null ? void 0 : _b.time;
          if (lastTime !== nowTime) {
            taskVideoAdAndPageCatch = null;
          }
        }
      } else {
        taskVideoAdAndPageCatch = {};
        taskVideoAdAndPageCatch[nowPage.value] = {
          count: 0,
          time: nowTime
        };
      }
      common_vendor.index.setStorageSync("taskVideoAdAndPageCatch", taskVideoAdAndPageCatch);
    };
    const okViewAdVideoFun = () => {
      if (taskVideoAd.value) {
        taskVideoAd.value.show().catch(() => {
          taskVideoAd.value.load().then(() => taskVideoAd.value.show()).catch((err) => {
            console.log("激励视频 广告显示失败");
          });
        });
      } else {
        common_vendor.index.$emit("tips", "请重新进入该页面后尝试", "info", 1500);
      }
    };
    const cancelViewAdVideoFun = () => {
      common_vendor.index.$emit("tips", "每天我(开发者)要付出40元的成本去维护这个平台，广告是我唯一的补贴来源哦", "info", 3e3);
      modelIsShowViewAdFlag.value = -Math.random();
    };
    const isViewTaskVideoAdFun = (task) => {
      showIsViewAdTaskName.value = task;
      modelIsShowViewAdFlag.value = Math.random();
    };
    const viewVideoGoTask = (callback) => {
      taskOkCallback.value = callback;
      okViewAdVideoFun();
    };
    common_vendor.onShow(() => {
      console.log("测试挂在-挂");
      common_vendor.index.$on("tips", tipsCallback);
      common_vendor.index.$on("userStore", setUserStore);
      common_vendor.index.$on("wxLogin", mpGetUserInfo);
      common_vendor.index.$on("checkUserPrivacyProtocol", checkUserPrivacyProtocol);
      common_vendor.nextTick$1(() => {
        common_vendor.index.$on("openWXLoin", openWxLoginModelFun);
        common_vendor.index.$on("openAuthMessageTip", openAuthMessageTipFun);
        common_vendor.index.$on("openPhonePopubFlag", openPhonePopubFlagFun);
        common_vendor.index.$on("openIsViewTaskVideoAd", isViewTaskVideoAdFun);
        common_vendor.index.$on("viewVideoGoTask", viewVideoGoTask);
        common_vendor.index.$on("showPushScreenAd", showPushScreenAd);
        common_vendor.index.$on("getUserInfo", getUserInfo);
        common_vendor.index.$on("autoShowPushScreenAd", autoShowPushScreenAd);
      });
      common_vendor.index.getStorageSync("token") && !(user_store == null ? void 0 : user_store.userInfo.id) && getUserInfo(true);
      common_vendor.index.getStorageSync("newUserActive") && (user_store == null ? void 0 : user_store.userInfo.id) && getUserInfo(true);
      handlePageByTaskVideoAd();
    });
    common_vendor.onHide(() => {
      console.log("测试挂在-卸", 0);
      common_vendor.index.$off("tips", tipsCallback);
      common_vendor.index.$off("userStore", setUserStore);
      common_vendor.index.$off("wxLogin", mpGetUserInfo);
      common_vendor.index.$off("openWXLoin", openWxLoginModelFun);
      common_vendor.index.$off("openAuthMessageTip", openAuthMessageTipFun);
      common_vendor.index.$off("openPhonePopubFlag", openPhonePopubFlagFun);
      common_vendor.index.$off("openIsViewTaskVideoAd", isViewTaskVideoAdFun);
      common_vendor.index.$off("viewVideoGoTask", viewVideoGoTask);
      common_vendor.index.$off("showPushScreenAd", showPushScreenAd);
      common_vendor.index.$off("checkUserPrivacyProtocol", checkUserPrivacyProtocol);
      common_vendor.index.$off("autoShowPushScreenAd", autoShowPushScreenAd);
      common_vendor.index.$off("getUserInfo", getUserInfo);
      protocolData.value.callback = null;
    });
    common_vendor.onLoad(() => {
      createTaskVideoAd();
      createPushScreenAd();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          type: msgType.value,
          message: messageText.value,
          duration: duration.value
        }),
        b: common_vendor.sr(tipsRef, "1451e355-0", {
          "k": "tipsRef"
        }),
        c: common_vendor.p({
          type: "message"
        }),
        d: common_vendor.p({
          openModalFlag: modalWXLoginFlag.value,
          okFun: mpGetUserInfo
        }),
        e: common_vendor.t(protocolData.value.title),
        f: common_vendor.o(() => goGetPrivacyInfo()),
        g: common_vendor.t(protocolData.value.title),
        h: common_vendor.o(() => handleRefulePrivacy()),
        i: common_vendor.o(() => handleAgreePrivacyAuthorization()),
        j: common_vendor.p({
          openModalFlag: protocolData.value.needAuthorization
        }),
        k: common_vendor.t(showIsViewAdTaskName.value),
        l: common_vendor.p({
          openModalFlag: modelIsShowViewAdFlag.value,
          okFun: okViewAdVideoFun,
          cancelFun: cancelViewAdVideoFun
        }),
        m: common_vendor.p({
          img: "suggestMessage.jpg"
        }),
        n: common_vendor.p({
          openModalFlag: modalAuthMessageFlag.value,
          okFun: viewedAuthMessageFun,
          cancelFun: viewedAuthMessageFun
        }),
        o: common_vendor.t(common_vendor.unref(noBindPhone) ? "绑定手机号" : "解绑手机号"),
        p: common_vendor.unref(noBindPhone)
      }, common_vendor.unref(noBindPhone) ? {
        q: common_vendor.t(tipConent.value)
      } : {}, {
        r: common_vendor.p({
          img: "bindPhone.png"
        }),
        s: common_vendor.o(() => openPhonePopubFlag.value = -Math.random()),
        t: common_vendor.unref(noBindPhone)
      }, common_vendor.unref(noBindPhone) ? {
        v: common_vendor.o(decryptPhoneNumber)
      } : {
        w: common_vendor.o(() => unBindPhoneFun())
      }, {
        x: common_vendor.p({
          openModalFlag: openPhonePopubFlag.value,
          cancelFun: viewedAuthMessageFun
        }),
        y: common_vendor.s(_ctx.__cssVars())
      });
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/components/Tips/Tips.vue"]]);
wx.createComponent(Component);
