"use strict";
const common_vendor = require("../../common/vendor.js");
const utils_imgJoinName = require("../../utils/imgJoinName.js");
require("../../config/globalConfig.js");
require("../../enum/ImgFolderEnum.js");
const _sfc_main = {
  __name: "Avatar",
  props: {
    size: {
      type: String
    },
    fileName: {
      type: String
    },
    radius: {
      type: String,
      default: "50rpx"
    },
    joinUrlFun: {
      type: Function,
      default: utils_imgJoinName.joinAvatarPath
    },
    isAllowPreview: {
      type: Boolean,
      default: false
    },
    isStop: {
      type: Boolean,
      default: false
    }
  },
  setup(__props) {
    const props = __props;
    common_vendor.useCssVars((_ctx) => ({
      "016d5131": __props.radius
    }));
    const clickImgFun = () => {
      props.isAllowPreview && common_vendor.index.previewImage({
        urls: [props.joinUrlFun(props.fileName)]
      });
    };
    const initStyle = (size) => {
      return `width: ${size}; height: ${size};`;
    };
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: __props.isStop
      }, __props.isStop ? {
        b: common_vendor.s(initStyle(__props.size)),
        c: __props.joinUrlFun(__props.fileName),
        d: common_vendor.o(clickImgFun),
        e: common_vendor.s(_ctx.__cssVars())
      } : {
        f: common_vendor.s(initStyle(__props.size)),
        g: __props.joinUrlFun(__props.fileName),
        h: common_vendor.o(clickImgFun),
        i: common_vendor.s(_ctx.__cssVars())
      });
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/components/Avatar/Avatar.vue"]]);
wx.createComponent(Component);
