"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  __name: "MyTag",
  props: {
    bgc: {
      type: String,
      default: "rgba(0,0,0,0)"
    },
    borderBgc: {
      type: String,
      default: "#F5F5F5"
    },
    color: {
      type: String,
      default: "white"
    },
    fontSize: {
      type: String,
      default: "20rpx"
    }
  },
  setup(__props) {
    common_vendor.useCssVars((_ctx) => ({
      "0178f1f9": __props.bgc,
      "421f6ded": __props.borderBgc,
      "72623895": __props.fontSize,
      "f1d37c44": __props.color
    }));
    return (_ctx, _cache) => {
      return {
        a: common_vendor.s(_ctx.__cssVars())
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/components/MyTag/MyTag.vue"]]);
wx.createComponent(Component);
