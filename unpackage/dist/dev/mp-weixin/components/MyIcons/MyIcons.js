"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  __name: "MyIcons",
  props: {
    size: {
      type: String
    },
    src: {
      type: String
    }
  },
  setup(__props) {
    common_vendor.ref("");
    const initStyle = (size) => {
      return `width: ${size}; height: ${size};`;
    };
    return (_ctx, _cache) => {
      return {
        a: __props.src,
        b: common_vendor.s(initStyle(__props.size))
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/components/MyIcons/MyIcons.vue"]]);
wx.createComponent(Component);
