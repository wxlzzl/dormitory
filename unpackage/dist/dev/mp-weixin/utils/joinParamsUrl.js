"use strict";
const joinParamsUrl = (url, params) => {
  for (const obj of params) {
    const { key, value } = obj;
    url = url.replace("$" + key, value);
  }
  return url;
};
const getUrlParam = (url) => {
  let params = url.split("?")[1].split("&");
  let obj = {};
  params.map((v) => obj[v.split("=")[0]] = v.split("=")[1]);
  return obj;
};
exports.getUrlParam = getUrlParam;
exports.joinParamsUrl = joinParamsUrl;
