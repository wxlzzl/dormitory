"use strict";
const config_globalConfig = require("../config/globalConfig.js");
const enum_ImgFolderEnum = require("../enum/ImgFolderEnum.js");
const joinMainPath = (fileName, folder) => {
  const path = `https://${config_globalConfig.imgConfig.BUCKET}.cos.${config_globalConfig.imgConfig.REGION}.${config_globalConfig.imgConfig.TX_COS_DOMAIN}/${folder}/${fileName}`;
  return path;
};
const joinAvatarPath = (fileName) => {
  const path = joinMainPath(fileName, enum_ImgFolderEnum.ImgFolder.AVATAR_FOLDER);
  return path;
};
const joinPublicityCodePath = (fileName) => {
  const path = joinMainPath(fileName, enum_ImgFolderEnum.ImgFolder.PUBLICITY_CODE_IMG_FOLDER);
  return path;
};
const joinSystemPath = (fileName) => {
  const path = joinMainPath(fileName, enum_ImgFolderEnum.ImgFolder.SYSTEM_FOLDER);
  return path;
};
const joinDutyClockImg = (fileName) => {
  const path = joinMainPath(fileName, enum_ImgFolderEnum.ImgFolder.DUTY_CLOCK_IMG_FOLDER);
  return path;
};
const joinDormitoryCover = (fileName) => {
  const path = joinMainPath(fileName, enum_ImgFolderEnum.ImgFolder.DORMITORY_COVER_FOLDER);
  return path;
};
const joinBillProof = (fileName) => {
  const path = joinMainPath(fileName, enum_ImgFolderEnum.ImgFolder.BILL_PROOF_FOLDER);
  return path;
};
const joinEstateImg = (fileName) => {
  const path = joinMainPath(fileName, enum_ImgFolderEnum.ImgFolder.PUBLIC_ESTATE_FOLDER);
  return path;
};
const joinJoinBgcImg = (fileName) => {
  const path = joinMainPath(fileName, enum_ImgFolderEnum.ImgFolder.JOIN_BGC_FOLDER);
  return path;
};
const joinVoteImg = (fileName) => {
  const path = joinMainPath(fileName, enum_ImgFolderEnum.ImgFolder.VOTE_IMG);
  return path;
};
exports.joinAvatarPath = joinAvatarPath;
exports.joinBillProof = joinBillProof;
exports.joinDormitoryCover = joinDormitoryCover;
exports.joinDutyClockImg = joinDutyClockImg;
exports.joinEstateImg = joinEstateImg;
exports.joinJoinBgcImg = joinJoinBgcImg;
exports.joinPublicityCodePath = joinPublicityCodePath;
exports.joinSystemPath = joinSystemPath;
exports.joinVoteImg = joinVoteImg;
