"use strict";
Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" });
const common_vendor = require("./common/vendor.js");
if (!Math) {
  "./pages/Dormitory/Dormitory.js";
  "./pages/Dormitorys/Dormitorys.js";
  "./pages/User/User.js";
  "./pages/User/Login/Login.js";
  "./pages/Dormitorys/CreateDormitory/CreateDormitory.js";
  "./pages/Dormitory/DormitoryInvite/DormitoryInvite.js";
  "./pages/Dormitory/DormitorySet/DormitorySet.js";
  "./pages/Dormitory/Notice/Notice.js";
  "./pages/Dormitory/CreateNotice/CreateNotice.js";
  "./pages/Dormitory/Notice/NoticeInfo/NoticeInfo.js";
  "./pages/Dormitory/NoticeViewState/NoticeViewState.js";
  "./pages/Dormitory/AddBill/AddBill.js";
  "./pages/Dormitory/Bills/Bills.js";
  "./pages/Dormitory/Dutys/Dutys.js";
  "./pages/Dormitory/Estates/Estates.js";
  "./pages/Dormitory/AddEstate/AddEstate.js";
  "./pages/Dormitory/Memos/Memos.js";
  "./pages/Dormitory/Lots/Lots.js";
  "./pages/Dormitory/LotInfo/LotInfo.js";
  "./pages/User/AboutUs/AboutUs.js";
  "./pages/Dormitory/JoinBgcs/JoinBgcs.js";
  "./pages/Dormitory/InviteHelp/InviteHelp.js";
  "./pages/Dormitory/helpRecord/helpRecord.js";
  "./pages/Dormitory/DutyClock/DutyClock.js";
  "./pages/User/Publicity/Publicity.js";
  "./pages/Dormitory/DutyAdmin/DutyAdmin.js";
  "./pages/Dormitory/CreateRandomLots/CreateRandomLots.js";
  "./pages/Dormitory/RandomJoinRecord/RandomJoinRecord.js";
  "./pages/Dormitory/NewUserHelp/NewUserHelp.js";
  "./pages/Dormitory/CreateVote/CreateVote.js";
  "./pages/Dormitory/VoteInfo/VoteInfo.js";
}
const _sfc_main = {
  onLaunch: function() {
    console.warn("当前组件仅支持 uni_modules 目录结构 ，请升级 HBuilderX 到 3.1.0 版本以上！");
    console.log("App Launch");
  },
  onShow: function() {
    console.log("App Sow");
  },
  onHide: function() {
    console.log("App Hide");
  }
};
const App = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/App.vue"]]);
function createApp() {
  const app = common_vendor.createSSRApp(App);
  app.use(common_vendor.createPinia());
  return {
    app,
    Pinia: common_vendor.Pinia
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
