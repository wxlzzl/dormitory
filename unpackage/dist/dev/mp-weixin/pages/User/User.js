"use strict";
const common_vendor = require("../../common/vendor.js");
const utils_imgJoinName = require("../../utils/imgJoinName.js");
const utils_changeData = require("../../utils/changeData.js");
const enum_GlobalEnum = require("../../enum/GlobalEnum.js");
const stores_user = require("../../stores/user.js");
const stores_dynamicStyle = require("../../stores/dynamicStyle.js");
const service_model_user = require("../../service/model/user.js");
require("../../config/globalConfig.js");
require("../../enum/ImgFolderEnum.js");
require("../../service/request/http.js");
require("../../service/request/config.js");
require("../../utils/joinParamsUrl.js");
if (!Math) {
  (Tips + Avatar + MyIcons + MyTag + MyButton + EditUserList + MyCard + AboutUsList)();
}
const Tips = () => "../../components/Tips/Tips.js";
const MyIcons = () => "../../components/MyIcons/MyIcons.js";
const Avatar = () => "../../components/Avatar/Avatar.js";
const EditUserList = () => "../../components/User/EditUserList/EditUserList.js";
const AboutUsList = () => "../../components/User/AboutUsList/AboutUsList.js";
const MyCard = () => "../../components/MyCard/MyCard.js";
const MyTag = () => "../../components/MyTag/MyTag.js";
const MyButton = () => "../../components/MyButton/MyButton.js";
const _sfc_main = {
  __name: "User",
  setup(__props) {
    common_vendor.useCssVars((_ctx) => ({
      "3c5acdc0": common_vendor.unref(primary_main_bgc),
      "4e050a09": common_vendor.unref(primary_hot_color)
    }));
    const user_store = stores_user.userStore();
    const dynamicStyle_store = stores_dynamicStyle.dynamicStyleStore();
    const userModel = new service_model_user.UserModel();
    const userInfo = common_vendor.ref(user_store.userInfo);
    const joinDays = common_vendor.ref(0);
    common_vendor.watch(() => user_store.userInfo, () => {
      userInfo.value = user_store.userInfo;
      getJoinSystemDays();
    });
    const {
      primary_hot_color,
      user_box_bgc,
      primary_text_color,
      primary_main_bgc
    } = dynamicStyle_store.globalStyle;
    const toLogin = () => {
      common_vendor.index.$emit("wxLogin");
    };
    const getUserInfo = async () => {
      if (!common_vendor.index.getStorageSync("token"))
        return;
      const { data, success } = await userModel.getUserInfo();
      if (success) {
        data.createAt = utils_changeData.changeDate(data.createAt);
        common_vendor.index.$emit("userStore", data);
        userInfo.value = data;
        getJoinSystemDays();
      }
    };
    const getJoinSystemDays = () => {
      var _a;
      if (!((_a = userInfo.value) == null ? void 0 : _a.user_id))
        return;
      const timeFormat = "YYYY-MM-DD";
      const nowDate = utils_changeData.toTimeStamp(common_vendor.hooks().format(timeFormat));
      const joinDate = utils_changeData.toTimeStamp(common_vendor.hooks(userInfo.value.createAt).format(timeFormat));
      let max = "", min = "";
      if (nowDate > joinDate) {
        max = nowDate;
        min = joinDate;
      } else {
        max = joinDate;
        min = nowDate;
      }
      joinDays.value = parseInt(((max - min) / (24 * 60 * 60 * 1e3)).toFixed()) + 1;
    };
    common_vendor.onShow(() => {
      getUserInfo();
      getJoinSystemDays();
      common_vendor.nextTick$1(() => {
      });
    });
    return (_ctx, _cache) => {
      var _a, _b;
      return common_vendor.e({
        a: (_a = userInfo.value) == null ? void 0 : _a.user_id
      }, ((_b = userInfo.value) == null ? void 0 : _b.user_id) ? common_vendor.e({
        b: common_vendor.p({
          fileName: userInfo.value.user_avatar,
          size: "104rpx"
        }),
        c: common_vendor.t(userInfo.value.nick_name),
        d: userInfo.value.sex === common_vendor.unref(enum_GlobalEnum.SexEnum).GIRL
      }, userInfo.value.sex === common_vendor.unref(enum_GlobalEnum.SexEnum).GIRL ? {
        e: common_vendor.p({
          size: "14px",
          src: common_vendor.unref(utils_imgJoinName.joinSystemPath)("girl.png")
        })
      } : {
        f: common_vendor.p({
          size: "14px",
          src: common_vendor.unref(utils_imgJoinName.joinSystemPath)("boy.png")
        })
      }, {
        g: common_vendor.p({
          size: "14px",
          src: common_vendor.unref(utils_imgJoinName.joinSystemPath)("joinDate.png")
        }),
        h: common_vendor.t(joinDays.value),
        i: common_vendor.unref(utils_imgJoinName.joinSystemPath)("getUserInfo.png")
      }) : {
        j: common_vendor.p({
          src: common_vendor.unref(utils_imgJoinName.joinSystemPath)("wx.png"),
          size: "40rpx"
        }),
        k: common_vendor.p({
          bgc: "#65BFC7",
          clickFun: toLogin
        })
      }, {
        l: common_vendor.s(_ctx.__cssVars())
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/pages/User/User.vue"]]);
wx.createPage(MiniProgramPage);
