"use strict";
const common_vendor = require("../../../common/vendor.js");
const schema_register_schema = require("../../../schema/register_schema.js");
const service_model_user = require("../../../service/model/user.js");
require("../../../service/request/http.js");
require("../../../service/request/config.js");
require("../../../utils/joinParamsUrl.js");
if (!Array) {
  const _easycom_uni_easyinput2 = common_vendor.resolveComponent("uni-easyinput");
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  (_easycom_uni_easyinput2 + _easycom_uni_forms_item2 + _easycom_uni_forms2)();
}
const _easycom_uni_easyinput = () => "../../../uni_modules/uni-easyinput/components/uni-easyinput/uni-easyinput.js";
const _easycom_uni_forms_item = () => "../../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_forms = () => "../../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
if (!Math) {
  (Tips + _easycom_uni_easyinput + _easycom_uni_forms_item + _easycom_uni_forms)();
}
const Tips = () => "../../../components/Tips/Tips.js";
const _sfc_main = {
  __name: "Login",
  setup(__props) {
    const userModel = new service_model_user.UserModel();
    const registerRef = common_vendor.ref();
    const formData = common_vendor.reactive({
      user_name: "wxlwxl",
      pass_word: "wxlwxl666"
    });
    const submitRegister = () => {
      registerRef.value.validate().then(async (res) => {
        const {
          data,
          success
        } = await userModel.postRegister(res);
        if (success) {
          common_vendor.index.$emit("tips", data);
        }
      }).catch((err) => {
      });
    };
    const submitLogin = () => {
      registerRef.value.validate().then(async (res) => {
        const {
          data,
          success
        } = await userModel.postLogin(res);
        if (success) {
          common_vendor.index.setStorageSync("token", data);
          common_vendor.index.navigateBack();
        }
      }).catch((err) => {
      });
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o(($event) => formData.user_name = $event),
        b: common_vendor.p({
          type: "text",
          placeholder: "请输入用户名",
          modelValue: formData.user_name
        }),
        c: common_vendor.p({
          label: "用户名",
          name: "user_name"
        }),
        d: common_vendor.o(($event) => formData.pass_word = $event),
        e: common_vendor.p({
          type: "password",
          placeholder: "请输入密码",
          modelValue: formData.pass_word
        }),
        f: common_vendor.p({
          label: "密码",
          name: "pass_word"
        }),
        g: common_vendor.sr(registerRef, "cb8b312e-1", {
          "k": "registerRef"
        }),
        h: common_vendor.p({
          modelValue: formData,
          rules: common_vendor.unref(schema_register_schema.register_rules)
        }),
        i: common_vendor.o(submitRegister),
        j: common_vendor.o(submitLogin)
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/pages/User/Login/Login.vue"]]);
wx.createPage(MiniProgramPage);
