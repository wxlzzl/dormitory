"use strict";
const common_vendor = require("../../common/vendor.js");
const stores_dynamicStyle = require("../../stores/dynamicStyle.js");
const stores_user = require("../../stores/user.js");
const service_model_dormitory = require("../../service/model/dormitory.js");
const utils_imgJoinName = require("../../utils/imgJoinName.js");
const utils_changeData = require("../../utils/changeData.js");
const hooks_verify = require("../../hooks/verify.js");
require("../../service/request/http.js");
require("../../service/request/config.js");
require("../../utils/joinParamsUrl.js");
require("../../config/globalConfig.js");
require("../../enum/ImgFolderEnum.js");
require("../../utils/messageAuth.js");
require("../../enum/GlobalEnum.js");
if (!Array) {
  const _easycom_uni_segmented_control2 = common_vendor.resolveComponent("uni-segmented-control");
  const _easycom_uni_load_more2 = common_vendor.resolveComponent("uni-load-more");
  (_easycom_uni_segmented_control2 + _easycom_uni_load_more2)();
}
const _easycom_uni_segmented_control = () => "../../uni_modules/uni-segmented-control/components/uni-segmented-control/uni-segmented-control.js";
const _easycom_uni_load_more = () => "../../uni_modules/uni-load-more/components/uni-load-more/uni-load-more.js";
if (!Math) {
  (Tips + MyIcons + _easycom_uni_segmented_control + DormitoryCard + _easycom_uni_load_more + MyNull)();
}
const MyIcons = () => "../../components/MyIcons/MyIcons.js";
const DormitoryCard = () => "../../components/DormitoryCard/DormitoryCard.js";
const Tips = () => "../../components/Tips/Tips.js";
const MyNull = () => "../../components/MyNull/MyNull.js";
const _sfc_main = {
  __name: "Dormitorys",
  setup(__props) {
    common_vendor.useCssVars((_ctx) => ({
      "e0904398": common_vendor.unref(primary_main_bgc),
      "6d14fc7e": common_vendor.unref(primary_light_color)
    }));
    const dynamicStyle_store = stores_dynamicStyle.dynamicStyleStore();
    const user_store = stores_user.userStore();
    const dormitoryModel = new service_model_dormitory.DormitoryModel();
    const {
      primary_light_color,
      primary_main_bgc,
      primary_modal_bgc,
      primary_hot_color
    } = dynamicStyle_store.globalStyle;
    const nowDate = common_vendor.ref(/* @__PURE__ */ new Date());
    const query = common_vendor.ref({
      limit: 10,
      page: 0,
      orderBy: "createAt.desc",
      isSelfCreate: 0,
      week: nowDate.value.getDay(),
      time: utils_changeData.changeTimeByDuty((utils_changeData.toTime(Date.now()) + "").match(/\d{2}:\d{1,2}:\d{1,2}/) + "")
    });
    const isAllowScroll = common_vendor.ref(true);
    const loading = common_vendor.ref(false);
    const items = common_vendor.ref(["我加入的", "我创建的"]);
    const current = common_vendor.ref(0);
    const dormitoryList = common_vendor.ref([]);
    const loadingStatus = common_vendor.ref("more");
    const contentText = common_vendor.ref({
      contentdown: "更多宿舍",
      contentrefresh: "加载宿舍中",
      contentnomore: "没有更多宿舍了"
    });
    const onChooseFun = (e) => {
      hooks_verify.verifyIsLogin();
      query.value.isSelfCreate = e.currentIndex;
      init();
    };
    const getJoinedDormitorysFun = async () => {
      loadingStatus.value = "loading";
      const { data, success } = await dormitoryModel.getJoinedDormitorys(query.value);
      if (success) {
        if (loading.value === true) {
          common_vendor.index.$emit("tips", "刷新成功", "info");
        }
        loading.value = false;
        if (!data.length) {
          loadingStatus.value = "noMore";
          isAllowScroll.value = false;
          return;
        }
        dormitoryList.value = [...dormitoryList.value, ...data];
        if (data.length < query.value.limit) {
          loadingStatus.value = "noMore";
          isAllowScroll.value = false;
        } else {
          loadingStatus.value = "more";
        }
      } else {
        if (loading.value === true) {
          common_vendor.index.$emit("tips", "刷新失败", "info");
        }
        loading.value = false;
      }
      if (common_vendor.index.getStorageSync("isDeleteDormitory")) {
        common_vendor.index.$emit("tips", "宿舍已经被解散或者您不在该宿舍里", "error", 3e3);
        common_vendor.index.setStorageSync("isDeleteDormitory", false);
      }
    };
    const scrollToLowerFun = () => {
      if (isAllowScroll.value) {
        query.value.page++;
        getJoinedDormitorysFun();
      }
    };
    const init = () => {
      query.value.limit = 10, query.value.page = 0, query.value.orderBy = "createAt.desc";
      isAllowScroll.value = true;
      dormitoryList.value = [];
      loadingStatus.value = "more";
      nowDate.value = /* @__PURE__ */ new Date();
      getJoinedDormitorysFun();
    };
    const goCreateDormitory = () => {
      common_vendor.index.navigateTo({
        url: "/pages/Dormitorys/CreateDormitory/CreateDormitory"
      });
    };
    const imgLoadFlag = common_vendor.ref(false);
    const imgLoad = () => {
      imgLoadFlag.value = true;
    };
    common_vendor.watch(() => {
      var _a;
      return (_a = user_store.userInfo) == null ? void 0 : _a.id;
    }, () => {
      init();
    });
    common_vendor.onLoad(() => {
      common_vendor.index.getStorageSync("token") && init();
    });
    const startPullDownFun = () => {
      loading.value = true;
      common_vendor.index.getStorageSync("token") && init();
    };
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          size: "60rpx",
          src: common_vendor.unref(utils_imgJoinName.joinSystemPath)("dormitory.png")
        }),
        b: common_vendor.o(goCreateDormitory),
        c: common_vendor.p({
          size: "60rpx",
          src: common_vendor.unref(utils_imgJoinName.joinSystemPath)("createDormitoryHeaderHook.png")
        }),
        d: common_vendor.o(imgLoad),
        e: common_vendor.unref(utils_imgJoinName.joinSystemPath)("dormitorys.png"),
        f: common_vendor.o(onChooseFun),
        g: common_vendor.p({
          current: current.value,
          values: items.value,
          ["style-type"]: "button",
          ["active-color"]: common_vendor.unref(primary_light_color)
        }),
        h: common_vendor.f(dormitoryList.value, (item, k0, i0) => {
          return {
            a: item.id,
            b: "c218b0d6-4-" + i0,
            c: common_vendor.p({
              dormitoryData: item
            })
          };
        }),
        i: common_vendor.p({
          ["content-text"]: contentText.value,
          iconType: "circle",
          color: common_vendor.unref(primary_hot_color),
          status: loadingStatus.value
        }),
        j: !dormitoryList.value.length
      }, !dormitoryList.value.length ? {
        k: common_vendor.p({
          img: "nullContent.png"
        })
      } : {}, {
        l: common_vendor.unref(primary_light_color),
        m: loading.value,
        n: common_vendor.o(startPullDownFun),
        o: common_vendor.o(scrollToLowerFun),
        p: common_vendor.s(_ctx.__cssVars())
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/pages/Dormitorys/Dormitorys.vue"]]);
wx.createPage(MiniProgramPage);
