"use strict";
const common_vendor = require("../../common/vendor.js");
const utils_imgJoinName = require("../../utils/imgJoinName.js");
const utils_changeData = require("../../utils/changeData.js");
const utils_joinParamsUrl = require("../../utils/joinParamsUrl.js");
const stores_dynamicStyle = require("../../stores/dynamicStyle.js");
const stores_user = require("../../stores/user.js");
const service_model_dormitory = require("../../service/model/dormitory.js");
require("../../config/globalConfig.js");
require("../../enum/ImgFolderEnum.js");
require("../../service/request/http.js");
require("../../service/request/config.js");
if (!Math) {
  (Tips + Avatar + DromitoryOrtherData + DormitoryJoiner + DomitoryApplication)();
}
const Avatar = () => "../../components/Avatar/Avatar.js";
const Tips = () => "../../components/Tips/Tips.js";
const DormitoryJoiner = () => "../../components/Dormitory/DormtioryJoiner/DormitoryJoiner.js";
const DromitoryOrtherData = () => "../../components/Dormitory/DormitoryOrtherData/DormitoryOrtherData.js";
const DomitoryApplication = () => "../../components/Dormitory/DomitoryApplication/DomitoryApplication.js";
const _sfc_main = {
  __name: "Dormitory",
  setup(__props) {
    common_vendor.useCssVars((_ctx) => ({
      "89781168": coverWidth.value,
      "d9280f3e": coverHeight.value,
      "cb0d4e0a": coverbackgroundColor.value,
      "1052fe02": headerTransition,
      "ced224c8": coverBorderRedius.value,
      "7317887d": coverImgWidth.value,
      "53eae560": coverImgHeight.value,
      "b4dee0a2": common_vendor.unref(dormitory_cover_border_color),
      "1c779270": coverImgLeft.value,
      "2002f2b4": imgTransform.value,
      "dc96585a": dormitoryNameWidth.value,
      "649486a2": dormitoryNameTop.value,
      "3751845d": dormityoryNameLeft.value,
      "faad7a0e": dormitoryNameTransform.value,
      "04686d65": dormitoryNameTextAlign.value,
      "6809db98": padding,
      "dec45520": dormitoryNameColor.value,
      "5092752a": dormitoryIllustrationDisplay.value,
      "6da16f1b": infoWidth.value,
      "2cbd7a72": infoHeight.value,
      "709a9a0a": infobackgroundColor.value,
      "e0313fdc": infoLeft.value,
      "6fff53ea": infoTop.value,
      "6b175a2b": common_vendor.unref(primary_text_color),
      "6ed37fb9": common_vendor.unref(primary_text_hot_color),
      "4139fa00": common_vendor.unref(dormitory_data_bgc),
      "53f70923": dormitoryDataTop.value,
      "2ae651f9": dormitoryDataLeft.value,
      "2b907a5a": common_vendor.unref(joiner_bgc)
    }));
    const dynamicStyle_store = stores_dynamicStyle.dynamicStyleStore();
    const user_store = stores_user.userStore();
    const dormitoryModel = new service_model_dormitory.DormitoryModel();
    const dormitoryAllId = common_vendor.index.getStorageSync("homeData");
    const {
      primary_hot_color,
      primary_main_bgc,
      dormitory_cover_border_color,
      primary_text_color,
      primary_text_hot_color,
      info_bgc,
      dormitory_name_color,
      joiner_bgc,
      dormitory_data_bgc
    } = dynamicStyle_store.dormitoryStyle;
    const headerTransition = "all .3s linear";
    const coverWidth = common_vendor.ref("50%");
    const coverHeight = common_vendor.ref("45%");
    const coverbackgroundColor = common_vendor.ref(primary_hot_color);
    const coverBorderRedius = common_vendor.ref("20rpx");
    const infoWidth = common_vendor.ref("50%");
    const infoHeight = common_vendor.ref("45%");
    const infobackgroundColor = common_vendor.ref(info_bgc);
    const infoLeft = common_vendor.ref("50%");
    const infoTop = common_vendor.ref("0");
    const joinerMarginTop = common_vendor.ref("0");
    const dormitoryDataLeft = common_vendor.ref("-100%");
    const dormitoryDataTop = common_vendor.ref("100%");
    const coverImgWidth = common_vendor.ref("100%");
    const coverImgHeight = common_vendor.ref("50%");
    const coverImgLeft = common_vendor.ref("50%");
    const dormitoryNameWidth = common_vendor.ref("90%");
    const dormitoryNameTransform = common_vendor.ref("translate(0%,0%)");
    const dormityoryNameLeft = common_vendor.ref("0%");
    const dormitoryNameTextAlign = common_vendor.ref("center");
    const dormitoryNameColor = common_vendor.ref(dormitory_name_color);
    const dormitoryNameTop = common_vendor.ref("47%");
    const dormitoryIllustrationDisplay = common_vendor.ref("block");
    const imgSize = common_vendor.ref("200rpx");
    const imgTransform = common_vendor.ref("translate(-50%,-50%)");
    const padding = "20rpx";
    const scrollHeight = common_vendor.ref();
    const scrollTop = common_vendor.ref();
    const firstScrolled = common_vendor.ref();
    const isScrollLower = common_vendor.ref(false);
    const testValue = common_vendor.ref(0);
    const init = () => {
      scrollHeight.value = 0;
      scrollTop.value = 0;
      firstScrolled.value = false;
      newJoinerData.value = [];
    };
    const scrollFun = (e) => {
      const {
        deltaY: _deltaY,
        scrollHeight: _scrollHeight,
        scrollTop: _scrollTop
      } = e.detail;
      if (!firstScrolled.value) {
        scrollHeight.value = _scrollHeight;
        scrollTop.value = _scrollTop;
        firstScrolled.value = true;
      }
      testValue.value = _scrollTop;
      if (_scrollTop <= 8) {
        coverbackgroundColor.value = primary_hot_color;
        coverWidth.value = `50%`;
        coverHeight.value = `45%`;
        infoHeight.value = `45%`;
        infoWidth.value = "50%";
        infoLeft.value = "50%";
        infoTop.value = "0";
        joinerMarginTop.value = "0";
        dormitoryDataLeft.value = "-100%";
        dormitoryDataTop.value = "100%";
        coverImgWidth.value = "100%";
        imgSize.value = "200rpx";
        coverImgHeight.value = "50%";
        dormitoryNameWidth.value = "90%";
        dormitoryNameTransform.value = "translate(0,0%)";
        imgTransform.value = "translate(-50%,-50%)";
        dormityoryNameLeft.value = "0%";
        dormitoryNameTextAlign.value = "center";
        coverImgLeft.value = "50%";
        dormitoryNameTop.value = "47%";
        dormitoryIllustrationDisplay.value = "block";
        isScrollLower.value = false;
        coverBorderRedius.value = "20rpx";
      } else {
        coverWidth.value = "100%";
        coverHeight.value = `10%`;
        infoLeft.value = "100%";
        infoTop.value = "-10%";
        infoHeight.value = "20%";
        joinerMarginTop.value = "-17.5%";
        dormitoryDataLeft.value = "0";
        dormitoryDataTop.value = "30%";
        coverImgWidth.value = "35%";
        imgSize.value = "80rpx";
        coverImgHeight.value = "100%";
        dormitoryNameWidth.value = "40%";
        dormitoryNameTransform.value = "translate(0,-50%)";
        imgTransform.value = "translate(-50%,-50%)";
        coverImgLeft.value = "85%";
        dormityoryNameLeft.value = "45%";
        dormitoryNameTextAlign.value = "";
        dormitoryNameTop.value = "50%";
        dormitoryIllustrationDisplay.value = "none";
        isScrollLower.value = true;
        coverBorderRedius.value = "0";
      }
    };
    const unChooseDormitoryTip = "还未选择宿舍";
    const dormitoryInfo = common_vendor.ref({
      "dormitory": {
        "id": "877f9815e4afc5559a316f4d4cce086c",
        "user_id": "80efb504869290c53cd356a9803f07a5",
        "descript": "这是一段描述",
        "dormitory_name": unChooseDormitoryTip,
        "2022-12-19 10:53:43": "2022-12-19 10:53:43.000000",
        "dormitory_cover_url": "dormitory_cover_init_2.jpg"
      },
      "user": {
        "id": "cc4f6ec8718fb04f4abb18ca40f49086",
        "user_id": "80efb504869290c53cd356a9803f07a5",
        "2022-12-19 10:53:43": "2022-12-19 10:53:43.000000",
        "nick_name_by_dormitory": unChooseDormitoryTip
      },
      "admin": {
        "user_id": "80efb504869290c53cd356a9803f07a5",
        "user_avatar": "avatar_init_3.jpg",
        "nick_name_by_dormitory": unChooseDormitoryTip
      },
      "balance": "0.00",
      "billNums": 0
    });
    const deleteDormitoryJobInfo = common_vendor.ref();
    const getDeleteDormitoryFun = async () => {
      const { data, success } = await dormitoryModel.getDeleteDormitory({}, dormitoryAllId.dormitory_id);
      if (success) {
        deleteDormitoryJobInfo.value = data;
      }
    };
    const isDeleteingDormitory = common_vendor.ref(false);
    const getDormitoryInfoFun = async (relationId, dormitoryId) => {
      const { data, success } = await dormitoryModel.getDormitoryInfo({}, relationId, dormitoryId);
      if (success) {
        dormitoryInfo.value = data;
        if (data.dormitory.state === 3) {
          isDeleteingDormitory.value = true;
          common_vendor.nextTick$1(() => {
            getDeleteDormitoryFun();
          });
        } else {
          isDeleteingDormitory.value = false;
        }
      }
    };
    const newJoinerData = common_vendor.ref([
      {
        "user_id": "80efb504869290c53cd356a9803f07a5",
        "nick_name_by_dormitory": unChooseDormitoryTip,
        "user_avatar": "avatar_init_3.jpg"
      },
      {
        "user_id": "fcec462df61169a27f7213e41e265b3d",
        "nick_name_by_dormitory": unChooseDormitoryTip,
        "user_avatar": "avatar_init_3.jpg"
      }
    ]);
    const getNewJoinerDataFun = async (dormitoryId) => {
      const { data, success } = await dormitoryModel.getNewJoiner({}, dormitoryId);
      if (success) {
        newJoinerData.value = data;
      }
    };
    const initDormitory = () => {
      const { relation_id, dormitory_id } = dormitoryAllId;
      if (relation_id && common_vendor.index.getStorageSync("token")) {
        getDormitoryInfoFun(relation_id, dormitory_id);
        getNewJoinerDataFun(dormitory_id);
      }
    };
    common_vendor.onShareAppMessage((res) => {
      return {
        title: "您的宿舍好帮手",
        path: `/pages/Dormitory/Dormitory`,
        mpPath: `/pages/Dormitory/Dormitory`
      };
    });
    common_vendor.watch(() => {
      var _a;
      return (_a = user_store.userInfo) == null ? void 0 : _a.id;
    }, () => {
      init();
      initDormitory();
    });
    common_vendor.onShow(() => {
      init();
      initDormitory();
    });
    common_vendor.onLoad((opt) => {
      if (opt.q) {
        let url = decodeURIComponent(opt.q);
        const obj = utils_joinParamsUrl.getUrlParam(url);
        console.log("数据", obj);
        const {
          publicity_id,
          dormitory_id,
          isPublicity
        } = obj;
        if (publicity_id && isPublicity === "yes") {
          common_vendor.index.setStorageSync("newUserActive", {
            publicity_id
          });
        } else if (dormitory_id && isPublicity === "no") {
          common_vendor.index.navigateTo({
            url: `/pages/Dormitory/DormitoryInvite/DormitoryInvite?dormitory_id=${dormitory_id}`
          });
        }
      }
    });
    return (_ctx, _cache) => {
      var _a;
      return common_vendor.e({
        a: common_vendor.s(`width:${imgSize.value}; height: ${imgSize.value};`),
        b: common_vendor.unref(utils_imgJoinName.joinDormitoryCover)(dormitoryInfo.value.dormitory.dormitory_cover_url),
        c: common_vendor.t(dormitoryInfo.value.dormitory.dormitory_name),
        d: common_vendor.unref(utils_imgJoinName.joinSystemPath)("goHome.png"),
        e: common_vendor.p({
          fileName: dormitoryInfo.value.admin.user_avatar,
          size: "70rpx"
        }),
        f: common_vendor.t(dormitoryInfo.value.admin.nick_name_by_dormitory),
        g: common_vendor.t(dormitoryInfo.value.user.nick_name_by_dormitory),
        h: !isDeleteingDormitory.value
      }, !isDeleteingDormitory.value ? {
        i: common_vendor.t(common_vendor.unref(utils_changeData.changeNums)(newJoinerData.value.length))
      } : {
        j: common_vendor.t((_a = deleteDormitoryJobInfo.value) == null ? void 0 : _a.cronOrTime)
      }, {
        k: common_vendor.p({
          newJoinerData: newJoinerData.value.slice(0, 10),
          balance: dormitoryInfo.value.balance,
          billNums: dormitoryInfo.value.billNums
        }),
        l: common_vendor.p({
          queryData: common_vendor.unref(dormitoryAllId),
          isScrollLower: isScrollLower.value
        }),
        m: common_vendor.p({
          dormitoryInfo: dormitoryInfo.value
        }),
        n: common_vendor.o(scrollFun),
        o: common_vendor.s(_ctx.__cssVars())
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/pages/Dormitory/Dormitory.vue"]]);
_sfc_main.__runtimeHooks = 2;
wx.createPage(MiniProgramPage);
