"use strict";
const SexEnum = {
  BOY: 1,
  GIRL: 0
};
const DormitoryTypeEnum = {
  STUDENT: 0,
  SHARED: 1,
  FAMILY: 2
};
const ChangeDormitoryType = {
  "0": "学生",
  "1": "合租",
  "2": "家庭"
};
const ChangeIconDormitoryType = {
  "0": "studentType.png",
  "1": "sharedType.png",
  "2": "familyType.png"
};
const BillInOutType = {
  IN: 1,
  OUT: 0
};
const BillTypeSet = {
  ALLOWSET: 1,
  UN_ALLOWSET: 0
};
const DormitoryState = {
  NORMAL: 1,
  DELETED: 0,
  ILLEGAL: 2
};
const LotState = {
  GOING: 1,
  OVER: 2,
  DELETEED: 0
};
const JoinLotState = {
  WINNER: 1,
  LOSER: 2
};
const DutyClockState = {
  LOSE: 0,
  FIRST: 1,
  FULL: 2,
  GONING: 3
};
const WXData = {
  tmplIdsByDuty: [
    "uXFRIATEHqshEPDxiyMGMlaLGeoi2Uiq5LukaSms3T8",
    "JGgcXHYPrqtWCL9XhgAjPe15mVz3d8y6Xk70p2kgeAw",
    "3JjFG3JV6iYonycamxb5jQRuYnRAOJAe8T3rLBfoDjs"
  ],
  tmpIdActivityOver: [
    "dToPWdYJxJj_QV4nT8KIRPTNA6QiFM6rxwy1xbJC8xI"
  ],
  tmpIdVoteOver: [
    "Z-0nDSkrvq8EOSWpC1tx-Yo4h4cPTy6WWTiK6Dg4YPs"
  ]
};
const DefaultPhone = "15888888888";
const DutyAddType = {
  SELF_ADD: 1,
  NO_SELF_ADD: 2
};
const UserIsJoinActiveByNewUser = {
  NO: 1,
  IS: 2
};
const IsDormitoryPush = {
  YES: 1,
  NO: 0
};
const IsPauseDuty = {
  YES: 1,
  No: 0
};
const WeekType = {
  SIGLE: 0,
  DOUBLE: 1
};
const RandomLotsState = {
  GOING: 1,
  OVER: 2,
  DELETED: 0
};
const VoteDataState = {
  GOING: 1,
  OVER: 2,
  DELETED: 0
};
const VoteIsMult = {
  NO_MULT: 0,
  IS_MULT: 1
};
const VoteIsHiddenUserName = {
  NO_HIDDEN: 0,
  IS_HIDDEN: 1
};
const VoteResultView = {
  JOINED_VIEW: 1,
  OVER_VIEW: 0
};
const VoteType = {
  WORD: 1,
  IMG_WORD: 2
};
exports.BillInOutType = BillInOutType;
exports.BillTypeSet = BillTypeSet;
exports.ChangeDormitoryType = ChangeDormitoryType;
exports.ChangeIconDormitoryType = ChangeIconDormitoryType;
exports.DefaultPhone = DefaultPhone;
exports.DormitoryState = DormitoryState;
exports.DormitoryTypeEnum = DormitoryTypeEnum;
exports.DutyAddType = DutyAddType;
exports.DutyClockState = DutyClockState;
exports.IsDormitoryPush = IsDormitoryPush;
exports.IsPauseDuty = IsPauseDuty;
exports.JoinLotState = JoinLotState;
exports.LotState = LotState;
exports.RandomLotsState = RandomLotsState;
exports.SexEnum = SexEnum;
exports.UserIsJoinActiveByNewUser = UserIsJoinActiveByNewUser;
exports.VoteDataState = VoteDataState;
exports.VoteIsHiddenUserName = VoteIsHiddenUserName;
exports.VoteIsMult = VoteIsMult;
exports.VoteResultView = VoteResultView;
exports.VoteType = VoteType;
exports.WXData = WXData;
exports.WeekType = WeekType;
