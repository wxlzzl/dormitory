"use strict";
const ImgFolder = {
  AVATAR_FOLDER: "avatar",
  DORMITORY_COVER_FOLDER: "dormitory_cover",
  BILL_PROOF_FOLDER: "bill_proof",
  PUBLIC_ESTATE_FOLDER: "public_estate",
  SYSTEM_FOLDER: "system",
  JOIN_BGC_FOLDER: "join_bgc",
  DUTY_CLOCK_IMG_FOLDER: "duty_clock",
  PUBLICITY_CODE_IMG_FOLDER: "publicity_code",
  VOTE_IMG: "vote"
};
exports.ImgFolder = ImgFolder;
