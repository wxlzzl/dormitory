"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_uniPopup_components_uniPopup_popup = require("../uni-popup/popup.js");
const stores_dynamicStyle = require("../../../../stores/dynamicStyle.js");
const __default__ = {
  name: "uniPopupMessage",
  mixins: [uni_modules_uniPopup_components_uniPopup_popup.popup],
  props: {
    /**
     * 主题 success/warning/info/error	  默认 success
     */
    type: {
      type: String,
      default: "success"
    },
    /**
     * 消息文字
     */
    message: {
      type: String,
      default: ""
    },
    /**
     * 显示时间，设置为 0 则不会自动关闭
     */
    duration: {
      type: Number,
      default: 3e3
    },
    maskShow: {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      test: "red",
      ...stores_dynamicStyle.dynamicStyleStore().globalStyle
    };
  },
  created() {
    this.popup.maskShow = this.maskShow;
    this.popup.messageChild = this;
  },
  computed: {
    ...common_vendor.mapStores(stores_dynamicStyle.dynamicStyleStore()).globalStyle
  },
  methods: {
    timerClose() {
      if (this.duration === 0)
        return;
      clearTimeout(this.timer);
      this.timer = setTimeout(() => {
        this.popup.close();
      }, this.duration);
    }
  }
};
const __injectCSSVars__ = () => {
  common_vendor.useCssVars((_ctx) => ({
    "e4fc6884": _ctx.primary_tips_bgc,
    "344fbe63": _ctx.primary_tips_color,
    "68dc4e26": _ctx.warn_tips_bgc,
    "b9d9726a": _ctx.warn_tips_color,
    "34dee0f8": _ctx.error_tips_bgc,
    "78bc6d1d": _ctx.error_tips_color,
    "896f2e04": _ctx.info_tips_bgc,
    "0ae40ba3": _ctx.info_tips_color
  }));
};
const __setup__ = __default__.setup;
__default__.setup = __setup__ ? (props, ctx) => {
  __injectCSSVars__();
  return __setup__(props, ctx);
} : __injectCSSVars__;
const _sfc_main = __default__;
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.t($props.message),
    b: common_vendor.n("uni-popup__" + $props.type + "-text"),
    c: common_vendor.n("uni-popup__" + $props.type),
    d: common_vendor.s(_ctx.__cssVars())
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/wangxuelong/Documents/HBuilderProjects/dormitory/uni_modules/uni-popup/components/uni-popup-message/uni-popup-message.vue"]]);
wx.createComponent(Component);
