"use strict";
const service_request_http = require("../request/http.js");
const User = {
  postRegister: "/user/register",
  postLogin: "/login",
  getUserInfo: "/user/info",
  postUdateUserInfo: "/user/update/$user_id",
  postLoginByWX: "/login/wx",
  getSystemDoc: "/user/doc/$systemId",
  getPhoneNum: "/user/get/phone",
  getPublicityInfoByUserId: "/user/get/publicity/info/user_id",
  getPublicitySuccessUser: "/user/publicity/user/list/$publicityId",
  getOutPublicityBalance: "/user/all/out/publicity/balance/$publicityId",
  postOutPublicityBalance: "/user/out/publicity/balance/$publicityId",
  postApplyPublicity: "/user/apply/publicity/account",
  postJoinPublicity: "/user/join/app/by/publicity/$publicityId",
  postUpdatePublicityCodeImg: "/user/update/publicity/code/img/$publicityId",
  patchUnBindPhone: "/user/unBind/phone",
  patchUploadPushCount: "/user/push/count"
};
class UserModel extends service_request_http.HTTP {
  patchUploadPushCount(data) {
    return this.request({
      url: User.patchUploadPushCount,
      data,
      method: "patch",
      isToken: true,
      isLoading: true
    });
  }
  patchUnBindPhone(data) {
    return this.request({
      url: User.patchUnBindPhone,
      data,
      method: "patch",
      isToken: true,
      isLoading: true
    });
  }
  postUpdatePublicityCodeImg(data, publicityId) {
    return this.request({
      url: User.postUpdatePublicityCodeImg,
      data,
      method: "patch",
      data,
      params: [
        {
          key: "publicityId",
          value: publicityId
        }
      ]
    });
  }
  postJoinPublicity(data, publicityId) {
    return this.request({
      url: User.postJoinPublicity,
      method: "post",
      data,
      params: [
        {
          key: "publicityId",
          value: publicityId
        }
      ]
    });
  }
  postApplyPublicity(data) {
    return this.request({
      url: User.postApplyPublicity,
      method: "post",
      data
    });
  }
  postOutPublicityBalance(data, publicityId) {
    return this.request({
      url: User.postOutPublicityBalance,
      method: "post",
      data,
      params: [
        {
          key: "publicityId",
          value: publicityId
        }
      ]
    });
  }
  getOutPublicityBalance(data, publicityId) {
    return this.request({
      url: User.getOutPublicityBalance,
      method: "get",
      data,
      params: [
        {
          key: "publicityId",
          value: publicityId
        }
      ]
    });
  }
  getPublicitySuccessUser(data, publicityId) {
    return this.request({
      url: User.getPublicitySuccessUser,
      method: "get",
      data,
      params: [
        {
          key: "publicityId",
          value: publicityId
        }
      ]
    });
  }
  getPublicityInfoByUserId() {
    return this.request({
      url: User.getPublicityInfoByUserId,
      method: "get"
    });
  }
  getPhoneNum(data) {
    return this.request({
      url: User.getPhoneNum,
      method: "post",
      isLoading: true,
      data
    });
  }
  getSystemDoc(data, systemId) {
    return this.request({
      url: User.getSystemDoc,
      method: "get",
      isLoading: true,
      data,
      params: [
        {
          key: "systemId",
          value: systemId
        }
      ]
    });
  }
  postLoginByWX(data) {
    return this.request({
      url: User.postLoginByWX,
      method: "post",
      data,
      isLoading: true
    });
  }
  getUserInfo() {
    return this.request({
      url: User.getUserInfo,
      method: "get",
      isToken: true
    });
  }
  postRegister(data) {
    return this.request({
      url: User.postRegister,
      method: "post",
      data
    });
  }
  postLogin(data) {
    return this.request({
      url: User.postLogin,
      method: "post",
      data
    });
  }
  postUpdateUserInfo(data, user_id) {
    return this.request({
      url: User.postUdateUserInfo,
      method: "patch",
      isToken: true,
      data,
      params: [
        {
          key: "user_id",
          value: user_id
        }
      ]
    });
  }
}
exports.UserModel = UserModel;
