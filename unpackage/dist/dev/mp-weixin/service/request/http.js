"use strict";
const common_vendor = require("../../common/vendor.js");
const service_request_config = require("./config.js");
const utils_joinParamsUrl = require("../../utils/joinParamsUrl.js");
const _HTTP = class {
  //token是否已经过期
  constructor() {
    this.baseUrl = service_request_config.config.base_url;
  }
  request({
    url,
    data = {},
    method = "get",
    isToken = false,
    isLoading = false,
    params = []
  }) {
    return new Promise((resolve, reject) => {
      url = params.length ? utils_joinParamsUrl.joinParamsUrl(url, params) : url;
      if (isToken && _HTTP.isTokenExpire) {
        reject();
      }
      this._request(url, resolve, reject, data, method, isToken, isLoading);
    });
  }
  _request(url, resolve, reject, data = {}, method = "get", isToken, isLoading) {
    const requestConfig = {
      url: `${this.baseUrl}${url}`,
      method,
      data,
      header: {
        "authorization": service_request_config.TOKEN_PRE + common_vendor.index.getStorageSync("token")
      },
      success: (res) => {
        var _a;
        if (res.data.code === 200) {
          resolve(res.data);
          _HTTP.isTokenExpire = false;
        } else {
          const {
            data: data2,
            statusCode
          } = res;
          switch (statusCode) {
            case 401:
              common_vendor.index.$emit("tips", data2.data, "error");
              common_vendor.index.$emit("userStore", {});
              common_vendor.index.getStorageSync("token") && common_vendor.index.$emit("wxLogin");
              common_vendor.index.removeStorageSync("token");
              _HTTP.isTokenExpire = true;
              break;
            case 404:
              common_vendor.index.$emit("tips", "未知请求", "error");
              break;
            case 470:
              common_vendor.index.removeStorageSync("homeData");
              common_vendor.index.setStorageSync("isDeleteDormitory", true);
              common_vendor.index.reLaunch({
                url: "/pages/Dormitorys/Dormitorys"
              });
              break;
            case 471:
              common_vendor.index.$emit("tips", data2.data.msg, "error", 3e3);
              setTimeout(() => {
                common_vendor.index.reLaunch({
                  url: "/pages/Dormitory/Dormitory"
                });
              }, 3e3);
              break;
            default:
              common_vendor.index.$emit("tips", ((_a = data2.data) == null ? void 0 : _a.msg) || data2.data, "error");
              break;
          }
          resolve(res.data);
        }
        common_vendor.index.hideLoading();
      }
    };
    isLoading && common_vendor.index.showLoading({
      title: "加载中"
    });
    common_vendor.index.request(requestConfig);
  }
};
let HTTP = _HTTP;
HTTP.isTokenExpire = false;
exports.HTTP = HTTP;
