//性别
const SexEnum={
	BOY:1,
	GIRL:0
}

//宿舍类型
const DormitoryTypeEnum={
    STUDENT:0,
    SHARED:1,
    FAMILY:2
}

//宿舍类型转换
const ChangeDormitoryType={
	'0':'学生',
	'1':'合租',
	'2':'家庭'
}

//宿舍类型图标转换
const ChangeIconDormitoryType={
	'0':'studentType.png',
	'1':'sharedType.png',
	'2':'familyType.png'
}

// 账单支出与收入类型
const BillInOutType={
    IN:1,
    OUT:0
}

//是否允许修改账单类型
const BillTypeSet={
    ALLOWSET:1,
    UN_ALLOWSET:0
}

//宿舍状态
const DormitoryState={
    NORMAL:1,
    DELETED:0,
    ILLEGAL:2
}

//抓阄状态
const LotState={
	GOING:1,
	OVER:2,
	DELETEED:0
}

//参与抓阄的状态
const JoinLotState={
    WINNER:1,
    LOSER:2
}

//打卡类型
const DutyClockState={
	LOSE:0,
	FIRST:1,
	FULL:2,
	GONING:3
}

const WXData={
	tmplIdsByDuty:[
		'uXFRIATEHqshEPDxiyMGMlaLGeoi2Uiq5LukaSms3T8',
		'JGgcXHYPrqtWCL9XhgAjPe15mVz3d8y6Xk70p2kgeAw',
		'3JjFG3JV6iYonycamxb5jQRuYnRAOJAe8T3rLBfoDjs'
		],
	tmpIdActivityOver:[
		'dToPWdYJxJj_QV4nT8KIRPTNA6QiFM6rxwy1xbJC8xI'
	],
	tmpIdVoteOver:[
		'Z-0nDSkrvq8EOSWpC1tx-Yo4h4cPTy6WWTiK6Dg4YPs'
	]
}

const DefaultPhone = '15888888888'

//值日创建类型
const DutyAddType={
    SELF_ADD:1,
    NO_SELF_ADD:2
}

//是否参与过新用户的某个活动
const UserIsJoinActiveByNewUser={
    NO:1,
    IS:2
}

//宿舍是否接收push
const IsDormitoryPush={
    YES:1,
    NO:0
}

//宿舍是否暂停值日
const IsPauseDuty={
    YES:1,
    No:0
}

//单双周
const WeekType={
    SIGLE:0,
    DOUBLE:1
}

//宿舍抽签状态
const RandomLotsState = {
    GOING:1,
    OVER:2,
    DELETED:0,
}

//投票状态
const VoteDataState={
    GOING:1,
    OVER:2,
    DELETED:0
}

//投票是否多选
const VoteIsMult={
    NO_MULT:0,
    IS_MULT:1
}

//投票是否匿名;
const VoteIsHiddenUserName={
    NO_HIDDEN:0,
    IS_HIDDEN:1,
}

//投票的票数如何让参与人看到
const VoteResultView={
    JOINED_VIEW:1,
    OVER_VIEW:0
}

//投票类型
const VoteType = {
    WORD:1,
    IMG_WORD:2
}

export{
	SexEnum,
	VoteDataState,
	VoteIsMult,
	VoteIsHiddenUserName,
	VoteResultView,
	VoteType,
	DormitoryTypeEnum,
	ChangeDormitoryType,
	ChangeIconDormitoryType,
	BillInOutType,
	DormitoryState,
	LotState,
	JoinLotState,
	WXData,
	BillTypeSet,
	WeekType,
	DutyClockState,
	DefaultPhone,
	DutyAddType,
	UserIsJoinActiveByNewUser,
	IsDormitoryPush,
	IsPauseDuty,
	RandomLotsState
}