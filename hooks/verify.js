import {
	computed,
} from 'vue'

import {
	openMessageAuth
} from '@/utils/messageAuth.js'

import {
	userStore
} from '@/stores/user'

import { 
	DefaultPhone,
} from '../enum/GlobalEnum.js'

import moment from 'moment'

//验证是否已经登录
export const verifyIsLogin=()=>{
	if(!uni.getStorageSync('token')){
		uni.$emit('openWXLoin')
		throw new Error('请先登录')
	}
}

//验证表单参数
export const verifyFormData=(formData,schema)=>{
	for(const key in schema){
		const {required,tip} = schema[key]
		if(required && !formData[key]){
			uni.$emit('tips',`${tip}不能为空`,'warn')
			throw new Error(`${tip}不能为空`)
		}
	}
}

//验证是否需要弹出激励广告选项
export const verifyIsShowChooseAd=(pages,taskName,num=3)=>{
	let taskVideoAdAndPageCatch=uni.getStorageSync('taskVideoAdAndPageCatch')
	if(taskVideoAdAndPageCatch){
		const nowPage = pages[pages.length-1]?.route
		if(!nowPage){
			return false
		}
		
		const count = taskVideoAdAndPageCatch[nowPage].count
		taskVideoAdAndPageCatch[nowPage].count = count+1
		
		if(count===45){
			return false
		}
		if(count && count%num===0){
			uni.$emit('openIsViewTaskVideoAd',taskName)
			throw new Error(`需要观看广告`)
		}else{
			uni.setStorageSync('taskVideoAdAndPageCatch',taskVideoAdAndPageCatch)
			return false
		}
	}else{
		return false
	}
}

//验证是否操作的是示例
export const verifyHandleEg=()=>{
	const tip = '不能操作示例，请去宿舍列表创建或选择宿舍'
	if(!uni.getStorageSync('homeData')?.dormitory_id){
		uni.$emit('tips',tip,'warn')
		throw new Error(tip)
	}
}

//验证是否是宿舍管理员
export const verifyIsDormitoryAdmin=(admin,user)=>{
	const {user_id:admin_id} = admin
	const {user_id}  = user
	const tip= '您不是宿舍管理员'
	if(user_id!==admin_id){
		uni.$emit('tips',tip,'warn')
		throw new Error(tip)
	}
}

//验证是否已经触发过被动手机弹窗了
export const verifyIsLastOpenPhone=()=>{
	const isLastOpenTime = uni.getStorageSync('isLastOpenTime')
	const nowTime = moment().format('YYYY-MM-DD')
	//上次弹过，并且上次弹出时间和今天时间相等，则今天不能再弹出了
	if(isLastOpenTime && isLastOpenTime===nowTime){
		console.log('上次弹过，并且上次弹出时间和今天时间相等，则今天不能再弹出了')
		return true
	}else{
		//从没有弹过 、 上次弹过，但是不是今天的弹的，则今天要重新弹出了
		console.log('从没有弹过 、 上次弹过，但是不是今天的弹的，则今天要重新弹出了')
		setTimeout(()=>{
			uni.setStorageSync('isLastOpenTime',nowTime)
		},1000)
		return false
	}
}

//消息弹窗与手机号：适合那种绑定手机号没有回调激励的场景，如果需要激励参考joinBgcs.vue
export const MessageAndBindPhone = (isOnlyMessage=false)=>{
	const user_store = userStore()
	const noBindPhone = computed(()=>user_store.userInfo.phone === DefaultPhone)
	const callback = ()=>{
		noBindPhone.value && uni.$emit('openPhonePopubFlag',()=>{},false)
	}
	openMessageAuth(isOnlyMessage ? false : callback,{},false)	
}