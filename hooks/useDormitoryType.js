import {ref} from 'vue'

import {dynamicStyleStore} from '@/stores/dynamicStyle.js'

import {DormitoryTypeEnum} from '@/enum/GlobalEnum.js'

export function useDormitoryType() {
	const dynamicStyle_store=dynamicStyleStore()
	
	const {
		dormitory_type_student_bgc,
		dormitory_type_family_bgc,
		dormitory_type_shared,
		primary_quiet_color,
		primary_text_color,
		primary_hot_color,
		tip_text_color
	} = dynamicStyle_store.globalStyle
	
	const dormitoryTypes={
		'0':{
			id:DormitoryTypeEnum.STUDENT,
			iconName:'studentType.png',
			name:'同学',
			bgc:dormitory_type_student_bgc,
			color:'#37465D'
		},
		'1':{
			id:DormitoryTypeEnum.SHARED,
			iconName:'sharedType.png',
			name:'合租',
			bgc:'#A96A9C',
			color:'#73CFCE'
		},
		// '2':{
		// 	id:DormitoryTypeEnum.FAMILY,
		// 	iconName:'familyType.png',
		// 	name:'家庭',
		// 	bgc:dormitory_type_family_bgc,
		// 	color:'#DC475F'
		// }
	}
  
  return { dormitoryTypes }
}
