const register_rules = {
	user_name: {
		rules: [{
				required: true,
				errorMessage: "请输入用户名"
			},
			{
				minLength: 3,
				maxLength: 20,
				errorMessage: "用户名长度在{minLength}到{maxLength}个字符"
			}
		]
	},
	pass_word: {
		rules: [{
				required: true,
				errorMessage: "请输入密码"
			},
			{
				minLength: 3,
				maxLength: 20,
				errorMessage: "密码太简单了"
			}
		]
	}
}

export{
	register_rules
}