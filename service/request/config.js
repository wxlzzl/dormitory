let BASE_URL = ''
const env = 2
if (process.env.NODE_ENV == 'development') {
    BASE_URL = env ===1 ? `http://localhost:8000` : `https://ds.wangxuelong.vip`
} else {
    BASE_URL = env ===1 ? `http://localhost:8000` : `https://ds.wangxuelong.vip`
}
const config = {
	base_url: BASE_URL
}

const TOKEN_PRE='zzl_'

export { 
	config, 
	TOKEN_PRE
}