import {imgConfig} from '@/config/globalConfig.js'

import {ImgFolder} from '@/enum/ImgFolderEnum.js'

//公共拼接部分
const joinMainPath=(fileName,folder)=>{
	const path = `https://${imgConfig.BUCKET}.cos.${imgConfig.REGION}.${imgConfig.TX_COS_DOMAIN}/${folder}/${fileName}`
	
	return path
}

//拼接头像地址
const joinAvatarPath = (fileName) => {
	const path =joinMainPath(fileName,ImgFolder.AVATAR_FOLDER)
	return path
}

//拼接宣传二维码
const joinPublicityCodePath=(fileName)=>{
	const path =joinMainPath(fileName,ImgFolder.PUBLICITY_CODE_IMG_FOLDER)
	return path
}

//拼接系统图片地址
const joinSystemPath = (fileName)=>{
	const path = joinMainPath(fileName,ImgFolder.SYSTEM_FOLDER)
	
	return path
}

//拼接值日打卡图片
const joinDutyClockImg=(fileName)=>{
	const path = joinMainPath(fileName,ImgFolder.DUTY_CLOCK_IMG_FOLDER)
	return path
}

//拼接宿舍封面地址
const joinDormitoryCover = (fileName)=>{
	const path = joinMainPath(fileName,ImgFolder.DORMITORY_COVER_FOLDER)
	
	return path
}

//拼接账单交易凭证图片地址
const joinBillProof =(fileName)=>{
	const path=joinMainPath(fileName,ImgFolder.BILL_PROOF_FOLDER)
	
	return path
}

//拼接公共财产图片地址
const joinEstateImg =(fileName)=>{
	const path=joinMainPath(fileName,ImgFolder.PUBLIC_ESTATE_FOLDER)
	
	return path
}

//拼接加入者背景皮肤图片地址
const joinJoinBgcImg=(fileName)=>{
	const path=joinMainPath(fileName,ImgFolder.JOIN_BGC_FOLDER)
	
	return path
}

//拼接投票图片
const joinVoteImg=(fileName)=>{
	const path=joinMainPath(fileName,ImgFolder.VOTE_IMG)
	
	return path 
}

export{
	joinAvatarPath,
	joinSystemPath,
	joinDormitoryCover,
	joinBillProof,
	joinEstateImg,
	joinJoinBgcImg,
	joinDutyClockImg,
	joinPublicityCodePath,
	joinVoteImg
}