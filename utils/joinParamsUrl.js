//拼接请求参数
const joinParamsUrl =(url,params)=>{
	for(const obj of params){
		const {key,value} = obj
		url = url.replace('$'+key,value)
	}
	return url
}

//解析扫二维码拿到参数
const getUrlParam=(url)=>{
            let params = url.split("?")[1].split("&");
            let obj = {};
            params.map(v => (obj[v.split("=")[0]] = v.split("=")[1]));
            return obj
 }

export{
	joinParamsUrl,
	getUrlParam
}