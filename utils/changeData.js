import moment from 'moment'

//改变时间字符串
const changeDate=(date_time)=>{
    return date_time
}

//去除时间后面小数位
const deleteLot=(date_time)=>{
	return date_time.replace('.000000','')
}

//时间转为时间戳
const toTimeStamp=(date)=>{
	return new Date(date) - 0
}

//时间戳转时间
const toTime=(n,flag)=>{
	if(typeof n === 'number'){
		n = moment(new Date(n)).format("YYYY-MM-DD HH:mm:ss")
	}else if(typeof n === 'string'){
		console.log('现在',n)
		n = n.replace(/\//g, "-")
	}
	return n
}

function parseExpression(s) {
  s = s.replace(/\s/g, '') + 'e' // 去除所有空格，末尾加上一个结束符号,确保最后一个数加入到栈中
  let stack = []
  let preSign = '+' // 初始置为+，目的：让第一个数字入栈
  let curNum = 0 // 当前数字
  let reg = /[0-9]|./
  for (let i = 0; i < s.length; i++) {
    if (reg.test(s[i])) {
      // 当前字符为数字或点，拼接数字
      curNum = curNum + s[i]
    } else {
      // 当前字符是操作符，则判断运算上一符号
      if (preSign === '+') {
        stack.push(curNum)
      } else if (preSign === '-') {
        let last = stack[stack.length - 1]
        if (last === 0 && 1 / last < 0) {
          // 判断上一个数字是不是 -0，解决类似：2--7这种问题
          stack.push(curNum)
        } else {
          stack.push(-1 * curNum)
        }
      } else if (['+', '-'].includes(s[i]) && !curNum) {
        // 对数字的正负号做特殊处理 比如：2*-2
        if (['+', '-'].includes(curNum)) return 0 // 2*--2 有两个减号，直接返回 0
        curNum = s[i]
        continue
      } else if (preSign === '*') {
        stack.push(calc(stack.pop(), preSign, curNum)) // calc方法是用于解决js计算精度的方法
      } else if (preSign === '/') {
        stack.push(calc(stack.pop(), preSign, curNum))
      } else {
        return 0 // 既不是数字也不是符号也不是点，直接返回0
      }
      curNum = 0 // 运算后将curNum置为0
      preSign = s[i] // 记录当前符号
    }
  }
  let sum = stack.reduce((pre, num) => calc(pre, '+', num), 0) // 计算栈的和
  if (isNaN(sum) || !isFinite(sum)) sum = 0 // 如果结果是NaN,或者无穷，就置为0
  return sum
}

//获取每一个月有多少天
const getMonthDay=(year, month)=>{
  let days = new Date(year, month, 0).getDate()
  return days
}

//根据正则替换
const deleteStrByPatten=(str,patten,flag="")=>{
	return str.replace(patten,flag)
}

//获取现在星期
const getNowWeek=()=>{
	return parseInt("7123456".charAt(new Date().getDay()))
}

//是否是测试
const isTest=()=>{
	return false
	const date=Math.round(new Date() / 1000)
	return date>1674691805
}

//根据数字返回星期
const changeWeekByNum=(num)=>{
	const weekData={
		'1':'周一',
		'2':'周二',
		'3':'周三',
		'4':'周四',
		'5':'周五',
		'6':'周六',
		'7':'周日',
	}
	return weekData[num+'']
}

//数字越界显示加号
const changeNums=(num,limit=100)=>{
		let _num=(num || 0)+''
		if(num>=limit){
			_num=(limit-1)+'+'
		}
		return _num
}

//字符串脱敏
const changeStrHidden=(str,len)=>{
	if(!str) return ''
	const str1=str.slice(0,len)
	const end=str.length-len-1
	const str2=str.slice(end)
	return `${str1}*****${str2}`
}

//转换查找值日生time的格式
const changeTimeByDuty=(time)=>{
	return time.split(':')[0]+':00'
}

//宿舍卡片展示多个值日生的昵称拼接
const joinNameByDutyer=(data)=>{
	const {dormitory,lastDutyer} = data
	let text = '今天值日：'
	let arr = []
	if(!data.lastDutyer){
		text='该时间段没有值日生'
	}else{
		for(const item of data.lastDutyer){
			if(item.duty_weekType === dormitory.dormitory_weekType){
				arr.push(item.nick_name_by_dormitory)
			}
		}
		arr = [...(new Set(arr))]
		if(!arr.length){
			text='该时间段没有值日生'
		}else{
			text=text+arr.join('、')
		}
	}
	return text
}

//2023-1-15 09:00日期比较大小
const compareDate=(d1,d2)=>{
	if(((new Date(d1.replace(/-/g,"\/"))) > (new Date(d2.replace(/-/g,"\/"))))){
		return 1
	}else if(((new Date(d1.replace(/-/g,"\/"))) < (new Date(d2.replace(/-/g,"\/"))))){
		return -1
	}else{
		return 0
	}
}

export{
	changeDate,
	toTimeStamp,
	changeNums,
	changeTimeByDuty,
	joinNameByDutyer,
	toTime,
	parseExpression,
	getMonthDay,
	deleteLot,
	deleteStrByPatten,
	getNowWeek,
	changeWeekByNum,
	compareDate,
	changeStrHidden,
	isTest
}