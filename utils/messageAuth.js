import {
	WXData
} from '@/enum/GlobalEnum.js'

import {userStore} from '@/stores/user'

import {
		DefaultPhone
	} from '@/enum/GlobalEnum.js'

const user_store=userStore()

//授权手机号
const openGetPhoneModel=()=>{
	// if(DefaultPhone === user_store.userInfo.phone){
	// 	console.log('授权1111')
	// 	uni.$emit('tips','请授权手机号,方便收到通知哦','info',3000)
	// 	uni.$emit('openPhonePopubFlag',false,false)
	// }
}

const openMessageVoteOver =(fn,data)=>{
	const token=uni.getStorageSync('token')
	uni.requestSubscribeMessage({
		tmplIds:WXData.tmpIdVoteOver,
		success:res=>{
			const keys = Object.keys(res)
			for(let key of keys){
				if(res[key]==="reject"){
					uni.$emit('tips','请前往设置-通知管理，允许接收所有通知哦','info')
				}
			}
			fn && fn(data)
		},
		fail: (err) => {
			switch (err.errno) {
				case 1001:
					// openGetPhoneModel()
					uni.$emit('tips','您关闭了主开关，无法进行订阅，请点击右上角设置，打开主开关','warn')	
					//执行回调
					fn && fn(data)
					break
				case 2004:
					uni.$emit('tips','您关闭了主开关，无法进行订阅，请点击右上角设置，打开主开关','warn')				
					// openGetPhoneModel()
					//执行回调
					fn && fn(data)
					break
				case 20004:
					uni.$emit('tips','您关闭了主开关，无法进行订阅，请点击右上角设置，打开主开关','warn')	
					// openGetPhoneModel()
					//执行回调
					fn && fn(data)
					break
				default:
					console.log('未知错误',err)
					// openGetPhoneModel()
					//执行回调
					fn && fn(data)
					break
			}
		}
	})
}

const openMessageActivityOver =(fn,data)=>{
	const token=uni.getStorageSync('token')
	uni.requestSubscribeMessage({
		tmplIds:WXData.tmpIdActivityOver,
		success:res=>{
			const keys = Object.keys(res)
			for(let key of keys){
				if(res[key]==="reject"){
					uni.$emit('tips','请前往设置-通知管理，允许接收所有通知哦','info')
				}
			}
			fn && fn(data)
		},
		fail: (err) => {
			switch (err.errno) {
				case 1001:
					// openGetPhoneModel()
					uni.$emit('tips','您关闭了主开关，无法进行订阅，请点击右上角设置，打开主开关','warn')	
					//执行回调
					fn && fn(data)
					break
				case 2004:
					uni.$emit('tips','您关闭了主开关，无法进行订阅，请点击右上角设置，打开主开关','warn')				
					// openGetPhoneModel()
					//执行回调
					fn && fn(data)
					break
				case 20004:
					uni.$emit('tips','您关闭了主开关，无法进行订阅，请点击右上角设置，打开主开关','warn')	
					// openGetPhoneModel()
					//执行回调
					fn && fn(data)
					break
				default:
					console.log('未知错误',err)
					// openGetPhoneModel()
					//执行回调
					fn && fn(data)
					break
			}
		}
	})
}

//授权提醒
const openMessageAuth = (fn=false,data={},openBindPhoneModel=true) => {
	const token=uni.getStorageSync('token')
	if(!token) return
	const anthMessage=(_openBindPhoneModel)=>{
		console.log('测试参数',_openBindPhoneModel)
		uni.requestSubscribeMessage({
			tmplIds: WXData.tmplIdsByDuty,
			success: (res) => {
				console.log('授权', res);
				const keys = Object.keys(res)
				for(let key of keys){
					if(res[key]==="reject"){
						uni.$emit('tips','请前往设置-通知管理，允许接收所有通知哦','info')
					}
				}
				uni.getSetting({
					withSubscriptions:true,
					success:(res)=>{
						console.log('测试总是选择状态',res.subscriptionsSetting)
						uni.setStorageSync('tipUserAlwayMessage1',true)
					}
				})
				//这里会无限的调用手机弹窗（无回调），适合一些于手机号强关联的页面
				// _openBindPhoneModel && openGetPhoneModel()
				//执行回调
				fn && fn(data)
			},
			fail: (err) => {
				switch (err.errno) {
					case 1001:
						// openGetPhoneModel()
						uni.$emit('tips','您关闭了主开关，无法进行订阅，请点击右上角设置，打开主开关','warn')	
						//执行回调
						fn && fn(data)
						break
					case 2004:
						uni.$emit('tips','您关闭了主开关，无法进行订阅，请点击右上角设置，打开主开关','warn')				
						// openGetPhoneModel()
						//执行回调
						fn && fn(data)
						break
					case 20004:
						uni.$emit('tips','您关闭了主开关，无法进行订阅，请点击右上角设置，打开主开关','warn')	
						// openGetPhoneModel()
						//执行回调
						fn && fn(data)
						break
					default:
						console.log('未知错误',err)
						// openGetPhoneModel()
						//执行回调
						fn && fn(data)
						break
				}
			}
		})
	}
	
	if(!uni.getStorageSync('tipUserAlwayMessage1')){
		uni.$emit('openAuthMessageTip',anthMessage,openBindPhoneModel)
	}else{
		anthMessage(openBindPhoneModel)
	}
	
}

export{
	openMessageAuth,
	openMessageActivityOver,
	openMessageVoteOver
}
