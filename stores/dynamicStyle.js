import { defineStore } from 'pinia';

export const dynamicStyleStore = defineStore('dynamicStyleStore', {
	state: () => {
		return { 
			globalStyle:{
				//默认颜色
				primary_hot_color:'#EE355C',
				primary_cool_color:'#65BFC7',
				primary_light_color:'#61d6d5',
				primary_quiet_color:'#33465F',
				
				//默认字体颜色
				primary_text_color:'#33465F',
				primary_text_hot_color:'#EE355C',
				primary_tips_color:'#6BB6BF',
				tip_text_color:'#9A999B',
				error_tips_color:'#AE4763',
				info_tips_color:'#202A37',
				warn_tips_color:'#AB4F4C',
				shallow_text_color:' #818284',
				
				//默认背景颜色
				primary_modal_bgc:'#FFFFFF',
				primary_card_bgc:'#FFFFFF',
				primary_tips_bgc:'#96FFFF',
				primary_main_bgc:'#F5F5F5',
				error_tips_bgc:'#FF7DAE',
				info_tips_bgc:'#4C6281',
				warn_tips_bgc:'#FF8B85',
				dormitory_type_student_bgc:'#5DE598',
				dormitory_type_family_bgc:'#80D4D4',
				dormitory_type_shared:'#F7CF2E',
				primary_orther_color:'#3B4F68',
				
				//默认边框颜色
				primary_border_color:'#EAEAEA',
				cool_border_color:'#DEDEDE',
				info_border_color:'#33465F',
				
				//默认分割线
				primary_line_color:'#d8dddd',
			},
			dormitoryStyle:{
				primary_orther_color:'#3B4F68',
				primary_hot_color:'#EE355C',
				primary_cool_color:'#65BFC7',
				primary_light_color:'#61d6d5',
				primary_count1_color:'#E9C766',
				primary_shallow_color:'#FFCDD2',
				primary_main_bgc:'#F5F5F5',
				primary_line_color:'#d8dddd',
				dormitory_cover_border_color:'#FFFFFF',
				primary_text_color:'#33465F',
				primary_text_hot_color:'#EE355C',
				info_bgc:'#FFFFFF',
				dormitory_name_color:'#FFFFFF',
				joiner_bgc:'#FFFFFF',
				joiner_card_bgc:'#E7E7E7',
				joiner_color:'#33465F',
				dormitory_data_bgc:"#FFFFFF",
				dormitory_orther_left_shadow_one:'rgba(238,53,92, .6)',
				dormitory_orther_left_shadow_two:'rgba(255, 255, 255, .9)',
				dormitory_orther_right_shadow_one:'rgba(237,32,26, .6)',
				dormitory_orther_right_shadow_two:'rgba(255, 255, 255, .9)',
				primary_border_color:'#EAEAEA',
				shallow_text_color:'#818284',
				cool_border_color:'#DEDEDE',
				primary_success:'#B9FC5E'
			}
		};
	},
	actions: {
		setGlobalStyle(data){
			this.globalStyle=data
		},
		setDormitoryStyle(data){
			this.dormitoryStyle=data
		}
	},
});
